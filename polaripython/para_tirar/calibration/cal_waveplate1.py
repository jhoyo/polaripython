# !/usr/bin/env python
# -*- coding: utf-8 -*-

# ----------------------------------------------------------------------
# Name:      analysis
# Purpose:   Ecograb: analysis of polarimetry
#
# Author:    Luis Miguel Sanchez Brea
#
# Created:    2011
# RCS-ID:
# Copyright:
# Licence:    GPL
# ----------------------------------------------------------------------
"""
We determine the calibration parameters waveplates:
phase of waveplate, angle of waveplate

p0, p1, a, b, delta, theta_2, theta_4 = par
(0.15527116445544717, 0.8353421359172539, 0.21989901957649352, 0.20994246400140065, 84.15793122063049, 225.50579954421556, 70.42339825904094)

"""

from scipy import pi
from scipy.io import loadmat
from scipy import optimize

from phyton_optics import degrees, np, plt, sp
from phyton_optics.polarization_stokes import (
    intensity, polarized_light, polarizer_linear, quarter_waveplate, retarder,
    rotate_mueller, vacuum, diattenuating_retarder)

from polarimeter.polarimeter import (
    get_polarimeter_parameters, polarimeter_matrices,
    polarimeter_matrices_parameters, polarimeter_matrix, rotate_polarimeter)


def dibujar_resultado(angles_2, angles_4, Intensidad, title='', color='magma'):
    """dibuja un 2D con angulos1, angulos2, intensidad"""

    plt.figure()
    extension = sp.array(
        [angles_2[0], angles_2[-1], angles_4[0], angles_4[-1]]) * 180 / sp.pi
    IDimage = plt.imshow(
        Intensidad,
        interpolation='bilinear',
        aspect='auto',
        origin='lower',
        extent=extension)
    plt.xlabel("$\phi_2$")
    plt.ylabel("$\phi_4$")
    plt.suptitle(title)
    plt.axis(extension)
    plt.colorbar()
    IDimage.set_cmap(color)  # YlGnBu  RdBu


def load_experiment(filename='intensities_2_4_180305b_angle-polarizer.mat'):
    experimental_dict = loadmat(filename)
    angles_2_e = experimental_dict['angle_2'] * sp.pi / 180
    angles_4_e = experimental_dict['angle_4'] * sp.pi / 180
    I_experimental = experimental_dict['intensities']
    angles_2_e = angles_2_e.flatten()
    angles_4_e = angles_4_e.flatten()
    return angles_2_e, angles_4_e, I_experimental


def numerical_behaviour(par, is_polarizer, angles_2, angles_4):
    # polarimeter_behaviour():

    u0 = polarized_light(
        amplitude=1.7, angle=0 * degrees, phase=sp.pi / 2, pol_degree=1)

    P = polarimeter_matrices_parameters(par)
    # angles_2 = sp.linspace(0, pi / 2, 11)
    # angles_4 = sp.linspace(0, pi, 11)
    Angles_2, Angles_4 = sp.meshgrid(angles_2, angles_4)
    Intensities = sp.zeros_like(Angles_2, dtype=float)

    for i2, angle_2 in enumerate(angles_2):
        for i4, angle_4 in enumerate(angles_4):
            Intensities[i4, i2], _ = polarimeter_matrix(
                P,
                is_present=is_polarizer,
                angles=[0, angle_2, 0, angle_4],
                u0=u0)
    return Intensities


def err_func(par, other_params):
    angles_2, angles_4, I_experimental, is_polarizer = other_params
    return (numerical_behaviour(par, is_polarizer, angles_2, angles_4) -
            I_experimental).flatten()


def optimization(verbose=True, draw=True):
    """
    params_ini = [p0, p1, a, b, delta, theta_2, theta_4]

    Mejor resultado:
    (0.0, 0.843816, 1.0, 1.0, 84.381816, 225.56578, 70.41507)
    """
    angles_2, angles_4, I_experimental = load_experiment(
        filename=
        '../../notebooks/calibration/intensities_2_4_180305b_angle-polarizer.mat'
    )
    is_polarizer = [True, True, False, True]
    params_ini = [0.1, 1, 1, 1, 90 * degrees, 180 * degrees, 0 * degrees]
    params_ini = [0.128, 0.946, 1, 1, 90 * degrees, 225 * degrees, 0 * degrees]
    params_optim, success = optimize.leastsq(
        err_func, params_ini,
        [angles_2, angles_4, I_experimental, is_polarizer])

    if params_optim[0] < 0:
        params_optim[0] = 0

    # no entiendo porque esta normalización no afecta.
    params_optim[3] = params_optim[3] / params_optim[2]
    params_optim[2] = 1

    Intensities_fitting = numerical_behaviour(params_optim, is_polarizer,
                                              angles_2, angles_4)
    residuals = I_experimental - Intensities_fitting

    if verbose is True:
        p = params_optim
        print(p[0], p[1], p[2], p[3], p[4] * 180 / pi, p[5] * 180 / pi,
              p[6] * 180 / pi)
        print(abs(residuals).mean() / abs(I_experimental).mean() * 100)

    if draw is True:
        dibujar_resultado(
            angles_2, angles_4, I_experimental, title='I_experimental')
        dibujar_resultado(
            angles_2,
            angles_4,
            Intensities_fitting,
            title='Intensities_fitting')
        dibujar_resultado(
            angles_2,
            angles_4,
            I_experimental - Intensities_fitting,
            title='residuals',
            color='RdBu')


def main():
    optimization()


if __name__ == '__main__':
    main()
    plt.show()
