from __future__ import print_function
import copy_reg
import types
from multiprocessing import Pool

import time
import datetime
import pprint

from phyton_optics import degrees

from scipy import optimize, pi
from math import sqrt

from polarimeter.utils import plot_2d, generate_even_distribution

from matplotlib.colors import LogNorm
from matplotlib import ticker, colors

from polarimeter.polarimeter import Intensity_Rotating_Elements
import phyton_optics.polarization_jones as pj
import phyton_optics.polarization_stokes as ps
import phyton_optics.polarization_mueller_analysis as amm
import polarimeter.polarimeter as opsys
import phyton_optics.utils_multiprocessing as multiproc
import numpy as np
from itertools import product as prod


def _pickle_method(method):
    func_name = method.im_func.__name__
    obj = method.im_self
    cls = method.im_class
    if func_name.startswith(
            '__') and not func_name.endswith('__'):  # deal with mangled names
        cls_name = cls.__name__.lstrip('_')
        func_name = '_' + cls_name + func_name
    return _unpickle_method, (func_name, obj, cls)


def _unpickle_method(func_name, obj, cls):
    for cls in cls.__mro__:
        try:
            func = cls.__dict__[func_name]
        except KeyError:
            pass
        else:
            break
    return func.__get__(obj, cls)


copy_reg.pickle(types.MethodType, _pickle_method, _unpickle_method)


class auxiliar_multiprocessing(object):
    def __init__(self):
        pass
        #self.result = self.go()

    # Method that executes the multiprocessing
    def execute_multiprocessing(self,
                                function,
                                var_iterable,
                                dict_constants=dict(),
                                Ncores=8):
        # Store data in object
        self.external_function = function
        self.dict_constants = dict_constants
        # Start multiprocessing
        pool = Pool(Ncores)
        result = pool.map(self.method_single_proc, var_iterable)
        pool.close()
        pool.join()
        # Save and extract resultado
        self.result = result
        return result

    def method_single_proc(self, elem_iterable):
        # Method that is called in each iteration of the multiprocessing
        return self.external_function(elem_iterable, self.dict_constants)


# Funcion que aborda cada caso particular. Esta es la funcion que llamara el metodo de
# multithreading, y solo puede tener un argumento
def funcion_single_thread(params, constantes):
    # Operaciones de inicio
    #print(params)
    # Desacoplar los parametros
    (error, Nangulos) = params
    Nangulos = int(round(Nangulos))
    Naverages = constantes['Naverages']
    # Crear array con los resultados para despues promediar
    ajuste_p1 = np.zeros(Naverages)
    ajuste_p2 = np.zeros(Naverages)
    ajuste_p3 = np.zeros(Naverages)
    ajuste_p4 = np.zeros(Naverages)
    ajuste_p5 = np.zeros(Naverages)
    ajuste_p6 = np.zeros(Naverages)
    ajuste_fi1 = np.zeros(Naverages)
    ajuste_fi2 = np.zeros(Naverages)
    ajuste_fi3 = np.zeros(Naverages)
    ajuste_error = np.zeros(Naverages)
    no_solution = 0
    # Bucle para promediar
    for indAv in range(Naverages):
        # Matrices de Mueller
        (p1, p3, p5) = constantes['min_p1'] + (
            constantes['max_p1'] - constantes['min_p1']) * np.random.rand(3)
        (p2, p4, p6) = constantes['min_p2'] + (
            constantes['max_p2'] - constantes['min_p2']) * np.random.rand(3)
        (fi1, fi2, fi3) = constantes['min_angulo'] + (
            constantes['max_angulo'] -
            constantes['min_angulo']) * np.random.rand(3)
        Mp1_ini = ps.polarizer_linear(p1, p2, theta=0)
        Mp1 = ps.polarizer_linear(p1, p2, theta=fi1)
        Mp2 = ps.polarizer_linear(p3, p4, theta=fi2)
        Mp3 = ps.polarizer_linear(p5, p6, theta=fi3)
        M_malus_1 = [Mp3, Mp1_ini]
        M_malus_2 = [Mp3, Mp2]
        M_malus_3 = [Mp1, Mp2]
        M_triple_malus = [M_malus_1, M_malus_2, M_malus_3]
        theta = np.linspace(constantes['min_angulo'], constantes['max_angulo'],
                            Nangulos)
        # Crear el experimento "real"
        Intensity_1 = opsys.Intensity_Rotating_Elements(M_malus_1, [theta, 0],
                                                    constantes['Illum'])
        Intensity_2 = opsys.Intensity_Rotating_Elements(M_malus_2, [0, theta],
                                                    constantes['Illum'])
        Intensity_3 = opsys.Intensity_Rotating_Elements(M_malus_3, [theta, 0],
                                                    constantes['Illum'])
        # Anadir errores en los valores de intensidad
        max_intensity_1 = Intensity_1.max()
        max_intensity_2 = Intensity_2.max()
        max_intensity_3 = Intensity_3.max()
        Intensity_error_1 = Intensity_1 + error * max_intensity_1 * np.random.randn(
            Nangulos)
        Intensity_error_2 = Intensity_2 + error * max_intensity_2 * np.random.randn(
            Nangulos)
        Intensity_error_3 = Intensity_3 + error * max_intensity_3 * np.random.randn(
            Nangulos)
        Intensity_error = [
            Intensity_error_1, Intensity_error_2, Intensity_error_3
        ]
        # Ajustar el experimento con errores por minimos cuadrados
        par0 = [p1, p2, p3, p4, p5, p6, fi1, fi2, fi3]
        par1, success = optimize.leastsq(
            constantes['Funcion_error'],
            par0,
            args=(theta, constantes['Illum'], Intensity_error,
                  constantes['Funcion_modelo']))
        # Contabilizar solo si el ajuste funciono
        if success in range(1, 5):
            # Calcular el error en la distribucion
            residuos = error_modelo(par1, theta, constantes['Illum'],
                                    Intensity_error,
                                    constantes['Funcion_modelo'])
            # Dar resultados
            ajuste_error[indAv] = np.linalg.norm(residuos) / (
                Intensity_error_1.max() * sqrt(3 * Nangulos))
            ajuste_p1[indAv] = p1 - par1[0]
            ajuste_p2[indAv] = p2 - par1[1]
            ajuste_p3[indAv] = p3 - par1[2]
            ajuste_p4[indAv] = p4 - par1[3]
            ajuste_p5[indAv] = p5 - par1[4]
            ajuste_p6[indAv] = p6 - par1[5]
            ajuste_fi1[indAv] = abs(fi1 - par1[6]) % pi
            ajuste_fi2[indAv] = abs(fi2 - par1[7]) % pi
            ajuste_fi3[indAv] = abs(fi3 - par1[8]) % pi
            # Imprimir resultados para comparar
            if constantes['verbose']:
                print(
                    'Parametros iniciales: p1 = {}; p2 = {};  p3 = {}; p4 = {}; p5 = {}; p6 = {};'.
                    format(p1, p2, p3, p4, p5, p6))
                print(
                    'Angulos iniciales: fi1 = {} deg; fi2 = {} deg;  fi3 = {} deg;'.
                    format(fi1 / degrees, fi2 / degrees, fi3 / degrees))
                print(
                    'Parametros del ajuste: p1 = {}; p2 = {};  p3 = {}; p4 = {}; p5 = {}; p6 = {};'.
                    format(par1[0], par1[1], par1[2], par1[3], par1[4],
                           par1[5]))
                print(
                    'Angulos del ajuste: fi1 = {} deg; fi2 = {} deg;  fi3 = {} deg;'.
                    format(par1[6] / degrees, par1[7] / degrees,
                           par1[8] / degrees))
                print(
                    'Errores en param: p1 = {}; p2 = {};  p3 = {}; p4 = {}; p5 = {}; p6 = {};'.
                    format(ajuste_p1[indAv], ajuste_p2[indAv],
                           ajuste_p3[indAv], ajuste_p4[indAv],
                           ajuste_p5[indAv], ajuste_p6[indAv]))
                print(
                    'Errores en angulos: fi1 = {} deg; fi2 = {} deg;  fi3 = {} deg;'.
                    format(ajuste_fi1[indAv] / degrees,
                           ajuste_fi2[indAv] / degrees,
                           ajuste_fi3[indAv] / degrees))
                print(" ")
        else:
            # Dar resultados cuando el ajuste falla
            no_solution = no_solution + 1
            # Imprimir resultados para comparar
            if constantes['verbose']:
                print('El ajuste fallo.')
    # Hacer media
    if no_solution < Naverages:
        resultado = [
            np.mean(ajuste_error) * Naverages / (Naverages - no_solution),
            np.linalg.norm(ajuste_p1) / sqrt(Naverages - no_solution),
            np.linalg.norm(ajuste_p2) / sqrt(Naverages - no_solution),
            np.linalg.norm(ajuste_p3) / sqrt(Naverages - no_solution),
            np.linalg.norm(ajuste_p4) / sqrt(Naverages - no_solution),
            np.linalg.norm(ajuste_p5) / sqrt(Naverages - no_solution),
            np.linalg.norm(ajuste_p6) / sqrt(Naverages - no_solution),
            np.linalg.norm(ajuste_fi1) / sqrt(Naverages - no_solution),
            np.linalg.norm(ajuste_fi2) / sqrt(Naverages - no_solution),
            np.linalg.norm(ajuste_fi3) / sqrt(Naverages - no_solution),
            no_solution
        ]
        # Eliminar ceros
        for ind, elem in enumerate(resultado):
            if elem < constantes['tol_min:']:
                resultado[ind] = constantes['tol_min:']
    else:
        resultado = 1e6 * np.ones(11)
    # Imprimir resultados
    if constantes['verbose']:
        print(
            'Error medio: p1 = {}; p2 = {};  p3 = {}; p4 = {}; p5 = {}; p6 = {};'.
            format(resultado[1], resultado[2], resultado[3], resultado[4],
                   resultado[5], resultado[6]))
        print(
            'Error medio: fi1 = {} deg; fi2 = {} deg;  fi3 = {} deg; residuos = {}; Nfail = {};'.
            format(resultado[7] / degrees, resultado[8] / degrees,
                   resultado[9] / degrees, resultado[0], resultado[10]))
        print(" ")
    return resultado


# Funciones de modelo
def modelo_teor(par, th1, Illum):
    # Create Jones matrices
    Mp1_ini = ps.polarizer_linear(par[0], par[1], theta=0)
    Mp1 = ps.polarizer_linear(par[0], par[1], theta=par[6])
    Mp2 = ps.polarizer_linear(par[2], par[3], theta=par[7])
    Mp3 = ps.polarizer_linear(par[4], par[5], theta=par[8])
    # Realizar los tres experimentos de Ley de Malus
    M = [Mp3, Mp1_ini]
    th = [th1, 0]
    Iteor1 = Intensity_Rotating_Elements(M, th, Ei=Illum)
    M = [Mp3, Mp2]
    th = [0, th1]
    Iteor2 = Intensity_Rotating_Elements(M, th, Ei=Illum)
    M = [Mp1, Mp2]
    th = [th1, 0]
    Iteor3 = Intensity_Rotating_Elements(M, th, Ei=Illum)
    return Iteor1, Iteor2, Iteor3


def error_modelo(par, th1, Illum, Ireal, Funcion_modelo):
    Iteor = Funcion_modelo(par, th1, Illum)
    dif1 = Iteor[0] - Ireal[0]
    dif2 = Iteor[1] - Ireal[1]
    dif3 = Iteor[2] - Ireal[2]
    diferencia = np.concatenate((dif1, dif2, dif3))
    return diferencia


def function_to_test(iterable, constant):
    return iterable**2 * constant


"""
if __name__ == '__main__':
    dict_constants = {'x': 3, 'y': 4}
    N = 50000
    variable_process = np.linspace(0, 1, N)
    start = time.time()
    sc = auxiliar_multiprocessing()
    sc.execute_multiprocessing(function_to_test, variable_process, 1, 8)
    #print("{}".format(sp.array(sc.result)))
    print("8 processes pool took {} seconds".format(time.time() - start))
    start = time.time()
    res = range(N)
    for ind, val in enumerate(variable_process):
        res[ind] = function_to_test(val, 1)
    print("Single process pool took {} seconds".format(time.time() - start))
"""
if __name__ == '__main__':
    tic = time.time()
    # Diccionario con variables constanes para la funcion del experimento
    constants = {
        'Naverages': 100,
        'min_angulo': 0,
        'max_angulo': 180 * degrees,
        'min_angulo_th': 0 * degrees,
        'max_angulo_th': 180 * degrees,
        'min_p1': 0.8,
        'max_p1': 1,
        'min_p2': 0,
        'max_p2': 0.2,
        'Illum': ps.circular_light() * 2,
        'Funcion_modelo': modelo_teor,
        'Funcion_error': error_modelo,
        'tol_min:': 1e-10,
        'verbose': False
    }
    save_file = True
    # Variables para usar aqui
    Nerrores = 1
    Nmedidas = 1
    Ncores = 7
    (min_error, max_error) = (0, 0.02)
    (min_medidas, max_medidas) = (30, 200)
    # Crear arrays
    error_array = np.linspace(min_error, max_error, Nerrores)
    medidas_array = np.linspace(min_medidas, max_medidas, Nmedidas)
    # Fusionar ambos arrays en un solo iterable
    #merged_arrays = unir_en_iterable([error_array, medidas_array])
    merged_arrays = list(prod(error_array, medidas_array))

    # Test de que la funcion funciona
    #test = funcion_single_thread(merged_arrays[0], constants)
    start_test = time.time()
    test = funcion_single_thread((0.1, 115.), constants)
    end_test = time.time()
    print('La ejecucion de prueba tardo: {} s'.format(end_test - start_test))
    # Realizar el calculo
    am = auxiliar_multiprocessing()
    results = am.execute_multiprocessing(
        function=funcion_single_thread,
        var_iterable=merged_arrays,
        dict_constants=constants,
        Ncores=Ncores)
    print('Multithreading finalizado')

    # Desdoblar los resultados en matrices
    shape = [Nerrores, Nmedidas]
    results = multiproc.desdoblar_desde_iterable(results, shape)

    # Salvar datos en un archivo
    if save_file:
        constants_aux = {
            'Nerrores': Nerrores,
            'Nmedidas': Nmedidas,
            'min_error': min_error,
            'max_medidas': max_medidas,
            'min_medidas': min_medidas,
            'max_medidas': max_medidas,
            'error_array': error_array,
            'medidas_array': medidas_array
        }
        filename = "Triple_Malus_Law_{}".format(datetime.date.today())
        np.savez(
            filename + '.npz',
            results=results,
            constants=constants,
            constants_aux=constants_aux)

    # Final
    toc = time.time()
    print('Elapsed time is: {} s.'.format(toc - tic))
