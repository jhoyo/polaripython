# This script executes the automatic fits necessary for the polarimeter claibration in transmission.

from __future__ import print_function, division

import numpy as np
import scipy as sp
import matplotlib.pyplot as plt
import time
import datetime
import pprint
from math import sqrt, pi

from scipy.io import savemat, loadmat
from scipy.special import erf
from scipy import optimize
from phyton_optics import degrees

import phyton_optics.polarization_jones as pj
import phyton_optics.polarization_stokes as ps
import phyton_optics.polarization_mueller_analysis as amm
from phyton_optics.utils_common import ndgrid

from polarimeter import Intensity_Rotating_Elements

import polarimeter as opsys
import numpy as np
import os

from utils import dibujar_2d_fitting, plot_2d, plot_experiment_residuals_1D


## Funciones extra
def dist_angulos(th1, th2, th0=None):
    """Function to calculate the individual angles in order to introduce an experiment of the polarimeter calibration in the function
    to determine Mueller matrix."""

    # Create initial angle distribution
    (N1, N2) = (th1.size, th2.size)
    Nmeas = N1 * N2
    angles = np.zeros([4, Nmeas])
    # fill the medium values
    for ind1 in range(N1):
        for ind2 in range(N2):
            ind3 = ind1 * (N1 - 1) + ind2
            #             print([ind1, ind2, ind3])
            if th0 is None:
                angles[1, ind3] = th1[ind1]
                angles[2, ind3] = th2[ind2]
            else:
                angles[1, ind3] = th1[ind1] + th0[1]
                angles[2, ind3] = th2[ind2] + th0[2]
                angles[0, ind3] = th0[0]
                angles[3, ind3] = th0[3]
    return angles


def plot_2d_scattered(x,
                      y,
                      z,
                      Nxy=25,
                      method='linear',
                      title='',
                      color="magma",
                      xy_are_angles=True):
    """Function that plots a 2D surface given by 1D arrays of X, Y and Z by interpolating."""
    # First, we have to interpolate the data. For doing that, we need to create the space where our data is in.
    xspace, yspace = np.linspace(x.min(), x.max(), Nxy), np.linspace(
        y.min(), y.max(), Nxy)
    xgrid, ygrid = ndgrid(xspace, yspace)
    # Now we can interpolate
    z2d = sp.interpolate.griddata((x, y), z, (xgrid, ygrid), method=method)
    # Plot
    plot_2d(
        xgrid,
        ygrid,
        z2d,
        title=title,
        color=color,
        xy_are_angles=xy_are_angles)


## Funcion principal
def process_calibration(fecha, params=None, seed=None):
    ## Proceso de ajustes
    # Parametros del bucle
    if params is None:
        params = {}
        params["NmaxIteraciones"] = 20
        params["tolerancia"] = 1e-4
        params["PlotCadaPaso"] = False
        params["ordenarPes"] = True
        params["tol_while"] = 2.5e-4
        params["N_it_while"] = 10
        params["use_random_angles"] = True
        params["fix_parameters"] = True
        params["hacer_paso_11"] = True
        params["save_data"] = True

    if seed is None:
        seed = np.zeros(9)
        use_seed = False
    else:
        use_seed = True

    fecha = "{}".format(fecha)

    ## Funciones de ajuste
    # Funciones
    def model_paso5(par, th1):
        # Ordenar o no los pes del polarizador
        if params["ordenarPes"]:
            p1_c = max(np.abs(par[0]), np.abs(par[1]))
            p2_c = min(np.abs(par[0]), np.abs(par[1]))
            Mpmalo = ps.polarizer_linear(p1=p1_c, p2=p2_c)
        else:
            Mpmalo = ps.polarizer_linear(p1=par[0], p2=par[1])
        # Calcular
        M = [Mpmalo, Mp1]
        th = [0, th1 + par[2]]
        Iteor = Intensity_Rotating_Elements(M, th, Ei=Ifuente)
        return Iteor

    def err_paso5(par, th1, Ireal):
        Iteor = model_paso5(par, th1)
        diferencia = Iteor - Ireal
        return diferencia

    def model_paso6(par, th1):
        # Create Jones matrices
        M = [Mr2, Mp1]
        # Rotar la fuente para evitar errores
        th_error1 = errAmpIt * erf(par[1])
        th_error2 = el_errAmpIt * erf(par[2])
        Ipaso6 = ps.Stokes_azimuth_ellipticity(
            az=Saz + th_error1,
            el=Sel + th_error2,
            intensity=S0,
            pol_degree=Spol)
        # Calcular
        th = [th1 + par[0], +th0p1b]
        Iteor = Intensity_Rotating_Elements(M, th, Ei=Ipaso6)
        return Iteor

    def err_paso6(par, th1, Ireal):
        Iteor = model_paso6(par, th1)
        diferencia = Iteor - Ireal
        return diferencia

    def model_paso7(par, th1, th2):
        # Iluminacion y angulos
        Itest = ps.Stokes_azimuth_ellipticity(
            az=par[1], el=par[2], intensity=par[0], pol_degree=par[3])
        # Calcular
        th_error = errAmpIt * erf(par[4])
        th = [th1 + th0r2b + th_error, th2 + th0p1b]
        M = [Mr2, Mp1]
        I = Intensity_Rotating_Elements(M, th, Itest, False)
        return I

    def err_paso7(par, th1, th2, Ireal):
        dI = model_paso7(par, th1, th2) - Ireal
        return dI.flatten()

    def model_paso8(par, th1a, th1b, th1c):
        # Ordenar o no los pes del polarizador
        if params["ordenarPes"]:
            p1_1 = max(np.abs(par[0]), np.abs(par[1]))
            p2_1 = min(np.abs(par[0]), np.abs(par[1]))
            p1_2 = max(np.abs(par[2]), np.abs(par[3]))
            p2_2 = min(np.abs(par[2]), np.abs(par[3]))
            p1_3 = max(np.abs(par[4]), np.abs(par[5]))
            p2_3 = min(np.abs(par[4]), np.abs(par[5]))
            Mp1 = ps.polarizer_linear(p1=p1_1, p2=p2_1)
            Mp2 = ps.polarizer_linear(p1=p1_2, p2=p2_2)
            Mp3 = ps.polarizer_linear(p1=p1_3, p2=p2_3)
        else:
            Mp1 = ps.polarizer_linear(p1=par[0], p2=par[1])
            Mp2 = ps.polarizer_linear(p1=par[2], p2=par[3])
            Mp3 = ps.polarizer_linear(p1=par[4], p2=par[5])
        # Rotar la fuente para evitar errores
        th_error1 = errAmpIt * erf(par[9])
        th_error2 = el_errAmpIt * erf(par[10])
        Ipaso8 = ps.Stokes_azimuth_ellipticity(
            az=Saz + th_error1,
            el=Sel + th_error2,
            intensity=S0,
            pol_degree=Spol)
        # First, P3 and P1
        M = [Mp3, Mp1]
        th = [th1a + par[6], +th0p1b]
        Ia = Intensity_Rotating_Elements(M, th, Ei=Ipaso8)
        # Then, P3 and P2
        M = [Mp3, Mp2]
        th = [+par[6], th1b + par[7]]
        Ib = Intensity_Rotating_Elements(M, th, Ei=Ipaso8)
        # Last, P1 and P2
        M = [Mp1, Mp2]
        th = [th1c + par[8], +par[7]]
        Ic = Intensity_Rotating_Elements(M, th, Ei=Ipaso8)
        # End
        return (Ia, Ib, Ic)

    def err_paso8(par, th1a, th1b, th1c, IRa, IRb, IRc):
        (Ia, Ib, Ic) = model_paso8(par, th1a, th1b, th1c)
        dIa = Ia - IRa
        dIb = Ib - IRb
        dIc = Ic - IRc
        dI = np.concatenate((dIa, dIb, dIc))
        return dI

    def model_paso9(par, th1, th2):
        # Crear matrices
        Mr2 = ps.Mueller_Real_Retarder(par[0], par[1], par[2])
        Mp1 = ps.Mueller_Real_Retarder(p11, p12, par[4])
        # Rotar la fuente para evitar errores
        th_error = errAmpIt * erf(par[5])
        th_error1 = errAmpIt * erf(par[6])
        th_error2 = el_errAmpIt * erf(par[7])
        Ipaso9 = ps.Stokes_azimuth_ellipticity(
            az=Saz + th_error1,
            el=Sel + th_error2,
            intensity=S0,
            pol_degree=Spol)
        # Calcular
        M = [Mp1, Mr2, Mp2]
        th = [th1 + th0p1 + th_error, +par[3], th2 + th0p2]
        I = Intensity_Rotating_Elements(M, th, Ipaso9, False)
        return I

    def err_paso9(par, th1, th2, Ireal):
        dI = model_paso9(par, th1, th2) - Ireal
        return dI.flatten()

    def model_paso11a(par, th1, th2):
        Mr1 = ps.Mueller_Real_Retarder(par[0], par[1], par[2])
        # Rotar la fuente para evitar errores
        th_error1 = errAmpIt * erf(par[4])
        th_error2 = errAmpIt * erf(par[5])
        th_error3 = errAmpIt * erf(par[6])
        #     th_error3 = errAmpIt * erf(par[7])
        #     th_error4 = errAmpIt * erf(par[8])
        #     Ipaso11a = ps.Stokes_azimuth_ellipticity(
        #         az=Saz+th_error3, el=Sel+th_error4, intensity=S0, pol_degree=Spol)
        # Calcular
        M = [Mp1, Mr1, Mr2, Mp2]
        th = [
            th0p1 + th_error1, th1 + par[3], th2 + th0r2 + th_error3,
            th0p2 + th_error2
        ]
        I = Intensity_Rotating_Elements(M, th, Ifuente)
        return I

    def err_paso11a(par, th1, th2, Ireal):
        dI = model_paso11a(par, th1, th2) - Ireal
        return dI.flatten()

    def model_paso11b(th1, th2):
        # Calcular
        M = [Mp1, Mr1, Mr2, Mp2]
        th = [th0p1, th1 + th0r1, th2 + th0r2, th0p2 + pi / 2]
        I = Intensity_Rotating_Elements(M, th, Ifuente)
        return I

    ## Carga de datos
    # Load file
    filename = 'Paso_4_' + fecha + '.npz'
    data = np.load(filename)
    # Rename variables
    Iindividual = data['Iindividual']
    Naverage = data['Naverage']
    # Make stadistics
    ratio_individual = Iindividual[:, 0] / Iindividual[:, 1]
    mean = np.mean(Iindividual, axis=0)
    mean_phd2 = np.mean(Iindividual[:, 1])
    # Load file
    filename = 'Paso_5_' + fecha + '.npz'
    data = np.load(filename)
    # Rename variables
    angle1 = data['angle1']
    Intensity_Paso5 = data['intensity1']
    # Load file
    filename = 'Paso_6_' + fecha + '.npz'
    data = np.load(filename)
    # Rename variables
    Intensity_Paso6 = data['intensity1']
    # Load file
    filename = 'Paso_7_' + fecha + '.npz'
    data = np.load(filename)
    # Rename variables
    angles2x = data['angle2x']
    angles2y = data['angle2y']
    Intensity_Paso7 = data['intensity2']
    # Load file
    filename = 'Paso_8a_' + fecha + '.npz'
    data = np.load(filename)
    # Rename variables
    Intensity_Paso8a = data['intensity1']
    # Load file
    filename = 'Paso_8b_' + fecha + '.npz'
    data = np.load(filename)
    # Rename variables
    Intensity_Paso8b = data['intensity1']
    # Load file
    filename = 'Paso_8c_' + fecha + '.npz'
    data = np.load(filename)
    # Rename variables
    Intensity_Paso8c = data['intensity1']
    # Load file
    filename = 'Paso_9_' + fecha + '.npz'
    data = np.load(filename)
    # Rename variables
    Intensity_Paso9 = data['intensity2']
    # Load file
    filename = 'Paso_11a_' + fecha + '.npz'
    data = np.load(filename)
    # Rename variables
    Intensity_Paso11a = data['intensity2']
    # Load file
    filename = 'Paso_11b_' + fecha + '.npz'
    data = np.load(filename)
    # Rename variables
    Intensity_Paso11b = data['intensity2']
    print('Carga de datos finalizada')

    # Parametros iniciales
    (p11, p12, Dp1, p21, p22, p31, p32, pc1,
     pc2) = (0.95, 0.15, 0 * degrees, 0.95, 0.15, 0.95, 0.15, 0.95, 0.3)
    (R2p1, R2p2, Dr2) = (0.99, 0.99, 85 * degrees)
    Mp1 = ps.polarizer_linear(p1=p11, p2=p12)
    Mr2 = ps.Mueller_Real_Retarder(R2p1, R2p2, Dr2)
    (S0, Saz, Sel, Spol) = (mean[0], 0, 44 * degrees, 1)
    Ifuente = ps.Stokes_azimuth_ellipticity(
        az=Saz, el=Sel, intensity=S0, pol_degree=Spol)
    (th0p1b, th0p1, th0p2, th0p3, th0r2, th0r2b) = (0, 0, 0, 0, 0, 0)
    (R1p1, R1p2, Dr1, th0r1) = (0.99, 0.99, 85 * degrees, 0)
    # Parametros asociados a params["tolerancia"] de errores
    (th0E0, errAmp, el_errAmp, exp_crec, max_change,
     el_max_change) = (0, 90 * degrees, 3 * degrees, 1.5, 20 * degrees,
                       1 * degrees)
    (errAmpIt, el_errAmpIt) = (errAmp, el_errAmp)
    # Inicializar donde guardo los datos de cada iteracion
    errores = np.zeros([params["NmaxIteraciones"], 8])
    pes = np.zeros([params["NmaxIteraciones"], 12])
    ilums = np.zeros([params["NmaxIteraciones"], 4])
    delays = np.zeros([params["NmaxIteraciones"], 3])
    fangles = np.zeros([params["NmaxIteraciones"], 7])
    errangles = np.zeros([params["NmaxIteraciones"], 11])
    seed = np.zeros(7)
    Nelements1D = angle1.size
    Nelements2D = angles2x.size * angles2y.size

    # Hacer el bucle
    start_time = time.time()
    for indIt in range(params["NmaxIteraciones"]):
        # Actualizar amplitud de error por iteracion
        errAmptest = errAmp / (indIt + 1)**exp_crec
        errAmpIt = max(errAmptest, errAmpIt - max_change)
        el_errAmptest = el_errAmp / (indIt + 1)**exp_crec
        el_errAmpIt = max(el_errAmptest, el_errAmpIt - el_max_change)
        if not params["use_random_angles"]:
            params["NmaxIteraciones"] = 1
        print('Iteracion {}'.format(indIt))
        if params["PlotCadaPaso"]:
            print(
                'Amplitud de error en angulos = {}'.format(errAmpIt / degrees))

        ## Comenzar por ajustar el angulo de P1 en el motor 4 (Paso 5)
        (contador, error_Paso, mejor_error) = (0, 1, 1)
        while contador < params["N_it_while"] and error_Paso > params["tol_while"]:
            # Inicializar angulo en la primera iteracion
            if indIt == 0:
                if use_seed:
                    th0p1b = seed[0]
                elif params["use_random_angles"]:
                    th0p1b = np.random.rand(1) * pi
                    th0p1b = th0p1b[0]
            # Parametros iniciales
            par0 = [pc1, pc2, th0p1b]
            # Normalizacion de intensidades experimentales en funcion de la referencia
            Iexperimental = Intensity_Paso5[:, 0] * Intensity_Paso5[:,
                                                                    1] / mean[
                                                                        1]
            # Ajuste
            par1, success = optimize.leastsq(
                err_paso5, par0, args=(angle1 * degrees, Iexperimental))
            # Calculo de errores
            error_Paso = err_paso5(par1, angle1 * degrees, Iexperimental)
            error_Paso = np.linalg.norm(error_Paso) / (
                Iexperimental.max() * Nelements1D)
            # Save random seed
            if indIt == 0:
                seed[0] = th0p1b
            # Guardar mejores resultados
            if error_Paso < mejor_error:
                par_mejor = par1
                mejor_error = error_Paso
                # Save random seed
                if indIt == 0:
                    seed[0] = th0p1b
            # Operaciones final de bucle
            contador = contador + 1
            if params["PlotCadaPaso"] and params["N_it_while"] > 1:
                print('Subiteracion {} da error {}.'.format(
                    contador, error_Paso))
        # Guardar resultados
        pc1 = max(np.abs(par_mejor[0]), np.abs(par_mejor[1]))
        pc2 = min(np.abs(par_mejor[0]), np.abs(par_mejor[1]))
        (pes[indIt, 8], pes[indIt, 9]) = (pc1, pc2)
        fangles[indIt, 4] = th0p1b
        errores[indIt, 0] = mejor_error
        # Extraer resultados para siguientes iteraciones.
        th0p1b = par_mejor[2] % pi
        # if pc1 == par_mejor[0]:
        #     th0p1b = par_mejor[2] % pi
        # else:
        #     th0p1b = (par_mejor[2] + pi / 2) % pi
        # Plots
        if params["PlotCadaPaso"] or indIt + 1 == params["NmaxIteraciones"]:
            print("Parametros del paso 5")
            print("pc1 = {}; pc2 = {}; Th0p1b = {} deg;".format(
                par_mejor[0], par_mejor[1], (par_mejor[2] % pi) / degrees))
            print("Error del paso 5: {}.".format(errores[indIt, 0]))
            I_fit = model_paso5(par_mejor, angle1 * degrees)
            plot_experiment_residuals_1D(
                angle1 * degrees, Iexperimental, I_fit, title='Paso5')

        ## Ahora el del retardador 2 la primera vez que lo metimos (Paso 6)
        (contador, error_Paso, mejor_error) = (0, 1, 1)
        while contador < params["N_it_while"] and error_Paso > params["tol_while"]:
            # Inicializar angulo en la primera iteracion
            if indIt == 0:
                if use_seed:
                    th0r2b = seed[1]
                elif params["use_random_angles"]:
                    th0r2b = np.random.rand(1) * pi
                    th0r2b = th0r2b[0]
    #             Saz = np.rando+.random.rand(1))*degrees
    # Parametros iniciales
            par0 = [th0r2b, th0E0, th0E0]
            # Normalizacion de intensidades experimentales en funcion de la referencia
            Iexperimental = Intensity_Paso6[:, 0] * Intensity_Paso6[:,
                                                                    1] / mean[
                                                                        1]
            # Ajuste
            par1, success = optimize.leastsq(
                err_paso6, par0, args=(angle1 * degrees, Iexperimental))
            # Calculo de errores
            error_Paso = err_paso6(par1, angle1 * degrees, Iexperimental)
            error_Paso = np.linalg.norm(error_Paso) / (
                Iexperimental.max() * Nelements1D)
            # Guardar mejores resultados
            if error_Paso < mejor_error:
                par_mejor = par1
                mejor_error = error_Paso
                Saz_mejor = Saz
                Sel_mejor = Sel
                # Save random seed
                if indIt == 0:
                    seed[1] = th0r2b
            # Operaciones final de bucle
            contador = contador + 1
            if params["PlotCadaPaso"] and params["N_it_while"] > 1:
                print('Subiteracion {} da error {}.'.format(
                    contador, error_Paso))
        # Extraer resultados para siguientes iteraciones
        th0r2b = par_mejor[0] % pi
        th_error1 = errAmpIt * erf(par_mejor[1])
        th_error2 = el_errAmpIt * erf(par_mejor[2])
        # Guardar resultados
        fangles[indIt, 5] = th0r2b
        errangles[indIt, 0:2] = (th_error1, th_error2)
        errores[indIt, 1] = mejor_error
        # Plots
        if params["PlotCadaPaso"] or indIt + 1 == params["NmaxIteraciones"]:
            print("Parametros del paso 6")
            print("Th0r2b = {} deg; Th0E_az = {} deg; Th0E_el = {} deg;".
                  format((par_mejor[0] % pi) / degrees, th_error1 / degrees,
                         th_error2 / degrees))
            print("Error del paso 6: {}.".format(errores[indIt, 1]))
            I_fit = model_paso6(par_mejor, angle1 * degrees)
            plot_experiment_residuals_1D(
                angle1 * degrees, Iexperimental, I_fit, title='Paso6')
        # Arreglar parametros anteriores que el nuevo ajuste pide cambiar
        if params["fix_parameters"]:
            Saz = Saz_mejor + th_error1
            Sel = Sel_mejor + th_error2
            Ipaso6 = ps.Stokes_azimuth_ellipticity(
                az=Saz, el=Sel, intensity=S0, pol_degree=Spol)

        ## Vamos a ajustar la iluminacion (Paso 7)
        # Parametros iniciales
        par0 = [S0, Saz, Sel, Spol, th0E0]
        # Normalizacion de intensidades experimentales en funcion de la referencia
        Iexperimental = Intensity_Paso7[:, :, 0] * Intensity_Paso7[:, :,
                                                                   1] / mean[1]
        #Iexperimental = np.transpose(Iexperimental)
        # Ajuste
        par1, success = optimize.leastsq(
            err_paso7,
            par0,
            args=(angles2x * degrees, angles2y * degrees, Iexperimental))
        # Extraer resultados para siguientes iteraciones
        (S0, Saz, Sel, Spol) = par1[0:4]
        Ifuente = ps.Stokes_azimuth_ellipticity(
            az=Saz, el=Sel, intensity=S0, pol_degree=Spol)
        th_error = errAmpIt * erf(par1[4])
        # Guardar resultados
        ilums[indIt, 0:4] = (S0, Saz, Sel, Spol)
        errangles[indIt, 2] = th_error
        # Calculo de errores
        error_Paso = err_paso7(par1, angles2x * degrees, angles2y * degrees,
                               Iexperimental)
        errores[indIt, 2] = np.linalg.norm(error_Paso) / (
            Iexperimental.max() * Nelements2D)
        # Plots
        if params["PlotCadaPaso"] or indIt + 1 == params["NmaxIteraciones"]:
            print("Parametros del paso 7")
            print(
                "S0 = {} V; Azimuth = {} deg; Ellipticity = {} deg; Pol. degree = {}; Th0_error = {} deg;".
                format(par1[0], par1[1] / degrees, par1[2] / degrees, par1[3],
                       th_error / degrees))
            print("Error del paso 7: {}.".format(errores[indIt, 2]))
            I_fit = model_paso7(par1, angles2x * degrees, angles2y * degrees)
            plt.figure(figsize=(20, 5))
            plt.subplot(1, 3, 1)
            plot_2d(
                angles2x * degrees,
                angles2y * degrees,
                Iexperimental,
                title='Paso7')
            plt.subplot(1, 3, 2)
            plot_2d(angles2x * degrees, angles2y * degrees, I_fit, title='Fit')
            plt.subplot(1, 3, 3)
            plot_2d(
                angles2x * degrees,
                angles2y * degrees,
                (Iexperimental - I_fit) / Iexperimental.max(),
                title='Normalized residuals')
        # Arreglar parametros anteriores que el nuevo ajuste pide cambiar
        if params["fix_parameters"]:
            th0p1b = th0p1b + th_error

        ## Ahora vamos a por los polarizadores (Paso 8)
        (contador, error_Paso, mejor_error) = (0, 1, 1)
        while contador < params["N_it_while"] and error_Paso > params["tol_while"]:
            # Inicializar angulo en la primera iteracion
            if indIt == 0:
                if use_seed:
                    (th0p1, th0p2, th0p3) = seed[2:5]
                elif params["use_random_angles"]:
                    (th0p1, th0p2, th0p3) = np.random.rand(3) * pi
            # Parametros iniciales
            par0 = [
                p11, p12, p21, p22, p31, p32, th0p3, th0p2, th0p1, th0E0, th0E0
            ]
            # Normalizacion de intensidades experimentales en funcion de la referencia
            IexpA = Intensity_Paso8a[:, 0] * Intensity_Paso8a[:, 1] / mean[1]
            IexpB = Intensity_Paso8b[:, 0] * Intensity_Paso8b[:, 1] / mean[1]
            IexpC = Intensity_Paso8c[:, 0] * Intensity_Paso8c[:, 1] / mean[1]
            maxval = np.array([IexpA.max(), IexpB.max(), IexpC.max()]).max()
            # Ajuste
            par1, success = optimize.leastsq(
                err_paso8,
                par0,
                args=(angle1 * degrees, angle1 * degrees, angle1 * degrees,
                      IexpA, IexpB, IexpC))
            # Calculo de errores
            error_Paso = err_paso8(par1, angle1 * degrees, angle1 * degrees,
                                   angle1 * degrees, IexpA, IexpB, IexpC)
            error_Paso = np.linalg.norm(error_Paso) / (
                maxval * 3 * Nelements1D)
            # Guardar mejores resultados
            if error_Paso < mejor_error:
                par_mejor = par1
                mejor_error = error_Paso
                # Save random seed
                if indIt == 0:
                    seed[2:5] = (th0p1, th0p2, th0p3)
            # Operaciones final de bucle
            contador = contador + 1
            if params["PlotCadaPaso"] and params["N_it_while"] > 1:
                print('Subiteracion {} da error {}.'.format(
                    contador, error_Paso))
        # Extraer resultados para siguientes iteraciones.
        p11 = max(np.abs(par_mejor[0]), np.abs(par_mejor[1]))
        p12 = min(np.abs(par_mejor[0]), np.abs(par_mejor[1]))
        p21 = max(np.abs(par_mejor[2]), np.abs(par_mejor[3]))
        p22 = min(np.abs(par_mejor[2]), np.abs(par_mejor[3]))
        p31 = max(np.abs(par_mejor[4]), np.abs(par_mejor[5]))
        p32 = min(np.abs(par_mejor[4]), np.abs(par_mejor[5]))
        th0p1 = par_mejor[8] % pi
        th0p2 = par_mejor[7] % pi
        th0p3 = par_mejor[6] % pi
        # if p11 == par_mejor[0]:
        #     th0p1 = par_mejor[8] % pi
        # else:
        #     th0p1 = (par_mejor[8] + pi / 2) % pi
        # if p21 == par_mejor[2]:
        #     th0p2 = par_mejor[7] % pi
        # else:
        #     th0p2 = (par_mejor[7] + pi / 2) % pi
        # if p31 == par_mejor[4]:
        #     th0p3 = par_mejor[6] % pi
        # else:
        #     th0p3 = (par_mejor[6] + pi / 2) % pi
        Mp2 = ps.polarizer_linear(p1=p21, p2=p22)
        th_error1 = errAmpIt * erf(par_mejor[9])
        th_error2 = el_errAmpIt * erf(par_mejor[10])
        # Guardar resultados
        pes[indIt, 0:6] = (p11, p12, p21, p22, p31, p32)
        fangles[indIt, 0:3] = (th0p1, th0p2, th0p3)
        errangles[indIt, 3:5] = (th_error1, th_error2)
        errores[indIt, 3] = mejor_error
        # Plots
        if params["PlotCadaPaso"] or indIt + 1 == params["NmaxIteraciones"]:
            print("Parametros del paso 8")
            print(
                "p11 = {}; p12 = {}; p21 = {}; p22 = {}; p31 = {}; p32 = {};".
                format(par_mejor[0], par_mejor[1], par_mejor[2], par_mejor[3],
                       par_mejor[4], par_mejor[5]))
            print(
                "Th0p1 = {} deg; Th0p2 = {} deg; Th0p3 = {} deg; Th0E_az = {} deg; Th0E_el = {} deg;".
                format((par_mejor[8] % pi) / degrees, (
                    par_mejor[7] % pi) / degrees, (par_mejor[6] % pi) /
                       degrees, th_error1 / degrees, th_error2 / degrees))
            print("Error del paso 8: {}.".format(errores[indIt, 3]))
            I_fit = model_paso8(par_mejor, angle1 * degrees, angle1 * degrees,
                                angle1 * degrees)
            plot_experiment_residuals_1D(
                np.concatenate([
                    angle1 * degrees, angle1 * degrees + 2 * pi,
                    angle1 * degrees + 4 * pi
                ]),
                np.concatenate([IexpA, IexpB, IexpC]),
                np.concatenate((I_fit[0], I_fit[1], I_fit[2])),
                title='Paso8')
        # Arreglar parametros anteriores que el nuevo ajuste pide cambiar
        if params["fix_parameters"]:
            Ifuente = ps.Stokes_azimuth_ellipticity(
                az=Saz + th_error1,
                el=Sel + th_error2,
                intensity=S0,
                pol_degree=Spol)

        ## Por ultimo, ajustamos el retardador (Paso 9)
        (contador, error_Paso, mejor_error) = (0, 1, 1)
        while contador < params["N_it_while"] and error_Paso > params["tol_while"]:
            # Inicializar angulo en la primera iteracion
            if indIt == 0:
                if use_seed:
                    th0r2 = seed[5]
                elif params["use_random_angles"]:
                    th0r2 = np.random.rand(1) * pi
                    th0r2 = th0r2[0]
            # Parametros iniciales
            par0 = [R2p1, R2p2, Dr2, th0r2, Dp1, th0E0, th0E0, th0E0]
            # Normalizacion de intensidades experimentales en funcion de la referencia
            Iexperimental = Intensity_Paso9[:, :,
                                            0] * Intensity_Paso9[:, :,
                                                                 1] / mean[1]
            # Ajuste
            par1, success = optimize.leastsq(
                err_paso9,
                par0,
                args=(angles2x * degrees, angles2y * degrees, Iexperimental))
            # Calculo de errores
            error_Paso = err_paso9(par1, angles2x * degrees,
                                   angles2y * degrees, Iexperimental)
            error_Paso = np.linalg.norm(error_Paso) / (
                Iexperimental.max() * Nelements2D)
            # Guardar mejores resultados
            if error_Paso < mejor_error:
                par_mejor = par1
                mejor_error = error_Paso
                # Save random seed
                if indIt == 0:
                    seed[5] = th0r2
            # Operaciones final de bucle
            contador = contador + 1
            if params["PlotCadaPaso"] and params["N_it_while"] > 1:
                print('Subiteracion {} da error {}.'.format(
                    contador, error_Paso))
        # Extraer resultados para siguientes iteraciones
        (R2p1, R2p2, Dr2, th0r2,
         Dp1) = (par_mejor[0], par_mejor[1], par_mejor[2] % pi,
                 par_mejor[3] % pi, par_mejor[4] % pi)
        Mr2 = ps.Mueller_Real_Retarder(R2p1, R2p2, Dr2)
        Mp1 = ps.Mueller_Real_Retarder(p11, p12, Dp1)
        th_error = errAmpIt * erf(par_mejor[5])
        th_error1 = errAmpIt * erf(par_mejor[6])
        th_error2 = el_errAmpIt * erf(par_mejor[7])
        # Guardar resultados
        pes[indIt, 6:8] = (R2p1, R2p2)
        delays[indIt, 0:2] = (Dr2, Dp1)
        fangles[indIt, 3] = th0r2
        errangles[indIt, 5:8] = (th_error, th_error1, th_error2)
        errores[indIt, 4] = mejor_error
        # Plots
        if params["PlotCadaPaso"] or indIt + 1 == params["NmaxIteraciones"]:
            print("Parametros del paso 9")
            print(
                "R2p1 = {}; R2p2 = {}; Delay_R2 = {}; Th0_R2 = {}; Delay_p1 = {};".
                format(par_mejor[0], par_mejor[1], (par_mejor[2] % pi) /
                       degrees, (par_mejor[3] % pi) / degrees, (
                           par_mejor[4] % pi) / degrees))
            print("Th0_error = {}; Th0E_az = {}; Th0E_az = {};".format(
                th_error / degrees, th_error1 / degrees, th_error2 / degrees))
            print("Error del paso 9: {}.".format(errores[indIt, 4]))
            I_fit = model_paso9(par_mejor, angles2x * degrees,
                                angles2y * degrees)
            plt.figure(figsize=(20, 5))
            plt.subplot(1, 3, 1)
            plot_2d(
                angles2x * degrees,
                angles2y * degrees,
                Iexperimental,
                title='Paso9')
            plt.subplot(1, 3, 2)
            plot_2d(angles2x * degrees, angles2y * degrees, I_fit, title='Fit')
            plt.subplot(1, 3, 3)
            plot_2d(
                angles2x * degrees,
                angles2y * degrees,
                (Iexperimental - I_fit) / Iexperimental.max(),
                title='Normalized residuals')
        # Arreglar parametros anteriores que el nuevo ajuste pide cambiar
        if params["fix_parameters"]:
            th0p1 = th0p1 + th_error
            Ifuente = ps.Stokes_azimuth_ellipticity(
                az=Saz + th_error1,
                el=Sel + th_error2,
                intensity=S0,
                pol_degree=Spol)

        ## Bonus, ajustar el primer retardador (Paso 11a)
        if params["hacer_paso_11"]:
            (contador, error_Paso, mejor_error) = (0, 1, 1)
            while contador < params["N_it_while"] and error_Paso > params["tol_while"]:
                # Inicializar angulo en la primera iteracion
                if indIt == 0:
                    if use_seed:
                        th0r1 = seed[6]
                    elif params["use_random_angles"]:
                        th0r1 = np.random.rand(1) * pi
                        th0r1 = th0r1[0]
                # Parametros iniciales
                par0 = [R1p1, R1p2, Dr1, th0r1, th0E0, th0E0, th0E0]
                # Normalizacion de intensidades experimentales en funcion de la referencia
                Iexperimental = Intensity_Paso11a[:, :,
                                                  0] * Intensity_Paso11a[:, :,
                                                                         1] / mean[
                                                                             1]
                # Ajuste
                par1, success = optimize.leastsq(
                    err_paso11a,
                    par0,
                    args=(angles2x * degrees, angles2y * degrees,
                          Iexperimental))
                # Calculo de errores
                error_Paso = err_paso11a(par1, angles2x * degrees,
                                         angles2y * degrees, Iexperimental)
                error_Paso = np.linalg.norm(error_Paso) / (
                    Iexperimental.max() * Nelements2D)
                # Guardar mejores resultados
                if error_Paso < mejor_error:
                    par_mejor = par1
                    mejor_error = error_Paso
                    # Save random seed
                    if indIt == 0:
                        seed[6] = th0r1
                # Operaciones final de bucle
                contador = contador + 1
                if params["PlotCadaPaso"] and params["N_it_while"] > 1:
                    print('Subiteracion {} da error {}.'.format(
                        contador, error_Paso))
            # Extraer resultados para siguientes iteraciones
            (R1p1, R1p2) = par_mejor[0:2]
            (Dr1, th0r1) = par_mejor[2:4] % pi
            Mr1 = ps.Mueller_Real_Retarder(R1p1, R1p2, Dr1)
            th_error1 = errAmpIt * erf(par_mejor[4])
            th_error2 = errAmpIt * erf(par_mejor[5])
            th_error3 = errAmpIt * erf(par_mejor[6])
            #         th_error = errAmpIt * erf(par_mejor[5])
            #         th_error1 = errAmpIt * erf(par_mejor[6])
            #         th_error2 = el_errAmpIt * erf(par_mejor[7])
            # Guardar resultados
            pes[indIt, 10:12] = (R1p1, R1p2)
            delays[indIt, 2] = Dr1
            fangles[indIt, 6] = (th0r1)
            errangles[indIt, 8:11] = (th_error1, th_error2, th_error3)
            #         errangles[indIt, 5:8] = (th_error,th_error1, th_error2)
            errores[indIt, 5] = mejor_error
            # Plots
            if params["PlotCadaPaso"] or indIt + 1 == params["NmaxIteraciones"]:
                print("Parametros del paso 11a")
                print("R1p1 = {}; R1p2 = {}; Delay_R1 = {}; Th0_R1 = {};".
                      format(par_mejor[0], par_mejor[1], (par_mejor[2] % pi) /
                             degrees, (par_mejor[3] % pi) / degrees))
                print("Error del paso 11a: {}.".format(errores[indIt, 5]))
                #             print("Th0_error = {}; Th0E_az = {}; Th0E_az = {};".format(
                #                 th_error/degrees, th_error1/degrees, th_error2/degrees))
                I_fit = model_paso11a(par_mejor, angles2x * degrees,
                                      angles2y * degrees)
                plt.figure(figsize=(20, 5))
                plt.subplot(1, 3, 1)
                plot_2d(
                    angles2x * degrees,
                    angles2y * degrees,
                    Iexperimental,
                    title='Paso11a')
                plt.subplot(1, 3, 2)
                plot_2d(
                    angles2x * degrees, angles2y * degrees, I_fit, title='Fit')
                plt.subplot(1, 3, 3)
                plot_2d(
                    angles2x * degrees,
                    angles2y * degrees,
                    (Iexperimental - I_fit) / Iexperimental.max(),
                    title='Normalized residuals')
            # Arreglar parametros anteriores que el nuevo ajuste pide cambiar
            if params["fix_parameters"]:
                th0p1 = th0p1 + th_error1
                th0p2 = th0p2 + th_error2
                th0r2 = th0r2 + th_error3
    #             Ifuente = ps.Stokes_azimuth_ellipticity(
    #                 az=Saz+th_error1, el=Sel+th_error2, intensity=S0, pol_degree=Spol)

    # Experimento de comprobacion (Paso11b)
        if params["hacer_paso_11"]:
            # No fits, so calculate directly
            Iexperimental = Intensity_Paso11b[:, :,
                                              0] * Intensity_Paso11b[:, :,
                                                                     1] / mean[
                                                                         1]
            I_fit = model_paso11b(angles2x * degrees, angles2y * degrees)
            # Calculo de errores
            error_Paso = I_fit - Iexperimental
            error_Paso = np.linalg.norm(error_Paso) / (
                Iexperimental.max() * Nelements2D)
            errores[indIt, 6] = error_Paso
            # Calculo matriz de mueller
            th0 = [th0p1, th0r1, th0r2, th0p2 + pi / 2]
            angles_Mueller = dist_angulos(angles2x * degrees,
                                          angles2y * degrees, th0)
            I_mueller = Iexperimental.flatten()
            Mme = [Mp1, Mr1, Mr2, Mp2]
            Mcalculated = opsys.polarimeter_experiment(Mme, Ifuente, I_mueller,
                                                       angles_Mueller)
            Mfiltered = amm.filter_reality_conditions(
                Mcalculated, tol=0.001, verbose=False)
            Mtarget = np.eye(4)
            error_mat = Mfiltered - Mtarget
            error_mat = np.linalg.norm(error_mat) / error_mat.size
            errores[indIt, 7] = mejor_error

            # Plots
            if params["PlotCadaPaso"] or indIt + 1 == params["NmaxIteraciones"]:
                print("Error del paso 11b: {}.".format(errores[indIt, 6]))
                print("La matriz del vacio es:")
                print(Mfiltered)
                print("Error matriz del vacio: {}.".format(error_mat))
                plt.figure(figsize=(20, 5))
                plt.subplot(1, 3, 1)
                plot_2d(
                    angles2x * degrees,
                    angles2y * degrees,
                    Iexperimental,
                    title='Paso11b')
                plt.subplot(1, 3, 2)
                plot_2d(
                    angles2x * degrees, angles2y * degrees, I_fit, title='Fit')
                plt.subplot(1, 3, 3)
                plot_2d(
                    angles2x * degrees,
                    angles2y * degrees,
                    (Iexperimental - I_fit) / Iexperimental.max(),
                    title='Normalized residuals')

        # Este es el determinante, asi que se representa en cada iteracion
        print("Error del ultimo paso: {}.".format(mejor_error))
        # No volver a correr los bucles while mas de una vez
        params["N_it_while"] = 1

        ## Ver si el error es suficientemente bajo
        iteracion_final = indIt
        if mejor_error < params["tolerancia"]:
            break

    end_time = time.time()
    print('Elapsed time is {} s.'.format(end_time - start_time))

    ## Save data
    polarimetro = {}
    polarimetro["p11"] = np.abs(pes[-1, 0])
    polarimetro["p12"] = np.abs(pes[-1, 1])
    polarimetro["Dp1"] = delays[-1, 1]
    polarimetro["p21"] = np.abs(pes[-1, 2])
    polarimetro["p22"] = np.abs(pes[-1, 3])
    polarimetro["p31"] = np.abs(pes[-1, 4])
    polarimetro["p32"] = np.abs(pes[-1, 5])
    polarimetro["th0p1"] = th0p1
    polarimetro["th0p2"] = th0p2

    polarimetro["R1p1"] = np.abs(pes[-1, 6])
    polarimetro["R1p2"] = np.abs(pes[-1, 7])
    polarimetro["R2p1"] = np.abs(pes[-1, 10])
    polarimetro["R2p2"] = np.abs(pes[-1, 11])
    polarimetro["Dr1"] = delays[-1, 2]
    polarimetro["Dr2"] = delays[-1, 0]
    polarimetro["th0r1"] = th0r1
    polarimetro["th0r2"] = th0r2

    polarimetro["normal"] = mean[1]

    polarimetro["Ifuente"] = Ifuente
    polarimetro["Mp1"] = Mp1
    polarimetro["Mp2"] = Mp2
    polarimetro["Mr1"] = Mr1
    polarimetro["Mr2"] = Mr2

    polarimetro["seed"] = seed

    # Save data
    if params["save_data"]:
        filename = "Calibracion_polarimetro_{}".format(fecha)
        np.savez(filename + '.npz', polarimetro=polarimetro)
        print('Data saved succesfully')

    ## Analize data
    # Plot errores
    plt.figure(figsize=(16, 8))
    plt.subplot(2, 2, 1)
    plt.plot(errores[0:indIt + 1, 0], 'k')
    plt.plot(errores[0:indIt + 1, 1], 'b')
    plt.plot(errores[0:indIt + 1, 2], 'r')
    plt.plot(errores[0:indIt + 1, 3], 'g')
    plt.plot(errores[0:indIt + 1, 4], 'y')
    plt.plot(errores[0:indIt + 1, 5], 'm')
    plt.plot(errores[0:indIt + 1, 6], 'c')
    plt.plot(errores[0:indIt + 1, 7], 'c--')
    plt.xlabel('Iteracion')
    plt.ylabel('Error relativo')
    plt.title('Evolucion del error')
    plt.legend(('Th0 pol.', 'Th0 ret.', 'Cal. illum', 'Cal. pols.', 'Cal. R2',
                'Cal. R1', 'Check', 'Mueller'))

    # Plot pes
    plt.figure(figsize=(16, 8))
    plt.subplot(2, 2, 1)
    plt.plot(pes[0:indIt + 1, 0], 'k')
    plt.plot(pes[0:indIt + 1, 2], 'b')
    plt.plot(pes[0:indIt + 1, 4], 'r')
    plt.plot(pes[0:indIt + 1, 6], 'g')
    plt.plot(pes[0:indIt + 1, 7], 'g--')
    plt.plot(pes[0:indIt + 1, 8], 'y')
    plt.plot(np.abs(pes[0:indIt + 1, 10]), 'm')
    plt.plot(np.abs(pes[0:indIt + 1, 11]), 'm--')
    plt.xlabel('Iteracion')
    plt.ylabel('p1 parameter')
    plt.title('Evolucion de los parametros p1')
    plt.legend(('p1 Pol 1', 'p1 Pol 2', 'p1 Pol 3', 'p1 Ret 1', 'p2 Ret 1',
                'p1 Pol con.', 'p1 R1', 'p2 R1'))

    plt.subplot(2, 2, 2)
    plt.plot(pes[0:indIt + 1, 1], 'k')
    plt.plot(pes[0:indIt + 1, 3], 'b')
    plt.plot(pes[0:indIt + 1, 5], 'r')
    plt.plot(pes[0:indIt + 1, 9], 'y')
    plt.xlabel('Iteracion')
    plt.ylabel('p2 parameter')
    plt.title('Evolucion de los parametros p2')
    plt.legend(('p2 Pol 1', 'p2 Pol 2', 'p2 Pol 3', 'p2 Pol con.'))

    # Illums
    plt.figure(figsize=(24, 8))
    plt.subplot(2, 3, 1)
    plt.plot(ilums[0:indIt + 1, 0], 'k')
    plt.xlabel('Iteracion')
    plt.ylabel('Intensity (V)')
    plt.title('Evolucion de la Intensidad total')

    plt.subplot(2, 3, 2)
    plt.plot(ilums[0:indIt + 1, 1] / degrees, 'b')
    plt.plot(ilums[0:indIt + 1, 2] / degrees, 'r')
    plt.xlabel('Iteracion')
    plt.ylabel('Angle (deg)')
    plt.title('Evolucion de los angulos de iluminacion')
    plt.legend(('Azimuth', 'Elipticidad'))

    plt.subplot(2, 3, 3)
    plt.plot(ilums[0:indIt + 1, 3], 'g')
    plt.xlabel('Iteracion')
    plt.ylabel('Parameter')
    plt.title('Evolucion de los parametros de iluminacion')
    plt.legend(('S_I' 'S_circ'))

    # Delays
    plt.figure(figsize=(16, 8))
    plt.subplot(2, 2, 1)
    plt.plot(delays[0:indIt + 1, 0] / degrees, 'k')
    plt.plot(delays[0:indIt + 1, 2] / degrees, 'b')
    plt.xlabel('Iteracion')
    plt.ylabel('Delay (deg)')
    plt.title('Evolucion del delay del retardador')
    plt.legend(('R2' 'R1'))

    plt.subplot(2, 2, 2)
    plt.plot(delays[0:indIt + 1, 1] / degrees, 'b')
    plt.xlabel('Iteracion')
    plt.ylabel('Delay (deg)')
    plt.title('Evolucion del delay del polarizador')

    # Angles
    plt.figure(figsize=(16, 8))
    plt.subplot(2, 2, 1)
    plt.plot(fangles[0:indIt + 1, 0] / degrees, 'k')
    plt.plot(fangles[0:indIt + 1, 1] / degrees, 'b')
    plt.plot(fangles[0:indIt + 1, 2] / degrees, 'r')
    plt.plot(fangles[0:indIt + 1, 3] / degrees, 'g')
    plt.plot(fangles[0:indIt + 1, 6] / degrees, 'm')
    plt.plot(fangles[0:indIt + 1, 4] / degrees, 'k--')
    plt.plot(fangles[0:indIt + 1, 5] / degrees, 'g--')
    plt.xlabel('Iteracion')
    plt.ylabel('Origin angle')
    plt.title('Evolucion del angulo inicial')
    plt.legend(('P1', 'P2', 'P3', 'R2', 'R1', 'P1b', 'R2b'))

    # Errangles
    plt.figure(figsize=(16, 8))
    plt.subplot(2, 2, 1)
    plt.plot(errangles[0:indIt + 1, 0] / degrees, 'k')
    plt.plot(errangles[0:indIt + 1, 2] / degrees, 'b')
    plt.plot(errangles[0:indIt + 1, 3] / degrees, 'r')
    plt.plot(errangles[0:indIt + 1, 5] / degrees, 'g')
    plt.plot(errangles[0:indIt + 1, 6] / degrees, 'y')
    plt.plot(errangles[0:indIt + 1, 8] / degrees, 'c')
    plt.plot(errangles[0:indIt + 1, 6] / degrees, 'k--')
    plt.plot(errangles[0:indIt + 1, 8] / degrees, 'b--')
    plt.xlabel('Iteracion')
    plt.ylabel('Extra angle')
    plt.title('Evolucion del angulo extra')
    plt.legend(('Az paso6', 'Th0 R2b paso7', 'Az paso8', 'Th0 P1 paso9',
                'Az paso9', 'Th0 R2', 'Th0 P1 Paso11', 'Th0 P2 Paso11'))
    plt.subplot(2, 2, 2)
    plt.plot(errangles[0:indIt + 1, 1] / degrees, 'k')
    plt.plot(errangles[0:indIt + 1, 4] / degrees, 'r')
    plt.plot(errangles[0:indIt + 1, 7] / degrees, 'y')
    plt.xlabel('Iteracion')
    plt.ylabel('Extra angle')
    plt.title('Evolucion de la elipticidad extra')
    plt.legend(('El paso6', 'El paso8', 'El paso9'))

    return polarimetro


def process_calibration_alternative(fecha, params=None, seed=None):
    ## Proceso de ajustes
    # Parametros del bucle
    if params is None:
        params = {}
        params["NmaxIteraciones"] = 20
        params["tolerancia"] = 1e-4
        params["PlotCadaPaso"] = False
        params["ordenarPes"] = True
        params["tol_while"] = 2.5e-4
        params["N_it_while"] = 10
        params["use_random_angles"] = True
        params["fix_parameters"] = True
        params["hacer_paso_11"] = True
        params["save_data"] = True

    if seed is None:
        seed = np.zeros(11)
        use_seed = False
    else:
        use_seed = True

    ## Funciones de ajuste
    # Funciones
    def model_paso5(par, th1):
        # Ordenar o no los pes del polarizador
        if params["ordenarPes"]:
            p1_c = max(np.abs(par[0]), np.abs(par[1]))
            p2_c = min(np.abs(par[0]), np.abs(par[1]))
            Mpmalo = ps.polarizer_linear(p1=p1_c, p2=p2_c)
        else:
            Mpmalo = ps.polarizer_linear(p1=par[0], p2=par[1])
        # Calcular
        M = [Mpmalo, Mp1]
        th = [0, th1 + par[2]]
        Iteor = Intensity_Rotating_Elements(M, th, Ei=Ifuente)
        return Iteor

    def err_paso5(par, th1, Ireal):
        Iteor = model_paso5(par, th1)
        diferencia = Iteor - Ireal
        return diferencia

    def model_paso6(par, th1):
        # Create Jones matrices
        M = [Mr2, Mp1]
        # Rotar la fuente para evitar errores
        th_error1 = errAmpIt * erf(par[1])
        th_error2 = el_errAmpIt * erf(par[2])
        Ipaso6 = ps.Stokes_azimuth_ellipticity(
            az=Saz + th_error1,
            el=Sel + th_error2,
            intensity=S0,
            pol_degree=Spol)
        # Calcular
        th = [th1 + par[0], +th0p1b]
        Iteor = Intensity_Rotating_Elements(M, th, Ei=Ipaso6)
        return Iteor

    def err_paso6(par, th1, Ireal):
        Iteor = model_paso6(par, th1)
        diferencia = Iteor - Ireal
        return diferencia

    def model_paso7(par, th1, th2):
        # Iluminacion y angulos
        Itest = ps.Stokes_azimuth_ellipticity(
            az=par[1], el=par[2], intensity=par[0], pol_degree=par[3])
        # Calcular
        th_error = errAmpIt * erf(par[4])
        th = [th1 + th0r2b + th_error, th2 + th0p1b]
        M = [Mr2, Mp1]
        I = Intensity_Rotating_Elements(M, th, Itest, False)
        return I

    def err_paso7(par, th1, th2, Ireal):
        dI = model_paso7(par, th1, th2) - Ireal
        return dI.flatten()

    def model_paso8(par, th1a, th1b, th1c):
        # Ordenar o no los pes del polarizador
        if params["ordenarPes"]:
            p1_1 = max(np.abs(par[0]), np.abs(par[1]))
            p2_1 = min(np.abs(par[0]), np.abs(par[1]))
            p1_2 = max(np.abs(par[2]), np.abs(par[3]))
            p2_2 = min(np.abs(par[2]), np.abs(par[3]))
            p1_3 = max(np.abs(par[4]), np.abs(par[5]))
            p2_3 = min(np.abs(par[4]), np.abs(par[5]))
            Mp1 = ps.polarizer_linear(p1=p1_1, p2=p2_1)
            Mp2 = ps.polarizer_linear(p1=p1_2, p2=p2_2)
            Mp3 = ps.polarizer_linear(p1=p1_3, p2=p2_3)
        else:
            Mp1 = ps.polarizer_linear(p1=par[0], p2=par[1])
            Mp2 = ps.polarizer_linear(p1=par[2], p2=par[3])
            Mp3 = ps.polarizer_linear(p1=par[4], p2=par[5])
        # Rotar la fuente para evitar errores
        th_error1 = errAmpIt * erf(par[9])
        th_error2 = el_errAmpIt * erf(par[10])
        Ipaso8 = ps.Stokes_azimuth_ellipticity(
            az=Saz + th_error1,
            el=Sel + th_error2,
            intensity=S0,
            pol_degree=Spol)
        # First, P3 and P1
        M = [Mp3, Mp1]
        th = [th1a + par[6], +th0p1b]
        Ia = Intensity_Rotating_Elements(M, th, Ei=Ipaso8)
        # Then, P3 and P2
        M = [Mp3, Mp2]
        th = [+par[6], th1b + par[7]]
        Ib = Intensity_Rotating_Elements(M, th, Ei=Ipaso8)
        # Last, P1 and P2
        M = [Mp1, Mp2]
        th = [th1c + par[8], +par[7]]
        Ic = Intensity_Rotating_Elements(M, th, Ei=Ipaso8)
        # End
        return (Ia, Ib, Ic)

    def err_paso8(par, th1a, th1b, th1c, IRa, IRb, IRc):
        (Ia, Ib, Ic) = model_paso8(par, th1a, th1b, th1c)
        dIa = Ia - IRa
        dIb = Ib - IRb
        dIc = Ic - IRc
        dI = np.concatenate((dIa, dIb, dIc))
        return dI

    def model_paso9(par, th1, th2):
        # Crear matrices
        Mr2 = ps.Mueller_Real_Retarder(par[0], par[1], par[2])
        Mp1 = ps.Mueller_Real_Retarder(p11, p12, par[4])
        # Rotar la fuente para evitar errores
        th_error = errAmpIt * erf(par[5])
        th_error1 = errAmpIt * erf(par[6])
        th_error2 = el_errAmpIt * erf(par[7])
        Ipaso9 = ps.Stokes_azimuth_ellipticity(
            az=Saz + th_error1,
            el=Sel + th_error2,
            intensity=S0,
            pol_degree=Spol)
        # Calcular
        M = [Mp1, Mr2, Mp2]
        th = [th1 + th0p1 + th_error, +par[3], th2 + th0p2]
        I = Intensity_Rotating_Elements(M, th, Ipaso9, False)
        return I

    def err_paso9(par, th1, th2, Ireal):
        dI = model_paso9(par, th1, th2) - Ireal
        return dI.flatten()

    def model_paso11a(par, th1, th2):
        Mr1 = ps.Mueller_Real_Retarder(par[0], par[1], par[2])
        # Rotar la fuente para evitar errores
        th_error1 = errAmpIt * erf(par[4])
        #     th_error3 = errAmpIt * erf(par[7])
        #     th_error4 = errAmpIt * erf(par[8])
        #     Ipaso11a = ps.Stokes_azimuth_ellipticity(
        #         az=Saz+th_error3, el=Sel+th_error4, intensity=S0, pol_degree=Spol)
        # Calcular
        M = [Mp1, Mr1, Mr2, Mp2]
        th = [par[5], th1 + par[3], th2 + th0r2 + th_error1, par[6]]
        I = Intensity_Rotating_Elements(M, th, Ifuente)
        return I

    def err_paso11a(par, th1, th2, Ireal):
        dI = model_paso11a(par, th1, th2) - Ireal
        return dI.flatten()

    def model_paso11b(th1, th2):
        # Calcular
        M = [Mp1, Mr1, Mr2, Mp2]
        th = [th0p1c, th1 + th0r1, th2 + th0r2, th0p2c + pi / 2]
        I = Intensity_Rotating_Elements(M, th, Ifuente)
        return I

    ## Carga de datos
    # Load file
    filename = 'Paso_4_' + fecha + '.npz'
    data = np.load(filename)
    # Rename variables
    Iindividual = data['Iindividual']
    Naverage = data['Naverage']
    # Make stadistics
    ratio_individual = Iindividual[:, 0] / Iindividual[:, 1]
    mean = np.mean(Iindividual, axis=0)
    mean_phd2 = np.mean(Iindividual[:, 1])
    # Load file
    filename = 'Paso_5_' + fecha + '.npz'
    data = np.load(filename)
    # Rename variables
    angle1 = data['angle1']
    Intensity_Paso5 = data['intensity1']
    # Load file
    filename = 'Paso_6_' + fecha + '.npz'
    data = np.load(filename)
    # Rename variables
    Intensity_Paso6 = data['intensity1']
    # Load file
    filename = 'Paso_7_' + fecha + '.npz'
    data = np.load(filename)
    # Rename variables
    angles2x = data['angle2x']
    angles2y = data['angle2y']
    Intensity_Paso7 = data['intensity2']
    # Load file
    filename = 'Paso_8a_' + fecha + '.npz'
    data = np.load(filename)
    # Rename variables
    Intensity_Paso8a = data['intensity1']
    # Load file
    filename = 'Paso_8b_' + fecha + '.npz'
    data = np.load(filename)
    # Rename variables
    Intensity_Paso8b = data['intensity1']
    # Load file
    filename = 'Paso_8c_' + fecha + '.npz'
    data = np.load(filename)
    # Rename variables
    Intensity_Paso8c = data['intensity1']
    # Load file
    filename = 'Paso_9_' + fecha + '.npz'
    data = np.load(filename)
    # Rename variables
    Intensity_Paso9 = data['intensity2']
    # Load file
    filename = 'Paso_11a_' + fecha + '.npz'
    data = np.load(filename)
    # Rename variables
    Intensity_Paso11a = data['intensity2']
    # Load file
    filename = 'Paso_11b_' + fecha + '.npz'
    data = np.load(filename)
    # Rename variables
    Intensity_Paso11b = data['intensity2']
    print('Carga de datos finalizada')

    # Parametros iniciales
    (p11, p12, Dp1, p21, p22, p31, p32, pc1,
     pc2) = (0.95, 0.15, 0 * degrees, 0.95, 0.15, 0.95, 0.15, 0.95, 0.3)
    (R2p1, R2p2, Dr2) = (0.99, 0.99, 85 * degrees)
    Mp1 = ps.polarizer_linear(p1=p11, p2=p12)
    Mr2 = ps.Mueller_Real_Retarder(R2p1, R2p2, Dr2)
    (S0, Saz, Sel, Spol) = (mean[0], 0, 44 * degrees, 1)
    Ifuente = ps.Stokes_azimuth_ellipticity(
        az=Saz, el=Sel, intensity=S0, pol_degree=Spol)
    (th0p1b, th0p1, th0p2, th0p3, th0r2, th0r2b, th0p1c, th0p2c) = (0, 0, 0, 0,
                                                                    0, 0, 0, 0)
    (R1p1, R1p2, Dr1, th0r1) = (0.99, 0.99, 85 * degrees, 0)
    # Parametros asociados a params["tolerancia"] de errores
    (th0E0, errAmp, el_errAmp, exp_crec, max_change,
     el_max_change) = (0, 90 * degrees, 3 * degrees, 1, 20 * degrees,
                       1 * degrees)
    (errAmpIt, el_errAmpIt) = (errAmp, el_errAmp)
    # Inicializar donde guardo los datos de cada iteracion
    errores = np.zeros([params["NmaxIteraciones"], 8])
    pes = np.zeros([params["NmaxIteraciones"], 12])
    ilums = np.zeros([params["NmaxIteraciones"], 4])
    delays = np.zeros([params["NmaxIteraciones"], 3])
    fangles = np.zeros([params["NmaxIteraciones"], 9])
    errangles = np.zeros([params["NmaxIteraciones"], 9])
    Nelements1D = angle1.size
    Nelements2D = angles2x.size * angles2y.size

    # Hacer el bucle
    start_time = time.time()
    for indIt in range(params["NmaxIteraciones"]):
        # Actualizar amplitud de error por iteracion
        errAmptest = errAmp / (indIt + 1)**exp_crec
        errAmpIt = max(errAmptest, errAmpIt - max_change)
        el_errAmptest = el_errAmp / (indIt + 1)**exp_crec
        el_errAmpIt = max(el_errAmptest, el_errAmpIt - el_max_change)
        if not params["use_random_angles"]:
            params["NmaxIteraciones"] = 1
        print('Iteracion {}'.format(indIt))
        if params["PlotCadaPaso"]:
            print(
                'Amplitud de error en angulos = {}'.format(errAmpIt / degrees))

        ## Comenzar por ajustar el angulo de P1 en el motor 4 (Paso 5)
        (contador, error_Paso, mejor_error) = (0, 1, 1)
        while contador < params["N_it_while"] and error_Paso > params["tol_while"]:
            # Inicializar angulo en la primera iteracion
            if indIt == 0:
                if use_seed:
                    th0p1b = seed[0]
                elif params["use_random_angles"]:
                    th0p1b = np.random.rand(1) * pi
                    th0p1b = th0p1b[0]
            # Parametros iniciales
            par0 = [pc1, pc2, th0p1b]
            # Normalizacion de intensidades experimentales en funcion de la referencia
            Iexperimental = Intensity_Paso5[:, 0] * Intensity_Paso5[:,
                                                                    1] / mean[
                                                                        1]
            # Ajuste
            par1, success = optimize.leastsq(
                err_paso5, par0, args=(angle1 * degrees, Iexperimental))
            # Calculo de errores
            error_Paso = err_paso5(par1, angle1 * degrees, Iexperimental)
            error_Paso = np.linalg.norm(error_Paso) / (
                Iexperimental.max() * Nelements1D)
            # Guardar mejores resultados
            if error_Paso < mejor_error:
                par_mejor = par1
                mejor_error = error_Paso
                # Save random seed
                if indIt == 0:
                    seed[0] = th0p1b
            # Operaciones final de bucle
            contador = contador + 1
            if params["PlotCadaPaso"] and params["N_it_while"] > 1:
                print('Subiteracion {} da error {}.'.format(
                    contador, error_Paso))
        # Guardar resultados
        pc1 = max(np.abs(par_mejor[0]), np.abs(par_mejor[1]))
        pc2 = min(np.abs(par_mejor[0]), np.abs(par_mejor[1]))
        (pes[indIt, 8], pes[indIt, 9]) = (pc1, pc2)
        fangles[indIt, 4] = th0p1b
        errores[indIt, 0] = mejor_error
        # Extraer resultados para siguientes iteraciones.
        th0p1b = par_mejor[2] % pi
        # if pc1 == par_mejor[0]:
        #     th0p1b = par_mejor[2] % pi
        # else:
        #     th0p1b = (par_mejor[2] + pi / 2) % pi
        # Plots
        if params["PlotCadaPaso"] or indIt + 1 == params["NmaxIteraciones"]:
            print("Parametros del paso 5")
            print("pc1 = {}; pc2 = {}; Th0p1b = {} deg;".format(
                par_mejor[0], par_mejor[1], (par_mejor[2] % pi) / degrees))
            print("Error del paso 5: {}.".format(errores[indIt, 0]))
            I_fit = model_paso5(par_mejor, angle1 * degrees)
            plot_experiment_residuals_1D(
                angle1 * degrees, Iexperimental, I_fit, title='Paso5')

        ## Ahora el del retardador 2 la primera vez que lo metimos (Paso 6)
        (contador, error_Paso, mejor_error) = (0, 1, 1)
        while contador < params["N_it_while"] and error_Paso > params["tol_while"]:
            # Inicializar angulo en la primera iteracion
            if indIt == 0:
                if use_seed:
                    th0r2b = seed[1]
                elif params["use_random_angles"]:
                    th0r2b = np.random.rand(1) * pi
                    th0r2b = th0r2b[0]
    #             Saz = np.rando+.random.rand(1))*degrees
    # Parametros iniciales
            par0 = [th0r2b, th0E0, th0E0]
            # Normalizacion de intensidades experimentales en funcion de la referencia
            Iexperimental = Intensity_Paso6[:, 0] * Intensity_Paso6[:,
                                                                    1] / mean[
                                                                        1]
            # Ajuste
            par1, success = optimize.leastsq(
                err_paso6, par0, args=(angle1 * degrees, Iexperimental))
            # Calculo de errores
            error_Paso = err_paso6(par1, angle1 * degrees, Iexperimental)
            error_Paso = np.linalg.norm(error_Paso) / (
                Iexperimental.max() * Nelements1D)
            # Guardar mejores resultados
            if error_Paso < mejor_error:
                par_mejor = par1
                mejor_error = error_Paso
                Saz_mejor = Saz
                Sel_mejor = Sel
                # Save random seed
                if indIt == 0:
                    seed[1] = th0r2b
            # Operaciones final de bucle
            contador = contador + 1
            if params["PlotCadaPaso"] and params["N_it_while"] > 1:
                print('Subiteracion {} da error {}.'.format(
                    contador, error_Paso))
        # Extraer resultados para siguientes iteraciones
        th0r2b = par_mejor[0] % pi
        th_error1 = errAmpIt * erf(par_mejor[1])
        th_error2 = el_errAmpIt * erf(par_mejor[2])
        # Guardar resultados
        fangles[indIt, 5] = th0r2b
        errangles[indIt, 0:2] = (th_error1, th_error2)
        errores[indIt, 1] = mejor_error
        # Plots
        if params["PlotCadaPaso"] or indIt + 1 == params["NmaxIteraciones"]:
            print("Parametros del paso 6")
            print("Th0r2b = {} deg; Th0E_az = {} deg; Th0E_el = {} deg;".
                  format((par_mejor[0] % pi) / degrees, th_error1 / degrees,
                         th_error2 / degrees))
            print("Error del paso 6: {}.".format(errores[indIt, 1]))
            I_fit = model_paso6(par_mejor, angle1 * degrees)
            plot_experiment_residuals_1D(
                angle1 * degrees, Iexperimental, I_fit, title='Paso6')
        # Arreglar parametros anteriores que el nuevo ajuste pide cambiar
        if params["fix_parameters"]:
            Saz = Saz_mejor + th_error1
            Sel = Sel_mejor + th_error2
            Ipaso6 = ps.Stokes_azimuth_ellipticity(
                az=Saz, el=Sel, intensity=S0, pol_degree=Spol)

        ## Vamos a ajustar la iluminacion (Paso 7)
        (contador, error_Paso, mejor_error) = (0, 1, 1)
        while contador < params["N_it_while"] and error_Paso > params["tol_while"]:
            # Inicializar angulo en la primera iteracion
            if indIt == 0:
                if use_seed:
                    (Saz, Sel) = seed[9:10]
                elif params["use_random_angles"]:
                    Saz = np.random.rand(1) * pi
                    Saz = Saz[0]
                    Sel = (41 + 2 * np.random.rand(1)) * degrees
            # Parametros iniciales
            par0 = [S0, Saz, Sel, Spol, th0E0]
            # Normalizacion de intensidades experimentales en funcion de la referencia
            Iexperimental = Intensity_Paso7[:, :,
                                            0] * Intensity_Paso7[:, :,
                                                                 1] / mean[1]
            #Iexperimental = np.transpose(Iexperimental)
            # Ajuste
            par1, success = optimize.leastsq(
                err_paso7,
                par0,
                args=(angles2x * degrees, angles2y * degrees, Iexperimental))
            # Calculo de errores
            error_Paso = err_paso7(par1, angles2x * degrees,
                                   angles2y * degrees, Iexperimental)
            error_Paso = np.linalg.norm(error_Paso) / (
                Iexperimental.max() * Nelements2D)
            # Guardar mejores resultados
            if error_Paso < mejor_error:
                par_mejor = par1
                mejor_error = error_Paso
                # Save random seed
                if indIt == 0:
                    seed[9] = Sel
            # Operaciones final de bucle
            contador = contador + 1
            if params["PlotCadaPaso"] and params["N_it_while"] > 1:
                print('Subiteracion {} da error {}.'.format(
                    contador, error_Paso))
        # Extraer resultados para siguientes iteraciones
        (S0, Saz, Sel, Spol) = par_mejor[0:4]
        Ifuente = ps.Stokes_azimuth_ellipticity(
            az=Saz, el=Sel, intensity=S0, pol_degree=Spol)
        th_error = errAmpIt * erf(par_mejor[4])
        # Guardar resultados
        ilums[indIt, 0:4] = (S0, Saz, Sel, Spol)
        errangles[indIt, 2] = th_error
        errores[indIt, 2] = mejor_error
        # Plots
        if params["PlotCadaPaso"] or indIt + 1 == params["NmaxIteraciones"]:
            print("Parametros del paso 7")
            print(
                "S0 = {} V; Azimuth = {} deg; Ellipticity = {} deg; Pol. degree = {}; Th0_error = {} deg;".
                format(par_mejor[0], par_mejor[1] / degrees, par_mejor[2] /
                       degrees, par_mejor[3], th_error / degrees))
            print("Error del paso 7: {}.".format(errores[indIt, 2]))
            I_fit = model_paso7(par_mejor, angles2x * degrees,
                                angles2y * degrees)
            plt.figure(figsize=(20, 5))
            plt.subplot(1, 3, 1)
            plot_2d(
                angles2x * degrees,
                angles2y * degrees,
                Iexperimental,
                title='Paso7')
            plt.subplot(1, 3, 2)
            plot_2d(angles2x * degrees, angles2y * degrees, I_fit, title='Fit')
            plt.subplot(1, 3, 3)
            plot_2d(
                angles2x * degrees,
                angles2y * degrees,
                (Iexperimental - I_fit) / Iexperimental.max(),
                title='Normalized residuals')
        # Arreglar parametros anteriores que el nuevo ajuste pide cambiar
        if params["fix_parameters"]:
            th0p1b = th0p1b + th_error

        ## Ahora vamos a por los polarizadores (Paso 8)
        (contador, error_Paso, mejor_error) = (0, 1, 1)
        while contador < params["N_it_while"] and error_Paso > params["tol_while"]:
            # Inicializar angulo en la primera iteracion
            if indIt == 0:
                if use_seed:
                    (th0p1, th0p2, th0p3) = seed[2:5]
                elif params["use_random_angles"]:
                    (th0p1, th0p2, th0p3) = np.random.rand(3) * pi
            # Parametros iniciales
            par0 = [
                p11, p12, p21, p22, p31, p32, th0p3, th0p2, th0p1, th0E0, th0E0
            ]
            # Normalizacion de intensidades experimentales en funcion de la referencia
            IexpA = Intensity_Paso8a[:, 0] * Intensity_Paso8a[:, 1] / mean[1]
            IexpB = Intensity_Paso8b[:, 0] * Intensity_Paso8b[:, 1] / mean[1]
            IexpC = Intensity_Paso8c[:, 0] * Intensity_Paso8c[:, 1] / mean[1]
            maxval = np.array([IexpA.max(), IexpB.max(), IexpC.max()]).max()
            # Ajuste
            par1, success = optimize.leastsq(
                err_paso8,
                par0,
                args=(angle1 * degrees, angle1 * degrees, angle1 * degrees,
                      IexpA, IexpB, IexpC))
            # Calculo de errores
            error_Paso = err_paso8(par1, angle1 * degrees, angle1 * degrees,
                                   angle1 * degrees, IexpA, IexpB, IexpC)
            error_Paso = np.linalg.norm(error_Paso) / (
                maxval * 3 * Nelements1D)
            # Guardar mejores resultados
            if error_Paso < mejor_error:
                par_mejor = par1
                mejor_error = error_Paso
                # Save random seed
                if indIt == 0:
                    seed[2:5] = (th0p1, th0p2, th0p3)
            # Operaciones final de bucle
            contador = contador + 1
            if params["PlotCadaPaso"] and params["N_it_while"] > 1:
                print('Subiteracion {} da error {}.'.format(
                    contador, error_Paso))
        # Extraer resultados para siguientes iteraciones. Hay que tener en cuenta que
        # el valor del angulo debe cambiar en 90 deg si el parametro correspondiente a p1
        # es menor que el de p2
        p11 = max(np.abs(par_mejor[0]), np.abs(par_mejor[1]))
        p12 = min(np.abs(par_mejor[0]), np.abs(par_mejor[1]))
        p21 = max(np.abs(par_mejor[2]), np.abs(par_mejor[3]))
        p22 = min(np.abs(par_mejor[2]), np.abs(par_mejor[3]))
        p31 = max(np.abs(par_mejor[4]), np.abs(par_mejor[5]))
        p32 = min(np.abs(par_mejor[4]), np.abs(par_mejor[5]))
        th0p1 = par_mejor[8] % pi
        th0p2 = par_mejor[7] % pi
        th0p3 = par_mejor[6] % pi
        # if p11 == par_mejor[0]:
        #     th0p1 = par_mejor[8] % pi
        # else:
        #     th0p1 = (par_mejor[8] + pi / 2) % pi
        # if p21 == par_mejor[2]:
        #     th0p2 = par_mejor[7] % pi
        # else:
        #     th0p2 = (par_mejor[7] + pi / 2) % pi
        # if p31 == par_mejor[4]:
        #     th0p3 = par_mejor[6] % pi
        # else:
        #     th0p3 = (par_mejor[6] + pi / 2) % pi
        Mp2 = ps.polarizer_linear(p1=p21, p2=p22)
        th_error1 = errAmpIt * erf(par_mejor[9])
        th_error2 = el_errAmpIt * erf(par_mejor[10])
        # Guardar resultados
        pes[indIt, 0:6] = (p11, p12, p21, p22, p31, p32)
        fangles[indIt, 0:3] = (th0p1, th0p2, th0p3)
        errangles[indIt, 3:5] = (th_error1, th_error2)
        errores[indIt, 3] = mejor_error
        # Plots
        if params["PlotCadaPaso"] or indIt + 1 == params["NmaxIteraciones"]:
            print("Parametros del paso 8")
            print(
                "p11 = {}; p12 = {}; p21 = {}; p22 = {}; p31 = {}; p32 = {};".
                format(par_mejor[0], par_mejor[1], par_mejor[2], par_mejor[3],
                       par_mejor[4], par_mejor[5]))
            print(
                "Th0p1 = {} deg; Th0p2 = {} deg; Th0p3 = {} deg; Th0E_az = {} deg; Th0E_el = {} deg;".
                format((par_mejor[8] % pi) / degrees, (
                    par_mejor[7] % pi) / degrees, (par_mejor[6] % pi) /
                       degrees, th_error1 / degrees, th_error2 / degrees))
            print("Error del paso 8: {}.".format(errores[indIt, 3]))
            I_fit = model_paso8(par_mejor, angle1 * degrees, angle1 * degrees,
                                angle1 * degrees)
            plot_experiment_residuals_1D(
                np.concatenate([
                    angle1 * degrees, angle1 * degrees + 2 * pi,
                    angle1 * degrees + 4 * pi
                ]),
                np.concatenate([IexpA, IexpB, IexpC]),
                np.concatenate((I_fit[0], I_fit[1], I_fit[2])),
                title='Paso8')
        # Arreglar parametros anteriores que el nuevo ajuste pide cambiar
        if params["fix_parameters"]:
            Ifuente = ps.Stokes_azimuth_ellipticity(
                az=Saz + th_error1,
                el=Sel + th_error2,
                intensity=S0,
                pol_degree=Spol)

        ## Por ultimo, ajustamos el retardador (Paso 9)
        (contador, error_Paso, mejor_error) = (0, 1, 1)
        while contador < params["N_it_while"] and error_Paso > params["tol_while"]:
            # Inicializar angulo en la primera iteracion
            if indIt == 0:
                if use_seed:
                    th0r2 = seed[5]
                elif params["use_random_angles"]:
                    th0r2 = np.random.rand(1) * pi
                    th0r2 = th0r2[0]
            # Parametros iniciales
            par0 = [R2p1, R2p2, Dr2, th0r2, Dp1, th0E0, th0E0, th0E0]
            # Normalizacion de intensidades experimentales en funcion de la referencia
            Iexperimental = Intensity_Paso9[:, :,
                                            0] * Intensity_Paso9[:, :,
                                                                 1] / mean[1]
            # Ajuste
            par1, success = optimize.leastsq(
                err_paso9,
                par0,
                args=(angles2x * degrees, angles2y * degrees, Iexperimental))
            # Calculo de errores
            error_Paso = err_paso9(par1, angles2x * degrees,
                                   angles2y * degrees, Iexperimental)
            error_Paso = np.linalg.norm(error_Paso) / (
                Iexperimental.max() * Nelements2D)
            # Guardar mejores resultados
            if error_Paso < mejor_error:
                par_mejor = par1
                mejor_error = error_Paso
                # Save random seed
                if indIt == 0:
                    seed[5] = th0r2
            # Operaciones final de bucle
            contador = contador + 1
            if params["PlotCadaPaso"] and params["N_it_while"] > 1:
                print('Subiteracion {} da error {}.'.format(
                    contador, error_Paso))
        # Extraer resultados para siguientes iteraciones
        (R2p1, R2p2, Dr2, th0r2,
         Dp1) = (par_mejor[0], par_mejor[1], par_mejor[2] % pi,
                 par_mejor[3] % pi, par_mejor[4] % pi)
        Mr2 = ps.Mueller_Real_Retarder(R2p1, R2p2, Dr2)
        Mp1 = ps.Mueller_Real_Retarder(p11, p12, Dp1)
        th_error = errAmpIt * erf(par_mejor[5])
        th_error1 = errAmpIt * erf(par_mejor[6])
        th_error2 = el_errAmpIt * erf(par_mejor[7])
        # Guardar resultados
        pes[indIt, 6:8] = (R2p1, R2p2)
        delays[indIt, 0:2] = (Dr2, Dp1)
        fangles[indIt, 3] = th0r2
        errangles[indIt, 5:8] = (th_error, th_error1, th_error2)
        errores[indIt, 4] = mejor_error
        # Plots
        if params["PlotCadaPaso"] or indIt + 1 == params["NmaxIteraciones"]:
            print("Parametros del paso 9")
            print(
                "R2p1 = {}; R2p2 = {}; Delay_R2 = {}; Th0_R2 = {}; Delay_p1 = {};".
                format(par_mejor[0], par_mejor[1], (par_mejor[2] % pi) /
                       degrees, (par_mejor[3] % pi) / degrees, (
                           par_mejor[4] % pi) / degrees))
            print("Th0_error = {}; Th0E_az = {}; Th0E_az = {};".format(
                th_error / degrees, th_error1 / degrees, th_error2 / degrees))
            print("Error del paso 9: {}.".format(errores[indIt, 4]))
            I_fit = model_paso9(par_mejor, angles2x * degrees,
                                angles2y * degrees)
            plt.figure(figsize=(20, 5))
            plt.subplot(1, 3, 1)
            plot_2d(
                angles2x * degrees,
                angles2y * degrees,
                Iexperimental,
                title='Paso9')
            plt.subplot(1, 3, 2)
            plot_2d(angles2x * degrees, angles2y * degrees, I_fit, title='Fit')
            plt.subplot(1, 3, 3)
            plot_2d(
                angles2x * degrees,
                angles2y * degrees,
                (Iexperimental - I_fit) / Iexperimental.max(),
                title='Normalized residuals')
        # Arreglar parametros anteriores que el nuevo ajuste pide cambiar
        if params["fix_parameters"]:
            th0p1 = th0p1 + th_error
            Ifuente = ps.Stokes_azimuth_ellipticity(
                az=Saz + th_error1,
                el=Sel + th_error2,
                intensity=S0,
                pol_degree=Spol)

        ## Bonus, ajustar el primer retardador (Paso 11a)
        if params["hacer_paso_11"]:
            (contador, error_Paso, mejor_error) = (0, 1, 1)
            while contador < params["N_it_while"] and error_Paso > params["tol_while"]:
                # Inicializar angulo en la primera iteracion
                if indIt == 0:
                    if use_seed:
                        (th0r1, th0p1c, th0p2c) = seed[6:9]
                    elif params["use_random_angles"]:
                        (th0r1, th0p1c, th0p2c) = np.random.rand(3) * pi
                par0 = [R1p1, R1p2, Dr1, th0r1, th0E0, th0p1c, th0p2c]
                # Normalizacion de intensidades experimentales en funcion de la referencia
                Iexperimental = Intensity_Paso11a[:, :,
                                                  0] * Intensity_Paso11a[:, :,
                                                                         1] / mean[
                                                                             1]
                # Ajuste
                par1, success = optimize.leastsq(
                    err_paso11a,
                    par0,
                    args=(angles2x * degrees, angles2y * degrees,
                          Iexperimental))
                # Calculo de errores
                error_Paso = err_paso11a(par1, angles2x * degrees,
                                         angles2y * degrees, Iexperimental)
                error_Paso = np.linalg.norm(error_Paso) / (
                    Iexperimental.max() * Nelements2D)
                # Guardar mejores resultados
                if error_Paso < mejor_error:
                    par_mejor = par1
                    mejor_error = error_Paso
                    # Save random seed
                    if indIt == 0:
                        seed[6:9] = (th0r1, th0p1c, th0p2c)
                # Operaciones final de bucle
                contador = contador + 1
                if params["PlotCadaPaso"] and params["N_it_while"] > 1:
                    print('Subiteracion {} da error {}.'.format(
                        contador, error_Paso))
            # Extraer resultados para siguientes iteraciones
            (R1p1, R1p2) = par_mejor[0:2]
            (Dr1, th0r1) = par_mejor[2:4] % pi
            (th0p1c, th0p2c) = par_mejor[5:7]
            Mr1 = ps.Mueller_Real_Retarder(R1p1, R1p2, Dr1)
            th_error1 = errAmpIt * erf(par_mejor[4])
            #         th_error = errAmpIt * erf(par_mejor[5])
            #         th_error1 = errAmpIt * erf(par_mejor[6])
            #         th_error2 = el_errAmpIt * erf(par_mejor[7])
            # Guardar resultados
            pes[indIt, 10:12] = (R1p1, R1p2)
            delays[indIt, 2] = Dr1
            fangles[indIt, 6] = (th0r1)
            errangles[indIt, 8] = th_error1
            #         errangles[indIt, 5:8] = (th_error,th_error1, th_error2)
            errores[indIt, 5] = mejor_error
            # Plots
            if params["PlotCadaPaso"] or indIt + 1 == params["NmaxIteraciones"]:
                print("Parametros del paso 11a")
                print("R1p1 = {}; R1p2 = {}; Delay_R1 = {}; Th0_R1 = {};".
                      format(par_mejor[0], par_mejor[1], (par_mejor[2] % pi) /
                             degrees, (par_mejor[3] % pi) / degrees))
                print("Error del paso 11a: {}.".format(errores[indIt, 5]))
                #             print("Th0_error = {}; Th0E_az = {}; Th0E_az = {};".format(
                #                 th_error/degrees, th_error1/degrees, th_error2/degrees))
                I_fit = model_paso11a(par_mejor, angles2x * degrees,
                                      angles2y * degrees)
                plt.figure(figsize=(20, 5))
                plt.subplot(1, 3, 1)
                plot_2d(
                    angles2x * degrees,
                    angles2y * degrees,
                    Iexperimental,
                    title='Paso11a')
                plt.subplot(1, 3, 2)
                plot_2d(
                    angles2x * degrees, angles2y * degrees, I_fit, title='Fit')
                plt.subplot(1, 3, 3)
                plot_2d(
                    angles2x * degrees,
                    angles2y * degrees,
                    (Iexperimental - I_fit) / Iexperimental.max(),
                    title='Normalized residuals')
            # Arreglar parametros anteriores que el nuevo ajuste pide cambiar
            if params["fix_parameters"]:
                th0r2 = th0r2 + th_error1
    #             Ifuente = ps.Stokes_azimuth_ellipticity(
    #                 az=Saz+th_error1, el=Sel+th_error2, intensity=S0, pol_degree=Spol)

    # Experimento de comprobacion (Paso11b)
        if params["hacer_paso_11"]:
            # No fits, so calculate directly
            Iexperimental = Intensity_Paso11b[:, :,
                                              0] * Intensity_Paso11b[:, :,
                                                                     1] / mean[
                                                                         1]
            I_fit = model_paso11b(angles2x * degrees, angles2y * degrees)
            # Calculo de errores
            error_Paso = I_fit - Iexperimental
            error_Paso = np.linalg.norm(error_Paso) / (
                Iexperimental.max() * Nelements2D)
            errores[indIt, 6] = error_Paso
            # Calculo matriz de mueller
            th0 = [th0p1c, th0r1, th0r2, th0p2c + pi / 2]
            angles_Mueller = dist_angulos(angles2x * degrees,
                                          angles2y * degrees, th0)
            I_mueller = Iexperimental.flatten()
            Mme = [Mp1, Mr1, Mr2, Mp2]
            Mcalculated = opsys.polarimeter_experiment(Mme, Ifuente, I_mueller,
                                                       angles_Mueller)
            Mfiltered = amm.filter_reality_conditions(
                Mcalculated, tol=0.001, verbose=False)
            Mtarget = np.eye(4)
            error_mat1 = Mcalculated - Mtarget
            error_mat2 = np.linalg.norm(error_mat1) / error_mat1.size
            error_mat2 = Mfiltered - Mtarget
            error_mat2 = np.linalg.norm(error_mat2) / error_mat2.size
            errores[indIt, 7] = error_mat2

            # Plots
            if params["PlotCadaPaso"] or indIt + 1 == params["NmaxIteraciones"]:
                print("Error del paso 11b: {}.".format(errores[indIt, 6]))
                print("La matriz del vacio sin filtrar es:")
                print(Mcalculated)
                print("Error matriz del vacio: {}.".format(error_mat1))
                print("")
                print("La matriz del vacio filtrada es:")
                print(Mfiltered)
                print("Error matriz del vacio: {}.".format(error_mat2))
                plt.figure(figsize=(20, 5))
                plt.subplot(1, 3, 1)
                plot_2d(
                    angles2x * degrees,
                    angles2y * degrees,
                    Iexperimental,
                    title='Paso11b')
                plt.subplot(1, 3, 2)
                plot_2d(
                    angles2x * degrees, angles2y * degrees, I_fit, title='Fit')
                plt.subplot(1, 3, 3)
                plot_2d(
                    angles2x * degrees,
                    angles2y * degrees,
                    (Iexperimental - I_fit) / Iexperimental.max(),
                    title='Normalized residuals')

        # Este es el determinante, asi que se representa en cada iteracion
        print("Error del ultimo paso: {}.".format(error_mat2))
        # No volver a correr los bucles while mas de una vez
        params["N_it_while"] = 1

        ## Ver si el error es suficientemente bajo
        iteracion_final = indIt
        if error_mat2 < params["tolerancia"]:
            break

    end_time = time.time()
    print('Elapsed time is {} s.'.format(end_time - start_time))

    ## Save data
    polarimetro = {}
    polarimetro["p11"] = np.abs(pes[-1, 0])
    polarimetro["p12"] = np.abs(pes[-1, 1])
    polarimetro["Dp1"] = delays[-1, 1]
    polarimetro["p21"] = np.abs(pes[-1, 2])
    polarimetro["p22"] = np.abs(pes[-1, 3])
    polarimetro["p31"] = np.abs(pes[-1, 4])
    polarimetro["p32"] = np.abs(pes[-1, 5])
    polarimetro["th0p1"] = th0p1
    polarimetro["th0p2"] = th0p2
    polarimetro["th0p1c"] = th0p1c
    polarimetro["th0p2c"] = th0p2c

    polarimetro["R1p1"] = np.abs(pes[-1, 6])
    polarimetro["R1p2"] = np.abs(pes[-1, 7])
    polarimetro["R2p1"] = np.abs(pes[-1, 10])
    polarimetro["R2p2"] = np.abs(pes[-1, 11])
    polarimetro["Dr1"] = delays[-1, 2]
    polarimetro["Dr2"] = delays[-1, 0]
    polarimetro["th0r1"] = th0r1
    polarimetro["th0r2"] = th0r2

    polarimetro["normal"] = mean[1]

    polarimetro["Ifuente"] = Ifuente
    polarimetro["Mp1"] = Mp1
    polarimetro["Mp2"] = Mp2
    polarimetro["Mr1"] = Mr1
    polarimetro["Mr2"] = Mr2

    polarimetro["seed"] = seed

    # Save data
    if params["save_data"]:
        filename = "Calibracion_polarimetro_{}".format(fecha)
        np.savez(filename + '.npz', polarimetro=polarimetro)
        print('Data saved succesfully')

    ## Analize data
    # Plot errores
    plt.figure(figsize=(16, 8))
    plt.subplot(2, 2, 1)
    plt.plot(errores[0:indIt + 1, 0], 'k')
    plt.plot(errores[0:indIt + 1, 1], 'b')
    plt.plot(errores[0:indIt + 1, 2], 'r')
    plt.plot(errores[0:indIt + 1, 3], 'g')
    plt.plot(errores[0:indIt + 1, 4], 'y')
    plt.plot(errores[0:indIt + 1, 5], 'm')
    plt.plot(errores[0:indIt + 1, 6], 'c')
    plt.plot(errores[0:indIt + 1, 7], 'c--')
    plt.xlabel('Iteracion')
    plt.ylabel('Error relativo')
    plt.title('Evolucion del error')
    plt.legend(('Th0 pol.', 'Th0 ret.', 'Cal. illum', 'Cal. pols.', 'Cal. R2',
                'Cal. R1', 'Check', 'Mueller'))

    # Plot pes
    plt.figure(figsize=(16, 8))
    plt.subplot(2, 2, 1)
    plt.plot(pes[0:indIt + 1, 0], 'k')
    plt.plot(pes[0:indIt + 1, 2], 'b')
    plt.plot(pes[0:indIt + 1, 4], 'r')
    plt.plot(pes[0:indIt + 1, 6], 'g')
    plt.plot(pes[0:indIt + 1, 7], 'g--')
    plt.plot(pes[0:indIt + 1, 8], 'y')
    plt.plot(np.abs(pes[0:indIt + 1, 10]), 'm')
    plt.plot(np.abs(pes[0:indIt + 1, 11]), 'm--')
    plt.xlabel('Iteracion')
    plt.ylabel('p1 parameter')
    plt.title('Evolucion de los parametros p1')
    plt.legend(('p1 Pol 1', 'p1 Pol 2', 'p1 Pol 3', 'p1 Ret 1', 'p2 Ret 1',
                'p1 Pol con.', 'p1 R1', 'p2 R1'))

    plt.subplot(2, 2, 2)
    plt.plot(pes[0:indIt + 1, 1], 'k')
    plt.plot(pes[0:indIt + 1, 3], 'b')
    plt.plot(pes[0:indIt + 1, 5], 'r')
    plt.plot(pes[0:indIt + 1, 9], 'y')
    plt.xlabel('Iteracion')
    plt.ylabel('p2 parameter')
    plt.title('Evolucion de los parametros p2')
    plt.legend(('p2 Pol 1', 'p2 Pol 2', 'p2 Pol 3', 'p2 Pol con.'))

    # Illums
    plt.figure(figsize=(24, 8))
    plt.subplot(2, 3, 1)
    plt.plot(ilums[0:indIt + 1, 0], 'k')
    plt.xlabel('Iteracion')
    plt.ylabel('Intensity (V)')
    plt.title('Evolucion de la Intensidad total')

    plt.subplot(2, 3, 2)
    plt.plot(ilums[0:indIt + 1, 1] / degrees, 'b')
    plt.plot(ilums[0:indIt + 1, 2] / degrees, 'r')
    plt.xlabel('Iteracion')
    plt.ylabel('Angle (deg)')
    plt.title('Evolucion de los angulos de iluminacion')
    plt.legend(('Azimuth', 'Elipticidad'))

    plt.subplot(2, 3, 3)
    plt.plot(ilums[0:indIt + 1, 3], 'g')
    plt.xlabel('Iteracion')
    plt.ylabel('Parameter')
    plt.title('Evolucion de los parametros de iluminacion')
    plt.legend(('S_I' 'S_circ'))

    # Delays
    plt.figure(figsize=(16, 8))
    plt.subplot(2, 2, 1)
    plt.plot(delays[0:indIt + 1, 0] / degrees, 'k')
    plt.plot(delays[0:indIt + 1, 2] / degrees, 'b')
    plt.xlabel('Iteracion')
    plt.ylabel('Delay (deg)')
    plt.title('Evolucion del delay del retardador')
    plt.legend(('R2' 'R1'))

    plt.subplot(2, 2, 2)
    plt.plot(delays[0:indIt + 1, 1] / degrees, 'b')
    plt.xlabel('Iteracion')
    plt.ylabel('Delay (deg)')
    plt.title('Evolucion del delay del polarizador')

    # Angles
    plt.figure(figsize=(16, 8))
    plt.subplot(2, 2, 1)
    plt.plot(fangles[0:indIt + 1, 0] / degrees, 'k')
    plt.plot(fangles[0:indIt + 1, 1] / degrees, 'b')
    plt.plot(fangles[0:indIt + 1, 2] / degrees, 'r')
    plt.plot(fangles[0:indIt + 1, 3] / degrees, 'g')
    plt.plot(fangles[0:indIt + 1, 6] / degrees, 'm')
    plt.plot(fangles[0:indIt + 1, 4] / degrees, 'k--')
    plt.plot(fangles[0:indIt + 1, 5] / degrees, 'g--')
    plt.plot(fangles[0:indIt + 1, 6] / degrees, 'k.-')
    plt.plot(fangles[0:indIt + 1, 7] / degrees, 'b.-')
    plt.xlabel('Iteracion')
    plt.ylabel('Origin angle')
    plt.title('Evolucion del angulo inicial')
    plt.legend(('P1', 'P2', 'P3', 'R2', 'R1', 'P1b', 'R2b', 'P1c', 'P2c'))

    # Errangles
    plt.figure(figsize=(16, 8))
    plt.subplot(2, 2, 1)
    plt.plot(errangles[0:indIt + 1, 0] / degrees, 'k')
    plt.plot(errangles[0:indIt + 1, 2] / degrees, 'b')
    plt.plot(errangles[0:indIt + 1, 3] / degrees, 'r')
    plt.plot(errangles[0:indIt + 1, 5] / degrees, 'g')
    plt.plot(errangles[0:indIt + 1, 6] / degrees, 'y')
    plt.plot(errangles[0:indIt + 1, 8] / degrees, 'c')
    plt.plot(errangles[0:indIt + 1, 6] / degrees, 'k--')
    plt.plot(errangles[0:indIt + 1, 8] / degrees, 'b--')
    plt.xlabel('Iteracion')
    plt.ylabel('Extra angle')
    plt.title('Evolucion del angulo extra')
    plt.legend(('Az paso6', 'Th0 R2b paso7', 'Az paso8', 'Th0 P1 paso9',
                'Az paso9', 'Th0 R2', 'Th0 P1 Paso11', 'Th0 P2 Paso11'))
    plt.subplot(2, 2, 2)
    plt.plot(errangles[0:indIt + 1, 1] / degrees, 'k')
    plt.plot(errangles[0:indIt + 1, 4] / degrees, 'r')
    plt.plot(errangles[0:indIt + 1, 7] / degrees, 'y')
    plt.xlabel('Iteracion')
    plt.ylabel('Extra angle')
    plt.title('Evolucion de la elipticidad extra')
    plt.legend(('El paso6', 'El paso8', 'El paso9'))

    return polarimetro
