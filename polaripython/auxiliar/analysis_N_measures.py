# !/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------
# Author:    Jesus del Hoyo
# Fecha       2018/07/25 (version 1.0)
# License:    GPL
# -------------------------------------
"""
This script has the functions specific for the analysis of the number of measurements that must be performed during the calibration.
"""

import numpy as np
import os
import time
from scipy import optimize
import matplotlib.pyplot as plt

from py_pol.mueller import Mueller
from py_pol.stokes import Stokes

from polaripython.polarimeter import degrees, Intensity_Rotating_Elements, Analysis_Measurement_0D
"""
Optical eleemnts of the polarimeter (without angles).
"""
S0 = 4.28258966001
Azimuth = 23.5668146546 * degrees
Ellipticity = 47.8192903319 * degrees
Pol_degree = 1.0
p11 = 0.981060953688
p12 = 0.0514443038342
p21 = 0.975493283775
p22 = 0.048210103394
Delay_P1 = 56.0567412282 * degrees
R2p1 = 0.995476644328
R2p2 = 1.00385811866
Delay_R2 = 82.7605449275 * degrees
R1p1 = 0.983820656002
R1p2 = 0.988211852408
Delay_R1 = 82.9425129679 * degrees
Mp1 = Mueller()
Mp1.diattenuator_retarder_linear(p11, p12, Delay_P1)
Mp2 = Mueller()
Mp2.diattenuator_linear(p21, p22)
Mr1 = Mueller()
Mr1.diattenuator_retarder_linear(R1p1, R1p2, Delay_R1)
Mr2 = Mueller()
Mr2.diattenuator_retarder_linear(R2p1, R2p2, Delay_R2)
Ifuente = Stokes()
Ifuente.general_azimuth_ellipticity(
    intensity=S0, az=Azimuth, el=Ellipticity, pol_degree=Pol_degree)
"""
Common functions
"""


def take_intermediate_points_1D(angles, intensities, red):
    N = angles.size
    Nnew = (N - 1) / red
    if Nnew != np.round(Nnew):
        raise valueError('N/red = {} must be an int number'.format(Nnew))
    Nnew = int(Nnew)
    angles_new = np.zeros(Nnew + 1)
    intensities_new = np.zeros(Nnew + 1)
    for ind, angle in enumerate(angles):
        ind_new = ind / red
        if ind_new == np.round(ind_new):
            ind_new = int(ind_new)
            angles_new[ind_new] = angle
            intensities_new[ind_new] = intensities[ind]
    return angles_new, intensities_new


def take_intermediate_points_2D(angles_X, angles_Y, intensities, red):
    red = np.array(red)
    N = np.array(intensities.shape)
    Nnew = (N - 1) / red
    if any(Nnew != np.round(Nnew)):
        raise valueError('N/red = {} must be an int number'.format(Nnew))
    Nnew = np.array(Nnew, dtype=int)
    angles_X_new = np.zeros(Nnew[0] + 1)
    angles_Y_new = np.zeros(Nnew[1] + 1)
    intensities_new = np.zeros(Nnew + 1)
    for indX, angleX in enumerate(angles_X):
        indX_new = indX / red[0]
        if indX_new == np.round(indX_new):
            indX_new = int(indX_new)
            angles_X_new[indX_new] = angleX
            for indY, angleY in enumerate(angles_Y):
                indY_new = indY / red[1]
                if indY_new == np.round(indY_new):
                    indY_new = int(indY_new)
                    angles_Y_new[indY_new] = angleY
                    intensities_new[indX_new, indY_new] = intensities[indX,
                                                                      indY]
    return angles_X_new, angles_Y_new, intensities_new


"""
Funciones error-modelo
"""


def error_modelo_Malus(par, angulos, Iexp):
    Imodelo = par[0] + par[1] * np.cos(angulos - par[2])**2
    dif = Imodelo - Iexp
    return dif


def modelo_step_5a(par, th, mode):
    # Calcular
    M = [Mp1, Mr1, Mr2, Mp2]
    if mode in ('R2', 'R2'):
        th1 = [par[0], par[1], th + par[2], par[3]]
    if mode in ('P2', 'P2'):
        th1 = [par[0], par[1], par[2], th + par[3]]
    I = par[4] * Intensity_Rotating_Elements(M, th1, Ifuente, False)
    return I


def error_step_5a(par, th, mode, Iexp):
    I = modelo_step_5a(par, th, mode)
    dI = I - Iexp
    return dI


def error_step_5a_both(par, th, IexpA, IexpB):
    Ia = modelo_step_5a(par, th, 'R2')
    Ib = modelo_step_5a(par, th, 'P2')
    dIa = Ia - IexpA
    dIb = Ib - IexpB
    return np.concatenate((dIa, dIb))


def error_step_2b(par, th1, th2, Iexp):
    M = [Mr2, Mp2]
    th1 = [th1 + par[0], th2 + par[1]]
    I = par[2] * Intensity_Rotating_Elements(M, th1, Ifuente, False)
    dI = I - Iexp
    return dI.flatten()


def error_step_4b(par, th1, th2, Iexp):
    M = [Mp1, Mr2, Mp2]
    th1 = [par[0], th1 + par[1], th2 + par[2]]
    I = par[3] * Intensity_Rotating_Elements(M, th1, Ifuente, False)
    dI = I - Iexp
    return dI.flatten()


def error_step_5b(par, th1, th2, Iexp, mode):
    M = [Mp1, Mr1, Mr2, Mp2]
    if mode == 'A':
        th1 = [par[0], th1 + par[1], th2 + par[2], par[3]]
    elif mode == 'B':
        th1 = [par[0], th1 + par[1], par[2], th2 + par[3]]
    else:
        th1 = [par[0], par[1], th1 + par[2], th2 + par[3]]

    I = par[4] * Intensity_Rotating_Elements(M, th1, Ifuente, False)
    dI = I - Iexp
    return dI.flatten()


def error_step_5b_all(par, th1, th2, IexpA, IexpB, IexpC):
    dIa = (error_step_5b(par, th1, th2, IexpA, 'A') -
           (IexpA).flatten()).flatten()
    dIb = (error_step_5b(par, th1, th2, IexpB, 'B') -
           (IexpB).flatten()).flatten()
    dIc = (error_step_5b(par, th1, th2, IexpC, 'C') -
           (IexpC).flatten()).flatten()
    return np.concatenate((dIa, dIb, dIc))


"""
Functions that perform a single analysis
"""


def Malus_1D(type='fit'):
    # Ir a la carpeta correspondiente
    # path = r'D:\Codigo_UCM\polaripython\calibration\Calibration_2019-06-07\1D varios'
    path = r'D:\codigo\polaripython\calibration\Calibration_2019-06-07\1D varios'
    os.chdir(path)
    # Crear listas donde guardar todos los datos facilmente
    Nmax = 202
    angles = list(np.zeros(Nmax))
    intensities = list(np.zeros(Nmax))
    # Cargar archivos
    filename = 'Step_1_2019-06-07 N=201.npz'
    data = np.load(filename)
    N = 201
    angles[N] = data['angles_1']
    intensities[N] = data['Iexp']

    filename = 'Step_1_2019-06-07 N=181.npz'
    data = np.load(filename)
    N = 181
    angles[N] = data['angles_1']
    intensities[N] = data['Iexp']

    filename = 'Step_1_2019-06-07 N=161.npz'
    data = np.load(filename)
    N = 161
    angles[N] = data['angles_1']
    intensities[N] = data['Iexp']

    filename = 'Step_1_2019-06-07 N=141.npz'
    data = np.load(filename)
    N = 141
    angles[N] = data['angles_1']
    intensities[N] = data['Iexp']

    filename = 'Step_1_2019-06-07 N=121.npz'
    data = np.load(filename)
    N = 121
    angles[N] = data['angles_1']
    intensities[N] = data['Iexp']
    # Interpolar
    N = 201
    Nnew, factor = (101, 2)
    angles[Nnew], intensities[Nnew] = take_intermediate_points_1D(
        angles[N], intensities[N], factor)
    Nnew, factor = (51, 4)
    angles[Nnew], intensities[Nnew] = take_intermediate_points_1D(
        angles[N], intensities[N], factor)
    Nnew, factor = (41, 5)
    angles[Nnew], intensities[Nnew] = take_intermediate_points_1D(
        angles[N], intensities[N], factor)
    Nnew, factor = (26, 8)
    angles[Nnew], intensities[Nnew] = take_intermediate_points_1D(
        angles[N], intensities[N], factor)
    Nnew, factor = (21, 10)
    angles[Nnew], intensities[Nnew] = take_intermediate_points_1D(
        angles[N], intensities[N], factor)
    Nnew, factor = (11, 20)
    angles[Nnew], intensities[Nnew] = take_intermediate_points_1D(
        angles[N], intensities[N], factor)
    Nnew, factor = (9, 25)
    angles[Nnew], intensities[Nnew] = take_intermediate_points_1D(
        angles[N], intensities[N], factor)
    Nnew, factor = (6, 40)
    angles[Nnew], intensities[Nnew] = take_intermediate_points_1D(
        angles[N], intensities[N], factor)
    Nnew, factor = (5, 50)
    angles[Nnew], intensities[Nnew] = take_intermediate_points_1D(
        angles[N], intensities[N], factor)

    N = 181
    Nnew, factor = (91, 2)
    angles[Nnew], intensities[Nnew] = take_intermediate_points_1D(
        angles[N], intensities[N], factor)
    Nnew, factor = (61, 3)
    angles[Nnew], intensities[Nnew] = take_intermediate_points_1D(
        angles[N], intensities[N], factor)
    Nnew, factor = (46, 4)
    angles[Nnew], intensities[Nnew] = take_intermediate_points_1D(
        angles[N], intensities[N], factor)
    Nnew, factor = (31, 6)
    angles[Nnew], intensities[Nnew] = take_intermediate_points_1D(
        angles[N], intensities[N], factor)
    Nnew, factor = (16, 12)
    angles[Nnew], intensities[Nnew] = take_intermediate_points_1D(
        angles[N], intensities[N], factor)
    Nnew, factor = (19, 10)
    angles[Nnew], intensities[Nnew] = take_intermediate_points_1D(
        angles[N], intensities[N], factor)
    Nnew, factor = (37, 5)
    angles[Nnew], intensities[Nnew] = take_intermediate_points_1D(
        angles[N], intensities[N], factor)
    Nnew, factor = (13, 15)
    angles[Nnew], intensities[Nnew] = take_intermediate_points_1D(
        angles[N], intensities[N], factor)
    Nnew, factor = (10, 20)
    angles[Nnew], intensities[Nnew] = take_intermediate_points_1D(
        angles[N], intensities[N], factor)
    Nnew, factor = (7, 30)
    angles[Nnew], intensities[Nnew] = take_intermediate_points_1D(
        angles[N], intensities[N], factor)

    N = 161
    Nnew, factor = (81, 2)
    angles[Nnew], intensities[Nnew] = take_intermediate_points_1D(
        angles[N], intensities[N], factor)
    Nnew, factor = (33, 5)
    angles[Nnew], intensities[Nnew] = take_intermediate_points_1D(
        angles[N], intensities[N], factor)
    Nnew, factor = (17, 10)
    angles[Nnew], intensities[Nnew] = take_intermediate_points_1D(
        angles[N], intensities[N], factor)

    N = 141
    Nnew, factor = (71, 2)
    angles[Nnew], intensities[Nnew] = take_intermediate_points_1D(
        angles[N], intensities[N], factor)
    Nnew, factor = (36, 4)
    angles[Nnew], intensities[Nnew] = take_intermediate_points_1D(
        angles[N], intensities[N], factor)
    Nnew, factor = (29, 5)
    angles[Nnew], intensities[Nnew] = take_intermediate_points_1D(
        angles[N], intensities[N], factor)
    Nnew, factor = (15, 10)
    angles[Nnew], intensities[Nnew] = take_intermediate_points_1D(
        angles[N], intensities[N], factor)

    N = 121
    Nnew, factor = (61, 2)
    angles[Nnew], intensities[Nnew] = take_intermediate_points_1D(
        angles[N], intensities[N], factor)
    Nnew, factor = (25, 5)
    angles[Nnew], intensities[Nnew] = take_intermediate_points_1D(
        angles[N], intensities[N], factor)

    # Initial parameters
    par0 = [0, 2, 0]
    min_limit = [0, 0, 0]
    max_limit = [6, 6, 180 * degrees]
    jacobian = '3-point'
    # First method: We fit using the maximum number of points and take it as reference
    if type in ('max', 'Max', 'MAX'):
        result = optimize.least_squares(
            error_modelo_Malus,
            par0,
            bounds=(min_limit, max_limit),
            jac=jacobian,
            args=(angles[Nmax - 1], intensities[Nmax - 1]))
        error_aux = error_modelo_Malus(result.x, angles[Nmax - 1],
                                       intensities[Nmax - 1])
        error = [
            np.linalg.norm(error_aux) /
            (intensities[Nmax - 1].size * intensities[Nmax - 1].max())
        ]
        N_array = [Nmax - 1]
        for ind in range(Nmax):
            if (angles[ind] == 0).all():
                pass
            else:
                error_aux = error_modelo_Malus(result.x, angles[ind],
                                               intensities[ind])
                error.append(
                    np.linalg.norm(error_aux) /
                    (intensities[ind].size * intensities[ind].max()))
                N_array.append(ind)
    # Second method: Fit during all steps
    else:
        error = []
        N_array = []
        for ind in range(Nmax):
            if (angles[ind] == 0).all():
                pass
            else:
                angle_aux = angles[ind]
                int_aux = intensities[ind]
                result = optimize.least_squares(
                    error_modelo_Malus,
                    par0,
                    bounds=(min_limit, max_limit),
                    jac=jacobian,
                    args=(angle_aux, int_aux))
                error_aux = error_modelo_Malus(result.x, angle_aux, int_aux)
                error.append(
                    np.linalg.norm(error_aux) / (int_aux.size * int_aux.max()))
                N_array.append(ind)
    error = np.array(error)
    N_array = np.array(N_array)
    return N_array, error


def Otros_1D():
    start_time = time.time()
    # Ir a la carpeta correspondiente
    # path = r'D:\Codigo_UCM\polaripython\calibration\Calibration_2019-06-17\Step5a varios'
    path = r'D:\codigo\polaripython\calibration\Calibration_2019-06-17\Step5a varios'
    os.chdir(path)
    # Crear listas donde guardar todos los datos facilmente
    Nmax = 202
    angles = list(np.zeros(Nmax))
    intensities_A = list(np.zeros(Nmax))
    intensities_B = list(np.zeros(Nmax))
    # Cargar archivos
    N = 201
    filename = 'Step_5a_2019-06-17_N_201.npz'
    data = np.load(filename)
    angles[N] = data['angles_1']
    intensities_A[N] = data['IexpA']
    intensities_B[N] = data['IexpB']
    N = 181
    filename = 'Step_5a_2019-06-17_N_181.npz'
    data = np.load(filename)
    angles[N] = data['angles_1']
    intensities_A[N] = data['IexpA']
    intensities_B[N] = data['IexpB']
    N = 161
    filename = 'Step_5a_2019-06-17_N_161.npz'
    data = np.load(filename)
    angles[N] = data['angles_1']
    intensities_A[N] = data['IexpA']
    intensities_B[N] = data['IexpB']
    N = 141
    filename = 'Step_5a_2019-06-17_N_141.npz'
    data = np.load(filename)
    angles[N] = data['angles_1']
    intensities_A[N] = data['IexpA']
    intensities_B[N] = data['IexpB']
    N = 121
    filename = 'Step_5a_2019-06-17_N_121.npz'
    data = np.load(filename)
    angles[N] = data['angles_1']
    intensities_A[N] = data['IexpA']
    intensities_B[N] = data['IexpB']

    # Interpolar
    N = 201
    Nnew, factor = (101, 2)
    angles[Nnew], intensities_A[Nnew] = take_intermediate_points_1D(
        angles[N], intensities_A[N], factor)
    _, intensities_B[Nnew] = take_intermediate_points_1D(
        angles[N], intensities_B[N], factor)
    Nnew, factor = (51, 4)
    angles[Nnew], intensities_A[Nnew] = take_intermediate_points_1D(
        angles[N], intensities_A[N], factor)
    _, intensities_B[Nnew] = take_intermediate_points_1D(
        angles[N], intensities_B[N], factor)
    Nnew, factor = (41, 5)
    angles[Nnew], intensities_A[Nnew] = take_intermediate_points_1D(
        angles[N], intensities_A[N], factor)
    _, intensities_B[Nnew] = take_intermediate_points_1D(
        angles[N], intensities_B[N], factor)
    Nnew, factor = (26, 8)
    angles[Nnew], intensities_A[Nnew] = take_intermediate_points_1D(
        angles[N], intensities_A[N], factor)
    _, intensities_B[Nnew] = take_intermediate_points_1D(
        angles[N], intensities_B[N], factor)
    Nnew, factor = (21, 10)
    angles[Nnew], intensities_A[Nnew] = take_intermediate_points_1D(
        angles[N], intensities_A[N], factor)
    _, intensities_B[Nnew] = take_intermediate_points_1D(
        angles[N], intensities_B[N], factor)
    Nnew, factor = (11, 20)
    angles[Nnew], intensities_A[Nnew] = take_intermediate_points_1D(
        angles[N], intensities_A[N], factor)
    _, intensities_B[Nnew] = take_intermediate_points_1D(
        angles[N], intensities_B[N], factor)
    Nnew, factor = (9, 25)
    angles[Nnew], intensities_A[Nnew] = take_intermediate_points_1D(
        angles[N], intensities_A[N], factor)
    _, intensities_B[Nnew] = take_intermediate_points_1D(
        angles[N], intensities_B[N], factor)
    Nnew, factor = (6, 40)
    angles[Nnew], intensities_A[Nnew] = take_intermediate_points_1D(
        angles[N], intensities_A[N], factor)
    _, intensities_B[Nnew] = take_intermediate_points_1D(
        angles[N], intensities_B[N], factor)
    Nnew, factor = (5, 50)
    angles[Nnew], intensities_A[Nnew] = take_intermediate_points_1D(
        angles[N], intensities_A[N], factor)
    _, intensities_B[Nnew] = take_intermediate_points_1D(
        angles[N], intensities_B[N], factor)

    N = 181
    Nnew, factor = (91, 2)
    angles[Nnew], intensities_A[Nnew] = take_intermediate_points_1D(
        angles[N], intensities_A[N], factor)
    _, intensities_B[Nnew] = take_intermediate_points_1D(
        angles[N], intensities_B[N], factor)
    Nnew, factor = (61, 3)
    angles[Nnew], intensities_A[Nnew] = take_intermediate_points_1D(
        angles[N], intensities_A[N], factor)
    _, intensities_B[Nnew] = take_intermediate_points_1D(
        angles[N], intensities_B[N], factor)
    Nnew, factor = (46, 4)
    angles[Nnew], intensities_A[Nnew] = take_intermediate_points_1D(
        angles[N], intensities_A[N], factor)
    _, intensities_B[Nnew] = take_intermediate_points_1D(
        angles[N], intensities_B[N], factor)
    Nnew, factor = (31, 6)
    angles[Nnew], intensities_A[Nnew] = take_intermediate_points_1D(
        angles[N], intensities_A[N], factor)
    _, intensities_B[Nnew] = take_intermediate_points_1D(
        angles[N], intensities_B[N], factor)
    Nnew, factor = (16, 12)
    angles[Nnew], intensities_A[Nnew] = take_intermediate_points_1D(
        angles[N], intensities_A[N], factor)
    _, intensities_B[Nnew] = take_intermediate_points_1D(
        angles[N], intensities_B[N], factor)
    Nnew, factor = (19, 10)
    angles[Nnew], intensities_A[Nnew] = take_intermediate_points_1D(
        angles[N], intensities_A[N], factor)
    _, intensities_B[Nnew] = take_intermediate_points_1D(
        angles[N], intensities_B[N], factor)
    Nnew, factor = (37, 5)
    angles[Nnew], intensities_A[Nnew] = take_intermediate_points_1D(
        angles[N], intensities_A[N], factor)
    _, intensities_B[Nnew] = take_intermediate_points_1D(
        angles[N], intensities_B[N], factor)
    Nnew, factor = (13, 15)
    angles[Nnew], intensities_A[Nnew] = take_intermediate_points_1D(
        angles[N], intensities_A[N], factor)
    _, intensities_B[Nnew] = take_intermediate_points_1D(
        angles[N], intensities_B[N], factor)
    Nnew, factor = (10, 20)
    angles[Nnew], intensities_A[Nnew] = take_intermediate_points_1D(
        angles[N], intensities_A[N], factor)
    _, intensities_B[Nnew] = take_intermediate_points_1D(
        angles[N], intensities_B[N], factor)
    Nnew, factor = (7, 30)
    angles[Nnew], intensities_A[Nnew] = take_intermediate_points_1D(
        angles[N], intensities_A[N], factor)
    _, intensities_B[Nnew] = take_intermediate_points_1D(
        angles[N], intensities_B[N], factor)

    N = 161
    Nnew, factor = (81, 2)
    angles[Nnew], intensities_A[Nnew] = take_intermediate_points_1D(
        angles[N], intensities_A[N], factor)
    _, intensities_B[Nnew] = take_intermediate_points_1D(
        angles[N], intensities_B[N], factor)
    Nnew, factor = (33, 5)
    angles[Nnew], intensities_A[Nnew] = take_intermediate_points_1D(
        angles[N], intensities_A[N], factor)
    _, intensities_B[Nnew] = take_intermediate_points_1D(
        angles[N], intensities_B[N], factor)
    Nnew, factor = (17, 10)
    angles[Nnew], intensities_A[Nnew] = take_intermediate_points_1D(
        angles[N], intensities_A[N], factor)
    _, intensities_B[Nnew] = take_intermediate_points_1D(
        angles[N], intensities_B[N], factor)

    N = 141
    Nnew, factor = (71, 2)
    angles[Nnew], intensities_A[Nnew] = take_intermediate_points_1D(
        angles[N], intensities_A[N], factor)
    _, intensities_B[Nnew] = take_intermediate_points_1D(
        angles[N], intensities_B[N], factor)
    Nnew, factor = (36, 4)
    angles[Nnew], intensities_A[Nnew] = take_intermediate_points_1D(
        angles[N], intensities_A[N], factor)
    _, intensities_B[Nnew] = take_intermediate_points_1D(
        angles[N], intensities_B[N], factor)
    Nnew, factor = (29, 5)
    angles[Nnew], intensities_A[Nnew] = take_intermediate_points_1D(
        angles[N], intensities_A[N], factor)
    _, intensities_B[Nnew] = take_intermediate_points_1D(
        angles[N], intensities_B[N], factor)
    Nnew, factor = (15, 10)
    angles[Nnew], intensities_A[Nnew] = take_intermediate_points_1D(
        angles[N], intensities_A[N], factor)
    _, intensities_B[Nnew] = take_intermediate_points_1D(
        angles[N], intensities_B[N], factor)

    N = 121
    Nnew, factor = (61, 2)
    angles[Nnew], intensities_A[Nnew] = take_intermediate_points_1D(
        angles[N], intensities_A[N], factor)
    _, intensities_B[Nnew] = take_intermediate_points_1D(
        angles[N], intensities_B[N], factor)
    Nnew, factor = (25, 5)
    angles[Nnew], intensities_A[Nnew] = take_intermediate_points_1D(
        angles[N], intensities_A[N], factor)
    _, intensities_B[Nnew] = take_intermediate_points_1D(
        angles[N], intensities_B[N], factor)

    # Precrear variables
    error_abs_ret = []
    error_abs_pol = []
    error_abs_both = []
    error_rel_ret = []
    error_rel_pol = []
    error_rel_both = []
    par_ret = []
    par_pol = []
    par_both = []
    N_array = []
    par0 = [0, 0, 0, 0, 3]
    min_limit = [0, 0, 0, 0, 0]
    max_limit = [180 * degrees, 180 * degrees, 180 * degrees, 180 * degrees, 6]
    jacobian = '3-point'
    # We need a reference. Take the one with max points
    ImaxA = intensities_A[Nmax - 1].max()
    result_0A = optimize.least_squares(
        error_step_5a,
        par0,
        bounds=(min_limit, max_limit),
        jac=jacobian,
        args=(angles[Nmax - 1], 'R2', intensities_A[Nmax - 1]))
    ImaxB = intensities_B[Nmax - 1].max()
    result_0B = optimize.least_squares(
        error_step_5a,
        par0,
        bounds=(min_limit, max_limit),
        jac=jacobian,
        args=(angles[Nmax - 1], 'P2', intensities_B[Nmax - 1]))
    result_0C = optimize.least_squares(
        error_step_5a_both,
        par0,
        bounds=(min_limit, max_limit),
        jac=jacobian,
        args=(angles[Nmax - 1], intensities_A[Nmax - 1],
              intensities_B[Nmax - 1]))
    # Main loop
    for ind in range(Nmax):
        if (angles[ind] == 0).all():
            pass
        else:
            N_array.append(ind)
            # Rotate the retarder
            ImaxA = intensities_A[ind].max()
            error_aux = error_step_5a(result_0A.x, angles[ind], 'R2',
                                      intensities_A[ind])
            error_abs_ret.append(np.linalg.norm(error_aux) / (ind * ImaxA))
            result = optimize.least_squares(
                error_step_5a,
                par0,
                bounds=(min_limit, max_limit),
                jac=jacobian,
                args=(angles[ind], 'R2', intensities_A[ind]))
            error_aux = error_step_5a(result.x, angles[ind], 'R2',
                                      intensities_A[ind])
            error_rel_ret.append(np.linalg.norm(error_aux) / (ind * ImaxA))
            par_ret.append(result.x - result_0A.x)
            # Rotate the polarizer
            ImaxB = intensities_B[ind].max()
            error_aux = error_step_5a(result_0B.x, angles[ind], 'P2',
                                      intensities_B[ind])
            error_abs_pol.append(np.linalg.norm(error_aux) / (ind * ImaxA))
            result = optimize.least_squares(
                error_step_5a,
                par0,
                bounds=(min_limit, max_limit),
                jac=jacobian,
                args=(angles[ind], 'P2', intensities_B[ind]))
            error_aux = error_step_5a(result.x, angles[ind], 'P2',
                                      intensities_B[ind])
            error_rel_pol.append(np.linalg.norm(error_aux) / (ind * ImaxB))
            par_pol.append(result.x - result_0B.x)
            # Rotate both
            ImaxC = max(ImaxA, ImaxB)
            error_aux = error_step_5a_both(result_0C.x, angles[ind],
                                           intensities_A[ind],
                                           intensities_B[ind])
            error_abs_both.append(np.linalg.norm(error_aux) / (ind * ImaxC))
            result = optimize.least_squares(
                error_step_5a_both,
                par0,
                bounds=(min_limit, max_limit),
                jac=jacobian,
                args=(angles[ind], intensities_A[ind], intensities_B[ind]))
            error_aux = error_step_5a_both(
                result.x, angles[ind], intensities_A[ind], intensities_B[ind])
            error_rel_both.append(np.linalg.norm(error_aux) / (ind * ImaxC))
            par_both.append(result.x - result_0C.x)
    # Resultados
    error_abs = [
        np.array(error_abs_ret),
        np.array(error_abs_pol),
        np.array(error_abs_both)
    ]
    error_rel = [
        np.array(error_rel_ret),
        np.array(error_rel_pol),
        np.array(error_rel_both)
    ]
    param = [np.array(par_ret), np.array(par_pol), np.array(par_both)]
    N_array = np.array(N_array)
    # final
    end_time = time.time()
    print('Elapsed time is {} s.'.format(end_time - start_time))
    return N_array, error_abs, error_rel, param


def Paso_2b_2D():
    # Ir a la carpeta correspondiente
    # path = r'D:\Codigo_UCM\polaripython\calibration\Calibration_2019-06-17\Step2b varios'
    path = r'D:\codigo\polaripython\calibration\Calibration_2019-06-17\Step2b varios'
    os.chdir(path)
    # Crear listas donde guardar todos los datos facilmente
    Nmax = 32
    angles = list(np.zeros(Nmax))
    intensities = list(np.zeros(Nmax))
    # Cargar archivos
    N = 31
    filename = 'Step_2b_2019-06-17_N_31_31.npz'
    data = np.load(filename)
    angles[N] = data['angles2x']
    intensities[N] = data['Iexp']
    N = 29
    filename = 'Step_2b_2019-06-17_N_29_29.npz'
    data = np.load(filename)
    angles[N] = data['angles2x']
    intensities[N] = data['Iexp']
    N = 27
    filename = 'Step_2b_2019-06-17_N_27_27.npz'
    data = np.load(filename)
    angles[N] = data['angles2x']
    intensities[N] = data['Iexp']
    N = 25
    filename = 'Step_2b_2019-06-17_N_25_25.npz'
    data = np.load(filename)
    angles[N] = data['angles2x']
    intensities[N] = data['Iexp']
    N = 23
    filename = 'Step_2b_2019-06-17_N_23_23.npz'
    data = np.load(filename)
    angles[N] = data['angles2x']
    intensities[N] = data['Iexp']
    N = 21
    filename = 'Step_2b_2019-06-17_N_21_21.npz'
    data = np.load(filename)
    angles[N] = data['angles2x']
    intensities[N] = data['Iexp']
    N = 19
    filename = 'Step_2b_2019-06-17_N_19_19.npz'
    data = np.load(filename)
    angles[N] = data['angles2x']
    intensities[N] = data['Iexp']
    N = 17
    filename = 'Step_2b_2019-06-17_N_17_17.npz'
    data = np.load(filename)
    angles[N] = data['angles2x']
    intensities[N] = data['Iexp']
    # Interpolar
    N = 31
    Nnew, factor = (16, 2 * np.ones(2))
    angles[Nnew], _, intensities[Nnew] = take_intermediate_points_2D(
        angles[N], angles[N], intensities[N], factor)
    Nnew, factor = (11, 3 * np.ones(2))
    angles[Nnew], _, intensities[Nnew] = take_intermediate_points_2D(
        angles[N], angles[N], intensities[N], factor)
    Nnew, factor = (7, 5 * np.ones(2))
    angles[Nnew], _, intensities[Nnew] = take_intermediate_points_2D(
        angles[N], angles[N], intensities[N], factor)
    Nnew, factor = (6, 6 * np.ones(2))
    angles[Nnew], _, intensities[Nnew] = take_intermediate_points_2D(
        angles[N], angles[N], intensities[N], factor)
    # Nnew, factor = (4, 10 * np.ones(2))
    # angles[Nnew], _, intensities[Nnew] = take_intermediate_points_2D(
    #     angles[N], angles[N], intensities[N], factor)

    N = 29
    Nnew, factor = (15, 2 * np.ones(2))
    angles[Nnew], _, intensities[Nnew] = take_intermediate_points_2D(
        angles[N], angles[N], intensities[N], factor)
    Nnew, factor = (8, 4 * np.ones(2))
    angles[Nnew], _, intensities[Nnew] = take_intermediate_points_2D(
        angles[N], angles[N], intensities[N], factor)
    Nnew, factor = (5, 7 * np.ones(2))
    angles[Nnew], _, intensities[Nnew] = take_intermediate_points_2D(
        angles[N], angles[N], intensities[N], factor)

    N = 27
    Nnew, factor = (14, 2 * np.ones(2))
    angles[Nnew], _, intensities[Nnew] = take_intermediate_points_2D(
        angles[N], angles[N], intensities[N], factor)

    N = 25
    Nnew, factor = (13, 2 * np.ones(2))
    angles[Nnew], _, intensities[Nnew] = take_intermediate_points_2D(
        angles[N], angles[N], intensities[N], factor)
    Nnew, factor = (9, 3 * np.ones(2))
    angles[Nnew], _, intensities[Nnew] = take_intermediate_points_2D(
        angles[N], angles[N], intensities[N], factor)

    N = 23
    Nnew, factor = (12, 2 * np.ones(2))
    angles[Nnew], _, intensities[Nnew] = take_intermediate_points_2D(
        angles[N], angles[N], intensities[N], factor)

    N = 19
    Nnew, factor = (10, 2 * np.ones(2))
    angles[Nnew], _, intensities[Nnew] = take_intermediate_points_2D(
        angles[N], angles[N], intensities[N], factor)

    # Precrear variables
    error = []
    N_array = []
    par0 = [0, 0, 1]
    # Bucle
    for ind in range(Nmax):
        if (angles[ind] == 0).all():
            pass
        else:
            par1, success = optimize.leastsq(
                error_step_2b,
                par0,
                args=(angles[ind], angles[ind], intensities[ind]))
            error_aux = error_step_2b(par1, angles[ind], angles[ind],
                                      intensities[ind])
            error.append(
                np.linalg.norm(error_aux) / (ind**2 * intensities[ind].max()))
            N_array.append(ind**2)
    error = np.array(error)
    N_array = np.array(N_array)
    return N_array, error


def Paso_4b_2D():
    # Ir a la carpeta correspondiente
    # path = r'D:\Codigo_UCM\polaripython\calibration\Calibration_2019-06-17\Step4b varios'
    path = r'D:\codigo\polaripython\calibration\Calibration_2019-06-17\Step4b varios'
    os.chdir(path)
    # Crear listas donde guardar todos los datos facilmente
    Nmax = 32
    angles = list(np.zeros(Nmax))
    intensities = list(np.zeros(Nmax))
    # Cargar archivos
    N = 31
    filename = 'Step_4b_2019-06-17_N_31_31.npz'
    data = np.load(filename)
    angles[N] = data['angles2x']
    intensities[N] = data['Iexp']
    N = 29
    filename = 'Step_4b_2019-06-17_N_29_29.npz'
    data = np.load(filename)
    angles[N] = data['angles2x']
    intensities[N] = data['Iexp']
    N = 27
    filename = 'Step_4b_2019-06-17_N_27_27.npz'
    data = np.load(filename)
    angles[N] = data['angles2x']
    intensities[N] = data['Iexp']
    N = 25
    filename = 'Step_4b_2019-06-17_N_25_25.npz'
    data = np.load(filename)
    angles[N] = data['angles2x']
    intensities[N] = data['Iexp']
    N = 23
    filename = 'Step_4b_2019-06-17_N_23_23.npz'
    data = np.load(filename)
    angles[N] = data['angles2x']
    intensities[N] = data['Iexp']
    N = 21
    filename = 'Step_4b_2019-06-17_N_21_21.npz'
    data = np.load(filename)
    angles[N] = data['angles2x']
    intensities[N] = data['Iexp']
    N = 19
    filename = 'Step_4b_2019-06-17_N_19_19.npz'
    data = np.load(filename)
    angles[N] = data['angles2x']
    intensities[N] = data['Iexp']
    N = 17
    filename = 'Step_4b_2019-06-17_N_17_17.npz'
    data = np.load(filename)
    angles[N] = data['angles2x']
    intensities[N] = data['Iexp']
    # Interpolar
    N = 31
    Nnew, factor = (16, 2 * np.ones(2))
    angles[Nnew], _, intensities[Nnew] = take_intermediate_points_2D(
        angles[N], angles[N], intensities[N], factor)
    Nnew, factor = (11, 3 * np.ones(2))
    angles[Nnew], _, intensities[Nnew] = take_intermediate_points_2D(
        angles[N], angles[N], intensities[N], factor)
    Nnew, factor = (7, 5 * np.ones(2))
    angles[Nnew], _, intensities[Nnew] = take_intermediate_points_2D(
        angles[N], angles[N], intensities[N], factor)
    Nnew, factor = (6, 6 * np.ones(2))
    angles[Nnew], _, intensities[Nnew] = take_intermediate_points_2D(
        angles[N], angles[N], intensities[N], factor)
    # Nnew, factor = (4, 10 * np.ones(2))
    # angles[Nnew], _, intensities[Nnew] = take_intermediate_points_2D(
    #     angles[N], angles[N], intensities[N], factor)

    N = 29
    Nnew, factor = (15, 2 * np.ones(2))
    angles[Nnew], _, intensities[Nnew] = take_intermediate_points_2D(
        angles[N], angles[N], intensities[N], factor)
    Nnew, factor = (8, 4 * np.ones(2))
    angles[Nnew], _, intensities[Nnew] = take_intermediate_points_2D(
        angles[N], angles[N], intensities[N], factor)
    Nnew, factor = (5, 7 * np.ones(2))
    angles[Nnew], _, intensities[Nnew] = take_intermediate_points_2D(
        angles[N], angles[N], intensities[N], factor)

    N = 27
    Nnew, factor = (14, 2 * np.ones(2))
    angles[Nnew], _, intensities[Nnew] = take_intermediate_points_2D(
        angles[N], angles[N], intensities[N], factor)

    N = 25
    Nnew, factor = (13, 2 * np.ones(2))
    angles[Nnew], _, intensities[Nnew] = take_intermediate_points_2D(
        angles[N], angles[N], intensities[N], factor)
    Nnew, factor = (9, 3 * np.ones(2))
    angles[Nnew], _, intensities[Nnew] = take_intermediate_points_2D(
        angles[N], angles[N], intensities[N], factor)

    N = 23
    Nnew, factor = (12, 2 * np.ones(2))
    angles[Nnew], _, intensities[Nnew] = take_intermediate_points_2D(
        angles[N], angles[N], intensities[N], factor)

    N = 19
    Nnew, factor = (10, 2 * np.ones(2))
    angles[Nnew], _, intensities[Nnew] = take_intermediate_points_2D(
        angles[N], angles[N], intensities[N], factor)

    # Precrear variables
    error = []
    N_array = []
    par0 = [0, 0, 0, 1]
    # Bucle
    for ind in range(Nmax):
        if (angles[ind] == 0).all():
            pass
        else:
            par1, success = optimize.leastsq(
                error_step_4b,
                par0,
                args=(angles[ind], angles[ind], intensities[ind]))
            error_aux = error_step_4b(par1, angles[ind], angles[ind],
                                      intensities[ind])
            error.append(
                np.linalg.norm(error_aux) / (ind**2 * intensities[ind].max()))
            N_array.append(ind**2)
    error = np.array(error)
    N_array = np.array(N_array)
    return N_array, error


def Paso_5b_2D():
    start_time = time.time()
    # Ir a la carpeta correspondiente
    # path = r'D:\Codigo_UCM\polaripython\calibration\Calibration_2019-06-17\Step5b varios'
    path = r'D:\codigo\polaripython\calibration\Calibration_2019-06-17\Step5b varios'
    os.chdir(path)
    # Crear listas donde guardar todos los datos facilmente
    Nmax = 32
    angles = list(np.zeros(Nmax))
    I_A = list(np.zeros(Nmax))
    I_B = list(np.zeros(Nmax))
    I_C = list(np.zeros(Nmax))
    # Cargar archivos
    N = 31
    filename = 'Step_5b_2019-06-17_N_31_31.npz'
    data = np.load(filename)
    angles[N] = data['angles2x']
    I_A[N] = data['IexpA']
    I_B[N] = data['IexpB']
    I_C[N] = data['IexpC']
    # N = 29
    # filename = 'Step_5b_2019-06-17_N_29_29.npz'
    # data = np.load(filename)
    # angles[N] = data['angles2x']
    # I_A[N] = data['IexpA']
    # I_B[N] = data['IexpB']
    # I_C[N] = data['IexpC']
    N = 27
    filename = 'Step_5b_2019-06-17_N_27_27.npz'
    data = np.load(filename)
    angles[N] = data['angles2x']
    I_A[N] = data['IexpA']
    I_B[N] = data['IexpB']
    I_C[N] = data['IexpC']
    # N = 25
    # filename = 'Step_5b_2019-06-17_N_25_25.npz'
    # data = np.load(filename)
    # angles[N] = data['angles2x']
    # I_A[N] = data['IexpA']
    # I_B[N] = data['IexpB']
    # I_C[N] = data['IexpC']
    N = 23
    filename = 'Step_5b_2019-06-17_N_23_23.npz'
    data = np.load(filename)
    angles[N] = data['angles2x']
    I_A[N] = data['IexpA']
    I_B[N] = data['IexpB']
    I_C[N] = data['IexpC']
    # N = 21
    # filename = 'Step_5b_2019-06-17_N_21_21.npz'
    # data = np.load(filename)
    # angles[N] = data['angles2x']
    # I_A[N] = data['IexpA']
    # I_B[N] = data['IexpB']
    # I_C[N] = data['IexpC']
    N = 19
    filename = 'Step_5b_2019-06-17_N_19_19.npz'
    data = np.load(filename)
    angles[N] = data['angles2x']
    I_A[N] = data['IexpA']
    I_B[N] = data['IexpB']
    I_C[N] = data['IexpC']
    # N = 17
    # filename = 'Step_5b_2019-06-17_N_17_17.npz'
    # data = np.load(filename)
    # angles[N] = data['angles2x']
    # I_A[N] = data['IexpA']
    # I_B[N] = data['IexpB']
    # I_C[N] = data['IexpC']
    # Interpolar
    N = 31
    Nnew, factor = (16, 2 * np.ones(2))
    angles[Nnew], _, I_A[Nnew] = take_intermediate_points_2D(
        angles[N], angles[N], I_A[N], factor)
    _, _, I_B[Nnew] = take_intermediate_points_2D(angles[N], angles[N], I_B[N],
                                                  factor)
    _, _, I_C[Nnew] = take_intermediate_points_2D(angles[N], angles[N], I_C[N],
                                                  factor)
    Nnew, factor = (11, 3 * np.ones(2))
    angles[Nnew], _, I_A[Nnew] = take_intermediate_points_2D(
        angles[N], angles[N], I_A[N], factor)
    _, _, I_B[Nnew] = take_intermediate_points_2D(angles[N], angles[N], I_B[N],
                                                  factor)
    _, _, I_C[Nnew] = take_intermediate_points_2D(angles[N], angles[N], I_C[N],
                                                  factor)
    Nnew, factor = (7, 5 * np.ones(2))
    angles[Nnew], _, I_A[Nnew] = take_intermediate_points_2D(
        angles[N], angles[N], I_A[N], factor)
    _, _, I_B[Nnew] = take_intermediate_points_2D(angles[N], angles[N], I_B[N],
                                                  factor)
    _, _, I_C[Nnew] = take_intermediate_points_2D(angles[N], angles[N], I_C[N],
                                                  factor)
    Nnew, factor = (6, 6 * np.ones(2))
    angles[Nnew], _, I_A[Nnew] = take_intermediate_points_2D(
        angles[N], angles[N], I_A[N], factor)
    _, _, I_B[Nnew] = take_intermediate_points_2D(angles[N], angles[N], I_B[N],
                                                  factor)
    _, _, I_C[Nnew] = take_intermediate_points_2D(angles[N], angles[N], I_C[N],
                                                  factor)
    # Nnew, factor = (4, 10 * np.ones(2))
    # angles[Nnew], _, intensities[Nnew] = take_intermediate_points_2D(
    #     angles[N], angles[N], intensities[N], factor)

    # N = 29
    # Nnew, factor = (15, 2 * np.ones(2))
    # angles[Nnew], _, I_A[Nnew] = take_intermediate_points_2D(
    #     angles[N], angles[N], I_A[N], factor)
    # _, _, I_B[Nnew] = take_intermediate_points_2D(
    #     angles[N], angles[N], I_B[N], factor)
    # _, _, I_C[Nnew] = take_intermediate_points_2D(
    #     angles[N], angles[N], I_C[N], factor)
    # Nnew, factor = (8, 4 * np.ones(2))
    # angles[Nnew], _, I_A[Nnew] = take_intermediate_points_2D(
    #     angles[N], angles[N], I_A[N], factor)
    # _, _, I_B[Nnew] = take_intermediate_points_2D(
    #     angles[N], angles[N], I_B[N], factor)
    # _, _, I_C[Nnew] = take_intermediate_points_2D(
    #     angles[N], angles[N], I_C[N], factor)
    # Nnew, factor = (5, 7 * np.ones(2))
    # angles[Nnew], _, I_A[Nnew] = take_intermediate_points_2D(
    #     angles[N], angles[N], I_A[N], factor)
    # _, _, I_B[Nnew] = take_intermediate_points_2D(
    #     angles[N], angles[N], I_B[N], factor)
    # _, _, I_C[Nnew] = take_intermediate_points_2D(
    #     angles[N], angles[N], I_C[N], factor)

    N = 27
    Nnew, factor = (14, 2 * np.ones(2))
    angles[Nnew], _, I_A[Nnew] = take_intermediate_points_2D(
        angles[N], angles[N], I_A[N], factor)
    _, _, I_B[Nnew] = take_intermediate_points_2D(angles[N], angles[N], I_B[N],
                                                  factor)
    _, _, I_C[Nnew] = take_intermediate_points_2D(angles[N], angles[N], I_C[N],
                                                  factor)

    # N = 25
    # Nnew, factor = (13, 2 * np.ones(2))
    # angles[Nnew], _, I_A[Nnew] = take_intermediate_points_2D(
    #     angles[N], angles[N], I_A[N], factor)
    # _, _, I_B[Nnew] = take_intermediate_points_2D(
    #     angles[N], angles[N], I_B[N], factor)
    # _, _, I_C[Nnew] = take_intermediate_points_2D(
    #     angles[N], angles[N], I_C[N], factor)
    # Nnew, factor = (9, 3 * np.ones(2))
    # angles[Nnew], _, I_A[Nnew] = take_intermediate_points_2D(
    #     angles[N], angles[N], I_A[N], factor)
    # _, _, I_B[Nnew] = take_intermediate_points_2D(
    #     angles[N], angles[N], I_B[N], factor)
    # _, _, I_C[Nnew] = take_intermediate_points_2D(
    #     angles[N], angles[N], I_C[N], factor)

    N = 23
    Nnew, factor = (12, 2 * np.ones(2))
    angles[Nnew], _, I_A[Nnew] = take_intermediate_points_2D(
        angles[N], angles[N], I_A[N], factor)
    _, _, I_B[Nnew] = take_intermediate_points_2D(angles[N], angles[N], I_B[N],
                                                  factor)
    _, _, I_C[Nnew] = take_intermediate_points_2D(angles[N], angles[N], I_C[N],
                                                  factor)

    N = 19
    Nnew, factor = (10, 2 * np.ones(2))
    angles[Nnew], _, I_A[Nnew] = take_intermediate_points_2D(
        angles[N], angles[N], I_A[N], factor)
    _, _, I_B[Nnew] = take_intermediate_points_2D(angles[N], angles[N], I_B[N],
                                                  factor)
    _, _, I_C[Nnew] = take_intermediate_points_2D(angles[N], angles[N], I_C[N],
                                                  factor)

    # Precrear variables
    error_abs_A = []
    error_abs_B = []
    error_abs_C = []
    error_abs_all = []
    error_rel_A = []
    error_rel_B = []
    error_rel_C = []
    error_rel_all = []
    par_A = []
    par_B = []
    par_C = []
    par_all = []
    N_array = []
    N_array_all = []
    par0 = [0, 0, 0, 0, 1]
    min_limit = [0, 0, 0, 0, 0]
    max_limit = [180 * degrees, 180 * degrees, 180 * degrees, 180 * degrees, 6]
    jacobian = '3-point'
    print('Nmax is {}'.format(Nmax - 1))

    # We need a reference. Take the one with max points
    result_0A = optimize.least_squares(
        error_step_5b,
        par0,
        bounds=(min_limit, max_limit),
        jac=jacobian,
        args=(angles[Nmax - 1], angles[Nmax - 1], I_A[Nmax - 1], 'A'))
    result_0B = optimize.least_squares(
        error_step_5b,
        par0,
        bounds=(min_limit, max_limit),
        jac=jacobian,
        args=(angles[Nmax - 1], angles[Nmax - 1], I_B[Nmax - 1], 'B'))
    result_0C = optimize.least_squares(
        error_step_5b,
        par0,
        bounds=(min_limit, max_limit),
        jac=jacobian,
        args=(angles[Nmax - 1], angles[Nmax - 1], I_C[Nmax - 1], 'C'))
    result_0all = optimize.least_squares(
        error_step_5b_all,
        par0,
        bounds=(min_limit, max_limit),
        jac=jacobian,
        args=(angles[Nmax - 1], angles[Nmax - 1], I_A[Nmax - 1], I_B[Nmax - 1],
              I_C[Nmax - 1]))

    # Main Loop
    for ind in range(Nmax):
        if (angles[ind] == 0).all():
            pass
        else:
            print('Current N is {}'.format(ind))
            N_array.append(ind**2)

            # Modalidad A
            ImaxA = I_A[ind].max()
            error_aux = error_step_5b(result_0A.x, angles[ind], angles[ind],
                                      I_A[ind], 'A')
            error_abs_A.append(np.linalg.norm(error_aux) / (ind**2 * ImaxA))

            if ind == Nmax - 1:
                result = result_0A
            else:
                result = optimize.least_squares(
                    error_step_5b,
                    par0,
                    bounds=(min_limit, max_limit),
                    jac=jacobian,
                    args=(angles[ind], angles[ind], I_A[ind], 'A'))
            error_aux = error_step_5b(result.x, angles[ind], angles[ind],
                                      I_A[ind], 'A')
            error_rel_A.append(np.linalg.norm(error_aux) / (ind**2 * ImaxA))

            par_A.append(result.x - result_0A.x)

            # Modalidad B
            ImaxB = I_B[ind].max()
            error_aux = error_step_5b(result_0B.x, angles[ind], angles[ind],
                                      I_B[ind], 'B')
            error_abs_B.append(np.linalg.norm(error_aux) / (ind**2 * ImaxB))

            if ind == Nmax - 1:
                result = result_0B
            else:
                result = optimize.least_squares(
                    error_step_5b,
                    par0,
                    bounds=(min_limit, max_limit),
                    jac=jacobian,
                    args=(angles[ind], angles[ind], I_B[ind], 'B'))
            error_aux = error_step_5b(result.x, angles[ind], angles[ind],
                                      I_B[ind], 'B')
            error_rel_B.append(np.linalg.norm(error_aux) / (ind**2 * ImaxB))

            par_B.append(result.x - result_0B.x)

            # Modalidad C
            ImaxC = I_C[ind].max()
            error_aux = error_step_5b(result_0C.x, angles[ind], angles[ind],
                                      I_C[ind], 'C')
            error_abs_C.append(np.linalg.norm(error_aux) / (ind**2 * ImaxC))

            if ind == Nmax - 1:
                result = result_0C
            else:
                result = optimize.least_squares(
                    error_step_5b,
                    par0,
                    bounds=(min_limit, max_limit),
                    jac=jacobian,
                    args=(angles[ind], angles[ind], I_C[ind], 'C'))
            error_aux = error_step_5b(result.x, angles[ind], angles[ind],
                                      I_C[ind], 'C')
            error_rel_C.append(np.linalg.norm(error_aux) / (ind**2 * ImaxC))

            par_C.append(result.x - result_0C.x)

            # Modalidad all
            ImaxAll = np.array([ImaxA, ImaxB, ImaxC]).max()
            error_aux = error_step_5b_all(result_0all.x, angles[ind],
                                          angles[ind], I_A[ind], I_B[ind],
                                          I_C[ind])
            error_abs_all.append(
                np.linalg.norm(error_aux) / (3 * ind**2 * ImaxAll))

            result = optimize.least_squares(
                error_step_5b_all,
                par0,
                bounds=(min_limit, max_limit),
                jac=jacobian,
                args=(angles[ind], angles[ind], I_A[ind], I_B[ind], I_C[ind]))
            error_aux = error_step_5b_all(result.x, angles[ind], angles[ind],
                                          I_A[ind], I_B[ind], I_C[ind])
            error_rel_all.append(
                np.linalg.norm(error_aux) / (3 * ind**2 * ImaxAll))

            # N_array_all.append(3 * ind**2)
            par_all.append(result.x - result_0all.x)

    error_abs = [
        np.array(error_abs_A),
        np.array(error_abs_B),
        np.array(error_abs_C),
        np.array(error_abs_all)
    ]
    error_rel = [
        np.array(error_rel_A),
        np.array(error_rel_B),
        np.array(error_rel_C),
        np.array(error_rel_all)
    ]
    param = [
        np.array(par_A),
        np.array(par_B),
        np.array(par_C),
        np.array(par_all)
    ]
    N_array = np.array(N_array)
    # N_array_all = np.array(N_array_all)
    # final
    end_time = time.time()
    print('Elapsed time is {} s.'.format(end_time - start_time))
    return N_array, error_abs, error_rel, param


def Paso_6a(cal_file='Last_cal.npz'):
    start_time = time.time()
    # Ir a la carpeta correspondiente
    # path = r'D:\Codigo_UCM\polaripython\calibration\Calibration_2019-07-15'
    path = r'D:\codigo\polaripython\calibration\Calibration_2019-07-15'
    os.chdir(path)
    # Cargar archivos
    filename = 'Step_6a_2019-07-15.npz'
    data = np.load(filename)
    angles = data['angles']
    Iexp = data['Iexp']

    # Reconstruct polarimeter
    # path = r'D:\Codigo_UCM\polaripython\polaripython'
    path = r'D:\codigo\polaripython\polaripython'
    os.chdir(path)
    data = np.load(cal_file)
    # print(data['polarimeter'].item())
    polarimeter = data['polarimeter'].item()
    # print(polarimeter)

    # Prepare the calibration dictionary
    cal_dict = {}
    try:
        cal_dict['M_test'] = polarimeter['M']
    except:
        cal_dict['M_test'] = [
            polarimeter['Mp1'], polarimeter['Mr1'], polarimeter['Mr2'],
            polarimeter['Mp2']
        ]
    cal_dict['Ifuente_test'] = polarimeter['Ifuente']
    cal_dict['angles_polarimeter'] = angles
    cal_dict['intensity_polarimeter'] = Iexp

    cal_dict["fit completed"] = False
    cal_dict['type'] = 'calibration'
    cal_dict['plot_intensity'] = False
    cal_dict['is_vacuum'] = True
    cal_dict['save_analysis'] = False
    cal_dict['meas_name'] = None
    cal_dict['tolerance'] = 1e-6
    cal_dict['odd_number_refs'] = False

    # We need a reference. Take the one with max points
    cal_dict, Mfiltered, results_dict = Analysis_Measurement_0D(
        cal_dict, type_output='all', verbose=False, decompose=False)
    error_step_6a = results_dict['error_filter']
    print('The reference error ir: {}'.format(error_step_6a))

    # Create the variables for the Loops
    Npoints = np.arange(20, 205, 5)
    # Npoints = np.array([16])
    # Npoints[0] = 16
    # Npoints[37:42] = (196, 197, 198, 199, 200)
    Nrepeat = 50
    error = np.zeros(Npoints.size)
    error_dev = np.zeros(Npoints.size)

    # Main Loop
    for ind, N in enumerate(Npoints):
        # print('Iteration {} with {} number of points.'.format(ind, N))
        # Take away the case with maximum points
        if N >= 200:
            error[ind] = error_step_6a
        else:
            # Make a second loop for repetitions with the same number of points
            for ind_rep in range(Nrepeat):
                # Initialize the auxiliar error array for the first iteration
                if ind_rep == 0:
                    error_rep = np.zeros(Nrepeat)
                # Create the array with intensities and angles
                indices = np.random.choice(200, N, replace=False)
                # print(np.unique(indices, return_counts=True))
                cal_dict['angles_polarimeter'] = angles[indices]
                cal_dict['intensity_polarimeter'] = Iexp[indices]
                # Calculate
                try:
                    cal_dict, Mfiltered, results_dict = Analysis_Measurement_0D(
                        cal_dict,
                        type_output='all',
                        verbose=False,
                        decompose=False)
                    error_rep[ind_rep] = results_dict['error_filter']
                except:
                    error_rep[ind_rep] = np.nan
                    print('Calculation failed!!!')
            # Fix the nans
            error_rep = error_rep[~np.isnan(error_rep)]
            # print(error_rep)
            # Calculate the error and its deviation
            error[ind] = np.mean(error_rep)
            error_dev[ind] = np.std(error_rep)
            # print(error)
            # print(error_dev)

    # final
    end_time = time.time()
    print('Elapsed time is {} s.'.format(end_time - start_time))
    return Npoints, error, error_dev


def final():
    pass
