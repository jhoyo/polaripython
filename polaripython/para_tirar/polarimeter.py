# -*- coding: utf-8 -*-
"""Main module."""
import scipy as sp
from phyton_optics import um, degrees

from phyton_optics.polarization_stokes import (
    intensity, polarized_light, polarizer_linear, quarter_waveplate, rotate_mueller,
    retarder, rotation, diattenuating_retarder)

degrees_motor = 1


def get_light_parameters():
    """
    Returns the experimental value of incident light

    Parameters of polarized light
        amplitude = 1.8823
        angle = 45.199 * degrees
        phase = 89.796 * degrees

    """

    amplitude = 1.8823
    angle = 45.199 * degrees
    phase = 89.796 * degrees

    u0 = polarized_light(amplitude, angle, phase, 1)

    return [amplitude, angle, phase], u0


def get_polarimeter_parameters(kind):
    """returns parameters of polarizers

    Args:
        kind (str): 'ideal': ideal parameters
                    'real': computed with callibration, no se muy bien ahora
                    'exp': computed with callibration
    """

    p0_best = 0.128856857551
    p1_best = 0.935181526985

    p0_best = 0.129255330498
    p0_best = 0.939566314435

    p0_best = 0.12810158426
    p1_best = 0.934711947067

    p0_best = 0.128068490426
    p1_best = 0.934950416021

    theta_1 = -2.788 * degrees
    theta_2 = -44.5766 * degrees
    theta_3 = -131.763 * degrees
    theta_4 = -161.88 * degrees

    delta_best = 79.38 * degrees  #86 * degrees
    RD_p0_best = 1
    RD_p1_best = 1

    if kind == 'ideal':
        # Ideal Polarizers
        pol1_ideal = dict(theta_axis=0., p0=0., p1=1)
        pol2_ideal = dict(theta_axis=0., a=1., b=1., delta=sp.pi / 2)
        pol3_ideal = dict(theta_axis=0., a=1., b=1., delta=sp.pi / 2)
        pol4_ideal = dict(theta_axis=0., p0=0., p1=1)

        config_dicts = [pol1_ideal, pol2_ideal, pol3_ideal, pol4_ideal]

    elif kind == 'real':
        # PAra cálculo numérico
        pol1_real = dict(theta_axis=0., p0=p0_best, p1=p1_best)
        pol2_real = dict(
            theta_axis=0., a=RD_p0_best, b=RD_p1_best, delta=delta_best)
        pol3_real = dict(
            theta_axis=0., a=RD_p0_best, b=RD_p1_best, delta=delta_best)
        pol4_real = dict(theta_axis=0., p0=p0_best, p1=p1_best)

        config_dicts = [pol1_real, pol2_real, pol3_real, pol4_real]

    elif kind == 'exp':
        # 14/03/2018
        # polarimeter/calibration/cal_2pol_1waveplate.py
        pol1_exp = dict(theta_axis=theta_1, p0=p0_best, p1=p1_best)
        pol2_exp = dict(
            theta_axis=theta_2, a=RD_p0_best, b=RD_p1_best, delta=delta_best)
        pol3_exp = dict(
            theta_axis=theta_3, a=RD_p0_best, b=RD_p1_best, delta=delta_best)
        pol4_exp = dict(theta_axis=theta_4, p0=p0_best, p1=p1_best)

        config_dicts = [pol1_exp, pol2_exp, pol3_exp, pol4_exp]

    return config_dicts


def polarimeter_matrices(config_dict, angles=None):
    """ returns matrices of polarimeters

    Args:
        config_parameters (list): list with dictionaries of parameters
        angles (list): If not None rotation angles of polarizers,
                       If is  None rotation angles are those in dictionary

    Returns:
        list of 4x4 Mueller matrices
    """

    #dictionaries, one for each polarizer
    [pol1, pol2, pol3, pol4] = config_dict
    if angles is None:
        angles = [0., 0., 0., 0.]

    P1 = polarizer_linear(p1=pol1['p1'], p2=pol1['p0'], theta=angles[0])
    P2 = diattenuating_retarder(
        p1=pol2['a'], p2=pol2['b'], phase=pol2['delta'], theta=angles[1])
    P3 = diattenuating_retarder(
        p1=pol3['a'], p2=pol3['b'], phase=pol3['delta'], theta=angles[2])
    P4 = polarizer_linear(p1=pol4['p1'], p2=pol4['p0'], theta=angles[3])

    return P1, P2, P3, P4


def rotate_polarimeter(Polarizers, angles):
    """we have several mueller matrices at $theta=0$ and rotate a certain angle.

    Args:
        Polarizers (list of 4x4 Mueller matrices):
        angles (float, float, float, float): angle of rotation

    Returns:
        Polarizers (list of 4x4 Mueller matrices): rotated

    """
    P = []
    for i in range(len(Polarizers)):
        P.append(rotation(-angles[i]) * Polarizers[i] * rotation(angles[i]))

    return P


def polarimeter_matrix(Polarizers, is_present, angles, u0):
    """Generates a simulated intensity at the output of the polarimeter
    for a given "experiment":

    Args:
        Ps_0 (list of numpy.matrix): List with 4 4x4 numpy.matrix Mueller of 4 Polarizers at $\theta=0$
        is_present (list of bools): indicates if polarizer is present.
        angles (list of floats): angles of the 4 polarimeters
        incident_light (4x1 numpy.matrix): Stokes parameters of incident beam

    Returns:
        float: intensity if u0 is present
        4x4 numpy.matrix: Mueller of the polarizer
    """

    Polarizers2 = rotate_polarimeter(Polarizers, angles)

    M = Polarizers2[0]
    # print(Polarizers2[0])

    for i in range(1, len(Polarizers2)):
        # print(i)
        if is_present[i] is True:
            M = Polarizers2[i] * M
            # print(Polarizers2[i])

    if u0 is not None:
        intensity_0 = intensity(M * u0)
    else:
        intensity_0 = None

    return intensity_0, M


def polarimeter_matrices_parameters(par):
    """Se introducen los parámetros que definen las matrices y devuelve las matrices
    todo sin ángulos, para ángulo 0
        p0, p1, a, b, delta = par
    """
    p0, p1, a, b, delta = par
    # p0 = 0.128242248318
    #p1 = 0.946436027636
    # theta_4 = 70.44493761769274

    pol1 = dict(theta_axis=0., p0=p0, p1=p1)
    pol2 = dict(theta_axis=0., p0=a, p1=b, delta=delta)
    pol3 = dict(theta_axis=0., p0=a, p1=b, delta=delta)
    pol4 = dict(theta_axis=0., p0=p0, p1=p1)

    pol_dict = [pol1, pol2, pol3, pol4]
    P = polarimeter_matrices(pol_dict, angles=None)
    # pprint(P)
    return P
