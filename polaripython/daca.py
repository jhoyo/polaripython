# -*- coding: utf-8 -*-

from __future__ import print_function

import u3, LabJackPython
import numpy as np

V = 1.
mV = V / 1000


def get_intensity(u, d, AIN_number=1, verbose=True):
    ain1bits, = d.getFeedback(
        u.AIN(AIN_number))  # Read from raw bits from AIN0
    ainValue = d.binaryToCalibratedAnalogVoltage(
        ain1bits, isLowVoltage=False, channelNumber=0)
    if verbose is True:
        print("{:2.4f}".format(ainValue), end=', ')
    return ainValue
