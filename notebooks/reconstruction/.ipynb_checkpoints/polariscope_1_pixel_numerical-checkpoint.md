
# Table of Contents
 <p><div class="lev1 toc-item"><a href="#Polarimetry-with-Ecograb-polarimeter" data-toc-modified-id="Polarimetry-with-Ecograb-polarimeter-1"><span class="toc-item-num">1&nbsp;&nbsp;</span>Polarimetry with Ecograb polarimeter</a></div><div class="lev2 toc-item"><a href="#Definitions-of-variables-for-the-problem" data-toc-modified-id="Definitions-of-variables-for-the-problem-11"><span class="toc-item-num">1.1&nbsp;&nbsp;</span>Definitions of variables for the problem</a></div><div class="lev2 toc-item"><a href="#Definitions-of-matrices" data-toc-modified-id="Definitions-of-matrices-12"><span class="toc-item-num">1.2&nbsp;&nbsp;</span>Definitions of matrices</a></div>

# Polarimetry with Ecograb polarimeter
**Author** Luis Miguel Sánchez Brea

**Date** 14/12/2017

The objective of this notebook is use numerical Mueller Matrices, defined in phyton_optics.polarization_stokes in order to determine the intensity distribution for a polarimeter (P1, R1, S, R1, P2), where P1 and P2 are polarimeters, R1 and R2 are retarders, and S is a (4x4) matrix unknon sample. This sample is defined in polarization_states_generator.py. 
After the analysis, the results are implemented in analysis.py

Next analysis:
- use of image (matrices) for the object.



## Definitions of variables for the problem

For this problem, perhaps all these defintions should go in a .py module (with sympy)


```python
# Defintion of modules

%matplotlib notebook
from phyton_optics import sp, np, plt, degrees, um, mm
import phyton_optics.polarization_stokes as polarization
from phyton_optics.polarization_stokes import polarizer_linear, quarter_waveplate, light, intensity

```


```python
# Definition of matrices
angles= [0*degrees, 0*degrees, 0*degrees, 0*degrees]

incident_light= polarized_light(amplitude=1, angle=45*degrees, phase=0, p=1)

P1 =  polarizer_linear(p1=1, p2=0, theta=angles[0])

P2 = quarter_waveplate(theta=angles[1])

P3 = quarter_waveplate(theta=angles[2])

P4 =  polarizer_linear(p1=1, p2=0, theta=angles[3])

sample = polarizer_linear(p1=1, p2=0, theta=0*degrees)
```


```python
# Example of Mueller matrix of the system (included sample)
system=P4*P3*sample*P2*P1
```


```python
print P1
print P2
print P3
print P4
```

    [[ 0.5  0.5  0.   0. ]
     [ 0.5  0.5  0.   0. ]
     [ 0.   0.   0.   0. ]
     [ 0.   0.   0.   0. ]]
    [[ 1  0  0  0]
     [ 0  1  0  0]
     [ 0  0  0  1]
     [ 0  0 -1  0]]
    [[ 1  0  0  0]
     [ 0  1  0  0]
     [ 0  0  0  1]
     [ 0  0 -1  0]]
    [[ 0.5  0.5  0.   0. ]
     [ 0.5  0.5  0.   0. ]
     [ 0.   0.   0.   0. ]
     [ 0.   0.   0.   0. ]]



```python
# Output of light
output_light=system*incident_light

# This is the only known output parameter of the system
I = intensity(output_light)
print(I)
```

    0.5


## Paso todo a una función: polarization states generator 


```python
# an example:
angles= [0*degrees, 0*degrees, 0*degrees, 0*degrees]
incident_light= polarized_light(amplitude=1, angle=45*degrees, phase=0, p=1)
sample = polarizer_linear(p1=1, p2=0, theta=45*degrees)


def polarization_states_generator(angles, sample, incident_light):
    """generates a simulated intensity at the output of the polariemter
    
    Args:
        angles (float, float, float, float): angles of the 4 polarimeters
        sample (4x4 numpy.matrix): Mueller matrix of sample (unknown)
        incident_light (4x1 numpy.matrix): Stokes parameters of incident beam
    """
    P1 =  polarizer_linear(p1=1, p2=0, theta=angles[0])
    P2 = quarter_waveplate(theta=angles[1])
    P3 = quarter_waveplate(theta=angles[2])
    P4 =  polarizer_linear(p1=1, p2=0, theta=angles[3])
    
    #Generator and analyzer
    G = (P2*P1*incident_light)
    A = P4*P3
    a_first_line=A[0,:]
    b=(G*a_first_line).flatten()

    # I1=intensity(P4*P3*sample*P2*P1*incident_light)
    I1=intensity(A*sample*G)

    return I1, b, G, a_first_line

```


```python
print(sample)
```

    [[ 0.5  0.   0.5  0. ]
     [ 0.   0.   0.   0. ]
     [ 0.5  0.   0.5  0. ]
     [ 0.   0.   0.   0. ]]



```python
# Example of use
I0, b, G, A=polarization_states_generator(angles, sample, incident_light)
print I0
print G, A
print b.T

print G.shape
print A.shape

print b.shape

```

    0.125
    [[ 0.5]
     [ 0.5]
     [ 0. ]
     [ 0. ]] [[ 0.5  0.5  0.   0. ]]
    [[ 0.25]
     [ 0.25]
     [ 0.  ]
     [ 0.  ]
     [ 0.25]
     [ 0.25]
     [ 0.  ]
     [ 0.  ]
     [ 0.  ]
     [ 0.  ]
     [ 0.  ]
     [ 0.  ]
     [ 0.  ]
     [ 0.  ]
     [ 0.  ]
     [ 0.  ]]
    (4, 1)
    (1, 4)
    (1, 16)


## Experiment: random angles -> n intensity values
In this example we have n_measurements at random (known) angles and the system generates the intensity for this positions. These data will be used for determination of mueller matrix. The minimum number of data is 16, since we need to measure a 4x4 Mueller matrix.


```python
incident_light= polarized_light(amplitude=1, angle=0*degrees, phase=0, p=1)
sample = polarizer_linear(p1=1, p2=0, theta=45*degrees)
n_measurements=54
Angles=2*sp.pi*np.random.rand(n_measurements,4)
```

## Recovery of Mueller Matrix from "experimental intensities"
The technique implemented here is explained in:
**
We have an unknown matrix of the sample:

$M=\left[\begin{array}{cccc}
m_{00} & m_{01} & m_{02} & m_{03}\\
m_{10} & m_{11} & m_{12} & m_{13}\\
m_{20} & m_{21} & m_{22} & m_{23}\\
m_{30} & m_{31} & m_{32} & m_{33}
\end{array}\right]$

The incident light is 

$u=\left[\begin{array}{c}
u_{0}\\
u_{1}\\
u_{2}\\
u_{3}
\end{array}\right]$

The generator produced a stokes beam obtained as:

$g=P_2*P_1*u_0=\left[\begin{array}{c}
g_{0}\\
g_{1}\\
g_{2}\\
g_{3}
\end{array}\right]$

Since we only determine intensity distribution, from the analyzer we only need the first column: 

$A=P_4*P_3=\left[\begin{array}{cccc}
a_{00} & a_{01} & a_{02} & a_{03}\\
a_{10} & a_{11} & a_{12} & a_{13}\\
a_{20} & a_{21} & a_{22} & a_{23}\\
a_{30} & a_{31} & a_{32} & a_{33}
\end{array}\right]=\left[\begin{array}{cccc}
a_{0} & a_{1} & a_{2} & a_{3}\\
\bullet & \bullet & \bullet & \bullet\\
\bullet & \bullet & \bullet & \bullet\\
\bullet & \bullet & \bullet & \bullet
\end{array}\right]$

As a consequence: 

$\left[\begin{array}{c}
I\\
\bullet\\
\bullet\\
\bullet
\end{array}\right]=\left[\begin{array}{cccc}
a_{0} & a_{1} & a_{2} & a_{3}\\
\bullet & \bullet & \bullet & \bullet\\
\bullet & \bullet & \bullet & \bullet\\
\bullet & \bullet & \bullet & \bullet
\end{array}\right]\left[\begin{array}{cccc}
m_{00} & m_{01} & m_{02} & m_{03}\\
m_{10} & m_{11} & m_{12} & m_{13}\\
m_{20} & m_{21} & m_{22} & m_{23}\\
m_{30} & m_{31} & m_{32} & m_{33}
\end{array}\right]\left[\begin{array}{c}
g_{0}\\
g_{1}\\
g_{2}\\
g_{3}
\end{array}\right]
$

which is the theoretical analysis of the previous equations.

As a consequence, the intensity distribution is obtained as:

$I_{k}=\sum_{ij} a_{i}^{k}g_{j}^{k}m_{ij}$.

This problem can be stated as a linear equation: $A*M=I$, where I is a vector of K intensity  values,  A is (K x 16) matrix and M is a 16 elements array.


If we have K=16 measurements the problem is easily solved as 

$M= B^{-1} I$

which has an unique solution. If we have more than 16 experimental data (K>16) then we use generalized inverse problem:

$M=(B^T B)^{-1} B^T I$



```python
I=sp.zeros(n_measurements, dtype=float)
B=sp.zeros((n_measurements,16), dtype=float)
for i in range(n_measurements):
    angles=Angles[i,:]
    I[i],B[i,:],_,_=polarization_states_generator(angles, sample, incident_light)

    
I=np.matrix(I).T
B=np.matrix(B)


noise=.001*np.random.randn(len(I),1)

I=I+noise

detected_sample=(((B.T*B).I * B.T ) *  I).reshape(4,4)
```


```python
print(detected_sample)
# This are the data that we would obtain from the polarimeter (for 1 pixel)
sp.mean(abs(detected_sample-sample))

```

    [[ 0.50035546  0.00122203  0.50042837 -0.0006495 ]
     [ 0.0008879  -0.00173387 -0.00238635 -0.00114976]
     [ 0.50093752  0.00339975  0.50348084  0.00132252]
     [ 0.00105047  0.00186866 -0.00001683 -0.00056671]]





    0.001341033772501206




```python

```
