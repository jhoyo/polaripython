# Procedimiento de calibración del polarímetro

**Estabilidad de la fuente**

  -  *Procedimiento*: Se realizan diversas medidas para una cierta corriente de la fuente para verificar su estabilidad y valor medio.
  
  - *Estado del equipo*: No se utilizan polariadores.
  
  - *Resultado*:
      - Valor medio de la intensidad: **Voltaje = 3.45107 V.**
      - RMS of intensity distribution: despreciable

**Estado de polarización de la fuente**

 - *Procedimiento*: Estamos en el caso de unl diodo láser. La luz está lineamente polarizada y pasa por una lamina retardadora $\lambda/4$, por lo que a su salida la fuente tiene que ser aproximadamente circulamente polarizada.
 
 	 Por ello, al pasar por un polarizador lineal, tiene que fluctuar poco. Se introduce un polarizador lineal (P1) después de la fuente y se gira 360º.

 - *Estado del equipo*:
  	1. Diodo láser de Roithner .... de longitud de onda $\lambda$=850 nm (corriente = 314 mA).
  	1. Lente colimadora de Thorlabs DG10-600-B.
  	1. Difusor.
  	1. Lámina retardadora $\lambda/4$ ... de Throlabs para hacer el haz  	de luz circular.
  	1. Polarizador 1
	1. Se gira el polarizador una vuelta para ver el rizado de la fuente. 


 - *Resultado*: Vector de Stokes de la fuente de luz

	amplitude = 1.8823 V,  angle = 45.199º,   phase = 89.796º.
	
	Parámetros de stokes:
	Stokes parameters = 
		[ 3.54305329,
		  -0.0246113,
		   0.01261461,
		   3.54294535]	

	Como necesitamos el conocimiento de polarizador 1 tomamos:
        P1 = polarizer_linear(p1=.95, p2=0.20, theta=angle)


.
**Ángulo de los polarizadores lineales**

 - *Procedimiento*: Necesitamos una referencia *absoluta* para conocer el estado de polarización del primer polarizador.
Todavía no sabemos los parámetros, que se calculan en el siguiente paso. 
Aquí se pretende calcular el ángulo donde pasa toda la luz a través de los dos polarizadores, polarizador 1 o 4, y de calibración.
	1. Se gira el polarizador una vuelta para buscar la posición de máxima intensidad. 

 - *Estado del equipo*:

	1. Diodo láser de Roithner .... de longitud de onda $\lambda$=850 nm (corriente = 314 mA).
	1. Lente colimadora de Thorlabs DG10-600-B.
	1. Difusor.
	1. Lámina retardadora $\lambda/4$ ... de Throlabs para hacer el haz de luz circular.
	1. Polarizador de calibración
	1. Polarizador 1 o polarizador 4

 - *Resultado*: 
 	Polarizador 1: Cuando P1 está alineado con horizontal, el motor muestra 2.788º.
 
  	Polarizador 4: Cuando P4 está alineado con horizontal, el motor muestra 161.88º



   -  Linear polarizers can be studied with Jones formalism, but are not ideal .
   -  *Procedimiento*
      * 1  No polarizer, check intensity
      * 2  Polarizer P1 and polarizer P2. Move P2 360 degrees.
      * 3  Perform fit to Malus law. Determine minimum and maximum intensity
      * 4  Determine angle of maximum intensity (polarizers aligned).
    - **Parameters to measure**
      * I0 - maximum intensity without polarizers
      * Imin - minimum intensity of Malus law
      * Imax - maximum intensity of Malus Law
    - Results: (p0, p1, theta)
      * Data on 2018/02/14 - **p0 = 0.0653499037607, p1 = 0.758623923874, theta_4= 70.306º**

*  Parameters of $\lambda/4$ polarizers.
-  *Procedimiento*
