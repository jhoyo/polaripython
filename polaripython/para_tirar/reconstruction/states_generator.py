# !/usr/bin/env python
# -*- coding: utf-8 -*-

# ----------------------------------------------------------------------
# Name:      analysis
# Purpose:   Ecograb: analysis of polarimetry
#
# Author:    Luis Miguel Sanchez Brea
#
# Created:    2011
# RCS-ID:
# Copyright:
# Licence:    GPL
# ----------------------------------------------------------------------
"""
In this class generate K "theorical" images for analysis of polarization.
We assume we have a 4-rotators polarimeter and a vectorial sample. We give
the 4-vector positions of rotators for each image and returns the images

For the generation of polarization we use the article
E.A. Patterson "Digital photoelasticity: principles, practice and potential
Strain (2002) 38, 27-39"
"""

from phyton_optics import degrees, mm, nm, np, plt, sp, um
from phyton_optics.scalar_fields_XY import Scalar_field_XY
from phyton_optics.scalar_sources_XY import Scalar_source_XY
from phyton_optics.scalar_masks_XY import Scalar_mask_XY

from phyton_optics.vector_fields_XY import Vector_field_XY
from phyton_optics.vector_sources_XY import Vector_source_XY
from phyton_optics.vector_masks_XY import Vector_mask_XY

import phyton_optics.polarization_stokes as polarization
from phyton_optics.polarization_stokes import (
    intensity, polarized_light, polarizer_linear, quarter_waveplate, rotate_mueller)


def intensity_polarimeter(Ps_0, angles, sample, u0):
    """Generates a simulated intensity at the output of the polarimeter for a given "experiment":

    Args:
        Ps_0 (list): List with 4 4x4 numpy.matrix Mueller of 4 Polarizers at $\theta=0$
        angles (float, float, float, float): angles of the 4 polarimeters
        sample (4x4 numpy.matrix): Mueller matrix of sample (unknown)
        incident_light (4x1 numpy.matrix): Stokes parameters of incident beam
    """
    P1_0, P2_0, P3_0, P4_0 = Ps_0

    P1 = rotate_mueller(P1_0, angles[0])
    P2 = rotate_mueller(P2_0, angles[1])
    P3 = rotate_mueller(P3_0, angles[2])
    P4 = rotate_mueller(P4_0, angles[3])

    #Generator and analyzer
    G = (P2 * P1 * u0)
    A = (P4 * P3)[0, :]  # a_first_line

    # I1=intensity(P4*P3*sample*P2*P1*u0)

    return intensity(A * sample * G)


class Polarization_states_generator(object):
    """
    Class for simulation of a polarimeter.
    Angles of retarders and polarizers is given and intensity distributions are provided.
    In this class, Jones matrices are used. It is not so general as Muller matrices,
    but it is easy to multiply and even to propagate fields."""

    def __init__(self, x, y, wavelength, num_images):
        """Se inicia un new experimento"""
        self.x = x
        self.y = y
        self.wavelength = wavelength  # Longitud de onda
        self.vmask_XY = None
        self.num_images = num_images
        self.angles = sp.zeros((self.num_images, 4), dtype=float)
        self.light = None
        self.images = None

        # Se genera un mallado
        # self.X, self.Y = sp.meshgrid(x, y)
        # Se genera el field
        # self.u = sp.zeros(sp.shape(self.X), dtype=complex)

    def set_mask(self, vmask_XY):
        """
        define the image
        """
        self.vmask_XY = vmask_XY

    def set_light(self, stokes):
        self.light = stokes

    def set_angles(self, angles):
        """
        angular positions of polarizers that simulate the locations
        """
        self.angles = angles

    def set_images(self):
        """
        generate intensity images simulating polariscope
        """

        P1 = Vector_mask_XY(self.x, self.y, self.wavelength)
        R1 = Vector_mask_XY(self.x, self.y, self.wavelength)
        R2 = Vector_mask_XY(self.x, self.y, self.wavelength)
        P2 = Vector_mask_XY(self.x, self.y, self.wavelength)

        images = []

        # loop over num_images
        for angles_i in self.angles:
            # get angles in position
            theta1, theta2, theta3, theta4 = angles_i

            P1.polarizer_linear(theta1)
            R1.quarter_waveplate(theta2)
            R2.quarter_waveplate(theta3)
            P2.polarizer_linear(theta4)
            incident_light = self.light

            print P1.Ex[0, 0], P1.Ey[0, 0]

            output_field = P2 * R2 * self.vmask_XY * R1 * P1  # *incident_light # falta
            intensity = output_field.compute_intensity()

            images.append(intensity)

        self.images = images
        print images

    def save_images(self, filename):
        """save images to"""
        pass
