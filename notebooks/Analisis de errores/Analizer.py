from __future__ import print_function
import copy_reg
import types
from multiprocessing import Pool

import time
import datetime
import pprint

from phyton_optics import degrees, plt

from scipy import optimize, pi, sin, cos
from math import sqrt

from polarimeter.utils import plot_2d, generate_even_distribution

from matplotlib.colors import LogNorm
from matplotlib import ticker, colors

from polarimeter.polarimeter import Intensity_Rotating_Elements
import phyton_optics.polarization_jones as pj
import phyton_optics.polarization_stokes as ps
import phyton_optics.polarization_mueller_analysis as amm
import polarimeter.polarimeter as opsys
import phyton_optics.utils_multiprocessing as multiproc
import numpy as np
from itertools import product as prod


def _pickle_method(method):
    func_name = method.im_func.__name__
    obj = method.im_self
    cls = method.im_class
    if func_name.startswith(
            '__') and not func_name.endswith('__'):  # deal with mangled names
        cls_name = cls.__name__.lstrip('_')
        func_name = '_' + cls_name + func_name
    return _unpickle_method, (func_name, obj, cls)


def _unpickle_method(func_name, obj, cls):
    for cls in cls.__mro__:
        try:
            func = cls.__dict__[func_name]
        except KeyError:
            pass
        else:
            break
    return func.__get__(obj, cls)


copy_reg.pickle(types.MethodType, _pickle_method, _unpickle_method)


class auxiliar_multiprocessing(object):
    def __init__(self):
        pass
        #self.result = self.go()

    # Method that executes the multiprocessing
    def execute_multiprocessing(self,
                                function,
                                var_iterable,
                                dict_constants=dict(),
                                Ncores=8):
        # Store data in object
        print('Guardando datos')
        self.external_function = function
        self.dict_constants = dict_constants
        # Start multiprocessing if more than one core is required
        if Ncores > 1:
            pool = Pool(Ncores)
            print('Iniciando multiprocessing')
            result = pool.map(self.method_single_proc, var_iterable)
            print('Multiprocessing finalizado')
            pool.close()
            pool.join()
        # When only one core is asked, don't go to multiprocessing
        else:
            N = len(var_iterable)
            result = range(N)
            print('Iniciando el procesado con un solo nucleo')
            for ind, elem in enumerate(var_iterable):
                result[ind] = function(elem, dict_constants)

        # Save and extract resultado
        self.result = result
        return result

    def method_single_proc(self, elem_iterable):
        # Method that is called in each iteration of the multiprocessing
        return self.external_function(elem_iterable, self.dict_constants)


# Funcion que aborda cada caso particular. Esta es la funcion que llamara el metodo de
# multithreading, y solo puede tener un argumento
def funcion_single_thread(params, constantes):
    # Operaciones de inicio
    #print(constantes['verbose'])
    # Desacoplar los parametros
    (error, Nangulos) = params
    Nangulos = int(round(Nangulos))
    Naverages = constantes['Naverages']
    # Crear array con los resultados para despues promediar
    ajuste_az = np.zeros(Naverages)
    ajuste_elip = np.zeros(Naverages)
    ajuste_pol_degree = np.zeros(Naverages)
    ajuste_error = np.zeros(Naverages)
    no_solution = 0
    # Bucle para promediar
    for indAv in range(Naverages):
        # Matrices de Mueller
        p1 = constantes['min_p1'] + (
            constantes['max_p1'] - constantes['min_p1']) * np.random.rand(1)
        p2 = constantes['min_p2'] + (
            constantes['max_p2'] - constantes['min_p2']) * np.random.rand(1)
        (pR1, pR2) = constantes['min_pR'] + (
            constantes['max_pR'] - constantes['min_pR']) * np.random.rand(2)
        delta_R = constantes['min_delta_R'] + (
            constantes['max_delta_R'] -
            constantes['min_delta_R']) * np.random.rand(1)
        az = constantes['min_az'] + (
            constantes['max_az'] - constantes['min_az']) * np.random.rand(1)
        elip = constantes['min_elip'] + (
            constantes['max_elip'] - constantes['min_elip']) * np.random.rand(1)
        pol_degree = constantes['min_pol_degree'] + (
            constantes['max_pol_degree'] -
            constantes['min_pol_degree']) * np.random.rand(1)
        Mp = ps.polarizer_linear(p1, p2, theta=0)
        Mr = ps.Mueller_Real_Retarder(pR1, pR2, delta=delta_R, theta=0)
        M_sistema = [Mr, Mp]
        theta = np.linspace(constantes['min_angulo_th'],
                            constantes['max_angulo_th'], Nangulos)
        Illum = ps.general_Stokes(
            az, elip, pol_degree=pol_degree, I0=2, carac=False)
        # Crear el experimento "real"
        Intensity = opsys.Intensity_Rotating_Elements(M_sistema, [theta, theta],
                                                  Illum)

        # Anadir errores en los valores de intensidad
        max_intensity = Intensity.max()
        Intensity_error = Intensity + error * max_intensity * np.random.randn(
            Nangulos, Nangulos)
        # Ajustar el experimento con errores por minimos cuadrados
        par0 = [az, elip, pol_degree]
        par1, success = optimize.leastsq(
            constantes['Funcion_error'],
            par0,
            args=([theta, theta], M_sistema, Intensity_error,
                  constantes['Funcion_modelo']),
            epsfcn=0.01)
        #print(success)
        if success in range(1, 5):
            # Calcular el error en la distribucion
            residuos = error_modelo(par1, [theta, theta], M_sistema,
                                    Intensity_error,
                                    constantes['Funcion_modelo'])
            # Dar resultados
            ajuste_error[indAv] = np.linalg.norm(residuos) / (
                max_intensity * sqrt(Nangulos**2))
            ajuste_az[indAv] = abs(az - par1[0]) % pi
            ajuste_elip[indAv] = abs(elip - par1[1]) % (pi / 2)
            ajuste_pol_degree[indAv] = pol_degree - par1[2]
            # Imprimir resultados para comparar
            if constantes['verbose']:
                print(
                    'Parametros iniciales: azimuth = {} deg; elipticity = {} deg; polarization degree = {};'.
                    format(az / degrees, elip / degrees, pol_degree))
                print(
                    'Parametros ajustados: azimuth = {} deg; elipticity = {} deg; polarization degree = {};'.
                    format(par1[0] / degrees, par1[1] / degrees, par1[2]))
                print(
                    'Error medido: azimuth = {} deg; elipticity = {} deg; polarization degree = {};'.
                    format(ajuste_az[indAv] / degrees,
                           ajuste_elip[indAv] / degrees,
                           ajuste_pol_degree[indAv]))
                print('Residuos = {}; Ajustes sin solución = {};'.format(
                    ajuste_error[indAv], no_solution))
                print(" ")
        else:
            # Dar resultados
            no_solution = no_solution + 1
            # Imprimir resultados para comparar
            if constantes['verbose']:
                print('El ajuste fallo.')
    # Hacer media
    if no_solution < Naverages:
        resultado = [
            np.mean(ajuste_error) * Naverages / (Naverages - no_solution),
            np.linalg.norm(ajuste_az) / sqrt(Naverages - no_solution),
            np.linalg.norm(ajuste_elip) / sqrt(Naverages - no_solution),
            np.linalg.norm(ajuste_pol_degree) / sqrt(Naverages - no_solution),
            no_solution
        ]
        # Eliminar ceros
        for ind, elem in enumerate(resultado):
            if elem < constantes['tol_min:']:
                resultado[ind] = constantes['tol_min:']
    else:
        resultado = 1e6 * np.ones(5)
    # Imprimir resultados
    if constantes['verbose']:
        print(
            'Errores medios en param: azimuth = {} deg; elipticity = {} deg; polarization degree = {};'.
            format(resultado[1] / degrees, resultado[2] / degrees,
                   resultado[3]))
        print('Media en: residuos = {}; Ajustes sin solucion = {};'.format(
            resultado[0], resultado[4]))
        print(" ")
    return resultado


# Funciones de modelo
def modelo_teor(par, th1, M_sistema):
    # Create Jones matrices
    Illum = ps.general_Stokes(
        par[0], par[1], pol_degree=par[2], I0=2, carac=False)
    Iteor = Intensity_Rotating_Elements(M_sistema, th1, Ei=Illum)
    return Iteor


def error_modelo(par, th1, M_sistema, Ireal, Funcion_modelo):
    Iteor = Funcion_modelo(par, th1, M_sistema)
    diferencia = Iteor - Ireal
    return diferencia.flatten()


def function_to_test(iterable, constant):
    return iterable**2 * constant


"""
if __name__ == '__main__':
    dict_constants = {'x': 3, 'y': 4}
    N = 50000
    variable_process = np.linspace(0, 1, N)
    start = time.time()
    sc = auxiliar_multiprocessing()
    sc.execute_multiprocessing(function_to_test, variable_process, 1, 8)
    #print("{}".format(sp.array(sc.result)))
    print("8 processes pool took {} seconds".format(time.time() - start))
    start = time.time()
    res = range(N)
    for ind, val in enumerate(variable_process):
        res[ind] = function_to_test(val, 1)
    print("Single process pool took {} seconds".format(time.time() - start))
"""
if __name__ == '__main__':
    tic = time.time()
    # Diccionario con variables constanes para la funcion del experimento
    constants = {
        'Naverages': 200,
        'min_angulo_th': 0 * degrees,
        'max_angulo_th': 180 * degrees,
        'min_delta_R': 75 * degrees,
        'max_delta_R': 95 * degrees,
        'min_p1': 0.8,
        'max_p1': 1,
        'min_p2': 0,
        'max_p2': 0.2,
        'min_pR': 0.9,
        'max_pR': 1,
        'min_az': 0 * degrees,
        'max_az': 180 * degrees,
        'min_elip': -45 * degrees,
        'max_elip': 45 * degrees,
        'min_pol_degree': 0,
        'max_pol_degree': 1,
        'Funcion_modelo': modelo_teor,
        'Funcion_error': error_modelo,
        'tol_min:': 1e-10,
        'verbose': False
    }
    save_file = True
    # Variables para usar aqui
    Nerrores = 15
    Nmedidas = 21
    Ncores = 1
    (min_error, max_error) = (0, 0.02)
    (min_medidas, max_medidas) = (10, 30)
    # Crear arrays
    error_array = np.linspace(min_error, max_error, Nerrores)
    medidas_array = np.linspace(min_medidas, max_medidas, Nmedidas)
    # Fusionar ambos arrays en un solo iterable
    #merged_arrays = unir_en_iterable([error_array, medidas_array])
    merged_arrays = list(prod(error_array, medidas_array))

    # Test de que la funcion funciona
    #test = funcion_single_thread(merged_arrays[0], constants)
    start_test = time.time()
    test = funcion_single_thread((0.1, 14.), constants)
    end_test = time.time()
    print('La ejecucion de prueba tardo: {} s'.format(end_test - start_test))
    print(test)
    # Realizar el calculo
    if Nerrores * Nmedidas > 0:
        am = auxiliar_multiprocessing()
        print('Preparandose para el multiprocessing')
        results = am.execute_multiprocessing(
            function=funcion_single_thread,
            var_iterable=merged_arrays,
            dict_constants=constants,
            Ncores=Ncores)
        print('Multithreading finalizado')
        # Desdoblar los resultados en matrices
        shape = [Nerrores, Nmedidas]
        results = multiproc.desdoblar_desde_iterable(results, shape)
    else:
        results = 0

    # Salvar datos en un archivo
    if save_file:
        constants_aux = {
            'Nerrores': Nerrores,
            'Nmedidas': Nmedidas,
            'min_error': min_error,
            'max_medidas': max_medidas,
            'min_medidas': min_medidas,
            'max_medidas': max_medidas,
            'error_array': error_array,
            'medidas_array': medidas_array
        }
        filename = "Analizer_{}".format(datetime.date.today())
        np.savez(
            filename + '.npz',
            results=results,
            constants=constants,
            constants_aux=constants_aux)

    # Final
    toc = time.time()
    print('Elapsed time is: {} s.'.format(toc - tic))
