from __future__ import print_function
import copy_reg
import types
from multiprocessing import Pool

import time
import datetime
import pprint

from phyton_optics import degrees

from scipy import optimize, pi
from math import sqrt

from polarimeter.utils import plot_2d, generate_even_distribution

from matplotlib.colors import LogNorm
from matplotlib import ticker, colors

from polarimeter.polarimeter import Intensity_Rotating_Elements
import phyton_optics.polarization_jones as pj
import phyton_optics.polarization_stokes as ps
import phyton_optics.polarization_mueller_analysis as amm
import polarimeter.polarimeter as opsys
import phyton_optics.utils_multiprocessing as multiproc
import numpy as np
from itertools import product as prod


def _pickle_method(method):
    func_name = method.im_func.__name__
    obj = method.im_self
    cls = method.im_class
    if func_name.startswith(
            '__') and not func_name.endswith('__'):  # deal with mangled names
        cls_name = cls.__name__.lstrip('_')
        func_name = '_' + cls_name + func_name
    return _unpickle_method, (func_name, obj, cls)


def _unpickle_method(func_name, obj, cls):
    for cls in cls.__mro__:
        try:
            func = cls.__dict__[func_name]
        except KeyError:
            pass
        else:
            break
    return func.__get__(obj, cls)


copy_reg.pickle(types.MethodType, _pickle_method, _unpickle_method)


class auxiliar_multiprocessing(object):
    def __init__(self):
        pass
        #self.result = self.go()

    # Method that executes the multiprocessing
    def execute_multiprocessing(self,
                                function,
                                var_iterable,
                                dict_constants=dict(),
                                Ncores=8):
        # Store data in object
        self.external_function = function
        self.dict_constants = dict_constants
        # Start multiprocessing
        pool = Pool(Ncores)
        result = pool.map(self.method_single_proc, var_iterable)
        pool.close()
        pool.join()
        # Save and extract resultado
        self.result = result
        return result

    def method_single_proc(self, elem_iterable):
        # Method that is called in each iteration of the multiprocessing
        return self.external_function(elem_iterable, self.dict_constants)


# Funcion que aborda cada caso particular. Esta es la funcion que llamara el metodo de
# multithreading, y solo puede tener un argumento
def funcion_single_thread(params, constantes):
    # Operaciones de inicio
    #print(constantes['verbose'])
    # Desacoplar los parametros
    (error, Nangulos) = params
    Nangulos = int(round(Nangulos))
    Naverages = constantes['Naverages']
    # Crear array con los resultados para despues promediar
    ajuste_pR3 = np.zeros(Naverages)
    ajuste_pR4 = np.zeros(Naverages)
    ajuste_fi = np.zeros(Naverages)
    ajuste_delta_R2 = np.zeros(Naverages)
    ajuste_error = np.zeros(Naverages)
    no_solution = 0
    # Bucle para promediar
    for indAv in range(Naverages):
        # Matrices de Mueller
        (p1, p3) = constantes['min_p1'] + (
            constantes['max_p1'] - constantes['min_p1']) * np.random.rand(2)
        (p2, p4) = constantes['min_p2'] + (
            constantes['max_p2'] - constantes['min_p2']) * np.random.rand(2)
        (pR1, pR2, pR3, pR4) = constantes['min_pR'] + (
            constantes['max_pR'] - constantes['min_pR']) * np.random.rand(4)
        fi = constantes['min_angulo'] + (
            constantes['max_angulo'] -
            constantes['min_angulo']) * np.random.rand(1)
        delta_P = constantes['min_delta_P'] + (
            constantes['max_delta_P'] -
            constantes['min_delta_P']) * np.random.rand(1)
        (delta_R1, delta_R2) = constantes['min_delta_R'] + (
            constantes['max_delta_R'] -
            constantes['min_delta_R']) * np.random.rand(2)
        Mp1 = ps.Mueller_Real_Retarder(p1, p2, delta=delta_P, theta=0)
        Mp2 = ps.polarizer_linear(p3, p4, theta=0)
        Mr1 = ps.Mueller_Real_Retarder(pR1, pR2, delta=delta_R1, theta=0)
        Mr2 = ps.Mueller_Real_Retarder(pR3, pR4, delta=delta_R2, theta=fi)
        par_fijos = [Mp1, Mp2, Mr1]
        M_sistema = [Mp1, Mr1, Mr2, Mp2]
        theta = np.linspace(constantes['min_angulo_th'],
                            constantes['max_angulo_th'], Nangulos)
        # Crear el experimento "real"
        Intensity = opsys.Intensity_Rotating_Elements(
            M_sistema, [0, theta, theta, 0], constantes['Illum'])
        # Anadir errores en los valores de intensidad
        max_intensity = Intensity.max()
        Intensity_error = Intensity + error * max_intensity * np.random.randn(
            Nangulos, Nangulos)
        # Ajustar el experimento con errores por minimos cuadrados
        par0 = [pR3, pR4, delta_R2, fi]
        par1, success = optimize.leastsq(
            constantes['Funcion_error'],
            par0,
            args=(theta, par_fijos, constantes['Illum'], Intensity_error,
                  constantes['Funcion_modelo']))
        #print(success)
        if success in range(1, 5):
            # Calcular el error en la distribucion
            residuos = error_modelo(par1, theta, par_fijos,
                                    constantes['Illum'], Intensity_error,
                                    constantes['Funcion_modelo'])
            # Dar resultados
            ajuste_error[indAv] = np.linalg.norm(residuos) / (
                max_intensity * sqrt(Nangulos**2))
            ajuste_pR3[indAv] = pR3 - par1[0]
            ajuste_pR4[indAv] = pR4 - par1[1]
            ajuste_delta_R2[indAv] = abs(delta_R2 - par1[2]) % (2 * pi)
            ajuste_fi[indAv] = abs(fi - par1[3]) % pi
            # Imprimir resultados para comparar
            if constantes['verbose']:
                print('Parametros iniciales: pR3 = {}; pR4 = {};'.format(
                    pR3, pR4))
                print('Angulos iniciales: delta_R2 = {} deg; fi = {} deg;'.
                      format(delta_R2 / degrees, fi / degrees))
                print('Parametros ajustados: pR2 = {}; pR3 = {};'.format(
                    par1[0], par1[1]))
                print('Angulos ajustados: delta_R2 = {} deg; fi = {} deg;'.
                      format(par1[2] / degrees, par1[3] / degrees))
                print('Error en param: pR3 = {}; pR4 = {}; residuos = {};'.
                      format(ajuste_pR3[indAv], ajuste_pR4[indAv],
                             ajuste_error[indAv]))
                print(
                    'Error en angulos: delta_R2 = {} deg; fi = {} deg;'.format(
                        ajuste_delta_R2[indAv] / degrees,
                        ajuste_fi[indAv] / degrees))
                print(" ")
        else:
            # Dar resultados
            no_solution = no_solution + 1
            # Imprimir resultados para comparar
            if constantes['verbose']:
                print('El ajuste fallo.')
    # Hacer media
    if no_solution < Naverages:
        resultado = [
            np.mean(ajuste_error) * Naverages / (Naverages - no_solution),
            np.linalg.norm(ajuste_pR3) / sqrt(Naverages - no_solution),
            np.linalg.norm(ajuste_pR4) / sqrt(Naverages - no_solution),
            np.linalg.norm(ajuste_delta_R2) / sqrt(Naverages - no_solution),
            np.linalg.norm(ajuste_fi) / sqrt(Naverages - no_solution),
            no_solution
        ]
        # Eliminar ceros
        for ind, elem in enumerate(resultado):
            if elem < constantes['tol_min:']:
                resultado[ind] = constantes['tol_min:']
    else:
        resultado = 1e6 * np.ones(6)
    # Imprimir resultados
    if constantes['verbose']:
        print('Errores medios en param: pR1 = {}; pR2 = {}; residuos = {};'.
              format(resultado[1], resultado[2], resultado[0]))
        print(
            'Error en angulos: delta_R = {} deg; fi = {} deg; delta_P = {} deg;'.
            format(resultado[3] / degrees, resultado[4] / degrees,
                   resultado[5] / degrees))
        print(" ")
    return resultado


# Funciones de modelo
def modelo_teor(par, th1, par_fijos, Illum):
    # Create Jones matrices
    Mp1 = par_fijos[0]
    Mp2 = par_fijos[1]
    Mr1 = par_fijos[2]
    Mr2 = ps.Mueller_Real_Retarder(par[0], par[1], delta=par[2], theta=par[3])
    M_sistema = [Mp1, Mr1, Mr2, Mp2]
    th = [0, th1, th1, 0]
    Iteor = Intensity_Rotating_Elements(M_sistema, th, Ei=Illum)
    return Iteor


def error_modelo(par, th1, par_fijos, Illum, Ireal, Funcion_modelo):
    Iteor = Funcion_modelo(par, th1, par_fijos, Illum)
    diferencia = Iteor - Ireal
    return diferencia.flatten()


def function_to_test(iterable, constant):
    return iterable**2 * constant


"""
if __name__ == '__main__':
    dict_constants = {'x': 3, 'y': 4}
    N = 50000
    variable_process = np.linspace(0, 1, N)
    start = time.time()
    sc = auxiliar_multiprocessing()
    sc.execute_multiprocessing(function_to_test, variable_process, 1, 8)
    #print("{}".format(sp.array(sc.result)))
    print("8 processes pool took {} seconds".format(time.time() - start))
    start = time.time()
    res = range(N)
    for ind, val in enumerate(variable_process):
        res[ind] = function_to_test(val, 1)
    print("Single process pool took {} seconds".format(time.time() - start))
"""
if __name__ == '__main__':
    tic = time.time()
    # Diccionario con variables constanes para la funcion del experimento
    constants = {
        'Naverages': 100,
        'min_angulo': 0 * degrees,
        'max_angulo': 180 * degrees,
        'min_angulo_th': 0 * degrees,
        'max_angulo_th': 180 * degrees,
        'min_delta_R': 77 * degrees,
        'max_delta_R': 97 * degrees,
        'min_delta_P': 0 * degrees,
        'max_delta_P': 360 * degrees,
        'min_p1': 0.8,
        'max_p1': 1,
        'min_p2': 0,
        'max_p2': 0.2,
        'min_pR': 0.9,
        'max_pR': 1,
        'Illum': ps.circular_light() * 2,
        'Funcion_modelo': modelo_teor,
        'Funcion_error': error_modelo,
        'tol_min:': 1e-10,
        'verbose': False
    }
    save_file = True
    # Variables para usar aqui
    Nerrores = 15
    Nmedidas = 21
    Ncores = 7
    (min_error, max_error) = (0, 0.02)
    (min_medidas, max_medidas) = (10, 30)
    # Crear arrays
    error_array = np.linspace(min_error, max_error, Nerrores)
    medidas_array = np.linspace(min_medidas, max_medidas, Nmedidas)
    # Fusionar ambos arrays en un solo iterable
    #merged_arrays = unir_en_iterable([error_array, medidas_array])
    merged_arrays = list(prod(error_array, medidas_array))

    # Test de que la funcion funciona
    #test = funcion_single_thread(merged_arrays[0], constants)
    start_test = time.time()
    test = funcion_single_thread((0.1, 14.), constants)
    end_test = time.time()
    print('La ejecucion de prueba tardo: {} s'.format(end_test - start_test))
    print(test)
    # Realizar el calculo
    am = auxiliar_multiprocessing()
    results = am.execute_multiprocessing(
        function=funcion_single_thread,
        var_iterable=merged_arrays,
        dict_constants=constants,
        Ncores=Ncores)
    print('Multithreading finalizado')

    # Desdoblar los resultados en matrices
    shape = [Nerrores, Nmedidas]
    results = multiproc.desdoblar_desde_iterable(results, shape)

    # Salvar datos en un archivo
    if save_file:
        constants_aux = {
            'Nerrores': Nerrores,
            'Nmedidas': Nmedidas,
            'min_error': min_error,
            'max_medidas': max_medidas,
            'min_medidas': min_medidas,
            'max_medidas': max_medidas,
            'error_array': error_array,
            'medidas_array': medidas_array
        }
        filename = "Second_Retarder_{}".format(datetime.date.today())
        np.savez(
            filename + '.npz',
            results=results,
            constants=constants,
            constants_aux=constants_aux)

    # Final
    toc = time.time()
    print('Elapsed time is: {} s.'.format(toc - tic))
