# !/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------
# Author:    Jesus del Hoyo
# Fecha       2018/07/25 (version 1.0)
# License:    GPL
# -------------------------------------
"""
We present several functions to calculate the intensity output of an optical
system using matrices calculation. It supports both Jones and Mueller
formalisms.

# Intensity_Rotating_Elements

"""

import numpy as np
import pprint
import datetime

from numpy import array, matrix, zeros, size
from copy import deepcopy
import os
from shutil import copyfile
import matplotlib.pyplot as plt

from py_pol.mueller import Mueller
from py_pol.stokes import Stokes
from py_pol.jones_matrix import Jones_matrix
from py_pol.jones_vector import Jones_vector

from .utils import iscolumn, plot_2d_scattered, generate_angles_distribution, sort_positions, percentage_complete, set_cal_dict, plot_experiment_residuals_1D, get_calibration

from . import um, degrees, AIN_signal, AIN_ref, theta_default, Vels, COMPORTS, N_motors

# Some imports can't be done in some computer (when the motors are not present)
try:
    from .daca import u3, LabJackPython, get_intensity
    from .motors.rotary_motors import Motors
except:
    u3, LabJackPython, get_intensity, Motors = (None, None, None, None)
    print('Warning: Acquisition card and motors functions not loaded!!!')

vacuum = Mueller('vacuum')
vacuum.vacuum()
global equipment


def Intensity_Rotating_Elements(J, th, Ei=1, stack=True):
    """
    This function calculates the matrix of a certain optical experiment composed of rotating elements. It can be used for both Jones and Mueller formalisms.

    Parameters:
        J (list of objects): List containing the Jones_matrix or Mueller objects of the first and third elements.
        th (list of floats): List containing the rotation angle of each element. Each element of the tuple must be a single value or a 1D-array. No more than 2 elements should be 1-D arrays. Units: rad.
        Ei: (Stokes or Jones_vector, or float): Electric field of the incoming light. If None, circularly polarized light will be used (default). If float value, it is amplitude. Default: 1.
        stack (bool): If true, stacks the results horizontally to create a 1D array.

    Output:
        If: Output intensity. If the angles are single elements, the output is a float number. If there are two 1-D arrays in the angles, the result is a 2-D array. Otherwise, it is a 1-D array.
    """
    # Security checks, size of J and th lists must be the same
    N = len(J)
    N2 = len(th)
    if N != N2:
        raise ValueError('The length of J {} and th {} must be the same.'.
                         format(len(J), len(th)))
    if N == 0:
        raise ValueError('At least one element must be present.')
    # First, it has to be checked if the angles are single values or arrays. If all are single values, the calculation can be done.
    singleCal = True
    Nmult = 0
    for ind, elem in enumerate(th):
        if size(elem) > 1:
            singleCal = False
            Nmult = Nmult + 1

    # If all angle elements are single values, make the calculation
    if singleCal:
        # Check if we are working with Jones or Stokes matrices. Assign the initial matrix the field/intensity vector.
        if J[0].type == 'Jones_matrix':
            # Jones matrices
            if isinstance(Ei, (float, int)):
                amp = Ei
                Ei = Jones_vector()
                Ei.circular_light(amplitude=amp)
        elif J[0].type == 'Mueller':
            # Stokes matrix
            if isinstance(Ei, (float, int)):
                amp = Ei
                Ei = Stokes()
                Ei.circular_light(intensity=amp)
        else:
            raise ValueError('Jones or Stokes matrix required.')
        # Loop for rotating each matrix
        M = Ei
        for ind, theta in enumerate(th):
            # Calculate rotation matrices
            Jr = J[ind].rotate(angle=theta, keep=True, returns_matrix=False)
            # Multiply
            M = Jr * M
        # End of loop, so calculate final intensity
        I = M.parameters.intensity()
        return I

    # Array in any of the angle elements
    else:
        # Loop to find the element with multiple angles
        for ind, elem in enumerate(th):
            L = size(elem)
            if L > 1:
                # Prealocate lists
                resAux = list(range(L))
                theta = list(th)  # Crear duplicado de la variable th
                # Loop inside this angle variable
                for indT, angle in enumerate(elem):
                    # Change the array for a single value
                    theta[ind] = angle
                    resAux[indT] = Intensity_Rotating_Elements(J, theta, Ei)
                # Merge results
                if stack:
                    res = resAux[0]
                    for indAux in range(1, L):
                        res = np.hstack((res, resAux[indAux]))
                    break
                else:
                    res = np.transpose(resAux)
        # Transform the linear array to a 2D array if necesary
        if stack and Nmult == 2:
            # Calculate new shape
            L2 = int(size(res) / L)
            newshape = [L, L2]
            # Reshape
            resF = np.array(res).reshape(newshape)
            # print(newshape)
            """
            # Initial step
            L2 = size(res) / L  # Length of the second array
            print([L, L2])
            resF = res[0:L2]
            # Loop along the first element angle
            for ind in range(1, L):
                ini = ind * L2
                fin = (ind + 1) * L2
                resF = np.vstack((resF, res[ini:fin]))
            """
        else:
            resF = res
        return resF


def b(M, illum, th, Mbefore=vacuum, Mafter=vacuum, co=False):
    """Calculates a row array with the 16 elements of one measurement of the polarizer

    Args:
        M (list): Polarimeter Mueller matrices [M1, M2, M3, M4].
        illum (array): Stokes vector of the illumination.
        th (float, float, float, float): angles of the 4 polarimeter elements.
        Mbefore (Mueller matrix): Mueller matrix of some fix elements before
            the sample. Default: vacuum.
        Mafter (Mueller matrix): Mueller matrix of some fix elements after the
            sample. Default: vacuum
        co (bool): if the complete output is delivered or not. Default: False.

    Output:
        b_alpha (1x16 float array): Array of the alpa-th row of the matrix B of
            the I = B*M problem of a polarimeter.
    """

    # Rotate matrices to their actual position
    M2 = list(range(4))
    for ind in range(4):
        M2[ind] = M[ind].rotate(th[ind], keep=True)

    # print(M2, Mbefore, Mafter)

    # Generator and analyzer arrays
    g = Mbefore * M2[1] * M2[0] * illum
    A = M2[3] * M2[2] * Mafter
    # Multiply each element of the first row of A with each element of g array
    a = A.M[0, :]
    b_i = g.M * a
    b_i = b_i.T
    # Now we just have to convert the 4x4 matrix in a 1x16 array.
    if co:
        Iteor = A * g
        Iteor = Iteor.parameters.intensity()
        return b_i.flatten(), Iteor
    else:
        return b_i.flatten()


def polarimeter_experiment(M,
                           th,
                           illum,
                           I,
                           Mbefore=vacuum,
                           Mafter=vacuum,
                           co=False,
                           verbose=False):
    """Calculates the Mueller matrix of a sample.

    Args:
        M (list): Polarimeter Mueller matrices [M1, M2, M3, M4].
        th (list): List of arrays containing the angles of the 4 polarimeter
            elements for each intensity measurement.
        illum (array): Stokes vector of the illumination.
        I (column array): Array with the intensity measurements of the
            polarimeter.
        Mbefore (Mueller matrix): Mueller matrix of some fix elements before
            the sample.
        Mafter (Mueller matrix): Mueller matrix of some fix elements after the
            sample.
        co (bool): if the complete output is delivered or not. Default: False.

    Output:
        M (matrix): Mueller matrix of the analyzed element.

    """
    # Make sure that I is a column array
    I = matrix(array(I))
    if not iscolumn(I):
        I = I.T

    # For loop for each element in I to calculate B matrix
    n = I.size
    B = zeros((n, 16))
    Iteor = zeros(n)
    for ind, theta in enumerate(th):
        # theta = [th[0][ind], th[1][ind], th[2][ind], th[3][ind]]
        if co:
            B[ind, :], Iteor[ind] = b(M, illum, theta, Mbefore, Mafter, co)
        else:
            B[ind, :] = b(M, illum, theta, Mbefore, Mafter)
    B = matrix(B)
    # Clear NaNs and Infs
    cond = np.isnan(B)
    if np.any(cond):
        B[cond] = 0
        if verbose:
            print('Warning: NaN values encountered in B matrix')
            print(M)
    cond = np.isinf(B)
    if np.any(cond):
        B[cond] = 1e6
        if verbose:
            print('Warning: Inf values encountered in B matrix')
            # print(M)

        # Calculate the inverse
        # print('B is:', B)
    B = B
    if n == 16:
        Binv = B.I
    elif n > 16:
        Binv = (B.T * B).I * B.T
    else:
        raise ValueError("Insuficient number of measurements")
    # Solve the problem
    m = Binv * I
    Ms = Mueller('Calculated')
    Ms.from_matrix(matrix(m.reshape(4, 4)))
    # Output
    if co:
        Iteor = matrix(Iteor)
        m2 = Binv * Iteor.T
        M2 = Mueller('Alternative')
        M2.from_matrix(np.matrix(m2.reshape(4, 4)))
        return Ms, Iteor, M2
    else:
        return Ms


def Analysis_Measurement_0D(analysis_dict,
                            type_output='all',
                            verbose=False,
                            filter=True,
                            decompose=True):
    """This function is used to analyze the data of a 0D-measurement of the polarimeter. It is intended to work both for a live measurmenet and for a stored one."""
    # Load calibration file
    if analysis_dict['type'] in ('Cal', 'cal', 'CAL', 'Calibration',
                                 'calibration'):
        if analysis_dict["fit completed"]:
            Mall = analysis_dict['M']
            Ifuente = analysis_dict["Ifuente"]
        else:
            Mall = analysis_dict['M_test']
            Ifuente = analysis_dict["Ifuente_test"]
        cal_dict = analysis_dict
    else:
        cal_dict = get_calibration(
            path=analysis_dict['cal_path'],
            name=analysis_dict['cal_file'],
            verbose=False)
        Mall = [
            cal_dict["Mp1"], cal_dict["Mr1"], cal_dict["Mr2"], cal_dict["Mp2"]
        ]
        Ifuente = cal_dict["Ifuente"]
        Mvac = Mueller()
        Mvac.vacuum()
        if cal_dict["Mbefore"] is None:
            cal_dict["Mbefore"] = Mvac
        if cal_dict["Mafter"] is None:
            cal_dict["Mafter"] = Mvac
        # Extract initial angles
        # try:
        #     theta1 = cal_dict["th0p1"]  # Change with future calibrations
        #     theta2 = cal_dict['th0r1']
        #     theta3 = cal_dict['th0r2']
        #     theta4 = cal_dict['th0p2']  # Change with future calibrations
        #     if analysis_dict['theta0'] in ('def', 'DEF', 'Def', 'default',
        #                                    'Default'):
        #         theta0 = -np.array([theta1, theta2, theta3, theta4
        #                             ])  # Extract Mbefore, Mafter and tolerance
        #     else:
        #         theta0 = analysis_dict['theta0']
        # except:
        #     theta0 = np.zeros(4)

        # Load data to be analyzed
    if analysis_dict['type'] in ('File', 'file', 'FILE'):
        # Load from file
        old_path = os.getcwd()
        os.chdir(analysis_dict['meas_final_path'])
        data = np.load(analysis_dict['meas_name'], encoding='latin1')
        # for key, value in data.iteritems():
        #     print(key)
        # Extract the data
        measurement_dict = data['dict_param']
        measurement_dict = np.take(measurement_dict, 0)
        angles = np.array(data['angles'])
        Iexp = data['Iexp']
    elif analysis_dict['type'] in ('Cal', 'cal', 'CAL', 'Calibration',
                                   'calibration'):
        angles = analysis_dict['angles_polarimeter']
        Iexp = analysis_dict['intensity_polarimeter']
    else:
        # Take directly the data
        angles = analysis_dict['angles']
        Iexp = analysis_dict['intensity']
        measurement_dict = None
    # If the intensity still has two dimensions, reduce it to one
    if len(Iexp.shape) > 1:
        if analysis_dict['is_vacuum']:
            analysis_dict['mean_ref'] = np.mean(Iexp[:, 1])
        Iexp = Iexp[:, 0] * analysis_dict['mean_ref'] / Iexp[:, 1]

    # Extract Mbefore, Mafter and tolerance
    try:
        if analysis_dict['Mbefore'] is None:
            Mbefore = Mueller()
            Mbefore.vacuum()
        else:
            Mbefore = analysis_dict['Mbefore']
        if analysis_dict['Mafter'] is None:
            Mafter = Mueller()
            Mafter.vacuum()
        else:
            Mafter = analysis_dict['Mafter']
    except:
        Mbefore = Mueller()
        Mbefore.vacuum()
        Mafter = Mbefore

    tolerance = analysis_dict['tolerance']

    # Calculate the Mueller matrix
    Mcalculated = polarimeter_experiment(
        M=Mall,
        illum=Ifuente,
        I=Iexp,
        th=angles,
        Mbefore=Mbefore,
        Mafter=Mafter,
        verbose=True)
    Mcalculated.name = 'Calculated'
    if verbose:
        print(Mcalculated)
        print('')

    # Normalize calculated matrix
    if analysis_dict['is_vacuum']:
        Mtarget = np.eye(4)
        error_direct = np.linalg.norm(Mtarget - Mcalculated.M) / 16
        analysis_dict["error_direct"] = error_direct
        analysis_dict['normalization'] = Mcalculated.m00
        if verbose:
            print(
                "RMS error of the calculated matrix: {}".format(error_direct))
            print("\n\nNew normalization factor: {}".format(
                analysis_dict['normalization']))
    elif verbose:
        print(
            "Normalization factor: {}".format(analysis_dict['normalization']))
    Mnormalized = Mcalculated
    Mnormalized.M = Mnormalized.M / analysis_dict['normalization']
    Mnormalized.update()
    Mnormalized.name = 'Normalized'
    if verbose:
        print(Mnormalized)
    if analysis_dict['is_vacuum']:
        error_norm = np.linalg.norm(Mtarget - Mnormalized.M) / 16
        analysis_dict["error_norm"] = error_norm
        if verbose:
            print("RMS error after normalization: {}\n".format(error_norm))

    # Filter matrix
    if filter:
        Mfiltered = Mnormalized.analysis.filter_physical_conditions(
            tol=tolerance, verbose=False)
        if analysis_dict['odd_number_refs']:
            # If we have an odd number of reflections, we have to include the effect of the reflection
            Mespejo = Mueller()
            Mespejo.mirror()
            if not analysis_dict['axes_aligned']:
                # If the axes of the optical elements of the polarimeter are not the same of the reflecting element, we have to "rotate" the mirror to calculate accurately the result
                if analysis_dict['axes_angle'] is None:
                    _, Mr, _ = Mfiltered.analysis.decompose_polar(
                        decomposition='DRP',
                        verbose=verbose,
                        give_all=False,
                        tol=tolerance,
                        filter=False)
                    _, az, _ = Mr.analysis.retarder(param='az-el')
                else:
                    az = analysis_dict['axes_angle']
                Mespejo.rotate(az)
            Mfiltered = Mfiltered * Mespejo
            # Mfiltered = Mespejo * Mfiltered # Esta no es la buena
        Mfiltered.name = 'Filtered'
        if verbose:
            print(Mfiltered)
        if analysis_dict['is_vacuum']:
            error_filter = np.linalg.norm(Mtarget - Mfiltered.M) / 16
            if verbose:
                print("RMS error after filtering: {}\n".format(error_filter))
    else:
        Mfiltered = Mcalculated
        if analysis_dict['is_vacuum']:
            error_filter = error_norm

    # Decompose and analyze
    if decompose:
        Md, Mr, Mp = Mfiltered.analysis.decompose_polar(
            decomposition='DRP',
            verbose=verbose,
            give_all=False,
            tol=tolerance,
            filter=False)
    else:
        Md, Mr, Mp = (None, None, None)
    # Create a dictionary with the results
    if analysis_dict['save_analysis'] or type_output in ('all', 'All', 'ALL'):
        results_dict = {}
        results_dict["Mcalculated"] = Mcalculated
        results_dict["Mnormalized"] = Mnormalized
        results_dict["Mfiltered"] = Mfiltered
        results_dict["Mp"] = Mp
        results_dict["Mr"] = Mr
        results_dict["Md"] = Md
        if decompose:
            results_dict["polarizance"] = Md.parameters.polarizance()
            results_dict[
                "depolarization index"] = Md.parameters.depolarization_index()
            (results_dict["retardance"], results_dict["alpha ret"],
             results_dict["delta ret"], results_dict["azimuth ret"],
             results_dict["ellipticity ret"]) = Mr.analysis.retarder()
            (results_dict["p1"], results_dict["p2"], results_dict["alpha pol"],
             results_dict["delta pol"], results_dict["azimuth pol"],
             results_dict["ellipticity pol"]) = Mr.analysis.diattenuator()
        else:
            results_dict["polarizance"] = None
            results_dict["depolarization_index"] = None
            (results_dict["retardance"], results_dict["alpha ret"],
             results_dict["delta ret"], results_dict["azimuth ret"],
             results_dict["ellipticity ret"]) = (None, None, None, None, None)
            (results_dict["p1"], results_dict["p2"], results_dict["alpha pol"],
             results_dict["delta pol"], results_dict["azimuth pol"],
             results_dict["ellipticity pol"]) = (None, None, None, None, None,
                                                 None)
        if analysis_dict['is_vacuum']:
            results_dict["error_filter"] = error_filter
            results_dict["error_norm"] = error_norm
            results_dict["error_direct"] = error_direct
            results_dict['normalization'] = analysis_dict['normalization']
        else:
            results_dict["error_filter"] = None
            results_dict["error_norm"] = None
            results_dict["error_direct"] = None
            results_dict['normalization'] = None

    # Save the file
    if analysis_dict['save_analysis']:
        if analysis_dict['analysis_name'] is None:
            name_file = analysis_dict['meas_name'] + '_analyzed.npz'
        else:
            name_file = analysis_dict['analysis_name']
        try:
            np.savez(
                name_file,
                angulos=angles,
                intensidad=Iexp,
                cal_dict=deepcopy(cal_dict),
                measurement_dict=deepcopy(measurement_dict),
                analysis_dict=deepcopy(analysis_dict),
                results_dict=deepcopy(results_dict))
        except:
            np.savez(
                name_file,
                angulos=angles,
                intensidad=Iexp,
                cal_dict=deepcopy(cal_dict),
                analysis_dict=deepcopy(analysis_dict),
                results_dict=deepcopy(results_dict))
        if verbose:
            print("\nFile succesfully saved")

    if verbose:
        print("\nAnalysis succesfully completed")

    # Return to old folder
    if analysis_dict['type'] in ('File', 'file', 'FILE'):
        os.chdir(old_path)

    # Output
    if type_output in ('all', 'All', 'ALL'):
        return analysis_dict, Mfiltered, results_dict
    elif type_output in ('filtered', 'Filtered', 'FILTERED'):
        return analysis_dict, Mfiltered
    else:
        return analysis_dict


def Make_Measurement_0D(dict_param, type_output='all', filename=None):
    """Function to make easily the measurements of the polarimeter with photodiodes."""
    # Start equipment
    try:
        equipment = dict_param["equipment"]
    except:
        equipment = initialize_motors_and_card()

    # Vacuum matrices (in case it must be used)
    Mvac = Mueller()
    Mvac.vacuum()

    # Preeparation if we are making a default measurement
    if dict_param['type'] in ('Measurement', 'measurement', 'Measure',
                              'measure'):
        # Load calibration file
        cal_dict = get_calibration(
            path=dict_param['cal_path'],
            name=dict_param['cal_file'],
            verbose=False)
        Mall = [
            cal_dict["Mp1"], cal_dict["Mr1"], cal_dict["Mr2"], cal_dict["Mp2"]
        ]
        if cal_dict["Mbefore"] is None:
            cal_dict["Mbefore"] = Mvac
        if cal_dict["Mafter"] is None:
            cal_dict["Mafter"] = Mvac
        Mall = [
            cal_dict["Mp1"], cal_dict["Mr1"], cal_dict["Mbefore"],
            cal_dict["Mafter"], cal_dict["Mr2"], cal_dict["Mp2"]
        ]
        Ifuente = cal_dict["Ifuente"]
        # print(cal_dict)
        try:
            mean_Int_old = cal_dict['mean_ref']
        except:
            mean_Int_old = 0
        # Extract initial angles
        try:
            theta1 = cal_dict["th0p1"]  # Change with future calibrations
            theta2 = cal_dict['th0r1']
            theta3 = cal_dict['th0r2']
            theta4 = cal_dict['th0p2']  # Change with future calibrations
            if dict_param['theta0'] in ('def', 'DEF', 'Def', 'default',
                                        'Default'):
                theta0 = -np.array([theta1, theta2, theta3, theta4
                                    ])  # Extract Mbefore, Mafter and tolerance
            else:
                theta0 = dict_param['theta0']
        except:
            theta0 = np.zeros(4)
        # Create folder of todays measurements if required
        old_path = os.getcwd()
        fecha = "_{}".format(datetime.date.today())
        if dict_param['meas_final_path'] is None:
            extra_name = 'Mueller_0D' + fecha
            os.chdir(dict_param['meas_path'])
            try:
                os.mkdir(extra_name)
            except:
                pass
            dict_param[
                'meas_final_path'] = dict_param['meas_path'] + r'\ ' [0] + extra_name
            print(dict_param['meas_final_path'])
        os.chdir(dict_param['meas_final_path'])
    # Preparation for measurements during calibration
    else:
        # Mueller and Stokes objects
        if dict_param['fit completed']:
            Mall = dict_param['M_def']
            Mall = [
                dict_param["Mp1"], dict_param["Mr1"], Mvac, Mvac,
                dict_param["Mr2"], dict_param["Mp2"]
            ]
            Ifuente = dict_param["Ifuente"]
        else:
            Mall = dict_param['M_test']
            Mall = [
                dict_param["Mp1_test"], dict_param["Mr1_test"], Mvac, Mvac,
                dict_param["Mr2_test"], dict_param["Mp2_test"]
            ]
            Ifuente = dict_param["Ifuente_test"]
            mean_Int_old = dict_param['mean_ref']
        # Initial angles
        theta0 = np.zeros(4)  # They are already enbeded in Mueller objects
        # Other
        fecha = "_{}".format(datetime.date.today())

    # Create angles array
    if dict_param['type_angles'] in ('Random', 'random', 'RANDOM'):
        angles = np.random.rand(dict_param['Nmeasurements'],
                                N_motors) * dict_param['limits']
    elif dict_param['type_angles'] in ('Custom', 'custom', 'CUSTOM'):
        angles = dict_param['angles']
    else:
        angles = generate_angles_distribution(
            Nmeasures=dict_param['Nmeasurements'],
            limits=dict_param['limits'],
            method=dict_param['type_angles'],
            N_total_motors=N_motors)
    # Order them
    pos_ini = np.array(equipment['motors'].get_position()) * degrees
    angles = sort_positions(angles, pos_ini[1])
    # print(angles)

    # Make the measurement
    Iexp = np.zeros([dict_param['Nmeasurements'], 2])
    Imodel = np.zeros(dict_param['Nmeasurements'])
    for ind in range(dict_param['Nmeasurements']):
        angles_ind = angles[ind, :]
        Iexp[ind, :] = go_and_measure_0D(
            equipment, theta=angles_ind, I0=dict_param['I0'])
        percentage_complete(ind, dict_param['Nmeasurements'])
        # Plot intensity figure
        if dict_param['plot_intensity']:
            angles_all = np.array([
                angles[ind, 0], angles[ind, 1], 0, 0, angles[ind, 2],
                angles[ind, 3]
            ])
            Imodel[ind] = Intensity_Rotating_Elements(
                Mall, angles_all, Ei=Ifuente)
            # print('Step {}'.format(ind))
            # print('Motor angles: {}'.format(angles_ind))
            # print('Model angles: {}'.format(angles_all))
        # Normalize the intensity
    if dict_param['is_vacuum']:
        mean_Int = np.mean(Iexp[:, 1])
        dict_param['mean_ref'] = mean_Int
        print('Mean reference intensity changed from {} V to {} V.'.format(
            mean_Int_old, mean_Int))
    else:
        mean_Int = dict_param['mean_ref']
    Iexp, _ = correct_intensity_0D(Iexp, mean_Int)
    # Print and plot if required
    if dict_param['plot_intensity']:
        x_rep = np.linspace(1, dict_param['Nmeasurements'],
                            dict_param['Nmeasurements'])
        plot_experiment_residuals_1D(
            x_rep,
            Iexp,
            Imodel,
            title='Polarimeter 0D experiment',
            xlabel='Measurement')

    # Analyze
    dict_param['angles'] = angles
    dict_param['intensity'] = Iexp

    # Save file
    np.savez(
        dict_param['meas_name'] + fecha + '.npz',
        angles=angles,
        Iexp=Iexp,
        dict_param=set_cal_dict(dict_param))
    print("\nExperimento concluido con exito")

    # Analyze
    dict_param['angles'] = angles
    dict_param['intensity'] = Iexp
    # try:
    dict_param, Mfiltered, results_dict = Analysis_Measurement_0D(
        dict_param, verbose=True)
    # except:
    #     Mfiltered = None
    #     results_dict = {}
    #     print('Warning: Analysis could not be performed')

    # Return to previous folder
    try:
        os.chdir(old_path)
    except:
        print('No old folder to return')

    # Output
    if type_output in ('all', 'All', 'ALL'):
        return dict_param, Mfiltered, results_dict, angles, Iexp
    elif type_output in ('complete', 'Complete', 'COMPLETE'):
        return dict_param, Mfiltered, results_dict
    elif type_output in ('filtered', 'Filtered', 'FILTERED'):
        return dict_param, Mfiltered
    else:
        return dict_param


def close_equipment(equipment):
    try:
        LabJackPython.Close()
        equipment['motors'].close()
        del equipment['motors']
    except:
        print('No equipment to close')


def finish_measurements(dict_params):
    """Function to perform the last operations in the measurement, as copy the files we were using for the analysis and measurement, and closing the motors."""
    # Close motors
    close_equipment()
    # Save filtes
    if dict_params['type'] in ('measure', 'Measure', 'Measurement',
                               'measurement'):
        destination = dict_params['meas_final_path'] + \
            r'\notebook_measure.ipynb'
        copyfile(dict_params['notebook'], destination)
        destination = dict_params['meas_final_path'] + \
            r'\python_functions_measure.py'
        copyfile(dict_params['python_functions'], destination)
        source = dict_params['cal_path'] + r'\'' + dict_params['cal_file']
        destination = dict_params['meas_final_path'] + \
            r'\calibration_measure.py'
        copyfile(source, destination)
    elif dict_params['type'] in ('file', 'File'):
        destination = dict_params['meas_final_path'] + \
            r'\notebook_analysis.ipynb'
        copyfile(dict_params['notebook'], destination)
        destination = dict_params['meas_final_path'] + \
            r'\python_functions_analysis.py'
        copyfile(dict_params['python_functions'], destination)
        source = dict_params['cal_path'] + r'\'' + dict_params['cal_file']
        destination = dict_params['meas_final_path'] + \
            r'\calibration_analysis.py'
        copyfile(source, destination)
    elif dict_params['type'] in ('cal', 'Cal', 'calibration', 'Calibration'):
        print(
            'Still not complete. End of measurements procedure not performed!!!'
        )
        # Complete


def measure_int(equipment, verbose=True):
    I1 = get_intensity(
        d=equipment['d'],
        u=equipment['u'],
        AIN_number=equipment['AIN_numbers'][0],
        verbose=verbose)
    I2 = get_intensity(
        d=equipment['d'],
        u=equipment['u'],
        AIN_number=equipment['AIN_numbers'][1],
        verbose=verbose)
    I = np.array([I1, I2])
    return I


def correct_intensity_0D(I, average=None):
    """Function that takes a Nx2 array of measured intensities and calculates the corrected Nx1 array. The signal channel is always index 0 and reference channel is index 1."""
    # See if we are in 1D or 2D measurement
    if I.ndim == 3:
        if average is None:
            average = np.mean(I[:, :, 1])
        I = I[:, :, 0] * average / I[:, :, 1]
    else:
        if average is None:
            average = np.mean(I[:, 1])
        I = I[:, 0] * average / I[:, 1]
    return I, average


def go_and_measure_0D(equipment,
                      theta=theta_default,
                      I0=None,
                      velocities=Vels,
                      kind='absolute',
                      unit='rad',
                      verbose=False):
    """Function to move the motors to a certain position and measure the intensity of the photodiodes"""
    if unit in ('rad', 'Rad', 'Radian', 'radian', 'radians', 'Radians'):
        theta = np.array(theta) / degrees
    equipment['motors'].move(
        positions=theta, velocities=velocities, kind=kind, verbose=verbose)
    intensity = measure_int(equipment, verbose=verbose)
    if I0 != None:
        intensity = intensity - I0
    return intensity


def initialize_motors_and_card(verbose=False, equipment={}):
    # Do it only if the equipment doesn't exist
    try:
        measure_int(
            equipment['d'],
            equipment['u'],
            equipment['AIN_numbers'],
            verbose=False)
    except:
        # Create the Dictionary
        equipment = {}
        # Start acquisition card
        try:
            d = u3.U3()
        except:
            LabJackPython.Close()
            d = u3.U3()
        data_callibration = d.getCalibrationData()
        if verbose:
            print("\n Callibration data:")
            pprint.pprint(data_callibration)
            # Read from raw bits from AIN0
            ain1bits, = d.getFeedback(u3.AIN(AIN_signal))
            ainValue = d.binaryToCalibratedAnalogVoltage(
                ain1bits, isLowVoltage=False, channelNumber=0)
            print(ainValue)
            # Read from raw bits from AIN0
            ain1bits, = d.getFeedback(u3.AIN(AIN_ref))
            ainValue = d.binaryToCalibratedAnalogVoltage(
                ain1bits, isLowVoltage=False, channelNumber=0)
            print(ainValue)
            # Start motors
            print(COMPORTS, Vels)
        try:
            equipment['motors'].Close()
            motors = Motors(RS232_COMPORT=COMPORTS, init_vels=Vels)
        except:
            motors = Motors(RS232_COMPORT=COMPORTS, init_vels=Vels)
        # Store values
        equipment['d'] = d
        equipment['u'] = u3
        equipment['AIN_numbers'] = [AIN_signal, AIN_ref]
        equipment['motors'] = motors
        return equipment


def find_theta_0_twined(Nangles=91,
                        intensity_0=[0, 0],
                        theta1=0,
                        initial_pos=None,
                        plot=False):
    # If no initial pos is introduced, use current pos
    if initial_pos is None:
        _, initial_pos, _ = equipment['motors'].get_position(verbose=False)
    initial_pos = initial_pos + [theta1, 0, 0, 0]
    # Move to it with first motor
    equipment['motors'].move(
        positions=initial_pos, velocities=Vels, kind='absolute', verbose=False)
    intensity_0 = np.array(intensity_0)

    # Make a loop using the last motor
    angles = np.linspace(0, 180, Nangles)
    intensity = np.zeros([Nangles, 2])
    for i, th in enumerate(angles):
        equipment['motors'].move(
            positions=initial_pos + [0, 0, 0, th],
            velocities=Vels,
            kind='absolute',
            verbose=False)
        intensity[i, :] = medir_intensidades(verbose=False) - intensity_0
    # Find the maximum of intensity
    I_norm = intensity[:, 0] / intensity[:, 1]
    par1, success = optimize.leastsq(
        malus, [0, 1, 0], args=(angles * degrees, I_norm))
    th0_4 = (par1[2] / degrees) % 180
    # Plot
    if plot:
        print('Theta0_4 is {} deg.'.format(th0_4))
        plt.figure()
        plt.plot(angles, I_norm)
        plt.xlabel('Angle (deg)')
        plt.ylabel('Norm. signal')
        plt.title('P2')

    # Loop with two last motors twined
    for i, th in enumerate(angles):
        equipment['motors'].move(
            positions=initial_pos + [0, 0, th, th + th0_4],
            velocities=Vels,
            kind='absolute',
            verbose=False)
        intensity[i, :] = medir_intensidades(verbose=False) - intensity_0
    # Find the maximum of intensity
    I_norm = intensity[:, 0] / intensity[:, 1]
    par1, success = optimize.leastsq(
        malus, [0, 1, 0], args=(angles * degrees, I_norm))
    th0_3 = (par1[2] / degrees) % 180
    print('End R2')
    if plot:
        print('Theta0_4 is {} deg.'.format(th0_3))
        plt.figure()
        plt.plot(angles, I_norm)
        plt.xlabel('Angle (deg)')
        plt.ylabel('Norm. signal')
        plt.title('R2')

    # Loop with three last motors twined
    for i, th in enumerate(angles):
        equipment['motors'].move(
            positions=initial_pos + [0, th, th + th0_3, th + th0_3 + th0_4],
            velocities=Vels,
            kind='absolute',
            verbose=False)
        intensity[i, :] = medir_intensidades(verbose=False) - intensity_0
    # Find the maximum of intensity
    I_norm = intensity[:, 0] / intensity[:, 1]
    par1, success = optimize.leastsq(
        malus, [0, 1, 0], args=(angles * degrees, I_norm))
    th0_2 = (par1[2] / degrees) % 180
    print('End R1')
    if plot:
        print('Theta0_4 is {} deg.'.format(th0_2))
        plt.figure()
        plt.plot(angles, I_norm)
        plt.xlabel('Angle (deg)')
        plt.ylabel('Norm. signal')
        plt.title('R1')

    # Final calculation
    th0 = initial_pos + [0, th0_2, th0_2 + th0_3, th0_2 + th0_3 + th0_4]
    th0 = th0 % 180
    return th0


def find_theta_0_brute(Nangles=10,
                       intensity_0=[0, 0],
                       theta1=0,
                       initial_pos=None,
                       limits=[0, 180]):
    # If no initial pos is introduced, use current pos
    if initial_pos is None:
        _, initial_pos, _ = equipment['motors'].get_position(verbose=False)
    initial_pos = initial_pos + [theta1, 0, 0, 0]
    # Move to it with first motor
    equipment['motors'].move(
        positions=initial_pos, velocities=Vels, kind='absolute', verbose=False)

    # Make three loops looking for the maximum
    angles = np.linspace(limits[0], limits[1], Nangles)
    I_norm = np.zeros([Nangles, Nangles, Nangles])
    for ind1, th1 in enumerate(angles):
        for ind2, th2 in enumerate(angles):
            for ind3, th3 in enumerate(angles):
                equipment['motors'].move(
                    positions=initial_pos + [0, th1, th2, th3],
                    velocities=Vels,
                    kind='absolute',
                    verbose=False)
                intensity = medir_intensidades(verbose=False) - intensity_0
                I_norm[ind1, ind2, ind3] = intensity[0] / intensity[1]

    # Find the maximum of intensity
    par1, success = optimize.leastsq(
        polarimeter_brute, [0, 0, 0, 1], args=(angles * degrees, I_norm))
    th0 = (np.array([0, par1[0], par1[1], par1[2]]) / degrees)
    print('Difference is {} deg.'.format(th0))
    th0 = initial_pos + th0
    print('Theta0 is {} deg.'.format(th0))
    return th0


def polarimeter_brute(par, angles, Iexp):
    Nangles = angles.size
    I_fit = np.zeros([Nangles, Nangles, Nangles])
    I_0 = Stokes()
    I_0.linear_light(intensity=par[3])
    M_brute = [pol_dict["Mr1"], pol_dict["Mr2"], pol_dict["Mp2"]]
    for i1, th1 in enumerate(angles):
        for i2, th2 in enumerate(angles):
            for i3, th3 in enumerate(angles):
                th = [th1 - par[0], th2 - par[1], th3 - par[2]]
                I_fit[i1, i2, i3] = Intensity_Rotating_Elements(
                    J=Mpol[1:4], th=th, Ei=I_0)
    return (Iexp - I_fit).flatten()
