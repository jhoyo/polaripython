"""Test for polarimeter module."""

import numpy as np

from py_pol.mueller import Mueller
from py_pol.stokes import Stokes
from py_pol.jones_matrix import Jones_matrix
from py_pol.jones_vector import Jones_vector

from . import eps

from polaripython.polarimeter import degrees, Intensity_Rotating_Elements, polarimeter_experiment


class TestPolarimeter(object):
    def test_GeneralRotatingElements(self):
        sol = 0.25
        M1 = Jones_matrix()
        M1.diattenuator_perfect(angle=0)
        M2 = Jones_matrix()
        M2.diattenuator_perfect(angle=0)
        M = [M1, M2]
        # Angles
        th = [0 * degrees, 45 * degrees]
        I = Intensity_Rotating_Elements(M, th)
        assert abs(sol -
                   I) < eps, sys._getframe().f_code.co_name + "Jones 0D no I0"

        sol = 0.5
        M1 = Jones_matrix()
        M1.diattenuator_perfect(angle=0)
        M2 = Jones_matrix()
        M2.diattenuator_perfect(angle=0)
        M = [M1, M2]
        I0 = Jones_vector()
        I0.linear_light(angle=0)
        # Angles
        th = [0 * degrees, 45 * degrees]
        I = Intensity_Rotating_Elements(M, th, I0)
        assert abs(
            sol - I) < eps, sys._getframe().f_code.co_name + "Jones 0D with I0"

        sol = 0.25
        M1 = Mueller()
        M1.diattenuator_perfect(angle=0)
        M2 = Mueller()
        M2.diattenuator_perfect(angle=0)
        M = [M1, M2]
        # Angles
        th = [0 * degrees, 45 * degrees]
        I = Intensity_Rotating_Elements(M, th)
        assert abs(
            sol - I) < eps, sys._getframe().f_code.co_name + "Mueller 0D no I0"

        sol = 0.5
        M1 = Mueller()
        M1.diattenuator_perfect(angle=0)
        M2 = Mueller()
        M2.diattenuator_perfect(angle=0)
        M = [M1, M2]
        I0 = Stokes()
        I0.linear_light(angle=0)
        # Angles
        th = [0 * degrees, 45 * degrees]
        I = Intensity_Rotating_Elements(M, th, I0)
        assert abs(sol - I) < eps, sys._getframe(
        ).f_code.co_name + "Mueller 0D with I0"

        sol = np.array([0.5, 0.25, 0])
        M1 = Jones_matrix()
        M1.diattenuator_perfect(angle=0)
        M2 = Jones_matrix()
        M2.diattenuator_perfect(angle=0)
        M = [M1, M2]
        # Angles
        th = [0 * degrees, [0, 45 * degrees, 90 * degrees]]
        I = Intensity_Rotating_Elements(M, th)
        assert np.linalg.norm(
            sol - I) < eps, sys._getframe().f_code.co_name + "Jones 1D"

        sol = np.matrix([[0.5, 0.25, 0], [0.25, 0.25, 0.25], [0.5, 0.25, 0]])
        M1 = Jones_matrix()
        M1.diattenuator_perfect(angle=0)
        M2 = Jones_matrix()
        M2.quarter_waveplate(angle=0)
        M3 = Jones_matrix()
        M3.diattenuator_perfect(angle=0)
        M = [M1, M2, M3]
        # Angles
        th = [
            0 * degrees, [0, 45 * degrees, 90 * degrees],
            [0, 45 * degrees, 90 * degrees]
        ]
        I = Intensity_Rotating_Elements(M, th)
        assert np.linalg.norm(
            sol - I) < eps, sys._getframe().f_code.co_name + "Jones 2D"

    def test_polarimeter_experiment(self):
        sol = np.identity(4)
        # Optical elements
        P1 = Mueller()
        P1.diattenuator_perfect(angle=0)
        P2 = Mueller()
        P2.diattenuator_perfect(angle=0)
        R1 = Mueller()
        R1.quarter_waveplate(angle=0)
        R2 = Mueller()
        R2.quarter_waveplate(angle=0)
        M = [P1, R1, R2, P2]
        I0 = Stokes()
        I0.circular_light()
        # Generate angles and the intensity array
        N = 16
        theta = np.zeros([N, 4])
        I = np.zeros(N)
        for ind in range(N):
            # Random angles
            angles = np.random.rand(4)
            theta[ind, :] = angles
            # Make the experiment with air as sample
            I[ind] = Intensity_Rotating_Elements(M, angles, I0)
        # Make the polarimeter experiment
        Mcalculated = polarimeter_experiment(M, theta, I0, I)
        print(Mcalculated)
        assert np.linalg.norm(
            sol - Mcalculated) < eps, sys._getframe().f_code.co_name + "Air"
