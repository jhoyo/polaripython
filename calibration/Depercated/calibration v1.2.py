# !/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------
# Author:    Jesus del Hoyo
# Fecha       2018/07/25 (version 1.0)
# License:    GPL
# -------------------------------------
"""
This script has the functions specific for the calibration of the polarimeter.

## Intensity_Rotating_Elements

"""

import numpy as np
import pprint

from copy import deepcopy
import os
import time
import datetime
import matplotlib.pyplot as plt
from shutil import copyfile
from scipy import optimize
from scipy.special import erf

from py_pol.mueller import Mueller
from py_pol.stokes import Stokes
from py_pol.jones_matrix import Jones_matrix
from py_pol.jones_vector import Jones_vector

from . import degrees
from .utils import iscolumn, plot_experiment_residuals_1D, plot_experiment_residuals_2D, percentage_complete, dist_angles, sort_pes, plot_error_Mueller, plot_experiment_1D, set_cal_dict, adapt_step_to_polarimeter
from .polarimeter import Intensity_Rotating_Elements, polarimeter_experiment, Analysis_Measurement_0D, go_and_measure_0D, initialize_motors_and_card, correct_intensity_0D, Make_Measurement_0D, Analysis_Measurement_0D, close_equipment, correct_intensity_0D, measure_int

# TODO: Light source stability, remove _test variables


def initialize_calibration(cal_dict):
    """This function initializes motors and card, and creates the required folder if it doesn't exist."""
    # try:
    # Motors and card
    cal_dict["equipment"] = initialize_motors_and_card(verbose=False)
    # Files
    date = datetime.date.today()
    cal_dict['date'] = date
    extra_name = 'Calibration_{}'.format(date)
    os.chdir(cal_dict['cal_path'])
    try:
        os.mkdir(extra_name)
    except:
        print('Folder already created')
    cal_dict['cal_final_path'] = cal_dict['cal_path'] + '\\' + extra_name
    print(cal_dict['cal_final_path'])
    os.chdir(cal_dict['cal_final_path'])
    print('Calibration initialized succesfully')
    # except:
    #     close_equipment(cal_dict["equipment"])
    #     cal_dict = {}
    return cal_dict


def make_step_0e(cal_dict,
                 num_data=60,
                 max_angle=180,
                 motor_num=3,
                 verbose=False):
    """Step 2: align R0. We make a loop wit one motor with a polarizer to check the circularity of the illumination"""
    # Create array of angles that will be used
    var_angle = np.linspace(0, max_angle, num_data)
    # Initialize data
    Iexp = np.zeros([num_data, 2])
    theta = np.zeros(4)
    # Make the loop
    percentage_complete()
    for ind, angle in enumerate(var_angle):
        theta[motor_num] = angle
        Iexp[ind, :] = go_and_measure_0D(
            cal_dict["equipment"], theta=theta, verbose=verbose)
        percentage_complete(ind + 1, num_data)
    # Use information in reference channel to correct data
    Iexp, _ = correct_intensity_0D(Iexp)
    # Fit to the function
    (Imax, Imin) = (np.max(Iexp), np.min(Iexp))
    par0 = [Imin, Imax - Imin, 0]
    par1, success = optimize.leastsq(
        error_step_0e, par0, args=(var_angle, Iexp))
    # Plot result if required
    Imodel = model_step_0e(par1, var_angle)
    plot_experiment_residuals_1D(var_angle, Iexp, Imodel, title='Step 0e')
    # Print results
    print('The values obtained are:')
    print('   - Imax       : {:.4f} V'.format(par1[1] + par1[0]))
    print('   - Imin       : {:.4f} V'.format(par1[0]))
    print('   - Difference : {:.4f} V'.format(par1[1]))


def model_step_0e(par, angle):
    """Function that serves as optimization for step 2."""
    Imodel = par[0] + par[1] * np.cos(angle - par[2])**2
    return Imodel


def error_step_0e(par, angle, Iexp):
    """Function that serves as optimization for step 2."""
    Imodel = model_step_0e(par, angle)
    dif = Iexp - Imodel
    return dif


def make_step_0g(cal_dict, Nmeasurements=60, Twait=1, verbose=False):
    """Function to make step 3: photodetector stability."""
    # Make the measurement
    Iexp = np.zeros([Nmeasurements, 2])
    percentage_complete()
    for ind in range(Nmeasurements):
        Iexp[ind, :] = measure_int(cal_dict["equipment"], verbose=verbose)
        time.sleep(Twait)
        percentage_complete(ind + 1, Nmeasurements)
    # Make stadistics
    ratio_individual = Iexp[:, 0] / Iexp[:, 1]
    mean = np.mean(Iexp, axis=0)
    error = np.std(Iexp, axis=0)
    ratio2 = np.mean(ratio_individual)
    ratio_error2 = np.std(ratio_individual)
    error_correction = np.sqrt((error[0] / mean[0])**2 +
                               2 * (error[1] / mean[1])**2)
    # Fake plot data
    t = range(Nmeasurements)
    meanCh1y = np.ones(Nmeasurements) * mean[0]
    meanCh1yUp = np.ones(Nmeasurements) * (mean[0] + error[0])
    meanCh1yDown = np.ones(Nmeasurements) * (mean[0] - error[0])
    meanCh2y = np.ones(Nmeasurements) * mean[1]
    meanCh2yUp = np.ones(Nmeasurements) * (mean[1] + error[1])
    meanCh2yDown = np.ones(Nmeasurements) * (mean[1] - error[1])
    meanRy2 = np.ones(Nmeasurements) * ratio2
    meanRyUp2 = np.ones(Nmeasurements) * (ratio2 + ratio_error2)
    meanRyDown2 = np.ones(Nmeasurements) * (ratio2 - ratio_error2)
    # Plot it
    plt.figure(figsize=(24, 4))
    plt.subplot(1, 3, 1)
    plt.plot(t, Iexp[:, 0] / meanCh1y, 'k')
    plt.plot(t, meanCh1y / meanCh1y, 'b')
    plt.plot(t, meanCh1yUp / meanCh1y, 'r--')
    plt.plot(t, meanCh1yDown / meanCh1y, 'r--')
    plt.title('Normalized PhD 1 (Signal)')
    plt.xlabel('Time (s)')
    plt.ylabel('Intensity (V)')
    plt.subplot(1, 3, 2)
    plt.plot(t, Iexp[:, 1] / meanCh2y, 'k')
    plt.plot(t, meanCh2y / meanCh2y, 'b')
    plt.plot(t, meanCh2yUp / meanCh2y, 'r--')
    plt.plot(t, meanCh2yDown / meanCh2y, 'r--')
    plt.title('Normalized PhD 2 (Reference)')
    plt.xlabel('Time (s)')
    plt.ylabel('Intensity (V)')
    plt.subplot(1, 3, 3)
    plt.plot(t, ratio_individual / meanRy2, 'k')
    plt.plot(t, meanRy2 / meanRy2, 'b')
    plt.plot(t, meanRyUp2 / meanRy2, 'r--')
    plt.plot(t, meanRyDown2 / meanRy2, 'r--')
    plt.title('Ratio PhD1 / PhD2')
    plt.xlabel('Time (s)')
    plt.ylabel('Intensity (V)')
    # Print result
    print('The resuts are:')
    print('   - Signal channel          : {:.4f} +- {:.4f} V.'.format(
        mean[0], error[0]))
    print('   - Reference channel       : {:.4f} +- {:.4f} V.'.format(
        mean[1], error[1]))
    print('   - Ratio error             : {:.2f} %.'.format(
        ratio_error2 * 100))
    print('   - Error in corrected I    : {:.2f} %.'.format(
        error_correction * 100))
    # Save data
    filename = "Step_0g_{}".format(datetime.date.today())
    np.savez(
        filename + '.npz',
        Nmeasurements=Nmeasurements,
        Twait=Twait,
        Iexp=Iexp,
        mean=mean,
        error=error,
        error_correction=error_correction)
    # Output
    return mean[1], error_correction


def model_step_1(par, th1, Mp1, Ifuente):
    # Ordenar o no los pes del polarizador
    p1_c, p2_c, th0p1b = sort_pes(par[0:3])
    Mpmalo = Mueller()
    Mpmalo.diattenuator_linear(p1=p1_c, p2=p2_c)
    # Calcular
    M = [Mpmalo, Mp1]
    th = [0, th1 + th0p1b]
    Imodel = Intensity_Rotating_Elements(M, th, Ei=Ifuente)
    return Imodel


def error_step_1(par, th1, Mp1, Ifuente, Iexp):
    Imodel = model_step_1(par, th1, Mp1, Ifuente)
    diferencia = Imodel - Iexp
    return diferencia


def make_step_1(cal_dict, verbose=False):
    """Function that performs step 5 of calibration: origin of angles."""
    if cal_dict["type"] in ('experiment', 'Experiment', 'exp', 'Exp'):
        Iexp = np.zeros([cal_dict["N_measures_1D"], 2], dtype=float)
        percentage_complete()
        for ind, angle in enumerate(cal_dict["angles_1"]):
            theta = np.array([0, 0, 0, angle])
            Iexp[ind, :] = go_and_measure_0D(
                cal_dict["equipment"],
                theta=theta,
                I0=cal_dict['I0'],
                verbose=verbose)
            percentage_complete(ind, cal_dict["N_measures_1D"])
        # Correct the Iexp
        Iexp, _ = correct_intensity_0D(Iexp, cal_dict['mean_ref'])
    # Test objects
    Mp1_test = Mueller()
    Mp1_test.diattenuator_linear(p1=0.95, p2=0.05)
    Ifuente = Stokes()
    Ifuente.circular_light(intensity=Iexp.max() / 0.95**4)
    par0 = [0.95, 0.4, 0.9]
    par1, success = optimize.leastsq(
        error_step_1,
        par0,
        args=(cal_dict["angles_1"], Mp1_test, Ifuente, Iexp))
    Imodel = model_step_1(par1, cal_dict["angles_1"], Mp1_test, Ifuente)
    # Plot results, 1D
    plot_experiment_residuals_1D(
        cal_dict["angles_1"],
        Iexp,
        Imodel,
        title='Step 1',
        xlabel='P1 angle (deg)')
    # Print results
    print('Preliminarr analysis first iteration:')
    print('   - Pmalo p1      : {:.3f}'.format(par1[0]))
    print('   - Pmalo p2      : {:.3f}'.format(par1[1]))
    print('   - P1 theta_0    : {:.1f} deg'.format(par1[2] / degrees))
    print('   - Diode I0      : {:.3f} V'.format(Iexp.max()))
    # Save data
    filename = "Step_1_{}".format(datetime.date.today())
    np.savez(
        filename + '.npz',
        angles_1=cal_dict["angles_1"],
        Iexp=Iexp,
        fit=par1,
        dict_param=set_cal_dict(cal_dict))
    # Output
    return Iexp, par1[2], Ifuente


def model_step_2a(par, th1, Mp1, Mr2, Ifuente):
    # Calcular
    M = [Mr2, Mp1]
    th = [par, th1]
    Imodel = Intensity_Rotating_Elements(M, th, Ei=Ifuente)
    return Imodel


def error_step_2a(par, th1, Mp1, Mr2, Ifuente, Iexp):
    Imodel = model_step_2a(par, th1, Mp1, Mr2, Ifuente)
    diferencia = Imodel - Iexp
    return diferencia


def make_step_2a(cal_dict, verbose=False):
    """Function that performs step 6 of calibration: theta0 of R2."""
    if cal_dict["type"] in ('experiment', 'Experiment', 'exp', 'Exp'):
        Iexp = np.zeros([cal_dict["N_measures_1D"], 2], dtype=float)
        percentage_complete()
        for ind, angle in enumerate(cal_dict["angles_1"]):
            theta = np.array([0, 0, 0, angle])
            Iexp[ind, :] = go_and_measure_0D(
                cal_dict["equipment"],
                theta=theta,
                I0=cal_dict['I0'],
                verbose=verbose)
            percentage_complete(ind, cal_dict["N_measures_1D"])
        # Correct the Iexp
        Iexp, _ = correct_intensity_0D(Iexp, cal_dict['mean_ref'])
    # Test objects
    Mp1_test = Mueller()
    Mp1_test.diattenuator_linear(
        p1=0.95, p2=0.05, angle=cal_dict["th0_p1b_test"])
    Mr2_test = Mueller()
    Mr2_test.retarder_linear(D=82.9 * degrees)
    # Ifuente = cal_dict['Ifuente_test']
    Ifuente = Stokes()
    Ifuente.circular_light(intensity=Iexp.max() / 0.95**4)
    par0 = 0
    # Make analysis
    par1, success = optimize.leastsq(
        error_step_2a,
        par0,
        args=(cal_dict["angles_1"], Mp1_test, Mr2_test, Ifuente, Iexp))
    Imodel = model_step_2a(par1, cal_dict["angles_1"], Mp1_test, Mr2_test,
                           Ifuente)
    # Plot results, 1D
    plot_experiment_residuals_1D(
        cal_dict["angles_1"],
        Iexp,
        Imodel,
        title='Step 2a',
        xlabel='R2 angle (deg)')
    # Print results
    print(par1)
    print('Preliminary analysis:')
    print('   - R1 theta_0    : {:.1f} deg'.format(par1[0] / degrees))
    # Save data
    filename = "Step_2a_{}".format(datetime.date.today())
    np.savez(
        filename + '.npz',
        angles_1=cal_dict["angles_1"],
        Iexp=Iexp,
        fit=par1,
        dict_param=set_cal_dict(cal_dict))
    # Output
    return Iexp, par1


def model_step_2b(par, th1, th2, Mp1, Mr2, error_amplitude):
    # Iluminacion y angulos
    Itest = Stokes()
    Itest.general_azimuth_ellipticity(
        intensity=par[0], az=par[1], el=par[2], pol_degree=par[3])
    # Calcular
    th_error = error_amplitude * erf(par[4])
    th = [th1 + th_error, th2]
    M = [Mr2, Mp1]
    I = Intensity_Rotating_Elements(M, th, Itest, False)
    return I


def error_step_2b(par, th1, th2, Mp1, Mr2, error_amplitude, Iexp):
    dI = model_step_2b(par, th1, th2, Mp1, Mr2, error_amplitude) - Iexp
    return dI.flatten()


def make_step_2b(cal_dict, verbose=False):
    """Function that performs step 7 of calibration: analysis of light source."""
    if cal_dict["type"] in ('experiment', 'Experiment', 'exp', 'Exp'):
        # Make the experiment
        Iexp = np.zeros(
            [cal_dict["N_measures_2D"][0], cal_dict["N_measures_2D"][1], 2],
            dtype=float)
        percentage_complete()
        for ind1, angleX in enumerate(cal_dict["angles_2X"]):
            for ind2, angleY in enumerate(cal_dict["angles_2Y"]):
                theta = np.array([0, 0, angleX, angleY])
                Iexp[ind1, ind2, :] = go_and_measure_0D(
                    cal_dict["equipment"],
                    theta=theta,
                    I0=cal_dict['I0'],
                    verbose=verbose)
                percentage_complete([ind1, ind2], cal_dict["N_measures_2D"])
        # Correct the Iexp
        Iexp, _ = correct_intensity_0D(Iexp, cal_dict['mean_ref'])
    # Test objects
    Mp1_test = Mueller()
    Mp1_test.diattenuator_linear(
        p1=0.95, p2=0.05, angle=cal_dict["th0_p1b_test"])
    Mr2_test = Mueller()
    Mr2_test.retarder_linear(D=82.9 * degrees, angle=cal_dict["th0_r2b_test"])
    par0 = [
        cal_dict['Ifuente_test'].parameters.intensity(), 0, np.pi / 4, 0, 0
    ]
    # Make analysis
    par1, success = optimize.leastsq(
        error_step_2b,
        par0,
        args=(cal_dict["angles_2X"], cal_dict["angles_2Y"], Mp1_test, Mr2_test,
              cal_dict["err_amp"], Iexp))
    Imodel = model_step_2b(par1, cal_dict["angles_2X"], cal_dict["angles_2Y"],
                           Mp1_test, Mr2_test, cal_dict["err_amp"])
    # Plot results, 1D
    Ifuente = Stokes('Illumination')
    Ifuente.general_azimuth_ellipticity(
        az=par1[1], el=par1[2], intensity=par1[0], pol_degree=par1[3])
    plot_experiment_residuals_2D(
        cal_dict["angles_2X"],
        cal_dict["angles_2Y"],
        Iexp,
        Imodel,
        title='Step 2b',
        xlabel='Angle R2 (deg)',
        ylabel='Angle P1 (deg)')
    # Print results
    print('Preliminary analysis:')
    print(Ifuente)
    # Save data
    filename = "Step_2b_{}".format(datetime.date.today())
    np.savez(
        filename + '.npz',
        angles2x=cal_dict["angles_2X"],
        angles2y=cal_dict["angles_2Y"],
        Iexp=Iexp,
        fit=Ifuente,
        dict_param=set_cal_dict(cal_dict))
    # Output
    return Iexp, Ifuente


def make_step_3a(cal_dict, verbose=False):
    """Function that performs step 8a of calibration."""
    if cal_dict["type"] in ('experiment', 'Experiment', 'exp', 'Exp'):
        Iexp = np.zeros([cal_dict["N_measures_1D"], 2], dtype=float)
        percentage_complete()
        for ind, angle in enumerate(cal_dict["angles_1"]):
            theta = np.array([0, 0, 0, angle])
            Iexp[ind, :] = go_and_measure_0D(
                cal_dict["equipment"],
                theta=theta,
                I0=cal_dict['I0'],
                verbose=verbose)
            percentage_complete(ind, cal_dict["N_measures_1D"])
        # Correct the Iexp
        Iexp, _ = correct_intensity_0D(Iexp, cal_dict['mean_ref'])
    # Plot results, 1D
    plot_experiment_1D(cal_dict["angles_1"], Iexp, title='Step 3a')
    # Save data

    filename = "Step_3a_{}".format(datetime.date.today())
    np.savez(
        filename + '.npz',
        angles_1=cal_dict["angles_1"],
        Iexp=Iexp,
        dict_param=set_cal_dict(cal_dict))
    # Output
    return Iexp


def make_step_3b(cal_dict, verbose=False):
    """Function that performs step 8a of calibration."""
    if cal_dict["type"] in ('experiment', 'Experiment', 'exp', 'Exp'):
        Iexp = np.zeros([cal_dict["N_measures_1D"], 2], dtype=float)
        percentage_complete()
        for ind, angle in enumerate(cal_dict["angles_1"]):
            theta = np.array([0, 0, 0, angle])
            Iexp[ind, :] = go_and_measure_0D(
                cal_dict["equipment"],
                theta=theta,
                I0=cal_dict['I0'],
                verbose=verbose)
            percentage_complete(ind, cal_dict["N_measures_1D"])
        # Correct the Iexp
        Iexp, _ = correct_intensity_0D(Iexp, cal_dict['mean_ref'])
    # Plot results, 1D
    plot_experiment_1D(cal_dict["angles_1"], Iexp, title='Step 3a')
    # Save data

    filename = "Step_3b_{}".format(datetime.date.today())
    np.savez(
        filename + '.npz',
        angles_1=cal_dict["angles_1"],
        Iexp=Iexp,
        dict_param=set_cal_dict(cal_dict))
    # Output
    return Iexp


def make_step_3c(cal_dict, verbose=False):
    """Function that performs step 8a of calibration."""
    if cal_dict["type"] in ('experiment', 'Experiment', 'exp', 'Exp'):
        Iexp = np.zeros([cal_dict["N_measures_1D"], 2], dtype=float)
        percentage_complete()
        for ind, angle in enumerate(cal_dict["angles_1"]):
            theta = np.array([0, 0, 0, angle])
            Iexp[ind, :] = go_and_measure_0D(
                cal_dict["equipment"],
                theta=theta,
                I0=cal_dict['I0'],
                verbose=verbose)
            percentage_complete(ind, cal_dict["N_measures_1D"])
        # Correct the Iexp
        Iexp, _ = correct_intensity_0D(Iexp, cal_dict['mean_ref'])
    # Plot results, 1D
    plot_experiment_1D(cal_dict["angles_1"], Iexp, title='Step 3a')
    # Save data

    filename = "Step_3c_{}".format(datetime.date.today())
    np.savez(
        filename + '.npz',
        angles_1=cal_dict["angles_1"],
        Iexp=Iexp,
        dict_param=set_cal_dict(cal_dict))
    # Output
    return Iexp


def model_step_3(par, th1, Ifuente, th0p1b):
    # Order parameters
    p11, p12, p21, p22, p31, p32, th0p1, th0p2, th0p3 = sort_pes(par[0:9])
    # Create Mueller objects
    Mp1 = Mueller()
    Mp1.diattenuator_linear(p1=p11, p2=p12)
    Mp2 = Mueller()
    Mp2.diattenuator_linear(p1=p21, p2=p22)
    Mp3 = Mueller()
    Mp3.diattenuator_linear(p1=p31, p2=p32)
    # First, P3 and P1
    M = [Mp3, Mp1]
    th = [th0p3, th1 + th0p1b]
    Ia = Intensity_Rotating_Elements(M, th, Ei=Ifuente)
    # Then, P3 and P2
    M = [Mp3, Mp2]
    th = [th0p3, th1 + th0p2]
    Ib = Intensity_Rotating_Elements(M, th, Ei=Ifuente)
    # Last, P1 and P2
    M = [Mp1, Mp2]
    th = [th0p1, th1 + th0p2]
    Ic = Intensity_Rotating_Elements(M, th, Ei=Ifuente)
    # End
    return Ia, Ib, Ic


def error_step_3(par, th1, Ifuente, th0p1b, IexpA, IexpB, IexpC):
    Ia, Ib, Ic = model_step_3(par, th1, Ifuente, th0p1b)
    errA = IexpA - Ia
    errB = IexpB - Ib
    errC = IexpC - Ic
    return np.concatenate((errA, errB, errC))


def analysis_step_3(cal_dict):
    "Makes the first analysis of step 8: measurement of polarizers"
    # initial guess
    par0 = [0.95, 0.05, 0.95, 0.05, 0.95, 0.05, 0, 0, 0]
    # Make analysis
    par1, success = optimize.leastsq(
        error_step_3,
        par0,
        args=(cal_dict["angles_1"], cal_dict["Ifuente_test"],
              cal_dict["th0_p1b_test"], cal_dict["I_step_3a"],
              cal_dict["I_step_3b"], cal_dict["I_step_3c"]))
    Imodel = model_step_3(par1, cal_dict["angles_1"], cal_dict["Ifuente_test"],
                          cal_dict["th0_p1b_test"])
    Imodel = np.concatenate(Imodel)
    # Plot results, 1D
    angles = np.concatenate(
        (cal_dict["angles_1"], cal_dict["angles_1"] + cal_dict["max_angle_1D"],
         cal_dict["angles_1"] + 2 * cal_dict["max_angle_1D"]))
    intensities = np.concatenate((cal_dict["I_step_3a"], cal_dict["I_step_3b"],
                                  cal_dict["I_step_3c"]))
    plot_experiment_residuals_1D(
        angles,
        intensities,
        Imodel,
        title='Step 3',
        xlabel='Angle 2nd pol. (deg)')
    # Print results
    print('Preliminary analysis:')
    print('   - P1 p1         : {:.3f}'.format(par1[0]))
    print('   - P1 p2         : {:.3f}'.format(par1[1]))
    print('   - P1 theta_0    : {:.1f} deg'.format(par1[6] / degrees))
    print('   - P2 p1         : {:.3f}'.format(par1[2]))
    print('   - P2 p2         : {:.3f}'.format(par1[3]))
    print('   - P2 theta_0    : {:.1f} deg'.format(par1[7] / degrees))
    print('   - P3 p1         : {:.3f}'.format(par1[4]))
    print('   - P3 p2         : {:.3f}'.format(par1[5]))
    print('   - P3 theta_0    : {:.1f} deg'.format(par1[8] / degrees))
    # Save data

    filename = "Step_3_analysis_{}".format(datetime.date.today())
    np.savez(
        filename + '.npz',
        angles_total=angles,
        Iexp_total=intensities,
        th0_p1=par1[6],
        th0_p2=par1[7],
        dict_param=set_cal_dict(cal_dict))
    # Extract info
    Mp1 = Mueller()
    Mp1.diattenuator_linear(p1=par1[0], p2=par1[1], angle=par1[6])
    Mp2 = Mueller()
    Mp2.diattenuator_linear(p1=par1[2], p2=par1[3], angle=par1[7])
    # Output
    return Mp1, par1[0], par1[1], par1[6], Mp2, par1[7]


def model_step_4a(par, th, Mp1, Mp2, Mr2, Ifuente, error_amplitude):
    # Calcular
    th_error = error_amplitude * erf(par[2])
    p1, p2, az, el = Mp1.analysis.diattenuator(param='az_el')
    Mp1.diattenuator_retarder_linear(p1, p2, par[1], az + th_error)
    M = [Mp1, Mr2, Mp2]
    th = [0, par[0], th]
    I = Intensity_Rotating_Elements(M, th, Ifuente, False)
    return I


def error_step_4a(par, th, Mp1, Mp2, Mr2, Ifuente, Iexp, error_amplitude):
    Imodel = model_step_4a(par, th, Mp1, Mp2, Mr2, Ifuente, error_amplitude)
    diferencia = Imodel - Iexp
    return diferencia


def make_step_4a(cal_dict, verbose=False):
    """Function that performs step 9 of calibration: reference angle of R2."""
    if cal_dict["type"] in ('experiment', 'Experiment', 'exp', 'Exp'):
        # Make the experiment
        Iexp = np.zeros([cal_dict["N_measures_1D"], 2], dtype=float)
        percentage_complete()
        for ind, angle in enumerate(cal_dict["angles_1"]):
            theta = np.array([0, 0, 0, angle])
            Iexp[ind, :] = go_and_measure_0D(
                cal_dict["equipment"],
                theta=theta,
                I0=cal_dict['I0'],
                verbose=verbose)
            percentage_complete(ind, cal_dict["N_measures_1D"])
        # Correct the Iexp
        Iexp, _ = correct_intensity_0D(Iexp, cal_dict['mean_ref'])
    # Make analysis
    par0 = 2
    Mr2 = Mueller()
    Mr2.diattenuator_retarder_linear(p1=0.99, p2=0.99, D=83 * degrees, angle=0)
    par1, success = optimize.leastsq(
        error_step_4a,
        par0,
        args=(cal_dict["angles_1"], cal_dict["Mp1_test"], cal_dict["Mp2_test"],
              Mr2, cal_dict["Ifuente_test"], Iexp))
    Imodel = model_step_4a(par1, cal_dict["angles_1"], cal_dict["Mp1_test"],
                           cal_dict["Mp2_test"], Mr2, cal_dict["Ifuente_test"])
    # Plot results, 1D
    plot_experiment_residuals_1D(
        cal_dict["angles_1"],
        Iexp,
        Imodel,
        title='Step 4a',
        xlabel='P2 angle (deg)')
    # Print results
    print('Preliminary analysis:')
    print('   - R2 angle         : {:.1f} deg'.format(par1[0] / degrees))
    # Save data

    filename = "Step_4a_{}".format(datetime.date.today())
    np.savez(
        filename + '.npz',
        angles_1=cal_dict["angles_1"],
        Iexp=Iexp,
        fit=par1[0],
        dict_param=set_cal_dict(cal_dict))
    # Output
    return Iexp, par1[0]


def model_step_4b(par, th1, th2, Mp2, Ifuente, p11, p12, th0p1, th0r2,
                  error_amplitude):
    # Calculate auxilliary angles
    th_error = error_amplitude * erf(par[4])
    # Create Mueller objects
    Mr2 = Mueller()
    Mr2.diattenuator_retarder_linear(
        p1=par[0], p2=par[1], D=par[2], angle=th0r2 + th_error)
    Mp1 = Mueller()
    Mp1.diattenuator_retarder_linear(p1=p11, p2=p12, D=par[3], angle=th0p1)
    # Calcular
    M = [Mp1, Mr2, Mp2]
    th = [0, th1, th2]
    I = Intensity_Rotating_Elements(M, th, Ifuente, False)
    return I


def error_step_4b(par, th1, th2, Mp2, Ifuente, p11, p12, th0p1, th0r2,
                  error_amplitude, Iexp):
    Imodel = model_step_4b(par, th1, th2, Mp2, Ifuente, p11, p12, th0p1, th0r2,
                           error_amplitude)
    dI = Imodel - Iexp
    return dI.flatten()


def model_step_4(par, th1, thX, thY, Mp2, Ifuente, p11, p12, th0p1,  error_amplitude):
    # Calculate auxilliary angles
    th_error = error_amplitude * erf(par[5])
    # Create Mueller objects
    Mr2 = Mueller()
    Mr2.diattenuator_retarder_linear(
        p1=par[0], p2=par[1], D=par[2], angle=par[3])
    Mp1 = Mueller()
    Mp1.diattenuator_retarder_linear(p1=p11, p2=p12, D=par[4], angle=th0p1)
    # Calcular
    M = [Mp1, Mr2, Mp2]
    th = [0, 0, th1]
    Ia = Intensity_Rotating_Elements(M, th, Ifuente, False)
    th = [0, thX, thY]
    Ib = Intensity_Rotating_Elements(M, th, Ifuente, False)
    return Ia, Ib


def error_step_4(par, th1, thX, thY, Mp2, Ifuente, p11, p12, th0p1,  error_amplitude, IexpA, IexpB):
    Ia, Ib = model_step_4(par, th1, thX, thY, Mp2, Ifuente,
                          p11, p12, th0p1,  error_amplitude)
    difA = IexpA - Ia
    difB = IexpB - Ib
    return np.concatenate((difA, difB.flatten()))


def make_step_4b(cal_dict, verbose=False):
    """Function that performs step 10 of calibration: calibration of R2."""
    if cal_dict["type"] in ('experiment', 'Experiment', 'exp', 'Exp'):
        # Make the experiment
        Iexp = np.zeros(
            [
                cal_dict["N_measures_2D_step_4b"][0],
                cal_dict["N_measures_2D_step_4b"][1], 2
            ],
            dtype=float)
        percentage_complete()
        for ind1, angleX in enumerate(cal_dict["angles_2X_step_4b"]):
            for ind2, angleY in enumerate(cal_dict["angles_2Y_step_4b"]):
                theta = np.array([0, 0, angleX, angleY])
                Iexp[ind1, ind2, :] = go_and_measure_0D(
                    cal_dict["equipment"],
                    theta=theta,
                    I0=cal_dict['I0'],
                    verbose=verbose)
                percentage_complete([ind1, ind2],
                                    cal_dict["N_measures_2D_step_4b"])
        # Correct the Iexp
        Iexp, _ = correct_intensity_0D(Iexp, cal_dict['mean_ref'])
    # Make analysis
    par0 = [0.99, 0.99, 83 * degrees, 90 * degrees, 0]
    par1, success = optimize.leastsq(
        error_step_4b,
        par0,
        args=(cal_dict["angles_2X_step_4b"], cal_dict["angles_2Y_step_4b"],
              cal_dict["Mp2_test"], cal_dict["Ifuente_test"], cal_dict["p11"],
              cal_dict["p12"], cal_dict["th0_p1_test"],
              cal_dict["th0_r2_test"], cal_dict["err_amp"], Iexp))
    Imodel = model_step_4b(par1, cal_dict["angles_2X_step_4b"],
                           cal_dict["angles_2Y_step_4b"], cal_dict["Mp2_test"],
                           cal_dict["Ifuente_test"], cal_dict["p11"],
                           cal_dict["p12"], cal_dict["th0_p1_test"],
                           cal_dict["th0_r2_test"], cal_dict["err_amp"])
    # Plot results, 1D
    plot_experiment_residuals_2D(
        cal_dict["angles_2X_step_4b"],
        cal_dict["angles_2Y_step_4b"],
        Iexp,
        Imodel,
        title='Step 4b',
        xlabel='Angle R2 (deg)',
        ylabel='Angle P2 (deg)')
    # Print results
    print('Analysis first iteration:')
    print('   - R2 p1            : {:.3f}'.format(par1[0]))
    print('   - R2 p2            : {:.3f}'.format(par1[1]))
    print('   - R2 retardance    : {:.1f} deg'.format(par1[2] / degrees))
    print('   - P1 retardance    : {:.1f} deg'.format(par1[3] / degrees))
    # Extract info
    fit_dict = {}
    Mp1 = Mueller()
    Mp1.diattenuator_retarder_linear(
        p1=cal_dict["p11"],
        p2=cal_dict["p12"],
        D=par1[4],
        angle=cal_dict["th0_p1_test"])
    fit_dict["Mp1"] = Mp1
    Mr2 = Mueller()
    Mr2.diattenuator_retarder_linear(
        p1=par1[0], p2=par1[1], D=par1[2], angle=par1[3])
    fit_dict["Mr2"] = Mr2
    # Save data

    filename = "Step_4b_{}".format(datetime.date.today())
    np.savez(
        filename + '.npz',
        angles2x=cal_dict["angles_2X_step_4b"],
        angles2y=cal_dict["angles_2Y_step_4b"],
        Iexp=Iexp,
        fit=fit_dict,
        dict_param=set_cal_dict(cal_dict))
    # Output
    return Iexp, Mp1, Mr2


def model_step_5a(par, th, Mp1, Mp2, Mr1, Mr2, Ifuente, mode):
    # Calcular
    M = [Mp1, Mr1, Mr2, Mp2]
    if mode in ('R2', 'R2'):
        th1 = [0, par, th, 0]
    if mode in ('P2', 'P2'):
        th1 = [0, par, 0, th]
    I = Intensity_Rotating_Elements(M, th1, Ifuente, False)
    return I


def error_step_5a(par, th, Mp1, Mp2, Mr1, Mr2, Ifuente, mode, Iexp):
    I = model_step_5a(par, th, Mp1, Mp2, Mr1, Mr2, Ifuente, mode)
    dI = I - Iexp
    return dI


def error_step_5a_all(par, th, Mp1, Mp2, Mr1, Mr2, Ifuente, IexpA, IexpB):
    I1 = model_step_5a(par, th, Mp1, Mp2, Mr1, Mr2, Ifuente, 'R2')
    I2 = model_step_5a(par, th, Mp1, Mp2, Mr1, Mr2, Ifuente, 'P2')
    dI1 = I1 - IexpA
    dI2 = I2 - IexpB
    return np.concatenate((dI1, dI2))


def make_step_5a(cal_dict, verbose=False):
    """Function that performs step 11 of calibration: reference angle of R2."""
    if cal_dict["type"] in ('experiment', 'Experiment', 'exp', 'Exp'):
        IexpA = np.zeros([cal_dict["N_measures_1D"], 2], dtype=float)
        # First rotate R2
        if cal_dict["step_5a"] in ('all', 'All', 'ALL', 'individual',
                                   'Individual', 'R2'):
            IexpA = np.zeros([cal_dict["N_measures_1D"], 2], dtype=float)
            percentage_complete()
            for ind, angle in enumerate(cal_dict["angles_1"]):
                theta = np.array([0, 0, angle, 0])
                IexpA[ind, :] = go_and_measure_0D(
                    cal_dict["equipment"],
                    theta=theta,
                    I0=cal_dict['I0'],
                    verbose=verbose)
                percentage_complete(ind, cal_dict["N_measures_1D"])
            # Correct the Iexp
            IexpA, _ = correct_intensity_0D(IexpA, cal_dict['mean_ref'])
        else:
            IexpA = None
        # Then rotate P2
        if cal_dict["step_5a"] in ('all', 'All', 'ALL', 'individual',
                                   'Individual', 'P2'):
            IexpB = np.zeros([cal_dict["N_measures_1D"], 2], dtype=float)
            percentage_complete()
            for ind, angle in enumerate(cal_dict["angles_1"]):
                theta = np.array([0, 0, 0, angle])
                IexpB[ind, :] = go_and_measure_0D(
                    cal_dict["equipment"],
                    theta=theta,
                    I0=cal_dict['I0'],
                    verbose=verbose)
                percentage_complete(ind, cal_dict["N_measures_1D"])
            # Correct the Iexp
            IexpB, _ = correct_intensity_0D(IexpB, cal_dict['mean_ref'])
        else:
            IexpB = None
    # Make analysis
    par0 = 3
    Mr1 = Mueller()
    Mr1.diattenuator_retarder_linear(p1=0.99, p2=0.99, D=83 * degrees, angle=0)
    # Only R2
    if cal_dict["step_5a"] in ('individual', 'Individual', 'R2'):
        par1, success = optimize.leastsq(
            error_step_5a,
            par0,
            args=(cal_dict["angles_1"], cal_dict["Mp1_test"],
                  cal_dict["Mp2_test"], Mr1, cal_dict["Mr2_test"],
                  cal_dict["Ifuente_test"], 'R2', IexpA))
        ImodelA = model_step_5a(par1, cal_dict["angles_1"],
                                cal_dict["Mp1_test"], cal_dict["Mp2_test"],
                                Mr1, cal_dict["Mr2_test"],
                                cal_dict["Ifuente_test"], 'R2')
    # Only P2
    if cal_dict["step_5a"] in ('individual', 'Individual', 'P2'):
        par1, success = optimize.leastsq(
            error_step_5a,
            par0,
            args=(cal_dict["angles_1"], cal_dict["Mp1_test"],
                  cal_dict["Mp2_test"], Mr1, cal_dict["Mr2_test"],
                  cal_dict["Ifuente_test"], 'P2', IexpB))
        ImodelB = model_step_5a(par1, cal_dict["angles_1"],
                                cal_dict["Mp1_test"], cal_dict["Mp2_test"],
                                Mr1, cal_dict["Mr2_test"],
                                cal_dict["Ifuente_test"], 'P2')
    # Both together
    if cal_dict["step_5a"] in ('all', 'All', 'ALL'):
        par1, success = optimize.leastsq(
            error_step_5a_all,
            par0,
            args=(cal_dict["angles_1"], cal_dict["Mp1_test"],
                  cal_dict["Mp2_test"], Mr1, cal_dict["Mr2_test"],
                  cal_dict["Ifuente_test"], IexpA, IexpB))
        ImodelA = model_step_5a(par1, cal_dict["angles_1"],
                                cal_dict["Mp1_test"], cal_dict["Mp2_test"],
                                Mr1, cal_dict["Mr2_test"],
                                cal_dict["Ifuente_test"], 'R2')
        ImodelB = model_step_5a(par1, cal_dict["angles_1"],
                                cal_dict["Mp1_test"], cal_dict["Mp2_test"],
                                Mr1, cal_dict["Mr2_test"],
                                cal_dict["Ifuente_test"], 'P2')
    # Errors
    if IexpA is None:
        errorA = 0
    else:
        errorA = np.linalg.norm(ImodelA - IexpA) / (
            cal_dict["N_measures_1D"] * np.max(IexpA))
    if IexpA is None:
        errorB = 0
    else:
        errorB = np.linalg.norm(ImodelB - IexpB) / (
            cal_dict["N_measures_1D"] * np.max(IexpB))
    # Plot results, 1D
    if IexpA is not None:
        plot_experiment_residuals_1D(
            cal_dict["angles_1"],
            IexpA,
            ImodelA,
            title='Step 5a: Rotating R2',
            xlabel='Rotation angle (deg)')
    if IexpB is not None:
        plot_experiment_residuals_1D(
            cal_dict["angles_1"],
            IexpB,
            ImodelB,
            title='Step 5a: Rotating P2',
            xlabel='Rotation angle (deg)')
    # Print results
    print('Errors:')
    print('   - Rotating R2      : {:.3f} %'.format(errorA * 100))
    print('   - Rotating P2      : {:.3f} %'.format(errorB * 100))
    print('Preliminary analysis:')
    print('   - R1 angle         : {:.1f} deg'.format(par1[0] / degrees))
    # Save data

    filename = "Step_5a_{}".format(datetime.date.today())
    np.savez(
        filename + '.npz',
        angles_1=cal_dict["angles_1"],
        IexpA=IexpA,
        IexpB=IexpB,
        fit=par1[0],
        dict_param=set_cal_dict(cal_dict))
    # Output
    return IexpA, IexpB, par1[0]


def model_step_5b(par, th1, th2, Mp1, Mr2, Mp2, Ifuente, th0r1,
                  error_amplitude, mode):
    # Rotar la fuente para evitar errores
    th_error1 = error_amplitude * erf(par[3])
    # Mueller objects
    Mr1 = Mueller()
    Mr1.diattenuator_retarder_linear(
        p1=par[0], p2=par[1], D=par[2], angle=th0r1 + th_error1)
    # Angles
    if mode in ('R1+R2', 'R1 R2'):
        th = [0, th1, th2, 0]
    elif mode in ('R1+P2', 'R1 P2'):
        th = [0, th1, 0, th2]
    elif mode in ('R2+P2', 'R2 P2'):
        th = [0, 0, th1, th2]
    # Calcular
    M = [Mp1, Mr1, Mr2, Mp2]
    I = Intensity_Rotating_Elements(M, th, Ifuente)
    return I


def error_step_5b(par, th1, th2, Mp1, Mr2, Mp2, Ifuente, th0r1,
                  error_amplitude, mode, Iexp):
    Imodel = model_step_5b(par, th1, th2, Mp1, Mr2, Mp2, Ifuente, th0r1,
                           error_amplitude, mode)
    dI = Imodel - Iexp
    return dI.flatten()


def error_step_5b_all(par, th1, th2, Mp1, Mr2, Mp2, Ifuente, th0r1,
                      error_amplitude, IexpA, IexpB, IexpC):
    ImodelA = model_step_5b(par, th1, th2, Mp1, Mr2, Mp2, Ifuente, th0r1,
                            error_amplitude, 'R1 R2')
    ImodelB = model_step_5b(par, th1, th2, Mp1, Mr2, Mp2, Ifuente, th0r1,
                            error_amplitude, 'R1 P2')
    ImodelC = model_step_5b(par, th1, th2, Mp1, Mr2, Mp2, Ifuente, th0r1,
                            error_amplitude, 'R2 P2')
    dIA = (ImodelA - IexpA).flatten()
    dIB = (ImodelB - IexpB).flatten()
    dIC = (ImodelC - IexpC).flatten()
    return np.concatenate((dIA, dIA, dIC))


def make_step_5b(cal_dict, verbose=False):
    """Function that performs step 5b of calibration: calibration of R1."""
    if cal_dict["type"] in ('experiment', 'Experiment', 'exp', 'Exp'):
        # Rotate R1 and R2
        if cal_dict["step_5b"] in ('all', 'All', 'ALL', 'individual',
                                   'Individual', 'R1+R2', 'R1 R2'):
            IexpA = np.zeros(
                [
                    cal_dict["N_measures_2D"][0], cal_dict["N_measures_2D"][1],
                    2
                ],
                dtype=float)
            # Make the experiment
            IexpA = np.zeros(
                [
                    cal_dict["N_measures_2D"][0], cal_dict["N_measures_2D"][1],
                    2
                ],
                dtype=float)
            percentage_complete()
            for ind1, angleX in enumerate(cal_dict["angles_2X"]):
                for ind2, angleY in enumerate(cal_dict["angles_2Y"]):
                    theta = np.array([0, angleX, angleY, 0])
                    IexpA[ind1, ind2, :] = go_and_measure_0D(
                        cal_dict["equipment"],
                        theta=theta,
                        I0=cal_dict['I0'],
                        verbose=verbose)
                    percentage_complete([ind1, ind2],
                                        cal_dict["N_measures_2D"])
            # Correct the Iexp
            IexpA, _ = correct_intensity_0D(IexpA, cal_dict['mean_ref'])
        else:
            IexpA = None
        # Rotate R1 and P2
        if cal_dict["step_5b"] in ('all', 'All', 'ALL', 'individual',
                                   'Individual', 'R1+P2', 'R1 P2'):
            # Make the experiment
            IexpB = np.zeros(
                [
                    cal_dict["N_measures_2D"][0], cal_dict["N_measures_2D"][1],
                    2
                ],
                dtype=float)
            percentage_complete()
            for ind1, angleX in enumerate(cal_dict["angles_2X"]):
                for ind2, angleY in enumerate(cal_dict["angles_2Y"]):
                    theta = np.array([0, angleX, 0, angleY])
                    IexpB[ind1, ind2, :] = go_and_measure_0D(
                        cal_dict["equipment"],
                        theta=theta,
                        I0=cal_dict['I0'],
                        verbose=verbose)
                    percentage_complete([ind1, ind2],
                                        cal_dict["N_measures_2D"])
            # Correct the Iexp
            IexpB, _ = correct_intensity_0D(IexpB, cal_dict['mean_ref'])
        else:
            IexpB = None
        # Rotate R2 and P2
        if cal_dict["step_5b"] in ('all', 'All', 'ALL', 'individual',
                                   'Individual', 'R2+P2', 'R2 P2'):
            # Make the experiment
            IexpC = np.zeros(
                [
                    cal_dict["N_measures_2D"][0], cal_dict["N_measures_2D"][1],
                    2
                ],
                dtype=float)
            percentage_complete()
            for ind1, angleX in enumerate(cal_dict["angles_2X"]):
                for ind2, angleY in enumerate(cal_dict["angles_2Y"]):
                    theta = np.array([0, 0, angleX, angleY])
                    IexpC[ind1, ind2, :] = go_and_measure_0D(
                        cal_dict["equipment"],
                        theta=theta,
                        I0=cal_dict['I0'],
                        verbose=verbose)
                    percentage_complete([ind1, ind2],
                                        cal_dict["N_measures_2D"])
            # Correct the Iexp
            IexpC, _ = correct_intensity_0D(IexpC, cal_dict['mean_ref'])
        else:
            IexpC = None

    # Make analysis 1st case
    par0 = [0.99, 0.99, 83 * degrees, 0]
    if cal_dict["step_5b"] in ('individual', 'Individual', 'R1+R2', 'R1 R2'):
        par1, success = optimize.leastsq(
            error_step_5b,
            par0,
            args=(cal_dict["angles_2X"], cal_dict["angles_2Y"],
                  cal_dict["Mp1_test"], cal_dict["Mr2_test"],
                  cal_dict["Mp2_test"], cal_dict["Ifuente_test"],
                  cal_dict["th0_r1_test"], cal_dict["err_amp"], 'R1+R2',
                  IexpA))
        ImodelA = model_step_5b(
            par1, cal_dict["angles_2X"], cal_dict["angles_2Y"],
            cal_dict["Mp1_test"], cal_dict["Mr2_test"], cal_dict["Mp2_test"],
            cal_dict["Ifuente_test"], cal_dict["th0_r1_test"],
            cal_dict["err_amp"], 'R1+R2')
    # Make analysis 2nd case
    if cal_dict["step_5b"] in ('individual', 'Individual', 'R1+P2', 'R1 P2'):
        par1, success = optimize.leastsq(
            error_step_5b,
            par0,
            args=(cal_dict["angles_2X"], cal_dict["angles_2Y"],
                  cal_dict["Mp1_test"], cal_dict["Mr2_test"],
                  cal_dict["Mp2_test"], cal_dict["Ifuente_test"],
                  cal_dict["th0_r1_test"], cal_dict["err_amp"], 'R1+P2',
                  IexpB))
        ImodelB = model_step_5b(
            par1, cal_dict["angles_2X"], cal_dict["angles_2Y"],
            cal_dict["Mp1_test"], cal_dict["Mr2_test"], cal_dict["Mp2_test"],
            cal_dict["Ifuente_test"], cal_dict["th0_r1_test"],
            cal_dict["err_amp"], 'R1+P2')
    # Make analysis 3rd case
    if cal_dict["step_5b"] in ('individual', 'Individual', 'R2+P2', 'R2 P2'):
        par1, success = optimize.leastsq(
            error_step_5b,
            par0,
            args=(cal_dict["angles_2X"], cal_dict["angles_2Y"],
                  cal_dict["Mp1_test"], cal_dict["Mr2_test"],
                  cal_dict["Mp2_test"], cal_dict["Ifuente_test"],
                  cal_dict["th0_r1_test"], cal_dict["err_amp"], 'R2+P2',
                  IexpC))
        ImodelC = model_step_5b(
            par1, cal_dict["angles_2X"], cal_dict["angles_2Y"],
            cal_dict["Mp1_test"], cal_dict["Mr2_test"], cal_dict["Mp2_test"],
            cal_dict["Ifuente_test"], cal_dict["th0_r1_test"],
            cal_dict["err_amp"], 'R2+P2')
    # Make analysis all together
    if cal_dict["step_5b"] in ('all', 'All', 'ALL'):
        par1, success = optimize.leastsq(
            error_step_5b_all,
            par0,
            args=(cal_dict["angles_2X"], cal_dict["angles_2Y"],
                  cal_dict["Mp1_test"], cal_dict["Mr2_test"],
                  cal_dict["Mp2_test"], cal_dict["Ifuente_test"],
                  cal_dict["th0_r1_test"], cal_dict["err_amp"], IexpA, IexpB,
                  IexpC))
        ImodelA = model_step_5b(
            par1, cal_dict["angles_2X"], cal_dict["angles_2Y"],
            cal_dict["Mp1_test"], cal_dict["Mr2_test"], cal_dict["Mp2_test"],
            cal_dict["Ifuente_test"], cal_dict["th0_r1_test"],
            cal_dict["err_amp"], 'R1+R2')
        ImodelB = model_step_5b(
            par1, cal_dict["angles_2X"], cal_dict["angles_2Y"],
            cal_dict["Mp1_test"], cal_dict["Mr2_test"], cal_dict["Mp2_test"],
            cal_dict["Ifuente_test"], cal_dict["th0_r1_test"],
            cal_dict["err_amp"], 'R1+P2')
        ImodelC = model_step_5b(
            par1, cal_dict["angles_2X"], cal_dict["angles_2Y"],
            cal_dict["Mp1_test"], cal_dict["Mr2_test"], cal_dict["Mp2_test"],
            cal_dict["Ifuente_test"], cal_dict["th0_r1_test"],
            cal_dict["err_amp"], 'R2+P2')
    if IexpA is not None:
        # Plot results, 1D
        plot_experiment_residuals_2D(
            cal_dict["angles_2X"],
            cal_dict["angles_2Y"],
            IexpA,
            ImodelA,
            title='Step 5b: R1 and R2',
            xlabel='Angle R1 (deg)',
            ylabel='Angle R2 (deg)')
        errorA = np.linalg.norm(ImodelA - IexpA) / (
            cal_dict["N_measures_1D"] * np.max(IexpA))
    else:
        errorA = 0
    if IexpB is not None:
        # Plot results, 1D
        plot_experiment_residuals_2D(
            cal_dict["angles_2X"],
            cal_dict["angles_2Y"],
            IexpB,
            ImodelB,
            title='Step 5b: R1 and P2',
            xlabel='Angle R1 (deg)',
            ylabel='Angle P2 (deg)')
        errorB = np.linalg.norm(ImodelB - IexpB) / (
            cal_dict["N_measures_1D"] * np.max(IexpB))
    else:
        errorB = 0
    if IexpC is not None:
        # Plot results, 1D
        plot_experiment_residuals_2D(
            cal_dict["angles_2X"],
            cal_dict["angles_2Y"],
            IexpC,
            ImodelC,
            title='Step 5b: R2 and P2',
            xlabel='Angle R2 (deg)',
            ylabel='Angle P2 (deg)')
        errorC = np.linalg.norm(ImodelC - IexpC) / (
            cal_dict["N_measures_1D"] * np.max(IexpC))
    else:
        errorC = 0
    # Calculations
    th0_r1_add = cal_dict["err_amp"] * erf(par1[3])
    # Print results
    print('Errors:')
    print('   - Rotating R1 and R2  : {:.3f} %'.format(errorA * 100))
    print('   - Rotating R1 and P2  : {:.3f} %'.format(errorB * 100))
    print('   - Rotating R2 and P2  : {:.3f} %'.format(errorC * 100))
    print('Analysis first iteration:')
    print('   - R1 p1               : {:.3f}'.format(par1[0]))
    print('   - R1 p2               : {:.3f}'.format(par1[1]))
    print('   - R1 retardance       : {:.1f} deg'.format(par1[2] / degrees))
    print('   - R1 extra theta_0    : {:.1f} deg'.format(th0_r1_add / degrees))
    # Extract info
    Mr1 = Mueller()
    Mr1.diattenuator_retarder_linear(
        p1=par1[0],
        p2=par1[1],
        D=par1[2],
        angle=th0_r1_add + cal_dict["th0_r1_test"])
    # Save data

    filename = "Step_5b_{}".format(datetime.date.today())
    np.savez(
        filename + '.npz',
        angles2x=cal_dict["angles_2X"],
        angles2y=cal_dict["angles_2Y"],
        IexpA=IexpA,
        IexpB=IexpB,
        IexpC=IexpC,
        fit=Mr1,
        dict_param=set_cal_dict(cal_dict))
    # Output
    return IexpA, IexpB, IexpC, Mr1


def make_step_6a(cal_dict):
    """Function that performs a polarimetry experiment with air."""
    # Adapt the dictionary of parameters for the analysis method
    cal_dict["fit completed"] = False
    cal_dict['limits'] = np.array([180, 180, 180, 180]) * degrees
    cal_dict['Nlimit'] = [0, 0, 0, 0]
    cal_dict['type'] = 'calibration'
    cal_dict['type_angles'] = 'random'
    cal_dict['mode_motors'] = 'absolute'
    cal_dict['Nmeasurements'] = cal_dict["N_measures_pol"]
    cal_dict['plot_vacuum'] = True
    cal_dict['is_vacuum'] = True
    cal_dict['meas_name'] = 'Step_6a'
    # Perform the analysis
    cal_dict, Mfiltered, results_dict, angles, Iexp = Make_Measurement_0D(
        cal_dict, type_output='all')

    return angles, Iexp


def make_step_6a_independent(cal_dict, polarimeter=None):
    """Function that performs a polarimetry experiment with air."""
    # Create the polarimeter data from a fit
    if polarimeter is None:
        polarimeter = {}
        polarimeter["p11"] = 0.988
        polarimeter["p12"] = 0.058
        polarimeter["Dp1"] = 63.2 * degrees
        polarimeter["p21"] = 0.984
        polarimeter["p22"] = 0.075
        polarimeter["th0p1"] = -61.0 * degrees
        polarimeter["th0p2"] = 88.7 * degrees
        polarimeter["R1p1"] = 0.993
        polarimeter["R1p2"] = 0.990
        polarimeter["R2p1"] = np.abs(pes[-1, 10])
        polarimeter["R2p2"] = np.abs(pes[-1, 11])
        polarimeter["Dr1"] = delays[-1, 0]
        polarimeter["Dr2"] = 82.7
        polarimeter["th0r1"] = 101.2 * degrees
        polarimeter["th0r2"] = 58.7 * degrees
        polarimeter["S0"] = 5.827
        polarimeter["Saz"] = 66.5 * degrees
        polarimeter["Sel"] = 47.5 * degrees
        polarimeter["Spol"] = 0.993

        Ifuente = Stokes()
        Ifuente.general_azimuth_ellipticity(
            az=polarimeter["Saz"], el=polarimeter["Sel"], intensity=polarimeter["S0"], pol_degree=polarimeter["Spol"])
        Mp1 = Mueller()
        Mp1.diattenuator_retarder_linear(
            p1=polarimeter["p11"], p2=polarimeter["p12"], D=polarimeter["Dp1"], angle=polarimeter["th0p1"])
        Mp2 = Mueller()
        Mp2.diattenuator_linear(
            p1=polarimeter["p21"], p2=polarimeter["p22"], angle=polarimeter["th0p2"])
        Mr1 = Mueller()
        Mr1.diattenuator_retarder_linear(
            p1=polarimeter["R1p1"], p2=polarimeter["R1p2"], D=polarimeter["Dr1"], angle=polarimeter["th0r1"])
        Mr2 = Mueller()
        Mr1.diattenuator_retarder_linear(
            p1=polarimeter["R2p1"], p2=polarimeter["R2p2"], D=polarimeter["Dr2"], angle=polarimeter["th0r2"])

        polarimeter["Ifuente"] = Ifuente
        polarimeter["Mp1"] = Mp1
        polarimeter["Mp2"] = Mp2
        polarimeter["Mr1"] = Mr1
        polarimeter["Mr2"] = Mr2
        polarimeter["Mbefore"] = None
        polarimeter["Mafter"] = None
    # Adapt the dictionary of parameters for the analysis method
    cal_dict["fit completed"] = False
    cal_dict['limits'] = np.array([180, 180, 180, 180]) * degrees
    cal_dict['Nlimit'] = [0, 0, 0, 0]
    cal_dict['type'] = 'calibration'
    cal_dict['type_angles'] = 'random'
    cal_dict['mode_motors'] = 'absolute'
    cal_dict['Nmeasurements'] = cal_dict["N_measures_pol"]
    cal_dict['plot_vacuum'] = True
    cal_dict['is_vacuum'] = True
    cal_dict['meas_name'] = 'Step_6a'
    cal_dict['meas_name'] = None
    # Perform the analysis
    cal_dict, Mfiltered, results_dict, angles, Iexp = Make_Measurement_0D(
        cal_dict, type_output='all')
    # Prints
    print("Step 6a result:")
    print("  - Normalization factor      = {:.4f}".format(
        results_dict['normalization']))
    print("  - Error                     = {:.3f} %".format(
        results_dict['error_norm'] * 100))
    print("   - Normalized Mueller matrix:")
    print(Mfiltered)
    # Plots
    I_fit = model_polarimeter_data(M, Ifuente,
                                   cal_dict['angles_polarimeter'],
                                   cal_dict['intensity_polarimeter'])
    diff = (cal_dict['intensity_polarimeter'] -
            I_fit) / cal_dict['intensity_polarimeter'].max()
    plt.figure(figsize=(16, 8))
    plt.subplot(2, 2, 1)
    plt.plot(cal_dict['intensity_polarimeter'], 'b')
    plt.plot(I_fit, 'k')
    plt.subplot(2, 2, 2)
    plt.plot(diff, 'b')
    plt.figure(figsize=(16, 8))
    plt.hist(diff)

    return angles, Iexp


def make_step_6b(cal_dict):
    """Function to measure the mueller matrix of the polarizer with known axes (Step 10d)."""
    # Adapt the dictionary of parameters for the analysis method
    cal_dict["fit completed"] = False
    cal_dict['limits'] = np.array([180, 180, 180, 180]) * degrees
    cal_dict['Nlimit'] = [0, 0, 0, 0]
    cal_dict['type'] = 'calibration'
    cal_dict['type_angles'] = 'random'
    cal_dict['mode_motors'] = 'absolute'
    cal_dict['Nmeasurements'] = cal_dict["N_measures_pol"]
    cal_dict['plot_vacuum'] = False
    cal_dict['is_vacuum'] = False
    cal_dict['meas_name'] = 'Step_6b'
    # Perform the analysis
    cal_dict, Mfiltered, results_dict, angles, Iexp = Make_Measurement_0D(
        cal_dict, type_output='all')
    # Print the angle
    print('The angle of the polarizer is: {:.1f} deg'.format(
        results_dict['azimuth pol'] / degrees))

    return angles, Iexp


def save_all_together(cal_dict):
    """Function to save the dictionary of the calibration, which contains all the interesting parameters."""
    filename = "All_together_{}".format(datetime.date.today())
    np.savez(filename + '.npz', parameters=set_cal_dict(cal_dict))


def Process_Calibration(cal_dict, seed=None):

    # If seed is None, use default one
    if seed is None:
        seed = np.zeros(9)
        use_seed = False
    else:
        use_seed = True
        cal_dict["N_it_while"] = 1
    date = "{}".format(datetime.date.today())

    # Initial parameters
    (p11, p12, Dp1, p21, p22, p31, p32, pc1,
     pc2) = (0.95, 0.05, 90 * degrees, 0.95, 0.05, 0.95, 0.05, 0.95, 0.3)
    (R2p1, R2p2, Dr2) = (0.99, 0.99, 83 * degrees)
    (R1p1, R1p2, Dr1, th0r1) = (0.99, 0.99, 83 * degrees, 0)
    (S0, Saz, Sel, Spol) = (cal_dict['Ifuente_test'].parameters.intensity(), 0,
                            44 * degrees, 1)
    (th0p1b, th0p1, th0p2, th0p3, th0r2, th0r2b) = (0, 0, 0, 0, 0, 0)
    # Polarimeter measurements
    Mbefore = Mueller()
    Mbefore.vacuum()
    cal_dict['Mbefore'] = Mbefore
    cal_dict['Mafter'] = Mbefore
    cal_dict['is_vacuum'] = True
    cal_dict['type'] = 'Calibration'
    cal_dict["fit completed"] = True
    cal_dict["odd_number_refs"] = False
    cal_dict["save_analysis"] = False

    # Mueller and Stokes objects
    Mp1 = Mueller('Polarizer 1')
    Mp2 = Mueller('Polarizer 2')
    Mr1 = Mueller('Retarder 1')
    Mr2 = Mueller('Retarder 2')
    Ifuente = Stokes('Illumination')
    # Error related parameters
    error_amplitude_vect = np.logspace(
        np.log10(cal_dict['ErrorAmpIni']), np.log10(cal_dict['ErrorAmpFin']),
        cal_dict["NmaxIt"])
    # error_amplitude_ellip_vect = np.logspace(
    #     np.log10(cal_dict['ErrorEllipAmpIni']),
    #     np.log10(cal_dict['ErrorEllipAmpFin']), cal_dict["NmaxIt"])
    th0E0 = 0
    # Extract angles for easiness
    angles_1 = cal_dict["angles_1"]
    angles_2x = cal_dict["angles_2X"]
    angles_2y = cal_dict["angles_2Y"]
    angles_2x_s4b = cal_dict["angles_2X_step_4b"]
    angles_2y_s4b = cal_dict["angles_2Y_step_4b"]
    angles_1_plot3 = np.concatenate((angles_1, angles_1 + np.pi,
                                     angles_1 + np.pi * 2))
    # Initialize data storage
    errores = np.zeros([cal_dict["NmaxIt"], 10])
    pes = np.zeros([cal_dict["NmaxIt"], 12])
    ilums = np.zeros([cal_dict["NmaxIt"], 4])
    delays = np.zeros([cal_dict["NmaxIt"], 4])
    angles = np.zeros([cal_dict["NmaxIt"], 7])
    error_angles = np.zeros([cal_dict["NmaxIt"], 3])
    Nelements2D = np.prod(cal_dict["N_measures_2D"])
    Nelements2D_s4b = np.prod(cal_dict["N_measures_2D_step_4b"])

    # Main iteration loop
    start_time = time.time()
    for indIt, error_amplitude in enumerate(error_amplitude_vect):
        # Start iteration operations
        # error_amplitude_ellip = error_amplitude_ellip_vect[indIt]
        print('Iteracion {}'.format(indIt))
        print('Angle error amplitude for current iteration = {}'.format(
            error_amplitude / degrees))
        # print('Ellipticity error amplitude for current iteration = {}'.
        #       format(error_amplitude_ellip / degrees))

        # Fit first angle of P1 in Step 1
        (counter, error_step, error_best) = (0, 1, 1)
        # Initial objects
        Mp1.diattenuator_linear(p1=p11, p2=p12, angle=0)
        Ifuente.general_azimuth_ellipticity(
            az=Saz, el=Sel, intensity=S0, pol_degree=Spol)
        # Extract intensity
        Iexperimental = cal_dict["I_step_1"]
        # Random loop
        while counter < cal_dict["N_it_while"] and error_step > cal_dict["tol_while"]:
            # Initialize angle in first iteration
            if indIt == 0:
                if use_seed:
                    th0p1b = seed[0]
                elif cal_dict["use_random_angles"]:
                    th0p1b = np.random.rand(1) * np.pi
                    th0p1b = th0p1b[0]
            # Initial parameters
            par0 = [pc1, pc2, th0p1b]
            # Ajuste
            par1, success = optimize.leastsq(
                error_step_1,
                par0,
                args=(angles_1, Mp1, Ifuente, Iexperimental))
            # Calculo de errores
            error_step = error_step_1(par1, angles_1, Mp1, Ifuente,
                                      Iexperimental)
            error_step = np.linalg.norm(error_step) / (
                Iexperimental.max() * cal_dict["N_measures_1D"])
            # Store best results
            if error_step < error_best:
                par_best = par1
                error_best = error_step
                # Save random seed
                if indIt == 0:
                    seed[0] = th0p1b
            # While loop end operations
            counter = counter + 1
            if cal_dict["PlotCadaPaso"] and cal_dict["N_it_while"] > 1:
                print('Subiteration {} has error {:.3f} %.'.format(
                    counter, error_step * 100))
        # Store results
        pc1, pc2, th0p1b = sort_pes(par_best[0:3])
        (pes[indIt, 8], pes[indIt, 9]) = (pc1, pc2)
        angles[indIt, 4] = th0p1b
        errores[indIt, 0] = error_best
        # Plots
        if cal_dict["PlotCadaPaso"] or indIt + 1 == cal_dict["NmaxIt"]:
            print("Step 1 parameters:")
            print("  - Known polarizer p1    = {:.3f};".format(pc1))
            print("  - Known polarizer p2    = {:.3f};".format(pc2))
            print("  - P1 initial angle      = {:.1f} deg;".format(
                th0p1b / degrees))
            I_fit = model_step_1(par_best, angles_1, Mp1, Ifuente)
            plot_experiment_residuals_1D(
                angles_1, Iexperimental, I_fit, title='Step 1')
        # Update variables
        Mp1.rotate(th0p1b)

        # Now the same with retarder 2 (Step 2a)
        # Initial objects
        Mr2.diattenuator_retarder_linear(R2p1, R2p2, Dr2, angle=0)
        # Extract intensity
        Iexperimental = cal_dict["I_step_2a"]
        # While loop
        (counter, error_step, error_best) = (0, 1, 1)
        while counter < cal_dict["N_it_while"] and error_step > cal_dict["tol_while"]:
            # Initialize angle in first iteration
            if indIt == 0:
                if use_seed:
                    th0r2b = seed[1]
                elif cal_dict["use_random_angles"]:
                    th0r2b = np.random.rand(1) * np.pi
                    th0r2b = th0r2b[0]
            # Initial parameters
            par0 = th0r2b
            # Fit
            par1, success = optimize.leastsq(
                error_step_2a,
                par0,
                args=(angles_1, Mp1, Mr2, Ifuente, Iexperimental))
            # Errors calculation
            error_step = error_step_2a(par1, angles_1, Mp1, Mr2, Ifuente,
                                       Iexperimental)
            error_step = np.linalg.norm(error_step) / (
                Iexperimental.max() * cal_dict["N_measures_1D"])
            # Store best results
            if error_step < error_best:
                par_best = par1
                error_best = error_step
                # Save random seed
                if indIt == 0:
                    seed[1] = th0r2b
            # While loop end operations
            counter = counter + 1
            if cal_dict["PlotCadaPaso"] and cal_dict["N_it_while"] > 1:
                print('Subiteration {} has error {:.3f} %.'.format(
                    counter, error_step * 100))
        # Extract results for the following iterations
        th0r2b = par_best[0] % np.pi
        # Store results
        angles[indIt, 5] = th0r2b
        errores[indIt, 1] = error_best
        # Plots
        if cal_dict["PlotCadaPaso"] or indIt + 1 == cal_dict["NmaxIt"]:
            print("Step 2a parameters:")
            print("  - R2 initial angle      = {:.1f} deg;".format(
                th0r2b / degrees))
            I_fit = model_step_2a(par_best, angles_1, Mp1, Mr2, Ifuente)
            plot_experiment_residuals_1D(
                angles_1, Iexperimental, I_fit, title='Step 6')
        # Update variables
        Mr2.rotate(th0r2b)

        # Fit illumination (Step 2b)
        # Extract intensity
        Iexperimental = cal_dict["I_step_2b"]
        # While loop
        (counter, error_step, error_best) = (0, 1, 1)
        while counter < cal_dict["N_it_while"] and error_step > cal_dict["tol_while"]:
            # Initialize angle in first iteration
            if indIt == 0:
                if use_seed:
                    Saz = seed[2]
                elif cal_dict["use_random_angles"]:
                    Saz = np.random.rand(1) * np.pi
                    Saz = Saz[0]
            # Initial parameters
            par0 = [S0, Saz, Sel, Spol, th0E0]
            # Fit par, th1, th2, Mp1, Mr2, error_amplitude, Iexp
            par1, success = optimize.leastsq(
                error_step_2b,
                par0,
                args=(angles_2x, angles_2y, Mp1, Mr2, error_amplitude,
                      Iexperimental))
            # Errors calculation
            error_step = error_step_2b(par1, angles_2x, angles_2y, Mp1, Mr2,
                                       error_amplitude, Iexperimental)
            error_step = np.linalg.norm(error_step) / (
                Iexperimental.max() * Nelements2D)
            # Store best results
            if error_step < error_best:
                par_best = par1
                error_best = error_step
                # Save random seed
                if indIt == 0:
                    seed[2] = Saz
            # While loop end operations
            counter = counter + 1
            if cal_dict["PlotCadaPaso"] and cal_dict["N_it_while"] > 1:
                print('Subiteration {} has error {:.3f} %.'.format(
                    counter, error_step * 100))
        # Extract results for the following iterations
        (S0, Saz, Sel, Spol) = par_best[0:4]
        th_error = error_amplitude * erf(par_best[4])
        # Update variables
        # Save results
        ilums[indIt, 0:4] = (S0, Saz, Sel, Spol)
        error_angles[indIt, 0] = th_error
        errores[indIt, 2] = error_best
        # Print info
        if cal_dict["PlotCadaPaso"] or indIt + 1 == cal_dict["NmaxIt"]:
            # Info
            print("Step 2b parameters:")
            print("  - Light intensity         = {:.3f} V;".format(S0))
            print("  - Light azimuth           = {:.1f} deg;".format(
                Saz / degrees))
            print("  - Light ellipticity       = {:.1f} deg;".format(
                Sel / degrees))
            print("  - Polarization degree     = {:.3f};".format(Spol))
            print("  - Ref. angle correction   = {:.31};".format(
                th_error / degrees))
            print("  - Stokes vector of illumination:")
            print(Ifuente)
            # Plots
            I_fit = model_step_2b(par1, angles_2x, angles_2y, Mp1, Mr2,
                                  error_amplitude)
            plot_experiment_residuals_2D(
                cal_dict["angles_2X"],
                cal_dict["angles_2Y"],
                Iexperimental,
                I_fit,
                title='Step 2b',
                xlabel='Angle R2 (deg)',
                ylabel='Angle P1 (deg)')
        # Update variables
        Ifuente.general_azimuth_ellipticity(
            az=Saz, el=Sel, intensity=S0, pol_degree=Spol)
        cal_dict['Ifuente'] = Ifuente
        if cal_dict["fix_parameters"]:
            th0r2b = th0r2b + th_error

        # Now, lets fit the polarizers (Step 3)
        (counter, error_step, error_best) = (0, 1, 1)
        # Load experimental intensity
        IexpA = cal_dict["I_step_3a"]
        IexpB = cal_dict["I_step_3b"]
        IexpC = cal_dict["I_step_3c"]
        Iexperimental = np.concatenate((IexpA, IexpB, IexpC))
        while counter < cal_dict["N_it_while"] and error_step > cal_dict["tol_while"]:
            # Initializwe angle for first iteration
            if indIt == 0:
                if use_seed:
                    (th0p1, th0p2, th0p3) = seed[3:6]
                elif cal_dict["use_random_angles"]:
                    (th0p1, th0p2, th0p3) = np.random.rand(3) * np.pi
            # Initial parameters
            par0 = [p11, p12, p21, p22, p31, p32, th0p3, th0p2, th0p1]
            # Fit
            par1, success = optimize.leastsq(
                error_step_3,
                par0,
                args=(angles_1, Ifuente, th0p1b, IexpA, IexpB, IexpC))
            # Error calculation
            error_step = error_step_3(par1, angles_1, Ifuente, th0p1b, IexpA,
                                      IexpB, IexpC)
            error_step = np.linalg.norm(error_step) / (
                Iexperimental.max() * 3 * cal_dict["N_measures_1D"])
            # Guardar mejores resultados
            if error_step < error_best:
                par_best = par1
                error_best = error_step
                # Save random seed
                if indIt == 0:
                    seed[3:6] = (th0p1, th0p2, th0p3)
            # Operaciones final de bucle
            counter = counter + 1
            if cal_dict["PlotCadaPaso"] and cal_dict["N_it_while"] > 1:
                print('Subiteration {} has error {:.3f} %.'.format(
                    counter, error_step * 100))
        # Extract results
        p11, p12, p21, p22, p31, p32, th0p1, th0p2, th0p3 = sort_pes(par_best)
        # Store results
        pes[indIt, 0:6] = (p11, p12, p21, p22, p31, p32)
        angles[indIt, 0:3] = (th0p1, th0p2, th0p3)
        errores[indIt, 3] = error_best
        # Info
        if cal_dict["PlotCadaPaso"] or indIt + 1 == cal_dict["NmaxIt"]:
            print("Step 3 parameters:")
            print("  - Polarizer 1:")
            print("    o P1         = {:.3f};".format(p11))
            print("    o P2         = {:.3f};".format(p12))
            print("    o Th0        = {:.1f} deg;".format(th0p1 / degrees))
            print("  - Polarizer 2:")
            print("    o P1         = {:.3f};".format(p21))
            print("    o P2         = {:.3f};".format(p22))
            print("    o Th0        = {:.1f} deg;".format(th0p2 / degrees))
            print("  - Polarizer 3:")
            print("    o P1         = {:.3f};".format(p31))
            print("    o P2         = {:.3f};".format(p32))
            print("    o Th0        = {:.1f} deg;".format(th0p3 / degrees))
            # print("  - Error angle  = {:.1f} deg;".format(th_error1 / degrees))
            # Plot
            IfitA, IfitB, IfitC = model_step_3(par_best, angles_1, Ifuente,
                                               th0p1b)
            I_fit = np.concatenate((IfitA, IfitB, IfitC))
            Iexperimental = np.concatenate((IexpA, IexpB, IexpC))
            plot_experiment_residuals_1D(
                angles_1_plot3, Iexperimental, I_fit, title='Step 3')
        # Create objects
        Mp1.diattenuator_linear(p1=p11, p2=p12, angle=th0p1)
        Mp2.diattenuator_linear(p1=p21, p2=p22, angle=th0p2)

        # Reference angle of retarder 2 (Step 4a)
        # Initial objects
        Mr2.diattenuator_retarder_linear(R2p1, R2p2, Dr2, angle=0)
        # Extract intensity
        Iexperimental = cal_dict["I_step_4a"]
        # While loop
        (counter, error_step, error_best) = (0, 1, 1)
        while counter < cal_dict["N_it_while"] and error_step > cal_dict["tol_while"]:
            # Initialize angle in first iteration
            if indIt == 0:
                if use_seed:
                    th0r2 = seed[6]
                elif cal_dict["use_random_angles"]:
                    th0r2 = np.random.rand(1) * np.pi
                    th0r2 = th0r2[0]
            # Initial parameters
            par0 = [th0r2, Dp1, th0E0]
            # Fit
            par1, success = optimize.leastsq(
                error_step_4a,
                par0,
                args=(angles_1, Mp1, Mp2, Mr2, Ifuente, Iexperimental,
                      error_amplitude))
            # Errors calculation
            error_step = error_step_4a(par1, angles_1, Mp1, Mp2, Mr2, Ifuente,
                                       Iexperimental, error_amplitude)
            error_step = np.linalg.norm(error_step) / (
                Iexperimental.max() * cal_dict["N_measures_1D"])
            # Store best results
            if error_step < error_best:
                par_best = par1
                error_best = error_step
                # Save random seed
                if indIt == 0:
                    seed[6] = th0r2
            # While loop end operations
            counter = counter + 1
            if cal_dict["PlotCadaPaso"] and cal_dict["N_it_while"] > 1:
                print('Subiteration {} has error {:.3f} %.'.format(
                    counter, error_step * 100))
        # Extract results for the following iterations
        th0r2 = par_best[0] % np.pi
        Dp1 = par_best[1] % np.pi
        th_error = error_amplitude * erf(par_best[2])
        # Store results
        angles[indIt, 3] = th0r2
        delays[indIt, 2] = Dp1
        errores[indIt, 4] = error_best
        # Plots
        if cal_dict["PlotCadaPaso"] or indIt + 1 == cal_dict["NmaxIt"]:
            print("Step 4a parameters:")
            print("  - R2 reference angle      = {:.1f} deg;".format(
                th0r2 / degrees))
            print("  - Delay of P1 (*)         = {:.1f} deg;".format(
                Dp1 / degrees))
            print("  - Error angle in P2       = {:.1f} deg;".format(
                th_error / degrees))
            I_fit = model_step_4a(par_best, angles_1, Mp1, Mp2, Mr2, Ifuente,
                                  error_amplitude)
            plot_experiment_residuals_1D(
                angles_1, Iexperimental, I_fit, title='Step 4a')

        # Now, lets fit second retarder (Step 4b)
        # Extract intensity
        Iexperimental = cal_dict["I_step_4b"]
        # Parametros iniciales
        par0 = [R2p1, R2p2, Dr2, Dp1, th0E0]
        # Ajuste
        par_best, success = optimize.leastsq(
            error_step_4b,
            par0,
            args=(angles_2x_s4b, angles_2y_s4b, Mp2, Ifuente, p11, p12, th0p1,
                  th0r2, error_amplitude, Iexperimental))
        # Error calculation
        error_step = error_step_4b(par_best, angles_2x_s4b, angles_2y_s4b, Mp2,
                                   Ifuente, p11, p12, th0p1, th0r2,
                                   error_amplitude, Iexperimental)
        error_step = np.linalg.norm(error_step) / (
            Iexperimental.max() * Nelements2D_s4b)
        # print error
        if cal_dict["PlotCadaPaso"]:
            print('Step has error {:.3f} %.'.format(error_step * 100))
        # Extract results
        (R2p1, R2p2, Dr2, Dp1) = (par_best[0], par_best[1],
                                  par_best[2] % np.pi, par_best[3] % np.pi)
        Mr2.diattenuator_retarder_linear(R2p1, R2p2, Dr2, angle=th0r2)
        Mp1.diattenuator_retarder_linear(p11, p12, Dp1, angle=th0p1)
        th_error = error_amplitude * erf(par_best[4])
        # Fix previous variables
        if cal_dict["fix_parameters"]:
            Mr2.rotate(th_error)
        # Guardar resultados
        pes[indIt, 6:8] = (R2p1, R2p2)
        delays[indIt, 1] = Dr2
        delays[indIt, 3] = Dp1
        error_angles[indIt, 1] = th_error
        errores[indIt, 5] = error_step
        # Info
        if cal_dict["PlotCadaPaso"] or indIt + 1 == cal_dict["NmaxIt"]:
            print("Step 4b parameters:")
            print("  - Retarder 2:")
            print("    o P1           = {:.3f};".format(R2p1))
            print("    o P2           = {:.3f};".format(R2p2))
            print("    o Retardance   = {:.1f} deg;".format(Dr2 / degrees))
            print("    o Extra Th0    = {:.1f} deg;".format(
                th_error / degrees))
            print("  - Polarizer 1:")
            print("    o Retardance   = {:.1f} deg;".format(Dp1 / degrees))
            # Plots
            I_fit = model_step_4b(par_best, angles_2x_s4b, angles_2y_s4b, Mp2,
                                  Ifuente, p11, p12, th0p1, th0r2,
                                  error_amplitude)
            plot_experiment_residuals_2D(
                angles_2x_s4b,
                angles_2y_s4b,
                Iexperimental,
                I_fit,
                title='Step 4b',
                xlabel='Angle R2 (deg)',
                ylabel='Angle P2 (deg)')

        # Reference angle of retarder 1 (Step 5a)
        # Initial objects
        Mr1.diattenuator_retarder_linear(R1p1, R1p2, Dr1, angle=0)
        # Extract intensity
        IexpA = cal_dict["Ia_Step_5a"]
        IexpB = cal_dict["Ib_Step_5a"]
        # While loop
        (counter, error_step, error_best) = (0, 1, 1)
        while counter < cal_dict["N_it_while"] and error_step > cal_dict["tol_while"]:
            # Initialize angle in first iteration
            if indIt == 0:
                if use_seed:
                    th0r1 = seed[7]
                elif cal_dict["use_random_angles"]:
                    th0r1 = np.random.rand(1) * np.pi
                    th0r1 = th0r1[0]
            # Initial parameters
            par0 = th0r1
            # Fit
            if cal_dict["step_5a"] in ('individual', 'Individual', 'R2'):
                par1a, success = optimize.leastsq(
                    error_step_5a,
                    par0,
                    args=(angles_1, Mp1, Mp2, Mr1, Mr2, Ifuente, 'R2', IexpA))
            else:
                par1a = None
            if cal_dict["step_5a"] in ('individual', 'Individual', 'P2'):
                par1b, success = optimize.leastsq(
                    error_step_5a,
                    par0,
                    args=(angles_1, Mp1, Mp2, Mr1, Mr2, Ifuente, 'P2', IexpB))
            else:
                par1b = None
            if cal_dict["step_5a"] in ('all', 'All', 'ALL'):
                par1a, success = optimize.leastsq(
                    error_step_5a_all,
                    par0,
                    args=(angles_1, Mp1, Mp2, Mr1, Mr2, Ifuente, IexpA, IexpB))
                par1b = par1a
            # Errors calculation
            Nerrors = 0
            if par1a is None:
                error_step = error_step_5a(par1b, angles_1, Mp1, Mp2, Mr1, Mr2,
                                           Ifuente, 'P2', IexpB)
                error_step = np.linalg.norm(error_step) / (
                    IexpB.max() * cal_dict["N_measures_1D"])
            elif par1b is None:
                error_step = error_step_5a(par1a, angles_1, Mp1, Mp2, Mr1, Mr2,
                                           Ifuente, 'R2', IexpA)
                error_step = np.linalg.norm(error_step) / (
                    IexpA.max() * cal_dict["N_measures_1D"])
            elif cal_dict["step_5a"] in ('all', 'All', 'ALL'):
                error_step = error_step_5a(par1a, angles_1, Mp1, Mp2, Mr1, Mr2,
                                           Ifuente, IexpA, IexpB)
                maxV = np.max([IexpA.max(), IexpB.max()])
                error_step = np.linalg.norm(error_step) / (
                    maxV * 2 * cal_dict["N_measures_1D"])
            else:
                error_stepA = error_step_5a(par1b, angles_1, Mp1, Mp2, Mr1,
                                            Mr2, Ifuente, 'P2', IexpB)
                error_stepA = np.linalg.norm(error_stepA) / (
                    IexpB.max() * cal_dict["N_measures_1D"])
                error_stepB = error_step_5a(par1a, angles_1, Mp1, Mp2, Mr1,
                                            Mr2, Ifuente, 'R2', IexpA)
                error_stepB = np.linalg.norm(error_stepB) / (
                    IexpA.max() * cal_dict["N_measures_1D"])
                error_step = np.min([error_stepA, error_stepB])
            # Store best results
            if error_step < error_best:
                par_bestA = par1a
                par_bestB = par1b
                error_best = error_step
                # Save random seed
                if indIt == 0:
                    seed[7] = th0r1
            # While loop end operations
            counter = counter + 1
            if cal_dict["PlotCadaPaso"] and cal_dict["N_it_while"] > 1:
                print('Subiteration {} has error {:.3f} %.'.format(
                    counter, error_step * 100))
        # Extract results for the following iterations and calculate Imodel
        if par1a is None:
            th0r1 = par_bestB[0] % np.pi
            Iexperimental = IexpB
            I_fit = model_step_5a(par_bestB, angles_1, Mp1, Mp2, Mr1, Mr2,
                                  Ifuente, 'P2')
            angles_rep = angles_1
        elif par1b is None:
            th0r1 = par_bestA[0] % np.pi
            Iexperimental = IexpA
            I_fit = model_step_5a(par_bestA, angles_1, Mp1, Mp2, Mr1, Mr2,
                                  Ifuente, 'R2')
            angles_rep = angles_1
        elif cal_dict["step_5a"] in ('all', 'All', 'ALL'):
            th0r1 = par_bestB[0] % np.pi
            Iexperimental = np.concatenate((IexpA, IexpB))
            I_fitA = model_step_5a(par_bestA, angles_1, Mp1, Mp2, Mr1, Mr2,
                                   Ifuente, 'R2')
            I_fitB = model_step_5a(par_bestB, angles_1, Mp1, Mp2, Mr1, Mr2,
                                   Ifuente, 'P2')
            I_fit = np.concatenate((I_fitA, I_fitB))
            angles_rep = np.concatenate((angles_1, angles_1))
        else:
            error_stepA = error_step_5a(par_bestB, angles_1, Mp1, Mp2, Mr1,
                                        Mr2, Ifuente, 'P2', IexpB)
            error_stepA = np.linalg.norm(error_stepA) / (
                IexpB.max() * cal_dict["N_measures_1D"])
            error_stepB = error_step_5a(par_bestA, angles_1, Mp1, Mp2, Mr1,
                                        Mr2, Ifuente, 'R2', IexpA)
            error_stepB = np.linalg.norm(error_stepB) / (
                IexpA.max() * cal_dict["N_measures_1D"])
            error_step = np.min([error_stepA, error_stepB])
            if error_stepA < error_stepB:
                th0r1 = par_bestA[0] % np.pi
                I_fit = model_step_5a(par_bestA, angles_1, Mp1, Mp2, Mr1, Mr2,
                                      Ifuente, 'R2')
            else:
                th0r1 = par_bestB[0] % np.pi
                I_fit = model_step_5a(par_bestB, angles_1, Mp1, Mp2, Mr1, Mr2,
                                      Ifuente, 'P2')
            angles_rep = angles_1
        # Store results
        angles[indIt, 6] = (th0r1)
        errores[indIt, 6] = error_best
        # Plots
        if cal_dict["PlotCadaPaso"] or indIt + 1 == cal_dict["NmaxIt"]:
            print("Step 5a parameters:")
            print("  - R1 reference angle      = {:.1f} deg;".format(
                th0r1 / degrees))
            plot_experiment_residuals_1D(
                angles_rep, Iexperimental, I_fit, title='Step 5a')

        # Fit retarder 1 (Step 5b)
        # Extract intensity
        IexpA = cal_dict["Ia_step_5b"]
        IexpB = cal_dict["Ib_step_5b"]
        IexpC = cal_dict["Ic_step_5b"]
        # Initial parameters
        par0 = [R1p1, R1p2, Dr1, th0E0]
        # Fit and error calculation
        if cal_dict["step_5b"] in ('all', 'All', 'ALL'):
            # Fit
            par1, success = optimize.leastsq(
                error_step_5b_all,
                par0,
                args=(angles_2x, angles_2y, Mp1, Mr2, Mp2, Ifuente, th0r1,
                      error_amplitude, IexpA, IexpB, IexpC))
            # Error
            error_step = error_step_5b_all(
                par1, angles_2x, angles_2y, Mp1, Mr2, Mp2, Ifuente, th0r1,
                error_amplitude, IexpA, IexpB, IexpC)
            Imax = np.max([np.max(IexpA), np.max(IexpB), np.max(IexpC)])
            error_step = np.linalg.norm(error_step) / (Imax * Nelements2D)
            I_fit_A = model_step_5b(par1, angles_2x, angles_2y, Mp1, Mr2, Mp2,
                                    Ifuente, th0r1, error_amplitude, 'R1+R2')
            I_fit_B = model_step_5b(par1, angles_2x, angles_2y, Mp1, Mr2, Mp2,
                                    Ifuente, th0r1, error_amplitude, 'R1+P2')
            I_fit_C = model_step_5b(par1, angles_2x, angles_2y, Mp1, Mr2, Mp2,
                                    Ifuente, th0r1, error_amplitude, 'R2+P2')
        else:
            if cal_dict["step_5b"] in ('R1+R2', 'R1 R2'):
                print(IexpA)
                par1a, success = optimize.leastsq(
                    error_step_5b,
                    par0,
                    args=(angles_2x, angles_2y, Mp1, Mr2, Mp2, Ifuente, th0r1,
                          error_amplitude, 'R1+R2', IexpA))
                error_step_A = error_step_5b(par1a, angles_2x, angles_2y, Mp1,
                                             Mr2, Mp2, Ifuente, th0r1,
                                             error_amplitude, 'R1+R2', IexpA)
                error_step_A = np.linalg.norm(error_step_A) / (
                    np.max(IexpA) * Nelements2D)
                I_fit_A = model_step_5b(par1a, angles_2x, angles_2y, Mp1, Mr2,
                                        Mp2, Ifuente, th0r1, error_amplitude,
                                        'R1+R2')
            else:
                error_step_A = 1e10
            if cal_dict["step_5b"] in ('R1+P2', 'R1 P2'):
                par1b, success = optimize.leastsq(
                    error_step_5b,
                    par0,
                    args=(angles_2x, angles_2y, Mp1, Mr2, Mp2, Ifuente, th0r1,
                          error_amplitude, 'R1+P2', IexpA))
                error_step_B = error_step_5b(par1b, angles_2x, angles_2y, Mp1,
                                             Mr2, Mp2, Ifuente, th0r1,
                                             error_amplitude, 'R1+P2', IexpA)
                error_step_B = np.linalg.norm(error_step_B) / (
                    np.max(IexpB) * Nelements2D)
                I_fit_B = model_step_5b(par1b, angles_2x, angles_2y, Mp1, Mr2,
                                        Mp2, Ifuente, th0r1, error_amplitude,
                                        'R1+P2')
            else:
                error_step_B = 1e10
            if cal_dict["step_5b"] in ('R2+P2', 'R2 P2'):
                par1c, success = optimize.leastsq(
                    error_step_5b,
                    par0,
                    args=(angles_2x, angles_2y, Mp1, Mr2, Mp2, Ifuente, th0r1,
                          error_amplitude, 'R2+P2', IexpC))
                error_step_C = error_step_5b(par1c, angles_2x, angles_2y, Mp1,
                                             Mr2, Mp2, Ifuente, th0r1,
                                             error_amplitude, 'R2+P2', IexpC)
                error_step_C = np.linalg.norm(error_step_C) / (
                    np.max(IexpC) * Nelements2D)
                I_fit_C = model_step_5b(par1c, angles_2x, angles_2y, Mp1, Mr2,
                                        Mp2, Ifuente, th0r1, error_amplitude,
                                        'R2+P2')
            else:
                error_step_C = 1e10
            # Final error and parameters
            error_step = [error_step_A, error_step_B, error_step_C]
            par1 = [par1a, par1b, par1c]
            index_min = np.argmin(error_step)
            par1 = par1[index_min]
            error_step = error_step[index_min]
        # Extraer resultados para siguientes iteraciones
        (R1p1, R1p2) = par1[0:2]
        Dr1 = par1[2] % np.pi
        th_error = error_amplitude * erf(par1[3])
        # Fix previous variables
        if cal_dict["fix_parameters"]:
            th0r1 = th0r1 + th_error
        Mr1.diattenuator_retarder_linear(R1p1, R1p2, Dr1, angle=th0r1)
        M = [Mp1, Mr1, Mr2, Mp2]
        cal_dict['M'] = M
        # Guardar resultados
        pes[indIt, 10:12] = (R1p1, R1p2)
        delays[indIt, 0] = Dr1
        error_angles[indIt, 2] = th_error
        errores[indIt, 7] = error_step
        # Info
        if cal_dict["PlotCadaPaso"] or indIt + 1 == cal_dict["NmaxIt"]:
            # Prints
            print("Step 5b parameters: Retarder 1.")
            print("  - P1           = {:.3f};".format(R1p1))
            print("  - P2           = {:.3f};".format(R1p2))
            print("  - Retardance   = {:.1f} deg;".format(Dr1 / degrees))
            print("  - Extra angle  = {:.1f} deg;".format(th_error / degrees))
            # Plots
            if cal_dict["step_5b"] in ('R1+R2', 'R1 R2', 'all', 'All', 'ALL'):
                plot_experiment_residuals_2D(
                    cal_dict["angles_2X"],
                    cal_dict["angles_2Y"],
                    IexpA,
                    I_fit_A,
                    title='Step 5b: R1 + R2',
                    xlabel='Angle R1 (deg)',
                    ylabel='Angle R2 (deg)')
            if cal_dict["step_5b"] in ('R1+P2', 'R1 P2', 'all', 'All', 'ALL'):
                plot_experiment_residuals_2D(
                    cal_dict["angles_2X"],
                    cal_dict["angles_2Y"],
                    IexpB,
                    I_fit_B,
                    title='Step 5b: R1 + P2',
                    xlabel='Angle R1 (deg)',
                    ylabel='Angle P2 (deg)')
            if cal_dict["step_5b"] in ('R2+P2', 'R2 P2', 'all', 'All', 'ALL'):
                plot_experiment_residuals_2D(
                    cal_dict["angles_2X"],
                    cal_dict["angles_2Y"],
                    IexpC,
                    I_fit_C,
                    title='Step 5b: R2 + P2',
                    xlabel='Angle R2 (deg)',
                    ylabel='Angle P2 (deg)')

        # Check 1: Use the data as a polarimeter experiment (Step 5c)
        do_step_5c = False
        if cal_dict["step_5b"] in ('R1+R2', 'R1 R2', 'all', 'All', 'ALL'):
            angles_pol, I_pol = adapt_step_to_polarimeter(
                cal_dict["angles_2X"], cal_dict["angles_2Y"],
                cal_dict["Ia_step_5b"])
            do_step_5c = True
        elif cal_dict["step_5b"] in ('R1+P2', 'R1 P2', 'all', 'All', 'ALL'):
            angles_pol, I_pol = adapt_step_to_polarimeter(
                cal_dict["angles_2X"], cal_dict["angles_2Y"],
                cal_dict["Ib_step_5b"])
            do_step_5c = True
        # Rest of the required parameters
        if do_step_5c:
            cal_dict["intensity_polarimeter"] = I_pol
            cal_dict["angles_polarimeter"] = angles_pol
            cal_dict['analysis_name'] = 'Step_5c_analysis'
            cal_dict, Mfiltered, results_dict = Analysis_Measurement_0D(
                cal_dict, type_output='all', verbose=False)
            # Guardar resultados
            errores[indIt, 8] = results_dict['error_norm']
            # Info
            if cal_dict["PlotCadaPaso"] or indIt + 1 == cal_dict["NmaxIt"]:
                # Prints
                print("Step 5c result:")
                print("  - Normalization factor      = {:.4f}".format(
                    results_dict['normalization']))
                print("  - Error                     = {:.3f} %".format(
                    results_dict['error_norm'] * 100))
                print("   - Normalized Mueller matrix:")
                print(Mfiltered)
                # Plots
                I_fit = model_polarimeter_data(M, Ifuente, angles_pol, I_pol)
                diff = (I_pol - I_fit) / I_pol.max()
                plt.figure(figsize=(16, 8))
                plt.subplot(2, 2, 1)
                plt.plot(cal_dict['intensity_polarimeter'], 'b')
                plt.plot(I_fit, 'k')
                plt.subplot(2, 2, 2)
                plt.plot(diff, 'b')
        else:
            errores[indIt, 8] = np.nan

        # Check experiment: Mueller matrix of air (step 6a)
        cal_dict["angles_polarimeter"] = cal_dict['angles_step_6a']
        cal_dict["intensity_polarimeter"] = cal_dict['I_step_6a']
        cal_dict['type'] = 'Calibration'
        cal_dict['analysis_name'] = 'Step_6a_analysis'
        cal_dict, Mfiltered, results_dict = Analysis_Measurement_0D(
            cal_dict, type_output='all', verbose=False)
        # Guardar resultados
        errores[indIt, 9] = results_dict['error_norm']
        # Info
        if cal_dict["PlotCadaPaso"] or indIt + 1 == cal_dict["NmaxIt"]:
            # Prints
            print("Step 6a result:")
            print("  - Normalization factor      = {:.4f}".format(
                results_dict['normalization']))
            print("  - Error                     = {:.3f} %".format(
                results_dict['error_norm'] * 100))
            print("   - Normalized Mueller matrix:")
            print(Mfiltered)
            # Plots
            I_fit = model_polarimeter_data(M, Ifuente,
                                           cal_dict['angles_polarimeter'],
                                           cal_dict['intensity_polarimeter'])
            diff = (cal_dict['intensity_polarimeter'] -
                    I_fit) / cal_dict['intensity_polarimeter'].max()
            plt.figure(figsize=(16, 8))
            plt.subplot(2, 2, 1)
            plt.plot(cal_dict['intensity_polarimeter'], 'b')
            plt.plot(I_fit, 'k')
            plt.subplot(2, 2, 2)
            plt.plot(diff, 'b')
            plt.figure(figsize=(16, 8))
            plt.hist(diff)

        # This error is shown in each iteration
        print("Error of last step: {:.3f} %".format(
            results_dict['error_norm'] * 100))
        # Loops for initial parameters are done only once
        cal_dict["N_it_while"] = 1

    # Save data
    polarimeter = {}
    polarimeter["p11"] = np.abs(pes[-1, 0])
    polarimeter["p12"] = np.abs(pes[-1, 1])
    polarimeter["Dp1"] = delays[-1, 2]
    polarimeter["p21"] = np.abs(pes[-1, 2])
    polarimeter["p22"] = np.abs(pes[-1, 3])
    polarimeter["p31"] = np.abs(pes[-1, 4])
    polarimeter["p32"] = np.abs(pes[-1, 5])
    polarimeter["th0p1"] = th0p1
    polarimeter["th0p2"] = th0p2

    polarimeter["R1p1"] = np.abs(pes[-1, 6])
    polarimeter["R1p2"] = np.abs(pes[-1, 7])
    polarimeter["R2p1"] = np.abs(pes[-1, 10])
    polarimeter["R2p2"] = np.abs(pes[-1, 11])
    polarimeter["Dr1"] = delays[-1, 0]
    polarimeter["Dr2"] = delays[-1, 1]
    polarimeter["th0r1"] = th0r1
    polarimeter["th0r2"] = th0r2

    # polarimeter["normal"] = mean[1]
    polarimeter["S0"] = S0
    polarimeter["Saz"] = Saz
    polarimeter["Sel"] = Sel
    polarimeter["Spol"] = Spol
    polarimeter["Ifuente"] = Ifuente
    polarimeter["Mp1"] = Mp1
    polarimeter["Mp2"] = Mp2
    polarimeter["Mr1"] = Mr1
    polarimeter["Mr2"] = Mr2
    polarimeter["Mbefore"] = None
    polarimeter["Mafter"] = None

    polarimeter["Date"] = cal_dict['date']
    polarimeter["seed"] = seed

    # Check experiment: Mueller matrix of polarizer of known axes (step 6b)
    cal_dict["angles_polarimeter"] = cal_dict['angles_step_6b']
    cal_dict["intensity_polarimeter"] = cal_dict['I_step_6b']
    cal_dict['is_vacuum'] = False
    cal_dict['type'] = 'Calibration'
    cal_dict['analysis_name'] = 'Step_6b_analysis'
    cal_dict, Mfiltered, results_dict = Analysis_Measurement_0D(
        cal_dict, type_output='all')
    # Prints
    print("Step 6b comparison measure / calibration:")
    print("  - p1                 = {:.4f}   /   {:.4f}".format(
        results_dict['p1'], polarimeter["p31"]))
    print("  - p2                 = {:.4f}   /   {:.4f}".format(
        results_dict['p2'], polarimeter["p32"]))
    print("  - Azimuth            = {:3.1f}   /     0.0    deg".format(
        results_dict['azimuth pol'] / degrees))
    print("  - Ellipticity angle  = {:3.1f}   /     0.0    deg".format(
        results_dict['ellipticity pol'] / degrees))
    print("  - Retardance         = {:3.1f}   /   {:3.1f}    deg".format(
        results_dict['retardance'] / degrees, polarimeter["Dp1"]))

    # Save data
    if cal_dict["save_data"]:
        filename = "Polarimeter_calibration_{}".format(date)
        np.savez(filename + '.npz', polarimeter=polarimeter, cal_dict=cal_dict)
        print('Saved filename:   ' + filename + '.npz')

    # Analize data
    # Plot errores
    plt.figure(figsize=(16, 8))
    plt.subplot(2, 2, 1)
    plt.plot(errores[0:indIt + 1, 0], 'k')
    plt.plot(errores[0:indIt + 1, 1], 'b')
    plt.plot(errores[0:indIt + 1, 2], 'b--')
    plt.plot(errores[0:indIt + 1, 3], 'r')
    plt.plot(errores[0:indIt + 1, 4], 'g')
    plt.plot(errores[0:indIt + 1, 5], 'g--')
    plt.plot(errores[0:indIt + 1, 6], 'm')
    plt.plot(errores[0:indIt + 1, 7], 'm--')
    plt.plot(errores[0:indIt + 1, 9], 'c')
    plt.plot(errores[0:indIt + 1, 8], 'c--')
    plt.xlabel('Iteration')
    plt.ylabel('Relative error')
    plt.title('Error evolution')
    plt.legend(('Step 1', 'Step 2a', 'Step 2b', 'Step 3', 'Step 4a', 'Step 4b',
                'Step 5a', 'Step 5b', 'Step 6a', 'Step 5b*'))

    # Plot pes
    plt.figure(figsize=(16, 8))
    plt.subplot(2, 2, 1)
    plt.plot(pes[0:indIt + 1, 0], 'k')
    plt.plot(pes[0:indIt + 1, 2], 'b')
    plt.plot(pes[0:indIt + 1, 4], 'r')
    plt.plot(pes[0:indIt + 1, 6], 'g')
    plt.plot(pes[0:indIt + 1, 7], 'g--')
    plt.plot(pes[0:indIt + 1, 8], 'y')
    plt.plot(np.abs(pes[0:indIt + 1, 10]), 'm')
    plt.plot(np.abs(pes[0:indIt + 1, 11]), 'm--')
    plt.xlabel('Iteration')
    plt.ylabel('p parameter')
    plt.title('Evolution of high p parameters')
    plt.legend(('Pol 1', 'Pol 2', 'Pol 3', 'p1 R1', 'p2 R1', 'Pol known',
                'p1 R1', 'p2 R1'))

    plt.subplot(2, 2, 2)
    plt.plot(pes[0:indIt + 1, 1], 'k')
    plt.plot(pes[0:indIt + 1, 3], 'b')
    plt.plot(pes[0:indIt + 1, 5], 'r')
    plt.plot(pes[0:indIt + 1, 9], 'y')
    plt.xlabel('Iteration')
    plt.ylabel('p parameter')
    plt.title('Evolution of low p parameters')
    plt.legend(('Pol 1', 'Pol 2', 'Pol 3', 'Pol known'))

    # Illums
    plt.figure(figsize=(24, 8))
    plt.subplot(2, 3, 1)
    plt.plot(ilums[0:indIt + 1, 0], 'k')
    plt.xlabel('Iteration')
    plt.ylabel('Intensity (V)')
    plt.title('Evolucion of total intensity')

    plt.subplot(2, 3, 2)
    plt.plot(ilums[0:indIt + 1, 1] / degrees, 'b')
    plt.plot(ilums[0:indIt + 1, 2] / degrees, 'r')
    plt.xlabel('Iteration')
    plt.ylabel('Angle (deg)')
    plt.title('Evolution of illumination angles')
    plt.legend(('Azimuth', 'Ellipticity'))

    plt.subplot(2, 3, 3)
    plt.plot(ilums[0:indIt + 1, 3], 'g')
    plt.xlabel('Iteration')
    plt.ylabel('Polarization degree')
    plt.title('Evolution of illumination polarization degree')

    # Delays
    plt.figure(figsize=(16, 8))
    plt.subplot(2, 2, 1)
    plt.plot(delays[0:indIt + 1, 1] / degrees, 'k')
    plt.plot(delays[0:indIt + 1, 0] / degrees, 'b')
    plt.xlabel('Iteration')
    plt.ylabel('Delay (deg)')
    plt.title('Evolution of retarder delay')
    plt.legend(('Ret 1' 'Ret 2'))

    plt.subplot(2, 2, 2)
    plt.plot(delays[0:indIt + 1, 2] / degrees, 'k')
    plt.plot(delays[0:indIt + 1, 3] / degrees, 'b')
    plt.xlabel('Iteration')
    plt.ylabel('Delay (deg)')
    plt.title('Evolution of P1 delay')
    plt.legend(('Step 4a', 'Step 4b'))

    # Angles
    plt.figure(figsize=(16, 8))
    plt.subplot(2, 2, 1)
    plt.plot(angles[0:indIt + 1, 0] / degrees, 'k')
    plt.plot(angles[0:indIt + 1, 1] / degrees, 'b')
    plt.plot(angles[0:indIt + 1, 2] / degrees, 'r')
    plt.plot(angles[0:indIt + 1, 3] / degrees, 'g')
    plt.plot(angles[0:indIt + 1, 6] / degrees, 'm')
    plt.plot(angles[0:indIt + 1, 4] / degrees, 'k--')
    plt.plot(angles[0:indIt + 1, 5] / degrees, 'g--')
    plt.xlabel('Iteration')
    plt.ylabel('Angle (deg)')
    plt.title('Evolution of initial angle')
    plt.legend(('Pol 1', 'Pol 2', 'Pol 3', 'Ret 2', 'Ret 1', 'Pol 1b',
                'Ret 2b'))

    # Errangles
    plt.figure(figsize=(16, 8))
    plt.plot(error_angles[0:indIt + 1, 0] / degrees)
    plt.plot(error_angles[0:indIt + 1, 1] / degrees)
    plt.plot(error_angles[0:indIt + 1, 2] / degrees)
    plt.plot(error_amplitude_vect / degrees, 'r--')
    plt.plot(-error_amplitude_vect / degrees, 'r--')
    plt.xlabel('Iteration')
    plt.ylabel('Correction angle (deg)')
    plt.title('Evolution of correction angle')
    plt.legend(('Th0 R2*', 'Th0 R2', 'Th0 R1', 'Limit', 'Limit'))

    end_time = time.time()
    print('Elapsed time is {} s.'.format(end_time - start_time))

    return polarimeter


def Postprocess_Calibration(folder, file, type='Final', load_dicts=False):
    """This function is used to process the calibration data. It is loaded from the last file containg all the data (throught the variable file) or the files created during each single step."""
    # Start by loading from mulipple files file
    if type in ('individual', 'Individual', 'INDIVIDUAL'):
        # Start operations
        cal_dict = {}
        cal_dict['date'] = file
        cal_dict['type'] = 'Calibration'
        cal_dict['fit completed'] = True
        seed = None
        os.chdir(folder)
        cal_dict["type"] = 'Individual'
        cal_dict["cal_final_path"] = folder
        cal_dict["save_data"] = True
        # Step 1
        filename = 'Step_1_' + file + '.npz'
        data = np.load(filename, encoding='latin1')
        cal_dict['I_step_1'] = data["Iexp"]
        cal_dict["angles_1"] = data["angles_1"]
        cal_dict["N_measures_1D"] = cal_dict["angles_1"].size
        if load_dicts:
            params = data["dict_param"]
        # Step 2a
        filename = 'Step_2a_' + file + '.npz'
        data = np.load(filename, encoding='latin1')
        cal_dict['I_step_2a'] = data["Iexp"]
        # params = data["dict_param"]
        # Step 2b
        filename = 'Step_2b_' + file + '.npz'
        data = np.load(filename, encoding='latin1')
        cal_dict['I_step_2b'] = data["Iexp"]
        cal_dict["angles_2X"] = data["angles2x"]
        cal_dict["angles_2Y"] = data["angles2y"]
        cal_dict["N_measures_2D"] = [
            data["angles2x"].size, data["angles2y"].size
        ]
        if load_dicts:
            params = data["dict_param"]
            cal_dict['Ifuente_test'] = params['Ifuente_test']
        else:
            cal_dict['Ifuente_test'] = data['fit'].item()
        # Step 3a
        filename = 'Step_3a_' + file + '.npz'
        data = np.load(filename, encoding='latin1')
        cal_dict['I_step_3a'] = data["Iexp"]
        if load_dicts:
            params = data["dict_param"]
        # Step 3b
        filename = 'Step_3b_' + file + '.npz'
        data = np.load(filename, encoding='latin1')
        cal_dict['I_step_3b'] = data["Iexp"]
        if load_dicts:
            params = data["dict_param"]
        # Step 3c
        filename = 'Step_3c_' + file + '.npz'
        data = np.load(filename, encoding='latin1')
        cal_dict['I_step_3c'] = data["Iexp"]
        if load_dicts:
            params = data["dict_param"]
        # Step 4a
        filename = 'Step_4a_' + file + '.npz'
        data = np.load(filename, encoding='latin1')
        cal_dict['I_step_4a'] = data["Iexp"]
        if load_dicts:
            params = data["dict_param"]
        # Step 4b
        filename = 'Step_4b_' + file + '.npz'
        data = np.load(filename, encoding='latin1')
        cal_dict['I_step_4b'] = data["Iexp"]
        cal_dict["angles_2X_step_4b"] = data["angles2x"]
        cal_dict["angles_2Y_step_4b"] = data["angles2y"]
        cal_dict["N_measures_2D_step_4b"] = [
            data["angles2x"].size, data["angles2y"].size
        ]
        if load_dicts:
            params = data["dict_param"]
        # Step 5a
        filename = 'Step_5a_' + file + '.npz'
        data = np.load(filename, encoding='latin1')
        cal_dict['Ia_Step_5a'] = data["IexpA"]
        cal_dict['Ib_Step_5a'] = data["IexpB"]
        if load_dicts:
            params = data["dict_param"]
        # Step 5b
        filename = 'Step_5b_' + file + '.npz'
        data = np.load(filename, encoding='latin1')
        cal_dict['Ia_step_5b'] = data["IexpA"]
        cal_dict['Ib_step_5b'] = data["IexpB"]
        cal_dict['Ic_step_5b'] = data["IexpC"]
        if load_dicts:
            params = data["dict_param"]
        # Step 6a
        filename = 'Step_6a_' + file + '.npz'
        # for key, value in data.items():
        #     print(key, value)
        data = np.load(filename, encoding='latin1')
        cal_dict['I_step_6a'] = data["Iexp"]
        cal_dict['angles_step_6a'] = data["angles"]
        if load_dicts:
            params = data["dict_param"]
        # Step 6b
        filename = 'Step_6b_' + file + '.npz'
        data = np.load(filename, encoding='latin1')
        cal_dict['I_step_6b'] = data["Iexp"]
        cal_dict['angles_step_6b'] = data["angles"]
        cal_dict["N_measures_pol"] = len([cal_dict['angles_step_6a']])
        if load_dicts:
            params = data["dict_param"]
            cal_dict["tol_while"] = params["tol_while"]
            cal_dict["N_it_while"] = params["N_it_while"]
            cal_dict["NmaxIt"] = params["NmaxIt"]
            cal_dict["tolerance"] = params["tolerance"]
            cal_dict["tolerance_fit"] = params["tolerance_fit"]
            cal_dict["PlotCadaPaso"] = params["PlotCadaPaso"]
            cal_dict["ordenarPes"] = params["ordenarPes"]
            cal_dict["ErrorAmpIni"] = params["ErrorAmpIni"]
            cal_dict["ErrorAmpFin"] = params["ErrorAmpFin"]
            cal_dict["use_random_angles"] = params["use_random_angles"]
            cal_dict["fix_parameters"] = params["fix_parameters"]
            cal_dict["step_5a"] = params["step_5a"]
            cal_dict["step_5b"] = params["step_5b"]
        else:
            cal_dict["NmaxIt"] = 2
            cal_dict["tolerance"] = 1e-4
            cal_dict["tolerance_fit"] = 1e-4
            cal_dict["PlotCadaPaso"] = False
            cal_dict["ordenarPes"] = True
            cal_dict["ErrorAmpIni"] = 90 * degrees
            cal_dict["ErrorAmpFin"] = 90 * degrees
            cal_dict["tol_while"] = 3e-4
            cal_dict["N_it_while"] = 10
            cal_dict["use_random_angles"] = True
            cal_dict["fix_parameters"] = True
            cal_dict["step_5a"] = "P2"
            cal_dict["step_5b"] = "R2+P2"
    # Some other options
    elif type in ('one_file', 'One_file', 'onefile', 'Onefile'):
        filename = 'All_together_' + file + '.npz'
        data = np.load(filename, encoding='latin1')
        cal_dict = data["parameters"]

    # Make the calculations
    Process_Calibration(cal_dict, seed=seed)


def model_polarimeter_data(M, Ifuente, angles, Iexp):
    """Function that calculates the modelled intensity respect to the angles and intensities measured during a polarimeter experiment."""
    N = Iexp.size
    Imodel = np.zeros(N)
    for ind, angle in enumerate(angles):
        Imodel[ind] = Intensity_Rotating_Elements(M, angle, Ei=Ifuente)
    return Imodel


def Final():
    pass
