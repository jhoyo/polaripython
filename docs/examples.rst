Examples
=============

.. toctree::
   :maxdepth: 4
   :numbered:
   :glob:

   source/examples/introductory/example.rst
   source/examples/advanced/example.rst

