# !/usr/local/bin/python
# -*- coding: utf-8 -*-

# ----------------------------------------------------------------------
# Name:        utils.py
# Purpose:     General purpose functions: Ecograb project
#
# Author:      Luis Miguel Sanchez Brea
#
# Created:     2018
# Copyright:   AOCG / UCM
# Licence:     GPL
# ----------------------------------------------------------------------
""" General purpose optics functions """

import matplotlib.pyplot as plt
import scipy as sp
from scipy import pi
from phyton_optics import degrees


def to_bits(variable_integer, num_bits=16, verbose=True):
    """
    takes an integer an generates a list with bits

    Args:
        variable_integer (int): integer with data
        num_bits (int): num of output bits: 8,16, 32, 64
    """
    if num_bits == 8:
        output = map(int, [x for x in '{:08b}'.format(variable_integer)])
    elif num_bits == 16:
        output = map(int, [x for x in '{:016b}'.format(variable_integer)])
    elif num_bits == 32:
        output = map(int, [x for x in '{:032b}'.format(variable_integer)])
    elif num_bits == 64:
        output = map(int, [x for x in '{:064b}'.format(variable_integer)])

    if verbose is True:
        print(output)
    return output


def dibujar_2d_fitting(x, y, Z, title='', color="magma"):

    extension = sp.array([x[0], x[-1], y[0], y[-1]]) / degrees

    plt.figure()
    IDimage = plt.imshow(
        Z,
        interpolation='bilinear',
        aspect='auto',
        origin='lower',
        extent=extension)
    plt.xlabel("$\phi_2$")
    plt.ylabel("$\phi_4$")
    plt.suptitle(title)

    plt.axis(extension)
    plt.axis('scaled')
    plt.colorbar()
    IDimage.set_cmap(color)  # YlGnBu  RdBu


def plot_experiment_residuals_1D(x, y, y_fitting, title=''):
    plt.figure(figsize=(16, 8))
    plt.subplot(2, 2, 1)
    plt.plot(x / degrees, y, "ro", lw=2, label='exp. data')
    plt.title(title)

    if y_fitting is not None:
        plt.plot(x / degrees, y_fitting, "r-", lw=2, label='fitting')
        plt.legend()

        residuals = y_fitting - y

        plt.subplot(2, 2, 2)
        plt.plot(x / degrees, residuals, 'k', lw=2)
        plt.title('residuals')
        plt.show()
