# !/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------
# Author:    Jesus del Hoyo
# Fecha       2018/07/25 (version 1.0)
# License:    GPL
# -------------------------------------
"""
This script has the functions specific for the calibration of the polarimeter.

# Intensity_Rotating_Elements

"""

import numpy as np
import pprint

from copy import deepcopy
import os
import time
import datetime
import matplotlib.pyplot as plt
from shutil import copyfile
from scipy import optimize
from scipy.special import erf

from py_pol.mueller import Mueller
from py_pol.stokes import Stokes
from py_pol.jones_matrix import Jones_matrix
from py_pol.jones_vector import Jones_vector
from py_pol.utils import put_in_limits

from . import degrees
from .utils import iscolumn, plot_experiment_residuals_1D, plot_experiment_residuals_2D, percentage_complete, dist_angles, sort_pes, limit_pes, plot_error_Mueller, plot_experiment_1D, set_cal_dict, adapt_step_to_polarimeter
from .polarimeter import Intensity_Rotating_Elements, polarimeter_experiment, Analysis_Measurement_0D, go_and_measure_0D, initialize_motors_and_card, correct_intensity_0D, Make_Measurement_0D, Analysis_Measurement_0D, close_equipment, correct_intensity_0D, measure_int
from .auxiliar.analysis_N_measures import take_intermediate_points_2D

air_flatten = np.eye(4).flatten()

# TODO: Light source stability, remove _test variables


def initialize_calibration(cal_dict):
    """This function initializes motors and card, and creates the required folder if it doesn't exist."""
    # try:
    # Motors and card
    cal_dict["equipment"] = initialize_motors_and_card(verbose=False)
    # Files
    date = datetime.date.today()
    cal_dict['date'] = date
    extra_name = 'Calibration_{}'.format(date)
    os.chdir(cal_dict['cal_path'])
    try:
        os.mkdir(extra_name)
    except:
        print('Folder already created')
    cal_dict['cal_final_path'] = cal_dict['cal_path'] + '\\' + extra_name
    print(cal_dict['cal_final_path'])
    os.chdir(cal_dict['cal_final_path'])
    print('Calibration initialized succesfully')
    # except:
    #     close_equipment(cal_dict["equipment"])
    #     cal_dict = {}
    return cal_dict


def make_step_0e(cal_dict,
                 num_data=60,
                 max_angle=180,
                 motor_num=3,
                 verbose=False):
    """Step 2: align R0. We make a loop wit one motor with a polarizer to check the circularity of the illumination"""
    # Create array of angles that will be used
    var_angle = np.linspace(0, max_angle, num_data)
    # Initialize data
    Iexp = np.zeros([num_data, 2])
    theta = np.zeros(4)
    # Make the loop
    percentage_complete()
    for ind, angle in enumerate(var_angle):
        theta[motor_num] = angle
        Iexp[ind, :] = go_and_measure_0D(
            cal_dict["equipment"], theta=theta, verbose=verbose)
        percentage_complete(ind + 1, num_data)
    # Use information in reference channel to correct data
    Iexp, _ = correct_intensity_0D(Iexp)
    # Fit to the function
    (Imax, Imin) = (np.max(Iexp), np.min(Iexp))
    par0 = [Imin, Imax - Imin, 0]
    par1, success = optimize.leastsq(
        error_step_0e, par0, args=(var_angle, Iexp))
    # Plot result if required
    Imodel = model_step_0e(par1, var_angle)
    plot_experiment_residuals_1D(var_angle, Iexp, Imodel, title='Step 0e')
    # Print results
    print('The values obtained are:')
    print('   - Imax       : {:.4f} V'.format(par1[1] + par1[0]))
    print('   - Imin       : {:.4f} V'.format(par1[0]))
    print('   - Difference : {:.4f} V'.format(par1[1]))


def model_step_0e(par, angle):
    """Function that serves as optimization for step 2."""
    Imodel = par[0] + par[1] * np.cos(angle - par[2])**2
    return Imodel


def error_step_0e(par, angle, Iexp):
    """Function that serves as optimization for step 2."""
    Imodel = model_step_0e(par, angle)
    dif = Iexp - Imodel
    return dif


def make_step_0g(cal_dict, Nmeasurements=60, Twait=1, verbose=False):
    """Function to make step 3: photodetector stability."""
    # Make the measurement
    Iexp = np.zeros([Nmeasurements, 2])
    percentage_complete()
    for ind in range(Nmeasurements):
        Iexp[ind, :] = measure_int(cal_dict["equipment"], verbose=verbose)
        time.sleep(Twait)
        percentage_complete(ind + 1, Nmeasurements)
    # Make stadistics
    ratio_individual = Iexp[:, 0] / Iexp[:, 1]
    mean = np.mean(Iexp, axis=0)
    error = np.std(Iexp, axis=0)
    ratio2 = np.mean(ratio_individual)
    ratio_error2 = np.std(ratio_individual)
    error_correction = np.sqrt((error[0] / mean[0])**2 +
                               2 * (error[1] / mean[1])**2)
    # Fake plot data
    t = range(Nmeasurements)
    meanCh1y = np.ones(Nmeasurements) * mean[0]
    meanCh1yUp = np.ones(Nmeasurements) * (mean[0] + error[0])
    meanCh1yDown = np.ones(Nmeasurements) * (mean[0] - error[0])
    meanCh2y = np.ones(Nmeasurements) * mean[1]
    meanCh2yUp = np.ones(Nmeasurements) * (mean[1] + error[1])
    meanCh2yDown = np.ones(Nmeasurements) * (mean[1] - error[1])
    meanRy2 = np.ones(Nmeasurements) * ratio2
    meanRyUp2 = np.ones(Nmeasurements) * (ratio2 + ratio_error2)
    meanRyDown2 = np.ones(Nmeasurements) * (ratio2 - ratio_error2)
    # Plot it
    plt.figure(figsize=(24, 4))
    plt.subplot(1, 3, 1)
    plt.plot(t, Iexp[:, 0] / meanCh1y, 'k')
    plt.plot(t, meanCh1y / meanCh1y, 'b')
    plt.plot(t, meanCh1yUp / meanCh1y, 'r--')
    plt.plot(t, meanCh1yDown / meanCh1y, 'r--')
    plt.title('Normalized PhD 1 (Signal)')
    plt.xlabel('Time (s)')
    plt.ylabel('Intensity (V)')
    plt.subplot(1, 3, 2)
    plt.plot(t, Iexp[:, 1] / meanCh2y, 'k')
    plt.plot(t, meanCh2y / meanCh2y, 'b')
    plt.plot(t, meanCh2yUp / meanCh2y, 'r--')
    plt.plot(t, meanCh2yDown / meanCh2y, 'r--')
    plt.title('Normalized PhD 2 (Reference)')
    plt.xlabel('Time (s)')
    plt.ylabel('Intensity (V)')
    plt.subplot(1, 3, 3)
    plt.plot(t, ratio_individual / meanRy2, 'k')
    plt.plot(t, meanRy2 / meanRy2, 'b')
    plt.plot(t, meanRyUp2 / meanRy2, 'r--')
    plt.plot(t, meanRyDown2 / meanRy2, 'r--')
    plt.title('Ratio PhD1 / PhD2')
    plt.xlabel('Time (s)')
    plt.ylabel('Intensity (V)')
    # Print result
    print('The resuts are:')
    print('   - Signal channel          : {:.4f} +- {:.4f} V.'.format(
        mean[0], error[0]))
    print('   - Reference channel       : {:.4f} +- {:.4f} V.'.format(
        mean[1], error[1]))
    print('   - Ratio error             : {:.2f} %.'.format(
        ratio_error2 * 100))
    print('   - Error in corrected I    : {:.2f} %.'.format(
        error_correction * 100))
    # Save data
    filename = "Step_0g_{}".format(datetime.date.today())
    np.savez(
        filename + '.npz',
        Nmeasurements=Nmeasurements,
        Twait=Twait,
        Iexp=Iexp,
        mean=mean,
        error=error,
        error_correction=error_correction)
    # Output
    return mean[0], mean[1], error_correction


def model_step_1(par, th1, Mp1, Ifuente):
    # Ordenar o no los pes del polarizador
    p1_c, p2_c, _ = sort_pes([par[0], par[1], 0])
    Mpmalo = Mueller()
    Mpmalo.diattenuator_linear(p1=p1_c, p2=p2_c)
    # Calcular
    M = [Mpmalo, Mp1]
    th = [0, th1 + par[2]]
    Imodel = Intensity_Rotating_Elements(M, th, Ei=Ifuente)
    return Imodel


def error_step_1(par, th1, Mp1, Ifuente, Iexp):
    Imodel = model_step_1(par, th1, Mp1, Ifuente)
    diferencia = Imodel - Iexp
    return diferencia


def make_step_1(cal_dict, verbose=False):
    """Function that performs step 5 of calibration: origin of angles."""
    if cal_dict["type"] in ('experiment', 'Experiment', 'exp', 'Exp'):
        Iexp = np.zeros([cal_dict["N_measures_1D"], 2], dtype=float)
        percentage_complete()
        for ind, angle in enumerate(cal_dict["angles_1"]):
            theta = np.array([0, 0, 0, angle])
            Iexp[ind, :] = go_and_measure_0D(
                cal_dict["equipment"],
                theta=theta,
                I0=cal_dict['I0'],
                verbose=verbose)
            percentage_complete(ind, cal_dict["N_measures_1D"])
        # Correct the Iexp
        Iexp, _ = correct_intensity_0D(Iexp, cal_dict['mean_ref'])
    # Test objects
    Mp1_test = Mueller()
    Mp1_test.diattenuator_linear(p1=0.95, p2=0.05)
    Ifuente = Stokes()
    Ifuente.circular_light(intensity=Iexp.max() / 0.95**4)
    par0 = [0.95, 0.4, 0.9]
    par1, success = optimize.leastsq(
        error_step_1,
        par0,
        args=(cal_dict["angles_1"], Mp1_test, Ifuente, Iexp))
    Imodel = model_step_1(par1, cal_dict["angles_1"], Mp1_test, Ifuente)
    # Extract results
    (p1, p2) = (par1[0], par1[1])
    th0p1 = (par1[2] % np.pi) / degrees
    Mpmalo = Mueller()
    Mpmalo.diattenuator_linear(p1, p2, angle=0)
    # Plot results, 1D
    plot_experiment_residuals_1D(
        cal_dict["angles_1"],
        Iexp,
        Imodel,
        title='Step 1',
        xlabel='P1 angle (deg)')
    # Print results
    print('Preliminarr analysis first iteration:')
    print('   - Pmalo p1      : {:.3f}'.format(p1))
    print('   - Pmalo p2      : {:.3f}'.format(p2))
    print('   - P1 theta_0    : {:.1f} deg'.format(th0p1))
    print('   - Diode I0      : {:.3f} V'.format(Iexp.max()))
    # Save data
    filename = "Step_1_{}".format(datetime.date.today())
    np.savez(
        filename + '.npz',
        angles_1=cal_dict["angles_1"],
        Iexp=Iexp,
        fit=par1,
        dict_param=set_cal_dict(cal_dict))
    # Output
    return Iexp, Mpmalo, th0p1, Ifuente


def model_step_2(par, th1, thX, thY, Mp1, Mpmalo, S0, Dr2):
    # Objetos
    Mr2 = Mueller()
    Mr2.diattenuator_retarder_linear(par[0], par[0], Dr2, angle=par[4])
    Ifuente = Stokes()
    Ifuente.general_azimuth_ellipticity(
        intensity=S0, az=par[1], el=par[2], pol_degree=par[3])
    # Medida 1D
    M = [Mpmalo, Mr2, Mp1]
    th = [0, 0, th1]
    I1 = Intensity_Rotating_Elements(M, th, Ei=Ifuente)
    # medidas 2D
    M = [Mr2, Mp1]
    th = [thX, thY]
    I2 = Intensity_Rotating_Elements(M, th, Ei=Ifuente)
    return I1, I2


def error_step_2(par, th1, thX, thY, Mp1, Mpmalo, S0, Dr2, Iexp1, Iexp2):
    I1_fit, I2_fit = model_step_2(par, th1, thX, thY, Mp1, Mpmalo, S0, Dr2)
    dI1 = Iexp1 - I1_fit
    dI2 = (Iexp2 - I2_fit).flatten()
    return np.concatenate((dI1, dI2))


def model_step_2a(par, th1, Mp1, Mpmalo, Dr2, Ifuente):
    # Calcular
    Mr2 = Mueller()
    Mr2.diattenuator_retarder_linear(par[0], par[0], Dr2, angle=par[1])
    M = [Mpmalo, Mr2, Mp1]
    th = [0, 0, th1]
    Imodel = Intensity_Rotating_Elements(M, th, Ei=Ifuente)
    return Imodel


def error_step_2a(par, th1, Mp1, Mpmalo, Dr2, Ifuente, Iexp):
    Imodel = model_step_2a(par, th1, Mp1, Mpmalo, Dr2, Ifuente)
    diferencia = Imodel - Iexp
    return diferencia


def make_step_2a(cal_dict, verbose=False):
    """Function that performs step 6 of calibration: theta0 of R2."""
    if cal_dict["type"] in ('experiment', 'Experiment', 'exp', 'Exp'):
        Iexp = np.zeros([cal_dict["N_measures_1D"], 2], dtype=float)
        percentage_complete()
        for ind, angle in enumerate(cal_dict["angles_1"]):
            theta = np.array([0, 0, 0, angle])
            Iexp[ind, :] = go_and_measure_0D(
                cal_dict["equipment"],
                theta=theta,
                I0=cal_dict['I0'],
                verbose=verbose)
            percentage_complete(ind, cal_dict["N_measures_1D"])
        # Correct the Iexp
        Iexp, _ = correct_intensity_0D(Iexp, cal_dict['mean_ref'])
    # Test objects
    Mp1_test = Mueller()
    Mp1_test.diattenuator_linear(
        p1=0.95, p2=0.05, angle=cal_dict["th0_p1b_test"])
    Dr2 = 82.7 * degrees
    # Ifuente = cal_dict['Ifuente_test']
    Ifuente = Stokes()
    Ifuente.circular_light(intensity=Iexp.max() / 0.95**4)
    par0 = [1, 0]
    # Make analysis
    par1, success = optimize.leastsq(
        error_step_2a,
        par0,
        args=(cal_dict["angles_1"], Mp1_test, cal_dict["Mpmalo"], Dr2, Ifuente,
              Iexp))
    Imodel = model_step_2a(par1, cal_dict["angles_1"], Mp1_test,
                           cal_dict["Mpmalo"], Dr2, Ifuente)
    # Plot results, 1D
    plot_experiment_residuals_1D(
        cal_dict["angles_1"],
        Iexp,
        Imodel,
        title='Step 2a',
        xlabel='R2 angle (deg)')
    # Print results
    print(par1)
    print('Preliminary analysis:')
    print('   - R1 theta_0    : {:.1f} deg'.format(par1[1] / degrees))
    # Save data
    filename = "Step_2a_{}".format(datetime.date.today())
    np.savez(
        filename + '.npz',
        angles_1=cal_dict["angles_1"],
        Iexp=Iexp,
        fit=par1,
        dict_param=set_cal_dict(cal_dict))
    # Output
    return Iexp, par1[1]


def model_step_2b(par, th1, th2, S0, Mp1, Mr2):
    # Correct parameters
    if par[2] > 1:
        par[2] = 1
    # Iluminacion y angulos
    Itest = Stokes()
    Itest.general_azimuth_ellipticity(
        intensity=S0, az=par[0], el=par[1], pol_degree=par[2])
    # Calcular
    th = [th1 + par[3], th2]
    M = [Mr2, Mp1]
    I = Intensity_Rotating_Elements(M, th, Itest, False)
    return I


def error_step_2b(par, th1, th2, S0, Mp1, Mr2, Iexp):
    dI = model_step_2b(par, th1, th2, S0, Mp1, Mr2) - Iexp
    return dI.flatten()


def make_step_2b(cal_dict, verbose=False):
    """Function that performs step 7 of calibration: analysis of light source."""
    if cal_dict["type"] in ('experiment', 'Experiment', 'exp', 'Exp'):
        # Make the experiment
        Iexp = np.zeros(
            [cal_dict["N_measures_2D"][0], cal_dict["N_measures_2D"][1], 2],
            dtype=float)
        percentage_complete()
        for ind1, angleX in enumerate(cal_dict["angles_2X"]):
            for ind2, angleY in enumerate(cal_dict["angles_2Y"]):
                theta = np.array([0, 0, angleX, angleY])
                Iexp[ind1, ind2, :] = go_and_measure_0D(
                    cal_dict["equipment"],
                    theta=theta,
                    I0=cal_dict['I0'],
                    verbose=verbose)
                percentage_complete([ind1, ind2], cal_dict["N_measures_2D"])
        # Correct the Iexp
        Iexp, _ = correct_intensity_0D(Iexp, cal_dict['mean_ref'])
    # Test objects
    Mp1_test = Mueller()
    Mp1_test.diattenuator_linear(
        p1=0.95, p2=0.05, angle=cal_dict["th0_p1b_test"])
    Mr2_test = Mueller()
    Mr2_test.retarder_linear(D=82.9 * degrees, angle=cal_dict["th0_r2b_test"])
    par0 = [
        cal_dict['Ifuente_test'].parameters.intensity(), 0, np.pi / 4, 0, 0
    ]
    # Make analysis
    par1, success = optimize.leastsq(
        error_step_2b,
        par0,
        args=(cal_dict["angles_2X"], cal_dict["angles_2Y"], Mp1_test, Mr2_test,
              cal_dict["err_amp"], Iexp))
    Imodel = model_step_2b(par1, cal_dict["angles_2X"], cal_dict["angles_2Y"],
                           Mp1_test, Mr2_test, cal_dict["err_amp"])
    # Plot results, 1D
    Ifuente = Stokes('Illumination')
    Ifuente.general_azimuth_ellipticity(
        az=par1[1], el=par1[2], intensity=par1[0], pol_degree=par1[3])
    plot_experiment_residuals_2D(
        cal_dict["angles_2X"],
        cal_dict["angles_2Y"],
        Iexp,
        Imodel,
        title='Step 2b',
        xlabel='Angle R2 (deg)',
        ylabel='Angle P1 (deg)')
    # Print results
    print('Preliminary analysis:')
    print(Ifuente)
    # Save data
    filename = "Step_2b_{}".format(datetime.date.today())
    np.savez(
        filename + '.npz',
        angles2x=cal_dict["angles_2X"],
        angles2y=cal_dict["angles_2Y"],
        Iexp=Iexp,
        fit=Ifuente,
        dict_param=set_cal_dict(cal_dict))
    # Output
    return Iexp, Ifuente


def make_step_3a(cal_dict, verbose=False):
    """Function that performs step 8a of calibration."""
    if cal_dict["type"] in ('experiment', 'Experiment', 'exp', 'Exp'):
        Iexp = np.zeros([cal_dict["N_measures_1D"], 2], dtype=float)
        percentage_complete()
        for ind, angle in enumerate(cal_dict["angles_1"]):
            theta = np.array([0, 0, 0, angle])
            Iexp[ind, :] = go_and_measure_0D(
                cal_dict["equipment"],
                theta=theta,
                I0=cal_dict['I0'],
                verbose=verbose)
            percentage_complete(ind, cal_dict["N_measures_1D"])
        # Correct the Iexp
        Iexp, _ = correct_intensity_0D(Iexp, cal_dict['mean_ref'])
    # Plot results, 1D
    plot_experiment_1D(cal_dict["angles_1"], Iexp, title='Step 3a')
    # Save data

    filename = "Step_3a_{}".format(datetime.date.today())
    np.savez(
        filename + '.npz',
        angles_1=cal_dict["angles_1"],
        Iexp=Iexp,
        dict_param=set_cal_dict(cal_dict))
    # Output
    return Iexp


def make_step_3b(cal_dict, verbose=False):
    """Function that performs step 8a of calibration."""
    if cal_dict["type"] in ('experiment', 'Experiment', 'exp', 'Exp'):
        Iexp = np.zeros([cal_dict["N_measures_1D"], 2], dtype=float)
        percentage_complete()
        for ind, angle in enumerate(cal_dict["angles_1"]):
            theta = np.array([0, 0, 0, angle])
            Iexp[ind, :] = go_and_measure_0D(
                cal_dict["equipment"],
                theta=theta,
                I0=cal_dict['I0'],
                verbose=verbose)
            percentage_complete(ind, cal_dict["N_measures_1D"])
        # Correct the Iexp
        Iexp, _ = correct_intensity_0D(Iexp, cal_dict['mean_ref'])
    # Plot results, 1D
    plot_experiment_1D(cal_dict["angles_1"], Iexp, title='Step 3a')
    # Save data

    filename = "Step_3b_{}".format(datetime.date.today())
    np.savez(
        filename + '.npz',
        angles_1=cal_dict["angles_1"],
        Iexp=Iexp,
        dict_param=set_cal_dict(cal_dict))
    # Output
    return Iexp


def make_step_3c(cal_dict, verbose=False):
    """Function that performs step 8a of calibration."""
    if cal_dict["type"] in ('experiment', 'Experiment', 'exp', 'Exp'):
        Iexp = np.zeros([cal_dict["N_measures_1D"], 2], dtype=float)
        percentage_complete()
        for ind, angle in enumerate(cal_dict["angles_1"]):
            theta = np.array([0, 0, 0, angle])
            Iexp[ind, :] = go_and_measure_0D(
                cal_dict["equipment"],
                theta=theta,
                I0=cal_dict['I0'],
                verbose=verbose)
            percentage_complete(ind, cal_dict["N_measures_1D"])
        # Correct the Iexp
        Iexp, _ = correct_intensity_0D(Iexp, cal_dict['mean_ref'])
    # Plot results, 1D
    plot_experiment_1D(cal_dict["angles_1"], Iexp, title='Step 3a')
    # Save data

    filename = "Step_3c_{}".format(datetime.date.today())
    np.savez(
        filename + '.npz',
        angles_1=cal_dict["angles_1"],
        Iexp=Iexp,
        dict_param=set_cal_dict(cal_dict))
    # Output
    return Iexp


def model_step_3(par, th1, Ifuente, th0p1b):
    # Order parameters
    p11, p12, p21, p22, p31, p32, th0p1, th0p2, th0p3 = par[0:9]
    # Create Mueller objects
    Mp1 = Mueller()
    Mp1.diattenuator_linear(p1=p11, p2=p12)
    Mp2 = Mueller()
    Mp2.diattenuator_linear(p1=p21, p2=p22)
    Mp3 = Mueller()
    Mp3.diattenuator_linear(p1=p31, p2=p32)
    # First, P3 and P1
    M = [Mp3, Mp1]
    th = [th0p3, th1 + th0p1b]
    Ia = Intensity_Rotating_Elements(M, th, Ei=Ifuente)
    # Then, P3 and P2
    M = [Mp3, Mp2]
    th = [th0p3, th1 + th0p2]
    Ib = Intensity_Rotating_Elements(M, th, Ei=Ifuente)
    # Last, P1 and P2
    M = [Mp1, Mp2]
    th = [th0p1, th1 + th0p2]
    Ic = Intensity_Rotating_Elements(M, th, Ei=Ifuente)
    # End
    return Ia, Ib, Ic


def error_step_3(par, th1, Ifuente, th0p1b, IexpA, IexpB, IexpC):
    Ia, Ib, Ic = model_step_3(par, th1, Ifuente, th0p1b)
    errA = IexpA - Ia
    errB = IexpB - Ib
    errC = IexpC - Ic
    return np.concatenate((errA, errB, errC))


def analysis_step_3(cal_dict):
    "Makes the first analysis of step 8: measurement of polarizers"
    # initial guess
    par0 = [0.95, 0.05, 0.95, 0.05, 0.95, 0.05, 0, 0, 0]
    # Make analysis
    par1, success = optimize.leastsq(
        error_step_3,
        par0,
        args=(cal_dict["angles_1"], cal_dict["Ifuente_test"],
              cal_dict["th0_p1b_test"], cal_dict["I_step_3a"],
              cal_dict["I_step_3b"], cal_dict["I_step_3c"]))
    Imodel = model_step_3(par1, cal_dict["angles_1"], cal_dict["Ifuente_test"],
                          cal_dict["th0_p1b_test"])
    Imodel = np.concatenate(Imodel)
    # Plot results, 1D
    angles = np.concatenate(
        (cal_dict["angles_1"], cal_dict["angles_1"] + cal_dict["max_angle_1D"],
         cal_dict["angles_1"] + 2 * cal_dict["max_angle_1D"]))
    intensities = np.concatenate((cal_dict["I_step_3a"], cal_dict["I_step_3b"],
                                  cal_dict["I_step_3c"]))
    plot_experiment_residuals_1D(
        angles,
        intensities,
        Imodel,
        title='Step 3',
        xlabel='Angle 2nd pol. (deg)')
    # Print results
    print('Preliminary analysis:')
    print('   - P1 p1         : {:.3f}'.format(par1[0]))
    print('   - P1 p2         : {:.3f}'.format(par1[1]))
    print('   - P1 theta_0    : {:.1f} deg'.format(par1[6] / degrees))
    print('   - P2 p1         : {:.3f}'.format(par1[2]))
    print('   - P2 p2         : {:.3f}'.format(par1[3]))
    print('   - P2 theta_0    : {:.1f} deg'.format(par1[7] / degrees))
    print('   - P3 p1         : {:.3f}'.format(par1[4]))
    print('   - P3 p2         : {:.3f}'.format(par1[5]))
    print('   - P3 theta_0    : {:.1f} deg'.format(par1[8] / degrees))
    # Save data

    filename = "Step_3_analysis_{}".format(datetime.date.today())
    np.savez(
        filename + '.npz',
        angles_total=angles,
        Iexp_total=intensities,
        th0_p1=par1[6],
        th0_p2=par1[7],
        dict_param=set_cal_dict(cal_dict))
    # Extract info
    Mp1 = Mueller()
    Mp1.diattenuator_linear(p1=par1[0], p2=par1[1], angle=par1[6])
    Mp2 = Mueller()
    Mp2.diattenuator_linear(p1=par1[2], p2=par1[3], angle=par1[7])
    # Output
    return Mp1, par1[0], par1[1], par1[6], Mp2, par1[7]


def model_step_4(par, th1, thX, thY, Mp2, Ifuente, p11, p12, th0p1):
    # Create Mueller objects
    Mr2 = Mueller()
    Mr2.diattenuator_retarder_linear(
        p1=par[0], p2=par[1], D=par[2], angle=par[3])
    Mp1 = Mueller()
    Mp1.diattenuator_retarder_linear(
        p1=p11, p2=p12, D=par[4], angle=th0p1 + par[5])
    # Calcular
    M = [Mp1, Mr2, Mp2]
    th = [0, 0, th1]
    Ia = Intensity_Rotating_Elements(M, th, Ifuente, False)
    th = [0, thX, thY]
    Ib = Intensity_Rotating_Elements(M, th, Ifuente, False)
    return Ia, Ib


def error_step_4(par, th1, thX, thY, Mp2, Ifuente, p11, p12, th0p1, IexpA,
                 IexpB):
    Ia, Ib = model_step_4(par, th1, thX, thY, Mp2, Ifuente, p11, p12, th0p1)
    difA = IexpA - Ia
    difB = IexpB - Ib
    return np.concatenate((difA, difB.flatten()))


def model_step_4a(par, th, Mp1, Mp2, Mr2, Ifuente):
    # Calcular
    p1, p2, az, el = Mp1.analysis.diattenuator(param='az_el')
    Mp1.diattenuator_retarder_linear(p1, p2, par[2], az + par[3])
    ret, az, el = Mr2.analysis.retarder(param='az_el')
    Mr2.diattenuator_retarder_linear(par[0], par[0], ret, par[1])
    M = [Mp1, Mr2, Mp2]
    th = [0, 0, th]
    I = Intensity_Rotating_Elements(M, th, Ifuente, False)
    return I


def error_step_4a(par, th, Mp1, Mp2, Mr2, Ifuente, Iexp):
    Imodel = model_step_4a(par, th, Mp1, Mp2, Mr2, Ifuente)
    diferencia = Imodel - Iexp
    return diferencia


def make_step_4a(cal_dict, verbose=False):
    """Function that performs step 9 of calibration: reference angle of R2."""
    if cal_dict["type"] in ('experiment', 'Experiment', 'exp', 'Exp'):
        # Make the experiment
        Iexp = np.zeros([cal_dict["N_measures_1D"], 2], dtype=float)
        percentage_complete()
        for ind, angle in enumerate(cal_dict["angles_1"]):
            theta = np.array([0, 0, 0, angle])
            Iexp[ind, :] = go_and_measure_0D(
                cal_dict["equipment"],
                theta=theta,
                I0=cal_dict['I0'],
                verbose=verbose)
            percentage_complete(ind, cal_dict["N_measures_1D"])
        # Correct the Iexp
        Iexp, _ = correct_intensity_0D(Iexp, cal_dict['mean_ref'])
    # Make analysis
    par0 = [1, 0, 0, 0]
    Mr2 = Mueller()
    Mr2.diattenuator_retarder_linear(p1=0.99, p2=0.99, D=83 * degrees, angle=0)
    par1, success = optimize.leastsq(
        error_step_4a,
        par0,
        args=(cal_dict["angles_1"], cal_dict["Mp1_test"], cal_dict["Mp2_test"],
              Mr2, cal_dict["Ifuente_test"], cal_dict["err_amp"], Iexp))
    Imodel = model_step_4a(par1, cal_dict["angles_1"], cal_dict["Mp1_test"],
                           cal_dict["Mp2_test"], Mr2, cal_dict["Ifuente_test"],
                           cal_dict["err_amp"])
    # Plot results, 1D
    plot_experiment_residuals_1D(
        cal_dict["angles_1"],
        Iexp,
        Imodel,
        title='Step 4a',
        xlabel='P2 angle (deg)')
    # Print results
    print('Preliminary analysis:')
    print('   - R2 angle         : {:.1f} deg'.format(par1[1] / degrees))
    # Save data

    filename = "Step_4a_{}".format(datetime.date.today())
    np.savez(
        filename + '.npz',
        angles_1=cal_dict["angles_1"],
        Iexp=Iexp,
        fit=par1[1],
        dict_param=set_cal_dict(cal_dict))
    # Output
    return Iexp, par1[1]


def model_step_4b(par, th1, th2, Mp2, Ifuente, p11, p12, th0p1, th0r2):
    # Ordenar o no los pes del polarizador
    p1, p2, th0r2 = sort_pes([par[0], par[1], th0r2])
    # Create Mueller objects
    Mr2 = Mueller()
    Mr2.diattenuator_retarder_linear(
        p1=p1, p2=p2, D=par[2], angle=th0r2 + par[4])
    Mp1 = Mueller()
    Mp1.diattenuator_retarder_linear(p1=p11, p2=p12, D=par[3], angle=th0p1)
    # Calcular
    M = [Mp1, Mr2, Mp2]
    th = [0, th1, th2]
    I = Intensity_Rotating_Elements(M, th, Ifuente, False)
    return I


def error_step_4b(par, th1, th2, Mp2, Ifuente, p11, p12, th0p1, th0r2, Iexp):
    Imodel = model_step_4b(par, th1, th2, Mp2, Ifuente, p11, p12, th0p1, th0r2)
    dI = Imodel - Iexp
    return dI.flatten()


def make_step_4b(cal_dict, verbose=False):
    """Function that performs step 10 of calibration: calibration of R2."""
    if cal_dict["type"] in ('experiment', 'Experiment', 'exp', 'Exp'):
        # Make the experiment
        Iexp = np.zeros(
            [
                cal_dict["N_measures_2D_step_4b"][0],
                cal_dict["N_measures_2D_step_4b"][1], 2
            ],
            dtype=float)
        percentage_complete()
        for ind1, angleX in enumerate(cal_dict["angles_2X_step_4b"]):
            for ind2, angleY in enumerate(cal_dict["angles_2Y_step_4b"]):
                theta = np.array([0, 0, angleX, angleY])
                Iexp[ind1, ind2, :] = go_and_measure_0D(
                    cal_dict["equipment"],
                    theta=theta,
                    I0=cal_dict['I0'],
                    verbose=verbose)
                percentage_complete([ind1, ind2],
                                    cal_dict["N_measures_2D_step_4b"])
        # Correct the Iexp
        Iexp, _ = correct_intensity_0D(Iexp, cal_dict['mean_ref'])
    # Make analysis
    par0 = [0.99, 0.99, 83 * degrees, 90 * degrees, 0]
    par1, success = optimize.leastsq(
        error_step_4b,
        par0,
        args=(cal_dict["angles_2X_step_4b"], cal_dict["angles_2Y_step_4b"],
              cal_dict["Mp2_test"], cal_dict["Ifuente_test"], cal_dict["p11"],
              cal_dict["p12"], cal_dict["th0_p1_test"],
              cal_dict["th0_r2_test"], cal_dict["err_amp"], Iexp))
    Imodel = model_step_4b(par1, cal_dict["angles_2X_step_4b"],
                           cal_dict["angles_2Y_step_4b"], cal_dict["Mp2_test"],
                           cal_dict["Ifuente_test"], cal_dict["p11"],
                           cal_dict["p12"], cal_dict["th0_p1_test"],
                           cal_dict["th0_r2_test"], cal_dict["err_amp"])
    # Plot results, 1D
    plot_experiment_residuals_2D(
        cal_dict["angles_2X_step_4b"],
        cal_dict["angles_2Y_step_4b"],
        Iexp,
        Imodel,
        title='Step 4b',
        xlabel='Angle R2 (deg)',
        ylabel='Angle P2 (deg)')
    # Print results
    print('Analysis first iteration:')
    print('   - R2 p1            : {:.3f}'.format(par1[0]))
    print('   - R2 p2            : {:.3f}'.format(par1[1]))
    print('   - R2 retardance    : {:.1f} deg'.format(par1[2] / degrees))
    print('   - P1 retardance    : {:.1f} deg'.format(par1[3] / degrees))
    # Extract info
    fit_dict = {}
    Mp1 = Mueller()
    Mp1.diattenuator_retarder_linear(
        p1=cal_dict["p11"],
        p2=cal_dict["p12"],
        D=par1[4],
        angle=cal_dict["th0_p1_test"])
    fit_dict["Mp1"] = Mp1
    Mr2 = Mueller()
    Mr2.diattenuator_retarder_linear(
        p1=par1[0], p2=par1[1], D=par1[2], angle=par1[3])
    fit_dict["Mr2"] = Mr2
    # Save data

    filename = "Step_4b_{}".format(datetime.date.today())
    np.savez(
        filename + '.npz',
        angles2x=cal_dict["angles_2X_step_4b"],
        angles2y=cal_dict["angles_2Y_step_4b"],
        Iexp=Iexp,
        fit=fit_dict,
        dict_param=set_cal_dict(cal_dict))
    # Output
    return Iexp, Mp1, Mr2


def model_step_5(par, th1, thX, thY, Mp1, Mp2, Mr2, Ifuente, mode1, mode2):
    # Mueller objects
    Mr1 = Mueller()
    Mr1.diattenuator_retarder_linear(
        p1=par[0], p2=par[1], D=par[2], angle=par[3])
    M = [Mp1, Mr1, Mr2, Mp2]
    # 1D
    if mode1 in ('R1'):
        th = [0, th1, par[4], 0]
        I1 = Intensity_Rotating_Elements(M, th, Ifuente, False)
    elif mode1 in ('R2'):
        th = [0, 0, th1 + par[4], 0]
        I1 = Intensity_Rotating_Elements(M, th, Ifuente, False)
    elif mode1 in ('P2'):
        th = [0, 0, par[4], th1]
        I1 = Intensity_Rotating_Elements(M, th, Ifuente, False)
    else:
        th = [0, th1, par[4], 0]
        I1a = Intensity_Rotating_Elements(M, th, Ifuente, False)
        th = [0, 0, th1 + par[4], 0]
        I1b = Intensity_Rotating_Elements(M, th, Ifuente, False)
        th = [0, 0, par[4], th1]
        I1c = Intensity_Rotating_Elements(M, th, Ifuente, False)
        I1 = (I1a, I1b, I1c)
    # 2D
    if mode2 in ('R1+R2', 'R1 R2'):
        th = [0, thX, thY + par[4], 0]
        I2 = Intensity_Rotating_Elements(M, th, Ifuente, False)
    elif mode2 in ('R1+P2', 'R1 P2'):
        th = [0, thX, par[4], thY]
        I2 = Intensity_Rotating_Elements(M, th, Ifuente, False)
    elif mode2 in ('R2+P2', 'R2 P2'):
        th = [0, 0, thX + par[4], thY]
        I2 = Intensity_Rotating_Elements(M, th, Ifuente, False)
    else:
        th = [0, thX, thY + par[4], 0]
        I2a = Intensity_Rotating_Elements(M, th, Ifuente, False)
        th = [0, thX, par[4], thY]
        I2b = Intensity_Rotating_Elements(M, th, Ifuente, False)
        th = [0, 0, thX + par[4], thY]
        I2c = Intensity_Rotating_Elements(M, th, Ifuente, False)
        I2 = (I2a, I2b, I2c)
    # Return
    return I1, I2


def error_step_5(par, th1, thX, thY, Mp1, Mp2, Mr2, Ifuente, mode1, mode2,
                 Iexp1, Iexp2):
    I1_teor, I2_teor = model_step_5(par, th1, thX, thY, Mp1, Mp2, Mr2, Ifuente,
                                    mode1, mode2)
    if mode1 in ('all', 'All', 'ALL'):
        dI1a = Iexp1[0] - I1_teor[0]
        dI1b = Iexp1[1] - I1_teor[1]
        dI1c = Iexp1[2] - I1_teor[2]
        dI1 = np.concatenate((dI1a, dI1b, dI1c))
    else:
        dI1 = Iexp1 - I1_teor
    if mode2 in ('all', 'All', 'ALL'):
        dI1a = (Iexp2[0] - I2_teor[0]).flatten()
        dI1b = (Iexp2[1] - I2_teor[1]).flatten()
        dI1c = (Iexp2[2] - I2_teor[2]).flatten()
        dI1 = np.concatenate((dI1a, dI1b, dI1c))
    else:
        dI2 = (Iexp2 - I2_teor).flatten()
    return np.concatenate((dI1, dI2))


def model_step_5a(par, th, Mp1, Mp2, Mr1, Mr2, Ifuente, mode):
    # Calcular
    ret, az, el = Mr1.analysis.retarder(param='az_el')
    Mr1.diattenuator_retarder_linear(par[0], par[0], ret, par[1])
    M = [Mp1, Mr1, Mr2, Mp2]
    if mode in ('R1'):
        th1 = [0, th, 0, 0]
    elif mode in ('R2'):
        th1 = [0, 0, th, 0]
    else:
        th1 = [0, 0, 0, th]
    I = Intensity_Rotating_Elements(M, th1, Ifuente, False)
    return I


def error_step_5a(par, th, Mp1, Mp2, Mr1, Mr2, Ifuente, mode, Iexp):
    I = model_step_5a(par, th, Mp1, Mp2, Mr1, Mr2, Ifuente, mode)
    dI = I - Iexp
    return dI


def error_step_5a_all(par, th, Mp1, Mp2, Mr1, Mr2, Ifuente, IexpA, IexpB,
                      IexpC):
    I1 = model_step_5a(par, th, Mp1, Mp2, Mr1, Mr2, Ifuente, 'R1')
    I2 = model_step_5a(par, th, Mp1, Mp2, Mr1, Mr2, Ifuente, 'R2')
    I3 = model_step_5a(par, th, Mp1, Mp2, Mr1, Mr2, Ifuente, 'P2')
    dI1 = I1 - IexpA
    dI2 = I2 - IexpB
    dI3 = I3 - IexpC
    return np.concatenate((dI1, dI2, dI3))


def make_step_5a(cal_dict, verbose=False):
    """Function that performs step 11 of calibration: reference angle of R2."""
    if cal_dict["type"] in ('experiment', 'Experiment', 'exp', 'Exp'):
        IexpA = np.zeros([cal_dict["N_measures_1D"], 2], dtype=float)
        # First rotate R1
        if cal_dict["step_5a"] in ('all', 'All', 'ALL', 'R1'):
            IexpA = np.zeros([cal_dict["N_measures_1D"], 2], dtype=float)
            percentage_complete()
            for ind, angle in enumerate(cal_dict["angles_1"]):
                theta = np.array([0, angle, 0, 0])
                IexpA[ind, :] = go_and_measure_0D(
                    cal_dict["equipment"],
                    theta=theta,
                    I0=cal_dict['I0'],
                    verbose=verbose)
                percentage_complete(ind, cal_dict["N_measures_1D"])
        # Correct the Iexp
        IexpA, _ = correct_intensity_0D(IexpA, cal_dict['mean_ref'])
    else:
        IexpA = None
    # Then rotate R2
    if cal_dict["step_5a"] in ('all', 'All', 'ALL', 'R2'):
        IexpB = np.zeros([cal_dict["N_measures_1D"], 2], dtype=float)
        percentage_complete()
        for ind, angle in enumerate(cal_dict["angles_1"]):
            theta = np.array([0, 0, angle, 0])
            IexpB[ind, :] = go_and_measure_0D(
                cal_dict["equipment"],
                theta=theta,
                I0=cal_dict['I0'],
                verbose=verbose)
            percentage_complete(ind, cal_dict["N_measures_1D"])
        # Correct the Iexp
        IexpB, _ = correct_intensity_0D(IexpB, cal_dict['mean_ref'])
    else:
        IexpB = None
    # Last, rotate P2
    if cal_dict["step_5a"] in ('all', 'All', 'ALL', 'P2'):
        IexpC = np.zeros([cal_dict["N_measures_1D"], 2], dtype=float)
        percentage_complete()
        for ind, angle in enumerate(cal_dict["angles_1"]):
            theta = np.array([0, 0, 0, angle])
            IexpC[ind, :] = go_and_measure_0D(
                cal_dict["equipment"],
                theta=theta,
                I0=cal_dict['I0'],
                verbose=verbose)
            percentage_complete(ind, cal_dict["N_measures_1D"])
        # Correct the Iexp
        IexpC, _ = correct_intensity_0D(IexpC, cal_dict['mean_ref'])
    else:
        IexpC = None
    # Make analysis
    par0 = [1, 0]
    Mr1 = Mueller()
    Mr1.diattenuator_retarder_linear(p1=0.99, p2=0.99, D=83 * degrees, angle=0)
    # Only R1
    if cal_dict["step_5a"] in ('individual', 'Individual', 'R1'):
        par1, success = optimize.leastsq(
            error_step_5a,
            par0,
            args=(cal_dict["angles_1"], cal_dict["Mp1_test"],
                  cal_dict["Mp2_test"], Mr1, cal_dict["Mr2_test"],
                  cal_dict["Ifuente_test"], 'R2', IexpA))
        ImodelA = model_step_5a(par1, cal_dict["angles_1"],
                                cal_dict["Mp1_test"], cal_dict["Mp2_test"],
                                Mr1, cal_dict["Mr2_test"],
                                cal_dict["Ifuente_test"], 'R1')
        print(par1)
    # Only R2
    if cal_dict["step_5a"] in ('individual', 'Individual', 'R2'):
        par1, success = optimize.leastsq(
            error_step_5a,
            par0,
            args=(cal_dict["angles_1"], cal_dict["Mp1_test"],
                  cal_dict["Mp2_test"], Mr1, cal_dict["Mr2_test"],
                  cal_dict["Ifuente_test"], 'R2', IexpB))
        ImodelB = model_step_5a(par1, cal_dict["angles_1"],
                                cal_dict["Mp1_test"], cal_dict["Mp2_test"],
                                Mr1, cal_dict["Mr2_test"],
                                cal_dict["Ifuente_test"], 'R2')
        print(par1)
    # Only P2
    if cal_dict["step_5a"] in ('individual', 'Individual', 'P2'):
        par1, success = optimize.leastsq(
            error_step_5a,
            par0,
            args=(cal_dict["angles_1"], cal_dict["Mp1_test"],
                  cal_dict["Mp2_test"], Mr1, cal_dict["Mr2_test"],
                  cal_dict["Ifuente_test"], 'P2', IexpC))
        ImodelC = model_step_5a(par1, cal_dict["angles_1"],
                                cal_dict["Mp1_test"], cal_dict["Mp2_test"],
                                Mr1, cal_dict["Mr2_test"],
                                cal_dict["Ifuente_test"], 'P2')
        print(par1)
    # Both together
    if cal_dict["step_5a"] in ('all', 'All', 'ALL'):
        par1, success = optimize.leastsq(
            error_step_5a_all,
            par0,
            args=(cal_dict["angles_1"], cal_dict["Mp1_test"],
                  cal_dict["Mp2_test"], Mr1, cal_dict["Mr2_test"],
                  cal_dict["Ifuente_test"], IexpA, IexpB))
        ImodelA = model_step_5a(par1, cal_dict["angles_1"],
                                cal_dict["Mp1_test"], cal_dict["Mp2_test"],
                                Mr1, cal_dict["Mr2_test"],
                                cal_dict["Ifuente_test"], 'R1')
        ImodelB = model_step_5a(par1, cal_dict["angles_1"],
                                cal_dict["Mp1_test"], cal_dict["Mp2_test"],
                                Mr1, cal_dict["Mr2_test"],
                                cal_dict["Ifuente_test"], 'R2')
        ImodelC = model_step_5a(par1, cal_dict["angles_1"],
                                cal_dict["Mp1_test"], cal_dict["Mp2_test"],
                                Mr1, cal_dict["Mr2_test"],
                                cal_dict["Ifuente_test"], 'P2')
    # Errors
    if IexpA is None:
        errorA = 100
    else:
        errorA = np.linalg.norm(ImodelA - IexpA) / (
            cal_dict["N_measures_1D"] * np.max(IexpA))
    if IexpB is None:
        errorB = 100
    else:
        errorB = np.linalg.norm(ImodelB - IexpB) / (
            cal_dict["N_measures_1D"] * np.max(IexpB))
    if IexpC is None:
        errorC = 100
    else:
        errorC = np.linalg.norm(ImodelC - IexpC) / (
            cal_dict["N_measures_1D"] * np.max(IexpC))
    # Plot results, 1D
    if IexpA is not None:
        plot_experiment_residuals_1D(
            cal_dict["angles_1"],
            IexpA,
            ImodelA,
            title='Step 5a: Rotating R1',
            xlabel='Rotation angle (deg)')
    if IexpB is not None:
        plot_experiment_residuals_1D(
            cal_dict["angles_1"],
            IexpB,
            ImodelB,
            title='Step 5a: Rotating R2',
            xlabel='Rotation angle (deg)')
    if IexpC is not None:
        plot_experiment_residuals_1D(
            cal_dict["angles_1"],
            IexpC,
            ImodelC,
            title='Step 5a: Rotating P2',
            xlabel='Rotation angle (deg)')
    # Print results
    print('Errors:')
    print('   - Rotating R1      : {:.3f} %'.format(errorA * 100))
    print('   - Rotating R2      : {:.3f} %'.format(errorB * 100))
    print('   - Rotating P2      : {:.3f} %'.format(errorC * 100))
    print('Preliminary analysis:')
    print('   - R1 angle         : {:.1f} deg'.format(par1[1] / degrees))
    # Save data

    filename = "Step_5a_{}".format(datetime.date.today())
    np.savez(
        filename + '.npz',
        angles_1=cal_dict["angles_1"],
        IexpA=IexpA,
        IexpB=IexpB,
        IexpC=IexpC,
        fit=par1[1],
        dict_param=set_cal_dict(cal_dict))
    # Output
    return IexpA, IexpB, IexpC, par1[1]


def model_step_5b(par, th1, th2, Mp1, Mr2, Mp2, Ifuente, th0r1, mode):
    # Mueller objects
    Mr1 = Mueller()
    Mr1.diattenuator_retarder_linear(
        p1=par[0], p2=par[1], D=par[2], angle=th0r1 + par[3])
    # Angles
    if mode in ('R1+R2', 'R1 R2'):
        th = [0, th1, th2, 0]
    elif mode in ('R1+P2', 'R1 P2'):
        th = [0, th1, 0, th2]
    elif mode in ('R2+P2', 'R2 P2'):
        th = [0, 0, th1, th2]
    # Calcular
    M = [Mp1, Mr1, Mr2, Mp2]
    I = Intensity_Rotating_Elements(M, th, Ifuente)
    return I


def error_step_5b(par, th1, th2, Mp1, Mr2, Mp2, Ifuente, th0r1, mode, Iexp):
    Imodel = model_step_5b(par, th1, th2, Mp1, Mr2, Mp2, Ifuente, th0r1, mode)
    dI = Imodel - Iexp
    return dI.flatten()


def error_step_5b_all(par, th1, th2, Mp1, Mr2, Mp2, Ifuente, th0r1, IexpA,
                      IexpB, IexpC):
    ImodelA = model_step_5b(par, th1, th2, Mp1, Mr2, Mp2, Ifuente, th0r1,
                            'R1 R2')
    ImodelB = model_step_5b(par, th1, th2, Mp1, Mr2, Mp2, Ifuente, th0r1,
                            'R1 P2')
    ImodelC = model_step_5b(par, th1, th2, Mp1, Mr2, Mp2, Ifuente, th0r1,
                            'R2 P2')
    dIA = (ImodelA - IexpA).flatten()
    dIB = (ImodelB - IexpB).flatten()
    dIC = (ImodelC - IexpC).flatten()
    return np.concatenate((dIA, dIA, dIC))


def make_step_5b(cal_dict, verbose=False):
    """Function that performs step 5b of calibration: calibration of R1."""
    # Rotate R1 and R2
    if cal_dict["step_5b"] in ('all', 'All', 'ALL', 'individual', 'Individual',
                               'R1+R2', 'R1 R2'):
        IexpA = np.zeros(
            [cal_dict["N_measures_2D"][0], cal_dict["N_measures_2D"][1], 2],
            dtype=float)
        # Make the experiment
        percentage_complete()
        for ind1, angleX in enumerate(cal_dict["angles_2X"]):
            for ind2, angleY in enumerate(cal_dict["angles_2Y"]):
                theta = np.array([0, angleX, angleY, 0])
                IexpA[ind1, ind2, :] = go_and_measure_0D(
                    cal_dict["equipment"],
                    theta=theta,
                    I0=cal_dict['I0'],
                    verbose=verbose)
                percentage_complete([ind1, ind2], cal_dict["N_measures_2D"])
        # Correct the Iexp
        IexpA, _ = correct_intensity_0D(IexpA, cal_dict['mean_ref'])
    else:
        IexpA = None
    # Rotate R1 and P2
    if cal_dict["step_5b"] in ('all', 'All', 'ALL', 'individual', 'Individual',
                               'R1+P2', 'R1 P2'):
        # Make the experiment
        IexpB = np.zeros(
            [cal_dict["N_measures_2D"][0], cal_dict["N_measures_2D"][1], 2],
            dtype=float)
        percentage_complete()
        for ind1, angleX in enumerate(cal_dict["angles_2X"]):
            for ind2, angleY in enumerate(cal_dict["angles_2Y"]):
                theta = np.array([0, angleX, 0, angleY])
                IexpB[ind1, ind2, :] = go_and_measure_0D(
                    cal_dict["equipment"],
                    theta=theta,
                    I0=cal_dict['I0'],
                    verbose=verbose)
                percentage_complete([ind1, ind2], cal_dict["N_measures_2D"])
        # Correct the Iexp
        IexpB, _ = correct_intensity_0D(IexpB, cal_dict['mean_ref'])
    else:
        IexpB = None
    # Rotate R2 and P2
    if cal_dict["step_5b"] in ('all', 'All', 'ALL', 'individual', 'Individual',
                               'R2+P2', 'R2 P2'):
        # Make the experiment
        IexpC = np.zeros(
            [cal_dict["N_measures_2D"][0], cal_dict["N_measures_2D"][1], 2],
            dtype=float)
        percentage_complete()
        for ind1, angleX in enumerate(cal_dict["angles_2X"]):
            for ind2, angleY in enumerate(cal_dict["angles_2Y"]):
                theta = np.array([0, 0, angleX, angleY])
                IexpC[ind1, ind2, :] = go_and_measure_0D(
                    cal_dict["equipment"],
                    theta=theta,
                    I0=cal_dict['I0'],
                    verbose=verbose)
                percentage_complete([ind1, ind2], cal_dict["N_measures_2D"])
        # Correct the Iexp
        IexpC, _ = correct_intensity_0D(IexpC, cal_dict['mean_ref'])
    else:
        IexpC = None

    # Make analysis 1st case
    par0 = [0.99, 0.99, 83 * degrees, 0]
    if cal_dict["step_5b"] in ('individual', 'Individual', 'R1+R2', 'R1 R2'):
        par1, success = optimize.leastsq(
            error_step_5b,
            par0,
            args=(cal_dict["angles_2X"], cal_dict["angles_2Y"],
                  cal_dict["Mp1_test"], cal_dict["Mr2_test"],
                  cal_dict["Mp2_test"], cal_dict["Ifuente_test"],
                  cal_dict["th0_r1_test"], cal_dict["err_amp"], 'R1+R2',
                  IexpA))
        ImodelA = model_step_5b(
            par1, cal_dict["angles_2X"], cal_dict["angles_2Y"],
            cal_dict["Mp1_test"], cal_dict["Mr2_test"], cal_dict["Mp2_test"],
            cal_dict["Ifuente_test"], cal_dict["th0_r1_test"],
            cal_dict["err_amp"], 'R1+R2')
    # Make analysis 2nd case
    if cal_dict["step_5b"] in ('individual', 'Individual', 'R1+P2', 'R1 P2'):
        par1, success = optimize.leastsq(
            error_step_5b,
            par0,
            args=(cal_dict["angles_2X"], cal_dict["angles_2Y"],
                  cal_dict["Mp1_test"], cal_dict["Mr2_test"],
                  cal_dict["Mp2_test"], cal_dict["Ifuente_test"],
                  cal_dict["th0_r1_test"], cal_dict["err_amp"], 'R1+P2',
                  IexpB))
        ImodelB = model_step_5b(
            par1, cal_dict["angles_2X"], cal_dict["angles_2Y"],
            cal_dict["Mp1_test"], cal_dict["Mr2_test"], cal_dict["Mp2_test"],
            cal_dict["Ifuente_test"], cal_dict["th0_r1_test"],
            cal_dict["err_amp"], 'R1+P2')
    # Make analysis 3rd case
    if cal_dict["step_5b"] in ('individual', 'Individual', 'R2+P2', 'R2 P2'):
        par1, success = optimize.leastsq(
            error_step_5b,
            par0,
            args=(cal_dict["angles_2X"], cal_dict["angles_2Y"],
                  cal_dict["Mp1_test"], cal_dict["Mr2_test"],
                  cal_dict["Mp2_test"], cal_dict["Ifuente_test"],
                  cal_dict["th0_r1_test"], cal_dict["err_amp"], 'R2+P2',
                  IexpC))
        ImodelC = model_step_5b(
            par1, cal_dict["angles_2X"], cal_dict["angles_2Y"],
            cal_dict["Mp1_test"], cal_dict["Mr2_test"], cal_dict["Mp2_test"],
            cal_dict["Ifuente_test"], cal_dict["th0_r1_test"],
            cal_dict["err_amp"], 'R2+P2')
    # Make analysis all together
    if cal_dict["step_5b"] in ('all', 'All', 'ALL'):
        par1, success = optimize.leastsq(
            error_step_5b_all,
            par0,
            args=(cal_dict["angles_2X"], cal_dict["angles_2Y"],
                  cal_dict["Mp1_test"], cal_dict["Mr2_test"],
                  cal_dict["Mp2_test"], cal_dict["Ifuente_test"],
                  cal_dict["th0_r1_test"], cal_dict["err_amp"], IexpA, IexpB,
                  IexpC))
        ImodelA = model_step_5b(
            par1, cal_dict["angles_2X"], cal_dict["angles_2Y"],
            cal_dict["Mp1_test"], cal_dict["Mr2_test"], cal_dict["Mp2_test"],
            cal_dict["Ifuente_test"], cal_dict["th0_r1_test"],
            cal_dict["err_amp"], 'R1+R2')
        ImodelB = model_step_5b(
            par1, cal_dict["angles_2X"], cal_dict["angles_2Y"],
            cal_dict["Mp1_test"], cal_dict["Mr2_test"], cal_dict["Mp2_test"],
            cal_dict["Ifuente_test"], cal_dict["th0_r1_test"],
            cal_dict["err_amp"], 'R1+P2')
        ImodelC = model_step_5b(
            par1, cal_dict["angles_2X"], cal_dict["angles_2Y"],
            cal_dict["Mp1_test"], cal_dict["Mr2_test"], cal_dict["Mp2_test"],
            cal_dict["Ifuente_test"], cal_dict["th0_r1_test"],
            cal_dict["err_amp"], 'R2+P2')
    if IexpA is not None:
        # Plot results, 1D
        plot_experiment_residuals_2D(
            cal_dict["angles_2X"],
            cal_dict["angles_2Y"],
            IexpA,
            ImodelA,
            title='Step 5b: R1 and R2',
            xlabel='Angle R1 (deg)',
            ylabel='Angle R2 (deg)')
        errorA = np.linalg.norm(ImodelA - IexpA) / (
            cal_dict["N_measures_1D"] * np.max(IexpA))
    else:
        errorA = 0
    if IexpB is not None:
        # Plot results, 1D
        plot_experiment_residuals_2D(
            cal_dict["angles_2X"],
            cal_dict["angles_2Y"],
            IexpB,
            ImodelB,
            title='Step 5b: R1 and P2',
            xlabel='Angle R1 (deg)',
            ylabel='Angle P2 (deg)')
        errorB = np.linalg.norm(ImodelB - IexpB) / (
            cal_dict["N_measures_1D"] * np.max(IexpB))
    else:
        errorB = 0
    if IexpC is not None:
        # Plot results, 1D
        plot_experiment_residuals_2D(
            cal_dict["angles_2X"],
            cal_dict["angles_2Y"],
            IexpC,
            ImodelC,
            title='Step 5b: R2 and P2',
            xlabel='Angle R2 (deg)',
            ylabel='Angle P2 (deg)')
        errorC = np.linalg.norm(ImodelC - IexpC) / (
            cal_dict["N_measures_1D"] * np.max(IexpC))
    else:
        errorC = 0
    # Calculations
    th0_r1_add = cal_dict["err_amp"] * erf(par1[3])
    # Print results
    print('Errors:')
    print('   - Rotating R1 and R2  : {:.3f} %'.format(errorA * 100))
    print('   - Rotating R1 and P2  : {:.3f} %'.format(errorB * 100))
    print('   - Rotating R2 and P2  : {:.3f} %'.format(errorC * 100))
    print('Analysis first iteration:')
    print('   - R1 p1               : {:.3f}'.format(par1[0]))
    print('   - R1 p2               : {:.3f}'.format(par1[1]))
    print('   - R1 retardance       : {:.1f} deg'.format(par1[2] / degrees))
    print('   - R1 extra theta_0    : {:.1f} deg'.format(th0_r1_add / degrees))
    # Extract info
    Mr1 = Mueller()
    Mr1.diattenuator_retarder_linear(
        p1=par1[0],
        p2=par1[1],
        D=par1[2],
        angle=th0_r1_add + cal_dict["th0_r1_test"])
    # Save data

    filename = "Step_5b_{}".format(datetime.date.today())
    np.savez(
        filename + '.npz',
        angles2x=cal_dict["angles_2X"],
        angles2y=cal_dict["angles_2Y"],
        IexpA=IexpA,
        IexpB=IexpB,
        IexpC=IexpC,
        fit=Mr1,
        dict_param=set_cal_dict(cal_dict))
    # Output
    return IexpA, IexpB, IexpC, Mr1


def make_step_6a(cal_dict):
    """Function that performs a polarimetry experiment with air."""
    # Adapt the dictionary of parameters for the analysis method
    cal_dict["fit completed"] = False
    cal_dict['limits'] = np.array([180, 180, 180, 180]) * degrees
    cal_dict['Nlimit'] = [0, 0, 0, 0]
    cal_dict['type'] = 'calibration'
    cal_dict['type_angles'] = 'random'
    cal_dict['mode_motors'] = 'absolute'
    cal_dict['Nmeasurements'] = cal_dict["N_measures_pol"]
    cal_dict['plot_intensity'] = True
    cal_dict['is_vacuum'] = True
    cal_dict['meas_name'] = 'Step_6a'
    # Perform the analysis
    cal_dict, Mfiltered, results_dict, angles, Iexp = Make_Measurement_0D(
        cal_dict, type_output='all')

    return angles, Iexp


def make_step_6a_independent(cal_dict, polarimeter=None):
    """Function that performs a polarimetry experiment with air."""
    # Create the polarimeter data from a fit
    if polarimeter is None:
        polarimeter = {}
        polarimeter["p11"] = 0.988
        polarimeter["p12"] = 0.058
        polarimeter["Dp1"] = 63.2 * degrees
        polarimeter["p21"] = 0.984
        polarimeter["p22"] = 0.075
        polarimeter["th0p1"] = -61.0 * degrees
        polarimeter["th0p2"] = 88.7 * degrees
        polarimeter["R1p1"] = 0.993
        polarimeter["R1p2"] = 0.990
        polarimeter["R2p1"] = np.abs(pes[-1, 10])
        polarimeter["R2p2"] = np.abs(pes[-1, 11])
        polarimeter["Dr1"] = delays[-1, 0]
        polarimeter["Dr2"] = 82.7
        polarimeter["th0r1"] = 101.2 * degrees
        polarimeter["th0r2"] = 58.7 * degrees
        polarimeter["S0"] = 5.827
        polarimeter["Saz"] = 66.5 * degrees
        polarimeter["Sel"] = 47.5 * degrees
        polarimeter["Spol"] = 0.993

        Ifuente = Stokes()
        Ifuente.general_azimuth_ellipticity(
            az=polarimeter["Saz"],
            el=polarimeter["Sel"],
            intensity=polarimeter["S0"],
            pol_degree=polarimeter["Spol"])
        Mp1 = Mueller()
        Mp1.diattenuator_retarder_linear(
            p1=polarimeter["p11"],
            p2=polarimeter["p12"],
            D=polarimeter["Dp1"],
            angle=polarimeter["th0p1"])
        Mp2 = Mueller()
        Mp2.diattenuator_linear(
            p1=polarimeter["p21"],
            p2=polarimeter["p22"],
            angle=polarimeter["th0p2"])
        Mr1 = Mueller()
        Mr1.diattenuator_retarder_linear(
            p1=polarimeter["R1p1"],
            p2=polarimeter["R1p2"],
            D=polarimeter["Dr1"],
            angle=polarimeter["th0r1"])
        Mr2 = Mueller()
        Mr1.diattenuator_retarder_linear(
            p1=polarimeter["R2p1"],
            p2=polarimeter["R2p2"],
            D=polarimeter["Dr2"],
            angle=polarimeter["th0r2"])

        polarimeter["Ifuente"] = Ifuente
        polarimeter["Mp1"] = Mp1
        polarimeter["Mp2"] = Mp2
        polarimeter["Mr1"] = Mr1
        polarimeter["Mr2"] = Mr2
        polarimeter["Mbefore"] = None
        polarimeter["Mafter"] = None
    # Adapt the dictionary of parameters for the analysis method
    cal_dict["fit completed"] = False
    cal_dict['limits'] = np.array([180, 180, 180, 180]) * degrees
    cal_dict['Nlimit'] = [0, 0, 0, 0]
    cal_dict['type'] = 'calibration'
    cal_dict['type_angles'] = 'random'
    cal_dict['mode_motors'] = 'absolute'
    cal_dict['Nmeasurements'] = cal_dict["N_measures_pol"]
    cal_dict['plot_intensity'] = make_plots
    cal_dict['is_vacuum'] = True
    cal_dict['meas_name'] = None
    # Perform the analysis
    cal_dict, Mfiltered, results_dict, angles, Iexp = Make_Measurement_0D(
        cal_dict, type_output='all')
    # Prints
    print("Step 6a result:")
    print("  - Normalization factor      = {:.4f}".format(
        results_dict['normalization']))
    print("  - Error                     = {:.3f} %".format(
        results_dict['error_norm'] * 100))
    print("   - Normalized Mueller matrix:")
    print(Mfiltered)
    # Plots
    I_fit = model_polarimeter_data(M, Ifuente, cal_dict['angles_polarimeter'],
                                   cal_dict['intensity_polarimeter'])
    diff = (cal_dict['intensity_polarimeter'] -
            I_fit) / cal_dict['intensity_polarimeter'].max()
    plt.figure(figsize=(16, 8))
    plt.subplot(2, 2, 1)
    plt.plot(cal_dict['intensity_polarimeter'], 'b')
    plt.plot(I_fit, 'k')
    plt.subplot(2, 2, 2)
    plt.plot(diff, 'b')
    plt.figure(figsize=(16, 8))
    plt.hist(diff)

    return angles, Iexp


def model_step_6a_angles(par, cal_dict):
    """Function used to optimize the final reference angles of elements."""
    # Rotate elements
    cal_dict2 = deepcopy(cal_dict)
    for ind, angle in enumerate(par):
        if ind < 3:
            cal_dict2["M"][ind].rotate(angle)
        else:
            cal_dict2["Ifuente"].rotate(angle)
    # Make the measurement. Sometimes the algorithm fails
    try:
        cal_dict, Mfiltered, results_dict = Analysis_Measurement_0D(
            cal_dict2, type_output='all', verbose=False, decompose=False)
    except:
        cal_dict = None
        results_dict = None
        Mfiltered = Mueller()
        Mfiltered.vacuum()
        Mfiltered = Mfiltered * 10
    return cal_dict, Mfiltered, results_dict


def error_step_6a_angles(par, cal_dict, target=air_flatten):
    cal_dict, Mfiltered, results_dict = model_step_6a_angles(par, cal_dict)
    dif = np.array(Mfiltered.M.flatten() - target, dtype=float)
    return dif[0]


def model_step_6a_all(par_opt, par, S0, cal_dict):
    """Function used to optimize the final reference angles of elements."""
    # Create the new corrected elements
    Ifuente = Stokes()
    Ifuente.general_azimuth_ellipticity(
        intensity=S0,
        az=par[0] + par_opt[0],
        el=par[1] + par_opt[1],
        pol_degree=par[2] + par_opt[2])
    Mp1 = Mueller()
    Mp1.diattenuator_retarder_linear(
        p1=par[3] + par_opt[3],
        p2=par[4] + par_opt[4],
        D=par[5] + par_opt[5],
        angle=par[7] + par_opt[6])
    Mr1 = Mueller()
    Mr1.diattenuator_retarder_linear(
        p1=par[8] + par_opt[7],
        p2=par[9] + par_opt[8],
        D=par[10] + par_opt[9],
        angle=par[11] + par_opt[10])
    Mr2 = Mueller()
    Mr2.diattenuator_retarder_linear(
        p1=par[12] + par_opt[11],
        p2=par[13] + par_opt[12],
        D=par[14] + par_opt[13],
        angle=par[16] + par_opt[14])
    Mp2 = Mueller()
    Mp2.diattenuator_linear(
        p1=par[17] + par_opt[15],
        p2=par[18] + par_opt[16],
        angle=par[19] + +par_opt[17])
    cal_dict2 = deepcopy(cal_dict)
    cal_dict2['M'] = [Mp1, Mr1, Mr2, Mp2]
    cal_dict2['Ifuente'] = Ifuente
    # Make the measurement. Sometimes the algorithm fails
    try:
        cal_dict, Mfiltered, results_dict = Analysis_Measurement_0D(
            cal_dict2, type_output='all', verbose=False, decompose=False)
    except:
        cal_dict = None
        results_dict = None
        Mfiltered = Mueller()
        Mfiltered.vacuum()
        Mfiltered = Mfiltered * 10
    return cal_dict, Mfiltered, results_dict


def error_step_6a_all(par_opt, par, S0, cal_dict, target=air_flatten):
    cal_dict, Mfiltered, results_dict = model_step_6a_all(
        par_opt, par, S0, cal_dict)
    dif = np.array(Mfiltered.M.flatten() - target)
    return dif[0]


def make_check(cal_dict):
    """Function that performs a polarimetry experiment with air."""
    N = 61
    angles = np.linspace(0, 180, N) * degrees
    M = cal_dict['M_test']
    Ifuente = cal_dict['Ifuente_test']
    # Make the measurement
    Iexp = np.zeros([N, 2])
    Imodel = np.zeros(N)
    for ind, angle in enumerate(angles):
        angles_ind = np.array([angle, 0, 0, 0])
        # angles_ind = np.random.rand(4) * np.pi
        Iexp[ind, :] = go_and_measure_0D(
            cal_dict['equipment'], theta=angles_ind, I0=cal_dict['I0'])
        Imodel[ind] = Intensity_Rotating_Elements(M, angles_ind, Ei=Ifuente)
    Iexp, _ = correct_intensity_0D(Iexp, cal_dict['mean_ref'])

    # Plots
    diff = (Iexp - Imodel) / Iexp.max()
    plt.figure(figsize=(16, 8))
    plt.subplot(2, 2, 1)
    plt.plot(Iexp, 'b')
    plt.plot(Imodel, 'k')
    plt.subplot(2, 2, 2)
    plt.plot(diff, 'b')
    # plt.figure(figsize=(16, 8))
    # plt.hist(diff)


def analysis_check(cal_dict):
    """Function that checks that the stored model and the experimental data are consistent."""
    Ifuente = cal_dict['Ifuente_test']
    # Step 3c
    Iexp = cal_dict['I_step_3c']
    M = [cal_dict['Mp1_test'], cal_dict['Mp2_test']]
    angles = [0, cal_dict['angles_1']]
    Imodel = Intensity_Rotating_Elements(M, angles, Ei=Ifuente)
    plot_experiment_residuals_1D(
        cal_dict['angles_1'],
        Iexp,
        Imodel,
        title='Step 3',
        xlabel='Angle 2nd pol. (deg)')
    # Step 4a
    try:
        Iexp = cal_dict['I_step_4a']
    except:
        Iexp = cal_dict['I_Step_4a']
    M = [cal_dict['Mp1_test'], cal_dict['Mr2_test'], cal_dict['Mp2_test']]
    angles = [0, 0, cal_dict['angles_1']]
    Imodel = Intensity_Rotating_Elements(M, angles, Ei=Ifuente)
    plot_experiment_residuals_1D(
        cal_dict['angles_1'],
        Iexp,
        Imodel,
        title='Step 4a',
        xlabel='Angle 2nd pol. (deg)')
    # Step 4b
    try:
        Iexp = cal_dict['I_step_4b']
    except:
        Iexp = cal_dict['I_Step_4b']
    angles = [0, cal_dict['angles_2X_step_4b'], cal_dict['angles_2Y_step_4b']]
    Imodel = Intensity_Rotating_Elements(M, angles, Ei=Ifuente)
    plot_experiment_residuals_2D(
        cal_dict["angles_2X_step_4b"],
        cal_dict["angles_2Y_step_4b"],
        Iexp,
        Imodel,
        title='Step 4b',
        xlabel='Angle R2 (deg)',
        ylabel='Angle P2 (deg)')
    # Step 5a
    try:
        Iexp = cal_dict['Ic_step_5a']
    except:
        Iexp = cal_dict['Ic_Step_5a']
    M = [
        cal_dict['Mp1_test'], cal_dict['Mr1_test'], cal_dict['Mr2_test'],
        cal_dict['Mp2_test']
    ]
    angles = [0, 0, 0, cal_dict['angles_1']]
    Imodel = Intensity_Rotating_Elements(M, angles, Ei=Ifuente)
    plot_experiment_residuals_1D(
        cal_dict['angles_1'],
        Iexp,
        Imodel,
        title='Step 5a',
        xlabel='Angle 2nd pol. (deg)')
    # Step 5b
    try:
        Iexp = cal_dict['Ic_step_5b']
    except:
        Iexp = cal_dict['Ic_Step_5b']
    angles = [0, 0, cal_dict['angles_2X'], cal_dict['angles_2Y']]
    Imodel = Intensity_Rotating_Elements(M, angles, Ei=Ifuente)
    plot_experiment_residuals_2D(
        cal_dict["angles_2X"],
        cal_dict["angles_2Y"],
        Iexp,
        Imodel,
        title='Step 5b',
        xlabel='Angle R2 (deg)',
        ylabel='Angle P2 (deg)')


def make_step_6b(cal_dict):
    """Function to measure the mueller matrix of the polarizer with known axes (Step 10d)."""
    # Adapt the dictionary of parameters for the analysis method
    cal_dict["fit completed"] = False
    cal_dict['limits'] = np.array([180, 180, 180, 180]) * degrees
    cal_dict['Nlimit'] = [0, 0, 0, 0]
    cal_dict['type'] = 'calibration'
    cal_dict['type_angles'] = 'random'
    cal_dict['mode_motors'] = 'absolute'
    cal_dict['Nmeasurements'] = cal_dict["N_measures_pol"]
    cal_dict['plot_intensity'] = True
    cal_dict['is_vacuum'] = False
    cal_dict['meas_name'] = 'Step_6b'
    # Perform the analysis
    cal_dict, Mfiltered, results_dict, angles, Iexp = Make_Measurement_0D(
        cal_dict, type_output='all')
    # Print the angle
    print('The angle of the polarizer is: {:.1f} deg'.format(
        results_dict['azimuth pol'] / degrees))

    return angles, Iexp


def model_all_together(par,
                       th1,
                       thX,
                       thY,
                       thX4b,
                       thY4b,
                       S0,
                       mode1,
                       mode2,
                       Npar=None):
    """Function that models the whole calibration experiment together up to step 5."""
    # Create objects
    Ifuente = Stokes()
    Ifuente.general_azimuth_ellipticity(
        intensity=S0, az=par[0], el=par[1], pol_degree=par[2])
    Mp1b = Mueller()
    Mp1b.diattenuator_retarder_linear(
        p1=par[3], p2=par[4], D=par[5], angle=par[6])
    Mp1 = Mueller()
    Mp1.diattenuator_retarder_linear(
        p1=par[3], p2=par[4], D=par[5], angle=par[7])
    Mr1 = Mueller()
    Mr1.diattenuator_retarder_linear(
        p1=par[8], p2=par[9], D=par[10], angle=par[11])
    Mr2b = Mueller()
    Mr2b.diattenuator_retarder_linear(
        p1=par[12], p2=par[13], D=par[14], angle=par[15])
    Mr2 = Mueller()
    Mr2.diattenuator_retarder_linear(
        p1=par[12], p2=par[13], D=par[14], angle=par[16])
    Mp2 = Mueller()
    Mp2.diattenuator_linear(p1=par[17], p2=par[18], angle=par[19])
    Mp3 = Mueller()
    Mp3.diattenuator_linear(p1=par[20], p2=par[21], angle=par[22])
    Mc = Mueller()
    Mc.diattenuator_linear(p1=par[23], p2=par[24], angle=0)
    # Make the calculations
    if Npar is None or Npar in [0, 1, 2, 3, 4, 6, 23, 24]:
        M = [Mc, Mp1b]
        th = [0, th1]
        Im_step1 = Intensity_Rotating_Elements(M, th, Ei=Ifuente)
    else:
        Im_step1 = None

    if Npar is None or Npar in [0, 1, 2, 3, 4, 6, 12, 13, 14, 15, 23, 24]:
        M = [Mc, Mr2b, Mp1b]
        th = [0, 0, th1]
        Im_step2a = Intensity_Rotating_Elements(M, th, Ei=Ifuente)
    else:
        Im_step2a = None

    if Npar is None or Npar in [0, 1, 2, 3, 4, 6, 12, 13, 14, 15]:
        M = [Mr2b, Mp1b]
        th = [thX, thY]
        Im_step2b = Intensity_Rotating_Elements(M, th, Ei=Ifuente)
    else:
        Im_step2b = None

    if Npar is None or Npar in [0, 1, 2, 3, 4, 6, 20, 21, 22]:
        M = [Mp3, Mp1b]
        th = [0, th1]
        Im_step3a = Intensity_Rotating_Elements(M, th, Ei=Ifuente)
    else:
        Im_step3a = None

    if Npar is None or Npar in [0, 1, 2, 17, 18, 19, 20, 21, 22]:
        M = [Mp3, Mp2]
        th = [0, th1]
        Im_step3b = Intensity_Rotating_Elements(M, th, Ei=Ifuente)
    else:
        Im_step3b = None

    if Npar is None or Npar in [0, 1, 2, 3, 4, 7, 17, 18, 19]:
        M = [Mp1, Mp2]
        th = [0, th1]
        Im_step3c = Intensity_Rotating_Elements(M, th, Ei=Ifuente)
    else:
        Im_step3c = None

    if Npar is None or Npar in [
            0, 1, 2, 3, 4, 5, 7, 12, 13, 14, 16, 17, 18, 19
    ]:
        M = [Mp1, Mr2, Mp2]
        th = [0, 0, th1]
        Im_step4a = Intensity_Rotating_Elements(M, th, Ifuente, False)
        th = [0, thX4b, thY4b]
        Im_step4b = Intensity_Rotating_Elements(M, th, Ifuente, False)
    else:
        Im_step4a = None
        Im_step4b = None

    if Npar is None or Npar in [
            0, 1, 2, 3, 4, 5, 7, 8, 9, 10, 11, 12, 13, 14, 16, 17, 18, 19
    ]:
        M = [Mp1, Mr1, Mr2, Mp2]
        if mode1 in ('R1'):
            th = [0, th1, 0, 0]
            Im_step5a = Intensity_Rotating_Elements(M, th, Ifuente, False)
        elif mode1 in ('R2'):
            th = [0, 0, th1, 0]
            Im_step5a = Intensity_Rotating_Elements(M, th, Ifuente, False)
        elif mode1 in ('P2'):
            th = [0, 0, 0, th1]
            Im_step5a = Intensity_Rotating_Elements(M, th, Ifuente, False)
        else:
            th = [0, th1, 0, 0]
            Im_step5aa = Intensity_Rotating_Elements(M, th, Ifuente, False)
            th = [0, 0, th1, 0]
            Im_step5ab = Intensity_Rotating_Elements(M, th, Ifuente, False)
            th = [0, 0, 0, th1]
            Im_step5ac = Intensity_Rotating_Elements(M, th, Ifuente, False)
            Im_step5a = (Im_step5aa, Im_step5ab, Im_step5ac)
        if mode2 in ('R1+R2', 'R1 R2'):
            th = [0, thX, thY, 0]
            Im_step5b = Intensity_Rotating_Elements(M, th, Ifuente, False)
        elif mode2 in ('R1+P2', 'R1 P2'):
            th = [0, thX, 0, thY]
            Im_step5b = Intensity_Rotating_Elements(M, th, Ifuente, False)
        elif mode2 in ('R2+P2', 'R2 P2'):
            th = [0, 0, thX, thY]
            Im_step5b = Intensity_Rotating_Elements(M, th, Ifuente, False)
        else:
            th = [0, thX, thY, 0]
            Im_step5ba = Intensity_Rotating_Elements(M, th, Ifuente, False)
            th = [0, thX, 0, thY]
            Im_step5bb = Intensity_Rotating_Elements(M, th, Ifuente, False)
            th = [0, 0, thX, thY]
            Im_step5bc = Intensity_Rotating_Elements(M, th, Ifuente, False)
            Im_step5b = (Im_step5ba, Im_step5bb, Im_step5bc)
    else:
        Im_step5a = None
        Im_step5b = None

    # Return
    return Im_step1, Im_step2a, Im_step2b, Im_step3a, Im_step3b, Im_step3c, Im_step4a, Im_step4b, Im_step5a, Im_step5b


def error_all_together(par,
                       th1,
                       thX,
                       thY,
                       thX4b,
                       thY4b,
                       S0,
                       mode1,
                       mode2,
                       Iexp,
                       Npar=None):
    # Create zeros variables
    zeros_1D = np.zeros(th1.size)
    zeros_2D = np.zeros([thX.size, thY.size])
    zeros_2D_4b = np.zeros([thX4b.size, thY4b.size])
    # Calculate the model intensities
    Im = model_all_together(par, th1, thX, thY, thX4b, thY4b, S0, mode1, mode2,
                            Npar)
    # Calculate the difference respect the experimental values
    dif = [
        zeros_1D, zeros_1D, zeros_2D, zeros_1D, zeros_1D, zeros_1D, zeros_1D,
        zeros_2D_4b, zeros_1D, zeros_2D
    ]
    for ind, elem in enumerate(Im):
        if elem is None or Iexp[ind] is None:
            pass
        else:
            dif[ind] = elem - Iexp[ind]
    # Concatenate the arrays
    dif_final = np.empty(0)
    for elem in dif:
        # print('dif_final', dif_final)
        # print('elem', elem.flatten())
        dif_final = np.concatenate((dif_final, elem.flatten()))
    return dif_final


def jacobian_all_together(par, th1, thX, thY, thX4b, thY4b, S0, mode1, mode2,
                          Iexp):
    """Fast jacobian for error_all_together function"""
    # Method variables
    step = 1e-8
    # Measure the initial point
    dI0 = error_all_together(par, th1, thX, thY, thX4b, thY4b, S0, mode1,
                             mode2, Iexp)
    # Preallocate memory
    M = dI0.size
    # print('Size of dI0', M)
    N = len(par)
    J = np.empty([M, N])
    # Calculate Jacobian
    for ind in range(N):
        par1 = par
        par1[ind] = par[ind] + step
        dI1 = error_all_together(
            par, th1, thX, thY, thX4b, thY4b, S0, mode1, mode2, Iexp, Npar=ind)
        # print('Size of dI1', dI1.size)
        aux = np.transpose((dI1 - dI0) / step)
        J[:, ind] = aux
    return J


def save_all_together(cal_dict):
    """Function to save the dictionary of the calibration, which contains all the interesting parameters."""
    filename = "All_together_{}".format(datetime.date.today())
    np.savez(filename + '.npz', parameters=set_cal_dict(cal_dict))


def Process_Calibration(cal_dict, seed=None):

    # If seed is None, use default one
    if seed is None:
        seed = np.zeros(9)
        use_seed = False
    else:
        use_seed = True
        cal_dict["N_it_while"] = 1
    date = "{}".format(datetime.date.today())

    # Initial parameters
    (p11, p12, Dp1, p21, p22, p31, p32, pc1,
     pc2) = (0.975, 0.05, 90 * degrees, 0.975, 0.05, 0.975, 0.05, 0.975, 0.3)
    (R2p1, R2p2, Dr2) = (0.975, 0.975, 83 * degrees)
    (R1p1, R1p2, Dr1, th0r1) = (0.975, 0.975, 83 * degrees, 0)
    (Saz, Sel, Spol) = (0, -42 * degrees, 0.975)
    (th0p1b, th0p1, th0p2, th0p3, th0r2, th0r2b) = (0, 0, 0, 0, 0, 0)
    try:
        S0 = cal_dict['S0']
    except:
        S0 = 5.9227

    # Randomize initial parameters
    try:
        seed2 = cal_dict['seed2']
        rand_amp = cal_dict['rand_amp']
    except:
        seed2 = 2 * np.random.rand(17) - 1
        rand_amp = [
            3 * degrees, 0.025, 0.025, 0.025, 3 * degrees, 0.025, 0.025,
            3 * degrees, 0.025, 0.025, 3 * degrees, 0.025, 0.025, 0.025, 0.025,
            0.025, 0.025
        ]
    # Apply the random changes
    Sel = Sel + rand_amp[0] * seed2[0]
    Spol = Spol + rand_amp[1] * seed2[1]
    p11 = p11 + rand_amp[2] * seed2[2]
    p12 = p12 + rand_amp[3] * seed2[3]
    Dp1 = Dp1 + rand_amp[4] * seed2[4]
    R1p1 = R1p1 + rand_amp[5] * seed2[5]
    R1p2 = R1p2 + rand_amp[6] * seed2[6]
    Dr1 = Dr1 + rand_amp[7] * seed2[7]
    R2p1 = R2p1 + rand_amp[8] * seed2[8]
    R2p2 = R2p2 + rand_amp[9] * seed2[9]
    Dr2 = Dr2 + rand_amp[10] * seed2[10]
    p21 = p21 + rand_amp[11] * seed2[11]
    p22 = p22 + rand_amp[12] * seed2[12]
    p31 = p31 + rand_amp[13] * seed2[13]
    p32 = p32 + rand_amp[14] * seed2[14]
    pc1 = pc1 + rand_amp[15] * seed2[15]
    pc2 = pc2 + rand_amp[16] * seed2[16]

    # Polarimeter measurements
    Mbefore = Mueller()
    Mbefore.vacuum()
    cal_dict['Mbefore'] = Mbefore
    cal_dict['Mafter'] = Mbefore
    cal_dict['is_vacuum'] = True
    cal_dict['type'] = 'Calibration'
    cal_dict["fit completed"] = True
    cal_dict["odd_number_refs"] = False
    cal_dict["save_analysis"] = False

    # Mueller and Stokes objects
    Mp1 = Mueller('Polarizer 1')
    Mp2 = Mueller('Polarizer 2')
    Mr1 = Mueller('Retarder 1')
    Mr2 = Mueller('Retarder 2')
    Ifuente = Stokes('Illumination')
    # Error related parameters
    error_amplitude_vect = np.logspace(
        np.log10(cal_dict['ErrorAmpIni']), np.log10(cal_dict['ErrorAmpFin']),
        cal_dict["NmaxIt"])
    # error_amplitude_ellip_vect = np.logspace(
    #     np.log10(cal_dict['ErrorEllipAmpIni']),
    #     np.log10(cal_dict['ErrorEllipAmpFin']), cal_dict["NmaxIt"])
    th0E0 = 0
    # Extract angles for easiness
    angles_1 = cal_dict["angles_1"]
    angles_2x = cal_dict["angles_2X"]
    angles_2y = cal_dict["angles_2Y"]
    angles_2x_s4b = cal_dict["angles_2X_step_4b"]
    angles_2y_s4b = cal_dict["angles_2Y_step_4b"]
    angles_1_plot3 = np.concatenate((angles_1, angles_1 + np.pi,
                                     angles_1 + np.pi * 2))
    # Initialize data storage
    errores = np.zeros([cal_dict["NmaxIt"], 8])
    pes = np.zeros([cal_dict["NmaxIt"], 12])
    ilums = np.zeros([cal_dict["NmaxIt"], 4])
    delays = np.zeros([cal_dict["NmaxIt"], 4])
    angles = np.zeros([cal_dict["NmaxIt"], 7])
    error_angles = np.zeros([cal_dict["NmaxIt"], 7])
    Nelements1D = cal_dict["N_measures_1D"]
    Nelements2D = np.prod(cal_dict["N_measures_2D"])
    Nelements2D_s4b = np.prod(cal_dict["N_measures_2D_step_4b"])

    # Interpolated data
    if cal_dict["interpolate_first_it"]:
        angles_2x_interp, angles_2y_interp, I4b_interp = take_intermediate_points_2D(
            angles_2x_s4b, angles_2y_s4b, cal_dict["I_step_4b"], [2, 2])

    # Main iteration loop
    start_time = time.time()
    for indIt, error_amplitude in enumerate(error_amplitude_vect):
        # Start iteration operations
        print('Iteracion {}'.format(indIt))
        print('Angle error amplitude for current iteration = {}'.format(
            error_amplitude / degrees))
        make_plots = cal_dict["PlotCadaPaso"] or indIt + \
            1 == cal_dict["NmaxIt"]

        # Fit first angle of P1 in Step 1
        (counter, error_step, error_best) = (0, 1, 1)
        # Initial objects
        Mp1.diattenuator_linear(p1=p11, p2=p12, angle=0)
        Ifuente.general_azimuth_ellipticity(
            az=Saz, el=Sel, intensity=S0, pol_degree=Spol)
        # Extract intensity
        Iexperimental = cal_dict["I_step_1"]
        # Limits
        min_limit = [0, 0, 0]
        max_limit = [1, 1, 180 * degrees]
        # Random loop
        while counter < cal_dict["N_it_while"] and error_step > cal_dict["tol_while"]:
            # Initialize angle in first iteration
            if indIt == 0 or cal_dict["N_it_while"] > 1:
                if use_seed:
                    th0p1b = seed[0]
                elif cal_dict["use_random_angles"]:
                    th0p1b = np.random.rand(1) * np.pi
                    th0p1b = th0p1b[0]
            # Initial parameters
            par0 = [pc1, pc2, th0p1b]
            # Ajuste
            result = optimize.least_squares(
                error_step_1,
                par0,
                args=(angles_1, Mp1, Ifuente, Iexperimental),
                bounds=(min_limit, max_limit),
                jac=cal_dict["jacobian"],
                method=cal_dict["fit_function_method"])
            par1 = result.x
            # Calculo de errores
            error_step = error_step_1(par1, angles_1, Mp1, Ifuente,
                                      Iexperimental)
            error_step = np.linalg.norm(error_step) / (
                Iexperimental.max() * Nelements1D)
            # Store best results
            if error_step < error_best:
                par_best = par1
                error_best = error_step
                # Save random seed
                if indIt == 0 or cal_dict["N_it_while"] > 1:
                    seed[0] = th0p1b
            # While loop end operations
            counter = counter + 1
            if cal_dict["PlotCadaPaso"] and cal_dict["N_it_while"] > 1:
                print('Subiteration {} has error {:.3f} %.'.format(
                    counter, error_step * 100))
        # Store results
        pc1, pc2 = par_best[0:2]
        th0p1b = par_best[2] % np.pi
        (pes[indIt, 8], pes[indIt, 9]) = (pc1, pc2)
        angles[indIt, 4] = th0p1b
        errores[indIt, 0] = error_best
        # Plots
        if make_plots:
            print("Step 1 parameters:")
            print("  - Known polarizer p1    = {:.3f};".format(pc1))
            print("  - Known polarizer p2    = {:.3f};".format(pc2))
            print("  - P1 initial angle      = {:.1f} deg;".format(
                th0p1b / degrees))
            I_fit = model_step_1(par_best, angles_1, Mp1, Ifuente)
            plot_experiment_residuals_1D(
                angles_1, Iexperimental, I_fit, title='Step 1')
        # Update variables
        Mp1.rotate(th0p1b)
        Mpmalo = Mueller()
        Mpmalo.diattenuator_linear(pc1, pc2, angle=0)

        # Step 2
        # Extract intensity
        Iexp1 = cal_dict["I_step_2a"]
        Iexp2 = cal_dict["I_step_2b"]
        Imax = max(Iexp1.max(), Iexp2.max())
        # Limits
        min_limit = [0, 0, -45 * degrees, 0, 0]
        max_limit = [1, 180 * degrees, 45 * degrees, 1, 180 * degrees]
        # While loop
        (counter, error_step, error_best) = (0, 1, 1)
        while counter < cal_dict["N_it_while"] and error_step > cal_dict["tol_while"]:
            # Initialize angle in first iteration
            if indIt == 0 or cal_dict["N_it_while"] > 1:
                if use_seed:
                    (th0r2b, Saz) = seed[1:3]
                elif cal_dict["use_random_angles"]:
                    (th0r2b, Saz) = np.random.rand(2) * np.pi
            # Initial parameters
            par0 = [R2p1, Saz, Sel, Spol, th0r2b]
            # Fit
            # print('min_limit', min_limit)
            # print('max_limit', max_limit)
            # print('par0', par0)
            result = optimize.least_squares(
                error_step_2,
                par0,
                args=(angles_1, angles_2x, angles_2y, Mp1, Mpmalo, S0, Dr2,
                      Iexp1, Iexp2),
                bounds=(min_limit, max_limit),
                jac=cal_dict["jacobian"],
                method=cal_dict["fit_function_method"])
            par1 = result.x
            # Errors calculation
            error_step = error_step_2(par1, angles_1, angles_2x, angles_2y,
                                      Mp1, Mpmalo, S0, Dr2, Iexp1, Iexp2)
            error_step = np.linalg.norm(error_step) / (
                Imax * (Nelements1D + Nelements2D))
            # Store best results
            if error_step < error_best:
                par_best = par1
                error_best = error_step
                # Save random seed
                if indIt == 0 or cal_dict["N_it_while"] > 1:
                    seed[1] = th0r2b
            # While loop end operations
            counter = counter + 1
            if cal_dict["PlotCadaPaso"] and cal_dict["N_it_while"] > 1:
                print('Subiteration {} has error {:.3f} %.'.format(
                    counter, error_step * 100))
        # Extract results for the following iterations
        (R2p1, Saz, Sel, Spol) = par_best[0:4]
        th0r2b = par_best[4]
        # Store results
        angles[indIt, 5] = th0r2b
        ilums[indIt, 0:4] = (S0, Saz, Sel, Spol)
        errores[indIt, 1] = error_best
        # Plots
        if make_plots:
            print("Step 2 parameters:")
            print("  - R2 initial angle      = {:.1f} deg;".format(
                th0r2b / degrees))
            print("  - Light intensity         = {:.3f} V;".format(S0))
            print("  - Light azimuth           = {:.1f} deg;".format(
                Saz / degrees))
            print("  - Light ellipticity       = {:.1f} deg;".format(
                Sel / degrees))
            print("  - Light pol degree        = {:.3f};".format(Spol))
            print("  - Stokes vector of illumination:")
            print(Ifuente)
            I1_fit, I2_fit = model_step_2(par_best, angles_1, angles_2x,
                                          angles_2y, Mp1, Mpmalo, S0, Dr2)
            plot_experiment_residuals_1D(
                angles_1, Iexp1, I1_fit, title='Step 2a')
            plot_experiment_residuals_2D(
                cal_dict["angles_2X"],
                cal_dict["angles_2Y"],
                Iexp2,
                I2_fit,
                title='Step 2b',
                xlabel='Angle R2 (deg)',
                ylabel='Angle P1 (deg)')
        # Update variables
        Ifuente.general_azimuth_ellipticity(
            az=Saz, el=Sel, intensity=S0, pol_degree=Spol)
        cal_dict['Ifuente'] = Ifuente

        # # Now the same with retarder 2 (Step 2a)
        # # Extract intensity
        # Iexperimental = cal_dict["I_step_2a"]
        # # Limits
        # min_limit = [0, 0]
        # max_limit = [1, 180 * degrees]
        # # While loop
        # (counter, error_step, error_best) = (0, 1, 1)
        # while counter < cal_dict["N_it_while"] and error_step > cal_dict["tol_while"]:
        #     # Initialize angle in first iteration
        #     if indIt == 0 or cal_dict["N_it_while"] > 1:
        #         if use_seed:
        #             th0r2b = seed[1]
        #         elif cal_dict["use_random_angles"]:
        #             th0r2b = np.random.rand(1) * np.pi
        #             th0r2b = th0r2b[0]
        #     # Initial parameters
        #     par0 = [R2p1, th0r2b]
        #     # Fit
        #     result = optimize.least_squares(
        #         error_step_2a,
        #         par0,
        #         args=(angles_1, Mp1, Mpmalo, Dr2, Ifuente, Iexperimental),
        #         bounds=(min_limit, max_limit),
        #         jac=cal_dict["jacobian"],
        #         method=cal_dict["fit_function_method"])
        #     par1 = result.x
        #     # Errors calculation
        #     error_step = error_step_2a(par1, angles_1, Mp1, Mpmalo, Dr2,
        #                                Ifuente, Iexperimental)
        #     error_step = np.linalg.norm(error_step) / (
        #         Iexperimental.max() * cal_dict["N_measures_1D"])
        #     # Store best results
        #     if error_step < error_best:
        #         par_best = par1
        #         error_best = error_step
        #         # Save random seed
        #         if indIt == 0 or cal_dict["N_it_while"] > 1:
        #             seed[1] = th0r2b
        #     # While loop end operations
        #     counter = counter + 1
        #     if cal_dict["PlotCadaPaso"] and cal_dict["N_it_while"] > 1:
        #         print('Subiteration {} has error {:.3f} %.'.format(
        #             counter, error_step * 100))
        # # Extract results for the following iterations
        # R2p1 = par_best[0]
        # th0r2b = par_best[1] % np.pi
        # # Store results
        # angles[indIt, 5] = th0r2b
        # errores[indIt, 1] = error_best
        # # Plots
        # if cal_dict["PlotCadaPaso"] or indIt + 1 == cal_dict["NmaxIt"]:
        #     print("Step 2a parameters:")
        #     print("  - R2 initial angle      = {:.1f} deg;".format(
        #         th0r2b / degrees))
        #     print("  - R2 guess p12          = {:.3f};".format(R2p1))
        #     I_fit = model_step_2a(par_best, angles_1, Mp1, Mpmalo, Dr2,
        #                           Ifuente)
        #     plot_experiment_residuals_1D(
        #         angles_1, Iexperimental, I_fit, title='Step 2a')
        # # Update variables
        # Mr2.diattenuator_retarder_linear(p1=R2p1, p2=R2p2, D=Dr2, angle=th0r2b)
        #
        # # Fit illumination (Step 2b)
        # # Extract intensity
        # Iexperimental = cal_dict["I_step_2b"]
        # # Limits
        # min_limit = [0, -45 * degrees, 0, -error_amplitude]
        # max_limit = [180 * degrees, 45 * degrees, 1, error_amplitude]
        # # While loop
        # (counter, error_step, error_best) = (0, 1, 1)
        # while counter < cal_dict["N_it_while"] and error_step > cal_dict["tol_while"]:
        #     # Initialize angle in first iteration
        #     if indIt == 0 or cal_dict["N_it_while"] > 1:
        #         if use_seed:
        #             Saz = seed[2]
        #         elif cal_dict["use_random_angles"]:
        #             Saz = np.random.rand(1) * np.pi
        #             Saz = Saz[0]
        #     # Initial parameters
        #     par0 = [Saz, Sel, Spol, th0E0]
        #     # Fit
        #     result = optimize.least_squares(
        #         error_step_2b,
        #         par0,
        #         args=(angles_2x, angles_2y, S0, Mp1, Mr2, Iexperimental),
        #         bounds=(min_limit, max_limit),
        #         jac=cal_dict["jacobian"],
        #         method=cal_dict["fit_function_method"])
        #     par1 = result.x
        #     # Errors calculation
        #     error_step = error_step_2b(par1, angles_2x, angles_2y, S0, Mp1,
        #                                Mr2, Iexperimental)
        #     error_step = np.linalg.norm(error_step) / (
        #         Iexperimental.max() * Nelements2D)
        #     # Store best results
        #     if error_step < error_best:
        #         par_best = par1
        #         error_best = error_step
        #         # Save random seed
        #         if indIt == 0 or cal_dict["N_it_while"] > 1:
        #             seed[2] = Saz
        #     # While loop end operations
        #     counter = counter + 1
        #     if cal_dict["PlotCadaPaso"] and cal_dict["N_it_while"] > 1:
        #         print('Subiteration {} has error {:.3f} %.'.format(
        #             counter, error_step * 100))
        # # Extract results for the following iterations
        # (Saz, Sel, Spol) = par_best[0:3]
        #
        # if Spol > 1:
        #     Spol = 1
        # th_error = par_best[3]
        # # Update variables
        # Ifuente.general_azimuth_ellipticity(
        #     az=Saz, el=Sel, intensity=S0, pol_degree=Spol)
        # par_best[0:3] = (Saz, Sel, Spol)
        # cal_dict['Ifuente'] = Ifuente
        # if cal_dict["fix_parameters"]:
        #     th0r2b = th0r2b + th_error
        # # Save results
        # ilums[indIt, 0:4] = (S0, Saz, Sel, Spol)
        # error_angles[indIt, 0] = th_error
        # errores[indIt, 2] = error_best
        # # Print info
        # if cal_dict["PlotCadaPaso"] or indIt + 1 == cal_dict["NmaxIt"]:
        #     # Info
        #     print("Step 2b parameters:")
        #     print("  - Light intensity         = {:.3f} V;".format(S0))
        #     print("  - Light azimuth           = {:.1f} deg;".format(
        #         Saz / degrees))
        #     print("  - Light ellipticity       = {:.1f} deg;".format(
        #         Sel / degrees))
        #     print("  - Polarization degree     = {:.3f};".format(Spol))
        #     print("  - Ref. angle correction   = {:.3f} deg;".format(
        #         th_error / degrees))
        #     print("  - Stokes vector of illumination:")
        #     print(Ifuente)
        #     # Plots
        #     I_fit = model_step_2b(par1, angles_2x, angles_2y, S0, Mp1, Mr2)
        #     plot_experiment_residuals_2D(
        #         cal_dict["angles_2X"],
        #         cal_dict["angles_2Y"],
        #         Iexperimental,
        #         I_fit,
        #         title='Step 2b',
        #         xlabel='Angle R2 (deg)',
        #         ylabel='Angle P1 (deg)')

        # Now, lets fit the polarizers (Step 3)
        # Load experimental intensity
        IexpA = cal_dict["I_step_3a"]
        IexpB = cal_dict["I_step_3b"]
        IexpC = cal_dict["I_step_3c"]
        Iexperimental = np.concatenate((IexpA, IexpB, IexpC))
        # Limits
        min_limit = [0, 0, 0, 0, 0, 0, 0, 0, 0]
        max_limit = [
            1, 1, 1, 1, 1, 1, 180 * degrees, 180 * degrees, 180 * degrees
        ]
        # While loop
        (counter, error_step, error_best) = (0, 1, 1)
        while counter < cal_dict["N_it_while"] and error_step > cal_dict["tol_while"]:
            # Initializwe angle for first iteration
            if indIt == 0 or cal_dict["N_it_while"] > 1:
                if use_seed:
                    (th0p1, th0p2, th0p3) = seed[3:6]
                elif cal_dict["use_random_angles"]:
                    (th0p1, th0p2, th0p3) = np.random.rand(3) * np.pi
            # Initial parameters
            par0 = [p11, p12, p21, p22, p31, p32, th0p1, th0p2, th0p3]
            # Fit
            result = optimize.least_squares(
                error_step_3,
                par0,
                args=(angles_1, Ifuente, th0p1b, IexpA, IexpB, IexpC),
                bounds=(min_limit, max_limit),
                jac=cal_dict["jacobian"],
                method=cal_dict["fit_function_method"])
            par1 = result.x
            # Error calculation
            error_step = error_step_3(par1, angles_1, Ifuente, th0p1b, IexpA,
                                      IexpB, IexpC)
            error_step = np.linalg.norm(error_step) / (
                Iexperimental.max() * 3 * Nelements1D)
            # Guardar mejores resultados
            if error_step < error_best:
                par_best = par1
                error_best = error_step
                # Save random seed
                if indIt == 0 or cal_dict["N_it_while"] > 1:
                    seed[3:6] = (th0p1, th0p2, th0p3)
            # Operaciones final de bucle
            counter = counter + 1
            if cal_dict["PlotCadaPaso"] and cal_dict["N_it_while"] > 1:
                print('Subiteration {} has error {:.3f} %.'.format(
                    counter, error_step * 100))
        # Extract results
        p11, p12, p21, p22, p31, p32 = par_best[0:6]
        (th0p1, th0p2, th0p3) = par_best[6:9]
        # Store results
        pes[indIt, 0:6] = (p11, p12, p21, p22, p31, p32)
        angles[indIt, 0:3] = (th0p1, th0p2, th0p3)
        errores[indIt, 3] = error_best
        # Info
        if make_plots:
            print("Step 3 parameters:")
            print("  - Polarizer 1:")
            print("    o P1         = {:.3f};".format(p11))
            print("    o P2         = {:.3f};".format(p12))
            print("    o Th0        = {:.1f} deg;".format(th0p1 / degrees))
            print("  - Polarizer 2:")
            print("    o P1         = {:.3f};".format(p21))
            print("    o P2         = {:.3f};".format(p22))
            print("    o Th0        = {:.1f} deg;".format(th0p2 / degrees))
            print("  - Polarizer 3:")
            print("    o P1         = {:.3f};".format(p31))
            print("    o P2         = {:.3f};".format(p32))
            print("    o Th0        = {:.1f} deg;".format(th0p3 / degrees))
            # print("  - Error angle  = {:.1f} deg;".format(th_error1 / degrees))
            # Plot
            IfitA, IfitB, IfitC = model_step_3(par_best, angles_1, Ifuente,
                                               th0p1b)
            I_fit = np.concatenate((IfitA, IfitB, IfitC))
            Iexperimental = np.concatenate((IexpA, IexpB, IexpC))
            plot_experiment_residuals_1D(
                angles_1_plot3, Iexperimental, I_fit, title='Step 3')
        # Create objects
        Mp1.diattenuator_linear(p1=p11, p2=p12, angle=th0p1)
        Mp2.diattenuator_linear(p1=p21, p2=p22, angle=th0p2)

        # Reference angle of retarder 2 (Step 4)
        # Extract intensity
        IexpA = cal_dict["I_step_4a"]
        if indIt == 0:
            IexpB = I4b_interp
            ax = angles_2x_interp
            ay = angles_2y_interp
        else:
            IexpB = cal_dict["I_step_4b"]
            ax = angles_2x_s4b
            ay = angles_2y_s4b
        Imax = max([IexpA.max(), IexpB.max()])
        Nmeas_step4 = Nelements1D + Nelements2D_s4b
        # Limits
        min_limit = [0, 0, 0, 0, -180 * degrees, -error_amplitude]
        max_limit = [
            1, 1, 180 * degrees, 180 * degrees, 180 * degrees, error_amplitude
        ]
        # While loop
        (counter, error_step, error_best) = (0, 1, 1)
        while counter < cal_dict["N_it_while"] and error_step > cal_dict["tol_while"]:
            # Initialize angle in first iteration
            if indIt == 0 or cal_dict["N_it_while"] > 1:
                if use_seed:
                    th0r2 = seed[6]
                elif cal_dict["use_random_angles"]:
                    th0r2 = np.random.rand(1) * np.pi
                    th0r2 = th0r2[0]
            # Initial parameters
            par0 = [R2p1, R2p2, Dr2, th0r2, Dp1, th0E0]
            # Fit
            result = optimize.least_squares(
                error_step_4,
                par0,
                args=(angles_1, ax, ay, Mp2, Ifuente, p11, p12, th0p1, IexpA,
                      IexpB),
                bounds=(min_limit, max_limit),
                jac=cal_dict["jacobian"],
                method=cal_dict["fit_function_method"])
            par1 = result.x
            # Errors calculation
            error_step = error_step_4(par1, angles_1, ax, ay, Mp2, Ifuente,
                                      p11, p12, th0p1, IexpA, IexpB)
            error_step = np.linalg.norm(error_step) / (Imax * Nmeas_step4)
            # Store best results
            if error_step < error_best:
                par_best = par1
                error_best = error_step
                # Save random seed
                if indIt == 0 or cal_dict["N_it_while"] > 1:
                    seed[6] = th0r2
            # While loop end operations
            counter = counter + 1
            if cal_dict["PlotCadaPaso"] and cal_dict["N_it_while"] > 1:
                print('Subiteration {} has error {:.3f} %.'.format(
                    counter, error_step * 100))
        # Extract results for the following iterations
        (R2p1, R2p2) = par_best[0:2]
        Dr2 = par_best[2] % np.pi
        Dp1 = par_best[4] % np.pi
        th_error = par_best[5]
        Mr2.diattenuator_retarder_linear(R2p1, R2p2, Dr2, angle=th0r2)
        Mp1.diattenuator_retarder_linear(p11, p12, Dp1, angle=th0p1 + th_error)
        # Store results
        pes[indIt, 6:8] = (R2p1, R2p2)
        delays[indIt, 1] = Dr2
        angles[indIt, 3] = th0r2
        delays[indIt, 2] = Dp1
        # delays[indIt, 3] = Dp1
        error_angles[indIt, 1] = th_error
        errores[indIt, 4] = error_best
        # Plots
        if make_plots:
            print("Step 4 parameters:")
            print("  - Retarder 2:")
            print("    o P1           = {:.3f};".format(R2p1))
            print("    o P2           = {:.3f};".format(R2p2))
            print("    o Retardance   = {:.1f} deg;".format(Dr2 / degrees))
            print("    o Ref angle    = {:.1f} deg;".format(th0r2 / degrees))
            print("  - Polarizer 1:")
            print("    o Retardance   = {:.1f} deg;".format(Dp1 / degrees))
            print("    o Error angle  = {:.1f} deg;".format(
                th_error / degrees))
            I_fit_A, I_fit_B = model_step_4(par_best, angles_1, ax, ay, Mp2,
                                            Ifuente, p11, p12, th0p1)
            plot_experiment_residuals_1D(
                angles_1, IexpA, I_fit_A, title='Step 4a')
            plot_experiment_residuals_2D(
                ax,
                ay,
                IexpB,
                I_fit_B,
                title='Step 4b',
                xlabel='Angle R2 (deg)',
                ylabel='Angle P2 (deg)')
        # Fix parameters
        if cal_dict["fix_parameters"]:
            th0p1 = th0p1 + th_error

        # Retarder 1 (Step 5)
        # Initial objects
        Mr1.diattenuator_retarder_linear(R1p1, R1p2, Dr1, angle=0)
        # Extract intensity 1D
        factor1 = 1
        I1expA = cal_dict["Ia_Step_5a"]
        I1expB = cal_dict["Ib_Step_5a"]
        I1expC = cal_dict["Ic_Step_5a"]
        if cal_dict["step_5a"] in ('R1'):
            Iexp1 = I1expA
            Imax1 = I1expA.max()
        elif cal_dict["step_5a"] in ('R2'):
            Iexp1 = I1expB
            Imax1 = I1expB.max()
        elif cal_dict["step_5a"] in ('P2'):
            Iexp1 = I1expC
            Imax1 = I1expC.max()
        else:
            Iexp1 = (I1expA, I1expB, I1expC)
            Imax1 = np.array([I1expA.max(), I1expB.max(), I1expC.max()]).max()
            factor1 = 3
        # Extract intensity 2D
        factor2 = 1
        I2expA = cal_dict["Ia_step_5b"]
        I2expB = cal_dict["Ib_step_5b"]
        I2expC = cal_dict["Ic_step_5b"]
        if cal_dict["step_5b"] in ('R1+R2', 'R1 R2'):
            Iexp2 = I2expA
            Imax2 = I2expA.max()
        elif cal_dict["step_5b"] in ('R1+P2', 'R1 P2'):
            Iexp2 = I2expB
            Imax2 = I2expB.max()
        elif cal_dict["step_5b"] in ('R2+P2', 'R2 P2'):
            Iexp2 = I2expC
            Imax2 = I2expC.max()
        else:
            Iexp2 = (I2expA, I2expB, I2expC)
            Imax2 = np.array([I2expA.max(), I2expB.max(), I2expC.max()]).max()
            factor2 = 3
        Imax = max(Imax1, Imax2)
        # Limits
        min_limit = [0, 0, 0, 0, -error_amplitude]
        max_limit = [1, 1, 180 * degrees, 180 * degrees, error_amplitude]
        # While loop
        (counter, error_step, error_best) = (0, 1, 1)
        while counter < cal_dict["N_it_while"] and error_step > cal_dict["tol_while"]:
            # Initialize angle in first iteration
            if indIt == 0 or cal_dict["N_it_while"] > 1:
                if use_seed:
                    th0r1 = seed[7]
                elif cal_dict["use_random_angles"]:
                    th0r1 = np.random.rand(1) * np.pi
                    th0r1 = th0r1[0]
            # Initial parameters
            par0 = [R1p1, R1p2, Dr1, th0r1, th0E0]
            # Fit
            result = optimize.least_squares(
                error_step_5,
                par0,
                args=(angles_1, angles_2x, angles_2y, Mp1, Mp2, Mr2, Ifuente,
                      cal_dict["step_5a"], cal_dict["step_5b"], Iexp1, Iexp2),
                bounds=(min_limit, max_limit),
                jac=cal_dict["jacobian"],
                method=cal_dict["fit_function_method"])
            par1 = result.x
            # Errors calculation
            error_step = error_step_5(
                par1, angles_1, angles_2x, angles_2y, Mp1, Mp2, Mr2, Ifuente,
                cal_dict["step_5a"], cal_dict["step_5b"], Iexp1, Iexp2)
            error_step = np.linalg.norm(error_step) / (
                Imax * (factor1 * Nelements1D + factor2 * Nelements2D))
            # Store best results
            if error_step < error_best:
                par_best = par1
                error_best = error_step
                # Save random seed
                if indIt == 0 or cal_dict["N_it_while"] > 1:
                    seed[7] = th0r1
            # While loop end operations
            counter = counter + 1
            if cal_dict["PlotCadaPaso"] and cal_dict["N_it_while"] > 1:
                print('Subiteration {} has error {:.3f} %.'.format(
                    counter, error_step * 100))
        # Extract results for the following iterations and calculate Imodel
        (R1p1, R1p2) = par_best[0:2]
        Dr1 = par_best[2] % np.pi
        th_error = par_best[4]
        Mr1.diattenuator_retarder_linear(p1=R1p1, p2=R1p2, D=Dr1, angle=th0r1)
        cal_dict["M"] = [Mp1, Mr1, Mr2, Mp2]
        I1_fit, I2_fit = model_step_5(par_best, angles_1, angles_2x, angles_2y,
                                      Mp1, Mp2, Mr2, Ifuente,
                                      cal_dict["step_5a"], cal_dict["step_5b"])
        angles_rep = angles_1
        if cal_dict["fix_parameters"]:
            Mr2.rotate(th_error)
            th0r2 = th0r2 + th_error
        # Store results
        angles[indIt, 6] = (th0r1)
        pes[indIt, 10:12] = (R1p1, R1p2)
        delays[indIt, 0] = Dr1
        errores[indIt, 5] = error_best
        error_angles[indIt, 2] = th_error
        # Plots
        if make_plots:
            print("Step 5 parameters:")
            print("  - P1           = {:.3f};".format(R1p1))
            print("  - P2           = {:.3f};".format(R1p2))
            print("  - Retardance   = {:.1f} deg;".format(Dr1 / degrees))
            print("  - R1 reference angle      = {:.1f} deg;".format(
                th0r1 / degrees))
            print("  - Error angle  = {:.1f} deg;".format(th_error / degrees))
            if cal_dict["step_5a"] in ('R1', 'R2', 'P2'):
                plot_experiment_residuals_1D(
                    angles_rep, Iexp1, I1_fit, title='Step 5a')
            else:
                plot_experiment_residuals_1D(
                    angles_rep, Iexp1[0], I1_fit[0], title='Step 5a (R1)')
                plot_experiment_residuals_1D(
                    angles_rep, Iexp1[1], I1_fit[1], title='Step 5a (R2)')
                plot_experiment_residuals_1D(
                    angles_rep, Iexp1[2], I1_fit[2], title='Step 5a (P2)')
            if cal_dict["step_5b"] in ('R1+R2', 'R1 R2', 'R1+P2', 'R1 P2',
                                       'R2+P2', 'R2 P2'):
                plot_experiment_residuals_2D(
                    cal_dict["angles_2X"],
                    cal_dict["angles_2Y"],
                    Iexp2,
                    I2_fit,
                    title='Step 5b',
                    xlabel='Angle R2 (deg)',
                    ylabel='Angle P2 (deg)')
            else:
                plot_experiment_residuals_2D(
                    cal_dict["angles_2X"],
                    cal_dict["angles_2Y"],
                    Iexp2[0],
                    I2_fit[0],
                    title='Step 5b (R1+R2)',
                    xlabel='Angle R2 (deg)',
                    ylabel='Angle P2 (deg)')
                plot_experiment_residuals_2D(
                    cal_dict["angles_2X"],
                    cal_dict["angles_2Y"],
                    Iexp2[1],
                    I2_fit[1],
                    title='Step 5b (R1+P2)',
                    xlabel='Angle R2 (deg)',
                    ylabel='Angle P2 (deg)')
                plot_experiment_residuals_2D(
                    cal_dict["angles_2X"],
                    cal_dict["angles_2Y"],
                    Iexp2[2],
                    I2_fit[2],
                    title='Step 5b (R2+P2)',
                    xlabel='Angle R2 (deg)',
                    ylabel='Angle P2 (deg)')

        # Check 1: Use the data as a polarimeter experiment (Step 5c)
        do_step_5c = False
        if cal_dict["step_5b"] in ('R1+R2', 'R1 R2', 'all', 'All', 'ALL'):
            angles_pol, I_pol = adapt_step_to_polarimeter(
                cal_dict["angles_2X"], cal_dict["angles_2Y"],
                cal_dict["Ia_step_5b"])
            do_step_5c = True
        elif cal_dict["step_5b"] in ('R1+P2', 'R1 P2', 'all', 'All', 'ALL'):
            angles_pol, I_pol = adapt_step_to_polarimeter(
                cal_dict["angles_2X"], cal_dict["angles_2Y"],
                cal_dict["Ib_step_5b"])
            do_step_5c = True
        # Rest of the required parameters
        if do_step_5c:
            cal_dict["intensity_polarimeter"] = I_pol
            cal_dict["angles_polarimeter"] = angles_pol
            cal_dict['analysis_name'] = 'Step_5c_analysis'
            cal_dict, Mfiltered, results_dict = Analysis_Measurement_0D(
                cal_dict, type_output='all', verbose=False)
            # Guardar resultados
            errores[indIt, 6] = results_dict['error_norm']
            # Info
            if make_plots:
                # Prints
                print("Step 5c result:")
                print("  - Normalization factor      = {:.4f}".format(
                    results_dict['normalization']))
                print("  - Error                     = {:.3f} %".format(
                    results_dict['error_norm'] * 100))
                print("   - Normalized Mueller matrix:")
                print(Mfiltered)
                # Plots
                I_fit = model_polarimeter_data(M, Ifuente, angles_pol, I_pol)
                diff = (I_pol - I_fit) / I_pol.max()
                plt.figure(figsize=(16, 8))
                plt.subplot(2, 2, 1)
                plt.plot(cal_dict['intensity_polarimeter'], 'b')
                plt.plot(I_fit, 'k')
                plt.subplot(2, 2, 2)
                plt.plot(diff, 'b')
        else:
            errores[indIt, 6] = np.nan

        # Check experiment: Mueller matrix of air (step 6a)
        cal_dict["angles_polarimeter"] = cal_dict['angles_step_6a']
        cal_dict["intensity_polarimeter"] = cal_dict['I_step_6a']
        cal_dict['type'] = 'Calibration'
        cal_dict['analysis_name'] = 'Step_6a_analysis'
        # Limits
        min_limit = [
            -error_amplitude, -error_amplitude, -error_amplitude,
            -error_amplitude
        ]
        max_limit = [
            error_amplitude, error_amplitude, error_amplitude, error_amplitude
        ]
        # Optimization of final angles
        par0 = [0, 0, 0, 0]
        # Fit
        result = optimize.least_squares(
            error_step_6a_angles,
            par0,
            args=(cal_dict, air_flatten),
            bounds=(min_limit, max_limit),
            jac=cal_dict["jacobian"],
            method=cal_dict["fit_function_method"])
        par1 = result.x
        cal_dict, Mfiltered, results_dict = model_step_6a_angles(
            par1, cal_dict)
        # Extract results
        par1 = np.array(par1)
        # Fix results
        th0p1 = (th0p1 + par1[0]) % np.pi
        th0r1 = (th0r1 + par1[1]) % np.pi
        th0r2 = (th0r2 + par1[2]) % np.pi
        th0p2 = (th0p2 + par1[3]) % np.pi
        # Guardar resultados
        errores[indIt, 7] = results_dict['error_norm']
        error_angles[indIt, 3] = par1[0]
        error_angles[indIt, 4] = par1[1]
        error_angles[indIt, 5] = par1[2]
        error_angles[indIt, 6] = par1[3]
        # Info
        if make_plots:
            # Prints
            print("Step 6a result:")
            print("  - Error th0 P1              = {:.1f} deg;".format(
                par1[0] / degrees))
            print("  - Error th0 R1              = {:.1f} deg;".format(
                par1[1] / degrees))
            print("  - Error th0 R2              = {:.1f} deg;".format(
                par1[2] / degrees))
            print("  - Error th0 P2              = {:.1f} deg;".format(
                par1[3] / degrees))
            print("  - Normalization factor      = {:.4f}".format(
                results_dict['normalization']))
            print("  - Error                     = {:.3f} %".format(
                results_dict['error_norm'] * 100))
            print("   - Normalized Mueller matrix:")
            print(Mfiltered)
            # Plots
            I_fit = model_polarimeter_data(cal_dict['M'], Ifuente,
                                           cal_dict['angles_polarimeter'],
                                           cal_dict['intensity_polarimeter'])
            diff = (cal_dict['intensity_polarimeter'] -
                    I_fit) / cal_dict['intensity_polarimeter'].max()
            plt.figure(figsize=(16, 8))
            plt.subplot(2, 2, 1)
            plt.plot(cal_dict['intensity_polarimeter'], 'b')
            plt.plot(I_fit, 'k')
            plt.subplot(2, 2, 2)
            plt.plot(diff, 'b')
            plt.figure(figsize=(16, 8))
            # plt.hist(diff)

        # This error is shown in each iteration
        print("Error of last step: {:.3f} %".format(
            results_dict['error_norm'] * 100))
        # Loops for initial parameters are done only once
        if cal_dict['Subiterations_only_1st']:
            cal_dict["N_it_while"] = 1

    # Save data (Experimental data)
    Iexp = [
        cal_dict['I_step_1'], cal_dict['I_step_2a'], cal_dict['I_step_2b'],
        cal_dict['I_step_3a'], cal_dict['I_step_3b'], cal_dict['I_step_3c'],
        cal_dict['I_step_4a'], cal_dict['I_step_4b'], 0, 0
    ]
    if cal_dict["step_5a"] in ('R1'):
        Iexp[8] = cal_dict['Ia_Step_5a']
    elif cal_dict["step_5a"] in ('R2'):
        Iexp[8] = cal_dict['Ib_Step_5a']
    elif cal_dict["step_5a"] in ('P2'):
        Iexp[8] = cal_dict['Ic_Step_5a']
    else:
        Iexp[8] = [
            cal_dict['Ia_Step_5a'], cal_dict['Ib_Step_5a'],
            cal_dict['Ic_Step_5a']
        ]
    if cal_dict["step_5b"] in ('R1+R2', 'R1 R2'):
        Iexp[9] = cal_dict['Ia_step_5b']
    elif cal_dict["step_5b"] in ('R1+P2', 'R1 P2'):
        Iexp[9] = cal_dict['Ib_step_5b']
    elif cal_dict["step_5b"] in ('R2+P2', 'R2 P2'):
        Iexp[9] = cal_dict['Ic_step_5b']
    else:
        Iexp[9] = [
            cal_dict['Ia_step_5b'], cal_dict['Ib_step_5b'],
            cal_dict['Ic_step_5b']
        ]
    th1 = cal_dict["angles_1"]
    thX, thY = (cal_dict["angles_2X"], cal_dict["angles_2Y"])
    thX4b, thY4b = (cal_dict["angles_2X_step_4b"],
                    cal_dict["angles_2Y_step_4b"])
    # Save data (results)
    results = {}
    results["errores"] = errores
    results["pes"] = pes
    results["ilums"] = ilums
    results["delays"] = delays
    results["angles"] = angles
    results["error_angles"] = error_angles

    polarimeter = {}
    polarimeter["p11"] = np.abs(pes[-1, 0])
    polarimeter["p12"] = np.abs(pes[-1, 1])
    polarimeter["Dp1"] = delays[-1, 2]
    polarimeter["p21"] = np.abs(pes[-1, 2])
    polarimeter["p22"] = np.abs(pes[-1, 3])
    polarimeter["p31"] = np.abs(pes[-1, 4])
    polarimeter["p32"] = np.abs(pes[-1, 5])
    polarimeter["th0p1"] = th0p1
    polarimeter["th0p2"] = th0p2

    polarimeter["R1p1"] = np.abs(pes[-1, 6])
    polarimeter["R1p2"] = np.abs(pes[-1, 7])
    polarimeter["R2p1"] = np.abs(pes[-1, 10])
    polarimeter["R2p2"] = np.abs(pes[-1, 11])
    polarimeter["Dr1"] = delays[-1, 0]
    polarimeter["Dr2"] = delays[-1, 1]
    polarimeter["th0r1"] = th0r1
    polarimeter["th0r2"] = th0r2

    # polarimeter["normal"] = mean[1]
    polarimeter["S0"] = S0
    polarimeter["Saz"] = Saz
    polarimeter["Sel"] = Sel
    polarimeter["Spol"] = Spol
    polarimeter["Ifuente"] = Ifuente
    polarimeter["Mp1"] = Mp1
    polarimeter["Mp2"] = Mp2
    polarimeter["Mr1"] = Mr1
    polarimeter["Mr2"] = Mr2
    polarimeter["Mbefore"] = None
    polarimeter["Mafter"] = None

    polarimeter["Date"] = cal_dict['date']
    polarimeter["seed"] = seed
    polarimeter['seed2'] = seed2
    polarimeter['rand_amp'] = rand_amp
    polarimeter["folder_exp_data"] = cal_dict["cal_final_path"]
    polarimeter["file_exp_data"] = cal_dict['date']
    polarimeter["Process_method"] = 'Iterative'
    polarimeter["Results"] = results

    # Check experiment: Mueller matrix of polarizer of known axes (step 6b)
    cal_dict["angles_polarimeter"] = cal_dict['angles_step_6b']
    cal_dict["intensity_polarimeter"] = cal_dict['I_step_6b']
    cal_dict['is_vacuum'] = False
    cal_dict['type'] = 'Calibration'
    cal_dict['analysis_name'] = 'Step_6b_analysis'
    cal_dict, Mfiltered, results_dict = Analysis_Measurement_0D(
        cal_dict, type_output='all')
    # Prints
    print("Step 6b comparison measure / calibration:")
    print("  - p1                 = {:.4f}   /   {:.4f}".format(
        results_dict['p1'], polarimeter["p31"]))
    print("  - p2                 = {:.4f}   /   {:.4f}".format(
        results_dict['p2'], polarimeter["p32"]))
    print("  - Azimuth            = {:3.1f}   /     0.0    deg".format(
        results_dict['azimuth pol'] / degrees))
    print("  - Ellipticity angle  = {:3.1f}   /     0.0    deg".format(
        results_dict['ellipticity pol'] / degrees))
    print("  - Retardance         = {:3.1f}   /   {:3.1f}    deg".format(
        results_dict['retardance'] / degrees, polarimeter["Dp1"]))

    # Save data
    if cal_dict["save_data"]:
        if cal_dict["save_data_name"] is None:
            filename = "Polarimeter_calibration_{}.npz".format(date)
        else:
            filename = cal_dict["save_data_name"]
        np.savez(filename, polarimeter=polarimeter, cal_dict=cal_dict)
        print('Saved filename:   ' + filename)

    # Analize data
    # Plot errores
    plt.figure(figsize=(16, 8))
    plt.subplot(2, 2, 1)
    plt.plot(errores[0:indIt + 1, 0], 'k')
    plt.plot(errores[0:indIt + 1, 1], 'b')
    # plt.plot(errores[0:indIt + 1, 2], 'b--')
    plt.plot(errores[0:indIt + 1, 3], 'r')
    plt.plot(errores[0:indIt + 1, 4], 'g')
    plt.plot(errores[0:indIt + 1, 5], 'm')
    # plt.plot(errores[0:indIt + 1, 6], 'c--')
    plt.plot(errores[0:indIt + 1, 7], 'c')
    plt.xlabel('Iteration')
    plt.ylabel('Error (%)')
    plt.title('Error evolution')
    plt.legend(('Step 1', 'Step 2', 'Step 3', 'Step 4', 'Step 5', 'Step 6'))

    # Plot pes
    plt.figure(figsize=(16, 8))
    plt.subplot(2, 2, 1)
    plt.plot(pes[0:indIt + 1, 0], 'k')
    plt.plot(pes[0:indIt + 1, 2], 'b')
    plt.plot(pes[0:indIt + 1, 4], 'r')
    plt.plot(pes[0:indIt + 1, 6], 'g')
    plt.plot(pes[0:indIt + 1, 7], 'g--')
    plt.plot(pes[0:indIt + 1, 8], 'y')
    plt.plot(np.abs(pes[0:indIt + 1, 10]), 'm')
    plt.plot(np.abs(pes[0:indIt + 1, 11]), 'm--')
    plt.xlabel('Iteration')
    plt.ylabel('p parameter')
    plt.title('Evolution of high p parameters')
    plt.legend(('Pol 1', 'Pol 2', 'Pol 3', 'p1 R1', 'p2 R1', 'Pol known',
                'p1 R1', 'p2 R1'))

    plt.subplot(2, 2, 2)
    plt.plot(pes[0:indIt + 1, 1], 'k')
    plt.plot(pes[0:indIt + 1, 3], 'b')
    plt.plot(pes[0:indIt + 1, 5], 'r')
    plt.plot(pes[0:indIt + 1, 9], 'y')
    plt.xlabel('Iteration')
    plt.ylabel('p parameter')
    plt.title('Evolution of low p parameters')
    plt.legend(('Pol 1', 'Pol 2', 'Pol 3', 'Pol known'))

    # Illums
    plt.figure(figsize=(24, 8))
    plt.subplot(2, 3, 1)
    plt.plot(ilums[0:indIt + 1, 0], 'k')
    plt.xlabel('Iteration')
    plt.ylabel('Intensity (V)')
    plt.title('Evolucion of total intensity')

    plt.subplot(2, 3, 2)
    plt.plot(ilums[0:indIt + 1, 1] / degrees, 'b')
    plt.plot(ilums[0:indIt + 1, 2] / degrees, 'r')
    plt.xlabel('Iteration')
    plt.ylabel('Angle (deg)')
    plt.title('Evolution of illumination angles')
    plt.legend(('Azimuth', 'Ellipticity'))

    plt.subplot(2, 3, 3)
    plt.plot(ilums[0:indIt + 1, 3], 'g')
    plt.xlabel('Iteration')
    plt.ylabel('Polarization degree')
    plt.title('Evolution of illumination polarization degree')

    # Delays
    plt.figure(figsize=(16, 8))
    plt.subplot(2, 2, 1)
    plt.plot(delays[0:indIt + 1, 1] / degrees, 'k')
    plt.plot(delays[0:indIt + 1, 0] / degrees, 'b')
    plt.xlabel('Iteration')
    plt.ylabel('Delay (deg)')
    plt.title('Evolution of retarder delay')
    plt.legend(('Ret 1' 'Ret 2'))

    plt.subplot(2, 2, 2)
    plt.plot(delays[0:indIt + 1, 2] / degrees, 'k')
    plt.plot(delays[0:indIt + 1, 3] / degrees, 'b')
    plt.xlabel('Iteration')
    plt.ylabel('Delay (deg)')
    plt.title('Evolution of P1 delay')
    plt.legend(('Step 4a', 'Step 4b'))

    # Angles
    plt.figure(figsize=(16, 8))
    plt.subplot(2, 2, 1)
    plt.plot(angles[0:indIt + 1, 0] / degrees, 'k')
    plt.plot(angles[0:indIt + 1, 1] / degrees, 'b')
    plt.plot(angles[0:indIt + 1, 2] / degrees, 'r')
    plt.plot(angles[0:indIt + 1, 3] / degrees, 'g')
    plt.plot(angles[0:indIt + 1, 6] / degrees, 'm')
    plt.plot(angles[0:indIt + 1, 4] / degrees, 'k--')
    plt.plot(angles[0:indIt + 1, 5] / degrees, 'g--')
    plt.xlabel('Iteration')
    plt.ylabel('Angle (deg)')
    plt.title('Evolution of initial angle')
    plt.legend(('Pol 1', 'Pol 2', 'Pol 3', 'Ret 2', 'Ret 1', 'Pol 1b',
                'Ret 2b'))

    # Errangles
    plt.figure(figsize=(16, 8))
    plt.plot(error_angles[0:indIt + 1, 0] / degrees)
    plt.plot(error_angles[0:indIt + 1, 1] / degrees)
    plt.plot(error_angles[0:indIt + 1, 2] / degrees)
    plt.plot(error_angles[0:indIt + 1, 3] / degrees)
    plt.plot(error_angles[0:indIt + 1, 4] / degrees)
    plt.plot(error_angles[0:indIt + 1, 5] / degrees)
    plt.plot(error_angles[0:indIt + 1, 6] / degrees)
    plt.plot(error_amplitude_vect / degrees, 'r--')
    plt.plot(-error_amplitude_vect / degrees, 'r--')
    plt.xlabel('Iteration')
    plt.ylabel('Correction angle (deg)')
    plt.title('Evolution of correction angle')
    plt.legend(('Th0 R2*', 'Th0 R2', 'Th0 R1', 'Th0 P1 Step6', 'Th0 R1 Step6',
                'Th0 R2 Step6', 'Th0 P2 Step6', 'Limit', 'Limit'))

    end_time = time.time()
    print('Elapsed time is {} s.'.format(end_time - start_time))

    return polarimeter


def Process_Calibration_Together(cal_dict, seed=None):
    """This function process the experimental data and fits all the parameters at the same time."""
    start_time_global = time.time()
    # If seed is None, use a random one
    if any(seed == None):
        use_seed = False
        cal_dict["NmaxIt"] = 1
    else:
        use_seed = True
    date = "{}".format(datetime.date.today())

    # Initial parameters
    (p11, p12, Dp1, p21, p22, p31, p32, pc1,
     pc2) = (0.975, 0.05, 90 * degrees, 0.975, 0.05, 0.975, 0.05, 0.975, 0.3)
    (R2p1, R2p2, Dr2) = (0.975, 0.975, 83 * degrees)
    (R1p1, R1p2, Dr1) = (0.975, 0.975, 83 * degrees)
    (Sel, Spol) = (-42 * degrees, 0.975)
    # Randomize initial parameters
    try:
        seed2 = cal_dict['seed2']
        rand_amp = cal_dict['rand_amp']
    except:
        seed2 = 2 * np.random.rand(17) - 1
        rand_amp = [
            3 * degrees, 0.025, 0.025, 0.025, 3 * degrees, 0.025, 0.025,
            3 * degrees, 0.025, 0.025, 3 * degrees, 0.025, 0.025, 0.025, 0.025,
            0.025, 0.025
        ]
    # Apply the random changes
    Sel = Sel + rand_amp[0] * seed2[0]
    Spol = Spol + rand_amp[1] * seed2[1]
    p11 = p11 + rand_amp[2] * seed2[2]
    p12 = p12 + rand_amp[3] * seed2[3]
    Dp1 = Dp1 + rand_amp[4] * seed2[4]
    R1p1 = R1p1 + rand_amp[5] * seed2[5]
    R1p2 = R1p2 + rand_amp[6] * seed2[6]
    Dr1 = Dr1 + rand_amp[7] * seed2[7]
    R2p1 = R2p1 + rand_amp[8] * seed2[8]
    R2p2 = R2p2 + rand_amp[9] * seed2[9]
    Dr2 = Dr2 + rand_amp[10] * seed2[10]
    p21 = p21 + rand_amp[11] * seed2[11]
    p22 = p22 + rand_amp[12] * seed2[12]
    p31 = p31 + rand_amp[13] * seed2[13]
    p32 = p32 + rand_amp[14] * seed2[14]
    pc1 = pc1 + rand_amp[15] * seed2[15]
    pc2 = pc2 + rand_amp[16] * seed2[16]
    # Initial parameters and parameter limits
    try:
        S0 = cal_dict['S0']
    except:
        S0 = 5.9227
    par0 = [
        seed[0], Sel, Spol, p11, p12, Dp1, seed[1], seed[2], R1p1, R1p2,
        82.9 * degrees, seed[3], R2p1, R2p2, Dr2, seed[4], seed[5], p21, p22,
        seed[6], p31, p32, seed[7], pc1, pc2
    ]
    min_limit = np.zeros(25)
    min_limit[1] = -45 * degrees
    max_limit = [
        180 * degrees, 45 * degrees, 1, 1, 1, 180 * degrees, 180 * degrees,
        180 * degrees, 1, 1, 180 * degrees, 180 * degrees, 1, 1, 180 * degrees,
        180 * degrees, 180 * degrees, 1, 1, 180 * degrees, 1, 1, 180 * degrees,
        1, 1
    ]

    cal_dict["angles_polarimeter"] = cal_dict['angles_step_6a']
    cal_dict["intensity_polarimeter"] = cal_dict['I_step_6a']
    cal_dict['type'] = 'Calibration'
    cal_dict['is_vacuum'] = True
    cal_dict['odd_number_refs'] = False
    cal_dict['save_analysis'] = False
    cal_dict['analysis_name'] = 'Step_6a_analysis'

    # Prepare data to fit
    Iexp = [
        cal_dict['I_step_1'], cal_dict['I_step_2a'], cal_dict['I_step_2b'],
        cal_dict['I_step_3a'], cal_dict['I_step_3b'], cal_dict['I_step_3c'],
        cal_dict['I_step_4a'], cal_dict['I_step_4b'], 0, 0
    ]

    if cal_dict["step_5a"] in ('R1'):
        Iexp[8] = cal_dict['Ia_Step_5a']
    elif cal_dict["step_5a"] in ('R2'):
        Iexp[8] = cal_dict['Ib_Step_5a']
    elif cal_dict["step_5a"] in ('P2'):
        Iexp[8] = cal_dict['Ic_Step_5a']
    else:
        Iexp[8] = [
            cal_dict['Ia_Step_5a'], cal_dict['Ib_Step_5a'],
            cal_dict['Ic_Step_5a']
        ]

    if cal_dict["step_5b"] in ('R1+R2', 'R1 R2'):
        Iexp[9] = cal_dict['Ia_step_5b']
    elif cal_dict["step_5b"] in ('R1+P2', 'R1 P2'):
        Iexp[9] = cal_dict['Ib_step_5b']
    elif cal_dict["step_5b"] in ('R2+P2', 'R2 P2'):
        Iexp[9] = cal_dict['Ic_step_5b']
    else:
        Iexp[9] = [
            cal_dict['Ia_step_5b'], cal_dict['Ib_step_5b'],
            cal_dict['Ic_step_5b']
        ]

    th1 = cal_dict["angles_1"]
    thX, thY = (cal_dict["angles_2X"], cal_dict["angles_2Y"])
    thX4b, thY4b = (cal_dict["angles_2X_step_4b"],
                    cal_dict["angles_2Y_step_4b"])

    # While loop
    (counter, error_step_6a, error_best_6a) = (0, 1, 1)
    while counter < cal_dict["NmaxIt"] and error_step_6a > cal_dict["tol_while"]:
        # Recalculate certain parameters
        if use_seed:
            seed = np.random.rand(8) * 180 * degrees
        par0 = [
            seed[0], -44 * degrees, 1, 0.975, 0.05, 45 * degrees, seed[1],
            seed[2], 1, 1, 82.9 * degrees, seed[3], 1, 1, 82.9 * degrees,
            seed[4], seed[5], 0.975, 0.05, seed[6], 0.975, 0.05, seed[7], 0.95,
            0.4
        ]
        # Fit steps 1 to 5
        start_time = time.time()
        print('Start fitting:')
        print('The seed is: ', seed)
        result = optimize.least_squares(
            error_all_together,
            par0,
            args=(th1, thX, thY, thX4b, thY4b, S0, cal_dict["step_5a"],
                  cal_dict["step_5b"], Iexp),
            bounds=(min_limit, max_limit),
            jac=cal_dict["jacobian"],
            method=cal_dict["fit_function_method"])
        # par = par0
        print('Fitting complete!!!!')
        dI = error_all_together(result.x, th1, thX, thY, thX4b, thY4b, S0,
                                cal_dict["step_5a"], cal_dict["step_5b"], Iexp)
        error_step = np.linalg.norm(dI) / dI.size
        print('- The Step 1-5 error is {:.5f} %'.format(error_step * 100))
        # Make optical element objects
        Ifuente = Stokes()
        Ifuente.general_azimuth_ellipticity(
            intensity=S0,
            az=result.x[0],
            el=result.x[1],
            pol_degree=result.x[2])
        Mp1 = Mueller()
        Mp1.diattenuator_retarder_linear(
            p1=result.x[3], p2=result.x[4], D=result.x[5], angle=result.x[7])
        Mr1 = Mueller()
        Mr1.diattenuator_retarder_linear(
            p1=result.x[8], p2=result.x[9], D=result.x[10], angle=result.x[11])
        Mr2 = Mueller()
        Mr2.diattenuator_retarder_linear(
            p1=result.x[12],
            p2=result.x[13],
            D=result.x[14],
            angle=result.x[16])
        Mp2 = Mueller()
        Mp2.diattenuator_linear(
            p1=result.x[17], p2=result.x[18], angle=result.x[19])
        cal_dict['M'] = [Mp1, Mr1, Mr2, Mp2]
        cal_dict['Ifuente'] = Ifuente
        # Check: step 6a
        if cal_dict['step_6a'] in ('angles', 'Angles', 'ANGLES'):
            # Calculate the current state of step 6a
            cal_dict, Mfiltered, results_dict = Analysis_Measurement_0D(
                cal_dict, type_output='all', verbose=False, decompose=False)
            error_current = results_dict['error_norm']
            print("- The current step 6a error is {:.5f} %".format(
                error_current * 100))
            # Start optimization
            result_6a = None
            par0_6a = [0, 0, 0, 0]
            max_limit_6a = np.array([1, 1, 1, 90]) * degrees
            min_limit_6a = -max_limit_6a

            result_6a = optimize.least_squares(
                error_step_6a_angles,
                par0_6a,
                args=(cal_dict, air_flatten),
                bounds=(min_limit_6a, max_limit_6a),
                jac=cal_dict["jacobian"],
                method=cal_dict["fit_function_method"])

            error_step_6a = error_step_6a_angles(result_6a.x, cal_dict)
            error_step_6a = np.linalg.norm(error_step_6a) / 16
            result_6a = result_6a.x

        elif cal_dict['step_6a'] in ('all', 'All', 'ALL'):
            # Calculate the current state of step 6a
            cal_dict, Mfiltered, results_dict = Analysis_Measurement_0D(
                cal_dict, type_output='all', verbose=False, decompose=False)
            error_current = results_dict['error_norm']
            print("- The current step 6a error is {:.5f} %".format(
                error_current * 100))
            # Start optimization
            par0_6a = np.zeros(18)
            max_limit_6a = np.array([
                90 * degrees,
                1 * degrees,
                0.02,
                0.02,
                0.02,
                1 * degrees,
                1 * degrees,
                0.02,
                0.02,
                1 * degrees,
                1 * degrees,
                0.02,
                0.02,
                1 * degrees,
                1 * degrees,
                0.02,
                0.02,
                1 * degrees,
            ])
            min_limit_6a = -max_limit_6a

            result_6a = optimize.least_squares(
                error_step_6a_all,
                par0_6a,
                args=(result.x, S0, cal_dict),
                bounds=(min_limit_6a, max_limit_6a),
                jac=cal_dict["jacobian"],
                method=cal_dict["fit_function_method"])

            error_step_6a = error_step_6a_all(result_6a.x, result.x, S0,
                                              cal_dict)
            error_step_6a = np.linalg.norm(error_step_6a) / error_step_6a.size
            result_6a = result_6a.x

        else:
            cal_dict, Mfiltered, results_dict = Analysis_Measurement_0D(
                cal_dict, type_output='all', verbose=False, decompose=False)
            error_step_6a = results_dict['error_norm']
            result_6a = None
            error_current = None
        if not (error_current is None):
            print("- The improvement in step 6 error is {:.5f} %".format(
                (error_current - error_step_6a) * 100))
        # While loop end operations
        counter = counter + 1
        time_it = time.time() - start_time
        print('Iteration time is {:.0f} minutes {:.1f} seconds.\n'.format(
            np.floor(time_it / 60), time_it % 60))
        if error_step_6a < error_best_6a:
            par = result.x
            par_6a = result_6a
            error_best_6a = error_step_6a
            error_best = error_step
            seed_best = seed
            n_it = counter

    # Fix the required parameters
    par_original = par
    if cal_dict['step_6a'] in ('angles', 'Angles', 'ANGLES'):
        par[0] = par[0] + par_6a[3]
        par[7] = par[7] + par_6a[0]
        par[11] = par[11] + par_6a[1]
        par[16] = par[16] + par_6a[2]
    elif cal_dict['step_6a'] in ('all', 'All', 'ALL'):
        par[0:6] = par[0:6] + par_6a[0:6]
        par[7:15] = par[7:15] + par_6a[6:14]
        par[16:20] = par[16:20] + par_6a[14:18]

    # Print the best result and its seed
    if cal_dict["NmaxIt"] > 1:
        print('\n\nThe best result corresponds to iteration {}:'.format(n_it))
        print('- The initial Step 1-5 error is {:.5f} %'.format(
            error_best * 100))
    # Recalculate fitting error
    dI = error_all_together(par, th1, thX, thY, thX4b, thY4b, S0,
                            cal_dict["step_5a"], cal_dict["step_5b"], Iexp)
    error_best = np.linalg.norm(dI) / dI.size
    # Print the best result and its seed
    if cal_dict["NmaxIt"] > 1:
        print('- The final Step 1-5 error is {:.5f} %'.format(
            error_best * 100))
        print("- The final step 6a error is {:.5f} %".format(
            error_best_6a * 100))

    # Make optical element objects
    Ifuente = Stokes()
    Ifuente.general_azimuth_ellipticity(
        intensity=S0, az=par[0], el=par[1], pol_degree=par[2])
    Mp1 = Mueller()
    Mp1.diattenuator_retarder_linear(
        p1=par[3], p2=par[4], D=par[5], angle=par[7])
    Mr1 = Mueller()
    Mr1.diattenuator_retarder_linear(
        p1=par[8], p2=par[9], D=par[10], angle=par[11])
    Mr2 = Mueller()
    Mr2.diattenuator_retarder_linear(
        p1=par[12], p2=par[13], D=par[14], angle=par[16])
    Mp2 = Mueller()
    Mp2.diattenuator_linear(p1=par[17], p2=par[18], angle=par[19])
    cal_dict['M'] = [Mp1, Mr1, Mr2, Mp2]
    cal_dict['Ifuente'] = Ifuente

    # Calculate the different steps with the final elements and represent them
    I_fit = model_all_together(par, th1, thX, thY, thX4b, thY4b, S0,
                               cal_dict["step_5a"], cal_dict["step_5b"])

    plot_experiment_residuals_1D(th1, Iexp[0], I_fit[0], title='Step 1')
    plot_experiment_residuals_1D(th1, Iexp[1], I_fit[1], title='Step 2a')
    plot_experiment_residuals_2D(
        thX,
        thY,
        Iexp[2],
        I_fit[2],
        title='Step 2b',
        xlabel='Angle R2 (deg)',
        ylabel='Angle P1 (deg)')
    plot_experiment_residuals_1D(th1, Iexp[3], I_fit[3], title='Step 3a')
    plot_experiment_residuals_1D(th1, Iexp[4], I_fit[4], title='Step 3b')
    plot_experiment_residuals_1D(th1, Iexp[5], I_fit[5], title='Step 3c')
    plot_experiment_residuals_1D(th1, Iexp[6], I_fit[6], title='Step 4a')
    plot_experiment_residuals_2D(
        thX4b,
        thY4b,
        Iexp[7],
        I_fit[7],
        title='Step 4b',
        xlabel='Angle R2 (deg)',
        ylabel='Angle P2 (deg)')
    plot_experiment_residuals_1D(th1, Iexp[8], I_fit[8], title='Step 5a')
    plot_experiment_residuals_2D(
        thX,
        thY,
        Iexp[9],
        I_fit[9],
        title='Step 5b',
        xlabel='Angle R2 (deg)',
        ylabel='Angle P2 (deg)')

    # Check: step 6a
    cal_dict, Mfiltered, results_dict = Analysis_Measurement_0D(
        cal_dict, type_output='all', verbose=False, decompose=False)
    # Print results
    print("Step 6a result:")
    print("  - Normalization factor      = {:.4f}".format(
        results_dict['normalization']))
    print("  - Error                     = {:.3f} %".format(
        results_dict['error_norm'] * 100))
    print("   - Normalized Mueller matrix:")
    print(Mfiltered)
    if cal_dict['step_6a'] in ('angles', 'Angles', 'ANGLES'):
        print('Corrections:')
        print('  - Azimuth     = {:.2f} deg.'.format(par_6a[3] / degrees))
        print('  - P1 angle    = {:.2f} deg.'.format(par_6a[0] / degrees))
        print('  - R1 angle    = {:.2f} deg.'.format(par_6a[1] / degrees))
        print('  - R2 angle    = {:.2f} deg.'.format(par_6a[2] / degrees))
    elif cal_dict['step_6a'] in ('all', 'All', 'ALL'):
        print('Corrections:')
        print('  - Illumination:')
        print('     o Azimuth              = {:.2f} deg.'.format(
            par_6a[0] / degrees))
        print('     o Ellipticity angle    = {:.2f} deg.'.format(
            par_6a[1] / degrees))
        print('     o Polarization degree  = {:.4f}.'.format(par_6a[2]))
        print('  - Polarizer 1:')
        print('     o p1                   = {:.4f}.'.format(par_6a[3]))
        print('     o p2                   = {:.4f}.'.format(par_6a[4]))
        print('     o Retardance           = {:.2f} deg.'.format(
            par_6a[5] / degrees))
        print('     o Angle                = {:.2f} deg.'.format(
            par_6a[6] / degrees))
        print('  - Retarder 1:')
        print('     o p1                   = {:.4f}.'.format(par_6a[7]))
        print('     o p2                   = {:.4f}.'.format(par_6a[8]))
        print('     o Retardance           = {:.2f} deg.'.format(
            par_6a[9] / degrees))
        print('     o Angle                = {:.2f} deg.'.format(
            par_6a[10] / degrees))
        print('  - Retarder 2:')
        print('     o p1                   = {:.4f}.'.format(par_6a[11]))
        print('     o p2                   = {:.4f}.'.format(par_6a[12]))
        print('     o Retardance           = {:.2f} deg.'.format(
            par_6a[13] / degrees))
        print('     o Angle                = {:.2f} deg.'.format(
            par_6a[14] / degrees))
        print('  - Polarizer 2:')
        print('     o p1                   = {:.4f}.'.format(par_6a[15]))
        print('     o p2                   = {:.4f}.'.format(par_6a[16]))
        print('     o Angle                = {:.2f} deg.'.format(
            par_6a[17] / degrees))
        par[0:6] = par[0:6] + par_6a[0:6]
        par[7:15] = par[7:15] + par_6a[6:14]
        par[16:20] = par[16:20] + par_6a[14:18]

    # Plot intensities
    I_fit = model_polarimeter_data(cal_dict['M'], Ifuente,
                                   cal_dict['angles_polarimeter'],
                                   cal_dict['intensity_polarimeter'])
    x = np.linspace(1, I_fit.size, I_fit.size)
    plot_experiment_residuals_1D(
        x,
        cal_dict['intensity_polarimeter'],
        I_fit,
        title='Step 6a',
        unit='meas',
        xlabel='N. measurement')

    # Print parameters
    print('Fit parameters:')
    print('  - Illumination:')
    print('     o Intensity            = {:.3f} V (fixed).'.format(S0))
    print('     o Azimuth              = {:.1f} deg.'.format(par[0] / degrees))
    print('     o Ellipticity angle    = {:.1f} deg.'.format(par[1] / degrees))
    print('     o Polarization degree  = {:.3f}.'.format(par[2]))
    print('  - Polarizer 1:')
    print('     o p1                   = {:.3f}.'.format(par[3]))
    print('     o p2                   = {:.3f}.'.format(par[4]))
    print('     o Retardance           = {:.1f} deg.'.format(par[5] / degrees))
    print('     o Angle (non-deff)     = {:.1f} deg.'.format(par[6] / degrees))
    print('     o Angle                = {:.1f} deg.'.format(par[7] / degrees))
    print('  - Retarder 1:')
    print('     o p1                   = {:.3f}.'.format(par[8]))
    print('     o p2                   = {:.3f}.'.format(par[9]))
    print('     o Retardance           = {:.1f} deg.'.format(
        par[10] / degrees))
    print('     o Angle                = {:.1f} deg.'.format(
        par[11] / degrees))
    print('  - Retarder 2:')
    print('     o p1                   = {:.3f}.'.format(par[12]))
    print('     o p2                   = {:.3f}.'.format(par[13]))
    print('     o Retardance           = {:.1f} deg.'.format(
        par[14] / degrees))
    print('     o Angle (non-deff)     = {:.1f} deg.'.format(
        par[15] / degrees))
    print('     o Angle                = {:.1f} deg.'.format(
        par[16] / degrees))
    print('  - Polarizer 2:')
    print('     o p1                   = {:.3f}.'.format(par[17]))
    print('     o p2                   = {:.3f}.'.format(par[18]))
    print('     o Angle                = {:.1f} deg.'.format(
        par[19] / degrees))
    print('  - Polarizer 3:')
    print('     o p1                   = {:.3f}.'.format(par[20]))
    print('     o p2                   = {:.3f}.'.format(par[21]))
    print('     o Angle                = {:.1f} deg.'.format(
        par[22] / degrees))
    print('  - Polarizer Known:')
    print('     o p1                   = {:.3f}.'.format(par[23]))
    print('     o p2                   = {:.3f}.'.format(par[24]))

    # Polarimeter dictionary with important data
    polarimeter = {}
    polarimeter["p11"] = par[3]
    polarimeter["p12"] = par[4]
    polarimeter["Dp1"] = par[5]
    polarimeter["p21"] = par[17]
    polarimeter["p22"] = par[18]
    polarimeter["p31"] = par[20]
    polarimeter["p32"] = par[21]
    polarimeter["th0p1"] = par[6]
    polarimeter["th0p2"] = par[19]

    polarimeter["R1p1"] = par[8]
    polarimeter["R1p2"] = par[9]
    polarimeter["R2p1"] = par[12]
    polarimeter["R2p2"] = par[13]
    polarimeter["Dr1"] = par[10]
    polarimeter["Dr2"] = par[14]
    polarimeter["th0r1"] = par[11]
    polarimeter["th0r2"] = par[16]

    polarimeter["S0"] = S0
    polarimeter["Saz"] = par[0]
    polarimeter["Sel"] = par[1]
    polarimeter["Spol"] = par[2]

    polarimeter["Ifuente"] = Ifuente
    polarimeter["Mp1"] = Mp1
    polarimeter["Mp2"] = Mp2
    polarimeter["Mr1"] = Mr1
    polarimeter["Mr2"] = Mr2
    polarimeter["M"] = cal_dict['M']
    polarimeter["Mbefore"] = None
    polarimeter["Mafter"] = None

    polarimeter["Date"] = cal_dict['date']
    polarimeter["seed"] = seed_best
    polarimeter['seed2'] = seed2
    polarimeter['rand_amp'] = rand_amp
    polarimeter["folder_exp_data"] = cal_dict["cal_final_path"]
    polarimeter["file_exp_data"] = cal_dict['date']
    polarimeter["par"] = par
    polarimeter["par_original"] = par_original
    polarimeter["Process_method"] = 'All together'

    polarimeter["Iexp"] = Iexp
    polarimeter["th1"] = th1
    polarimeter["thX"] = thX
    polarimeter["thY"] = thY
    polarimeter["thX4b"] = thX4b
    polarimeter["thY4b"] = thY4b

    time_global = time.time() - start_time_global
    polarimeter["fitting_time"] = time_global

    # Save data and finish
    if cal_dict["save_data"]:
        if cal_dict["save_data_name"] is None:
            filename = "Polarimeter_calibration_{}.npz".format(date)
        else:
            filename = cal_dict["save_data_name"]
        np.savez(filename, polarimeter=polarimeter, cal_dict=cal_dict)
        print('Saved filename:   ' + filename)

    print('Total elapsed time is {:.0f} minutes {:.1f} seconds.'.format(
        np.floor(time_global / 60), time_global % 60))

    return polarimeter


def Postprocess_Calibration(folder,
                            file,
                            type_data='Final',
                            type_process='Individual',
                            load_dicts=False,
                            dict_param=None):
    """This function is used to process the calibration data. It is loaded from the last file containg all the data (throught the variable file) or the files created during each single step."""
    # Start by loading from mulipple files file
    if type_data in ('individual', 'Individual', 'INDIVIDUAL'):
        # Start operations
        cal_dict = {}
        cal_dict['date'] = file
        cal_dict['type'] = 'Calibration'
        cal_dict['fit completed'] = True
        os.chdir(folder)
        cal_dict["type"] = 'Individual'
        cal_dict["cal_final_path"] = folder
        cal_dict["save_data"] = True
        # Step 1
        filename = 'Step_1_' + file + '.npz'
        data = np.load(filename, encoding='latin1')
        cal_dict['I_step_1'] = data["Iexp"]
        cal_dict["angles_1"] = data["angles_1"]
        cal_dict["N_measures_1D"] = cal_dict["angles_1"].size
        if load_dicts:
            params = data["dict_param"]
        # Step 2a
        filename = 'Step_2a_' + file + '.npz'
        data = np.load(filename, encoding='latin1')
        cal_dict['I_step_2a'] = data["Iexp"]
        # params = data["dict_param"]
        # Step 2b
        filename = 'Step_2b_' + file + '.npz'
        data = np.load(filename, encoding='latin1')
        cal_dict['I_step_2b'] = data["Iexp"]
        cal_dict["angles_2X"] = data["angles2x"]
        cal_dict["angles_2Y"] = data["angles2y"]
        cal_dict["N_measures_2D"] = [
            data["angles2x"].size, data["angles2y"].size
        ]
        if load_dicts:
            params = data["dict_param"]
            cal_dict['Ifuente_test'] = params['Ifuente_test']
        else:
            cal_dict['Ifuente_test'] = data['fit'].item()
        # Step 3a
        filename = 'Step_3a_' + file + '.npz'
        data = np.load(filename, encoding='latin1')
        cal_dict['I_step_3a'] = data["Iexp"]
        if load_dicts:
            params = data["dict_param"]
        # Step 3b
        filename = 'Step_3b_' + file + '.npz'
        data = np.load(filename, encoding='latin1')
        cal_dict['I_step_3b'] = data["Iexp"]
        if load_dicts:
            params = data["dict_param"]
        # Step 3c
        filename = 'Step_3c_' + file + '.npz'
        data = np.load(filename, encoding='latin1')
        cal_dict['I_step_3c'] = data["Iexp"]
        if load_dicts:
            params = data["dict_param"]
        # Step 4a
        filename = 'Step_4a_' + file + '.npz'
        data = np.load(filename, encoding='latin1')
        cal_dict['I_step_4a'] = data["Iexp"]
        if load_dicts:
            params = data["dict_param"]
        # Step 4b
        filename = 'Step_4b_' + file + '.npz'
        data = np.load(filename, encoding='latin1')
        cal_dict['I_step_4b'] = data["Iexp"]
        cal_dict["angles_2X_step_4b"] = data["angles2x"]
        cal_dict["angles_2Y_step_4b"] = data["angles2y"]
        cal_dict["N_measures_2D_step_4b"] = [
            data["angles2x"].size, data["angles2y"].size
        ]
        if load_dicts:
            params = data["dict_param"]
        # Step 5a
        filename = 'Step_5a_' + file + '.npz'
        data = np.load(filename, encoding='latin1')
        cal_dict['Ia_Step_5a'] = data["IexpA"]
        cal_dict['Ib_Step_5a'] = data["IexpB"]
        cal_dict['Ic_Step_5a'] = data["IexpC"]
        if load_dicts:
            params = data["dict_param"]
        # Step 5b
        filename = 'Step_5b_' + file + '.npz'
        data = np.load(filename, encoding='latin1')
        cal_dict['Ia_step_5b'] = data["IexpA"]
        cal_dict['Ib_step_5b'] = data["IexpB"]
        cal_dict['Ic_step_5b'] = data["IexpC"]
        if load_dicts:
            params = data["dict_param"]
        # Step 6a
        filename = 'Step_6a_' + file + '.npz'
        # for key, value in data.items():
        #     print(key, value)
        data = np.load(filename, encoding='latin1')
        cal_dict['I_step_6a'] = data["Iexp"]
        cal_dict['angles_step_6a'] = data["angles"]
        if load_dicts:
            params = data["dict_param"]
        # Step 6b
        filename = 'Step_6b_' + file + '.npz'
        data = np.load(filename, encoding='latin1')
        cal_dict['I_step_6b'] = data["Iexp"]
        cal_dict['angles_step_6b'] = data["angles"]
        cal_dict["N_measures_pol"] = len([cal_dict['angles_step_6a']])

        # Rest of parameters. Start by the default ones
        cal_dict["NmaxIt"] = 1
        cal_dict["tolerance"] = 1e-4
        cal_dict["tolerance_fit"] = 1e-4
        cal_dict["PlotCadaPaso"] = True
        cal_dict["ordenarPes"] = True
        cal_dict["interpolate_first_it"] = True
        cal_dict["ErrorAmpIni"] = 90 * degrees
        cal_dict["ErrorAmpFin"] = 10 * degrees
        cal_dict["tol_while"] = 3e-4
        cal_dict["N_it_while"] = 5
        cal_dict["use_random_angles"] = True
        cal_dict["fix_parameters"] = True
        cal_dict["subits_1st_step_only"] = False
        cal_dict["step_5a"] = "P2"
        cal_dict["step_5b"] = "R2+P2"
        cal_dict['step_6a'] = 'angles'
        cal_dict['Subiterations_only_1st'] = True
        cal_dict["jacobian"] = '2-point'  # '3-point'
        cal_dict["fit_function_method"] = 'trf'  # 'dogbox', 'lm'
        cal_dict["save_data_name"] = None
        seed = None
        # Load params from the saved file
        if load_dicts:
            try:
                params = data["dict_param"]
                cal_dict["tol_while"] = params["tol_while"]
                cal_dict["N_it_while"] = params["N_it_while"]
                cal_dict["NmaxIt"] = params["NmaxIt"]
                cal_dict["tolerance"] = params["tolerance"]
                cal_dict["tolerance_fit"] = params["tolerance_fit"]
                cal_dict["PlotCadaPaso"] = params["PlotCadaPaso"]
                cal_dict["ordenarPes"] = params["ordenarPes"]
                cal_dict["ErrorAmpIni"] = params["ErrorAmpIni"]
                cal_dict["ErrorAmpFin"] = params["ErrorAmpFin"]
                cal_dict["use_random_angles"] = params["use_random_angles"]
                cal_dict["fix_parameters"] = params["fix_parameters"]
                cal_dict["step_5a"] = params["step_5a"]
                cal_dict["step_5b"] = params["step_5b"]
                cal_dict['step_6a'] = params['step_6a']
                cal_dict['Subiterations_only_1st'] = params[
                    'Subiterations_only_1st']
                cal_dict["jacobian"] = params["jacobian"]
                cal_dict["fit_function_method"] = params["fit_function_method"]
                cal_dict["save_data_name"] = params["save_data_name"]
                seed = params["seed"]
            except:
                pass
        # Override with user defined params
        if not (dict_param == None):
            for key in dict_param:
                if key is 'seed':
                    seed = dict_param[key]
                else:
                    cal_dict[key] = dict_param[key]

    # Some other options
    elif type_data in ('one_file', 'One_file', 'onefile', 'Onefile'):
        filename = 'All_together_' + file + '.npz'
        data = np.load(filename, encoding='latin1')
        cal_dict = data["parameters"]

    # Make the calculations
    if type_process in ('individual', 'Individual', 'INDIVIDUAL'):
        polarimeter = Process_Calibration(cal_dict, seed=seed)
        return polarimeter
    else:
        polarimeter = Process_Calibration_Together(cal_dict, seed=seed)
        return polarimeter


def model_polarimeter_data(M, Ifuente, angles, Iexp):
    """Function that calculates the modelled intensity respect to the angles and intensities measured during a polarimeter experiment."""
    N = Iexp.size
    Imodel = np.zeros(N)
    for ind, angle in enumerate(angles):
        Imodel[ind] = Intensity_Rotating_Elements(M, angle, Ei=Ifuente)
    return Imodel


def Final():
    pass
