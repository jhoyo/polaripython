# !/usr/local/bin/python
# -*- coding: utf-8 -*-

# ----------------------------------------------------------------------
# Name:        utils.py
# Purpose:     General purpose functions: Ecograb project
#
# Author:      Luis Miguel Sanchez Brea
#
# Created:     2018
# Copyright:   AOCG / UCM
# Licence:     GPL
# ----------------------------------------------------------------------
""" General purpose optics functions """
from __future__ import print_function, division

import os
import matplotlib.pyplot as plt
import scipy as sp
import numpy as np
from scipy import pi
from polaripython import degrees, def_cal_path, def_cal_name


def to_bits(variable_integer, num_bits=16, verbose=True):
    """
    takes an integer an generates a list with bits

    Args:
        variable_integer (int): integer with data
        num_bits (int): num of output bits: 8,16, 32, 64
    """
    if num_bits == 8:
        output = map(int, [x for x in '{:08b}'.format(variable_integer)])
    elif num_bits == 16:
        output = map(int, [x for x in '{:016b}'.format(variable_integer)])
    elif num_bits == 32:
        output = map(int, [x for x in '{:032b}'.format(variable_integer)])
    elif num_bits == 64:
        output = map(int, [x for x in '{:064b}'.format(variable_integer)])

    if verbose is True:
        print(output)
    return output


def sort_positions(M, pos_ini):
    """
    Luis Miguel Sánchez Brea
    10/09/2018
    Pequeña utilidad para ordenar las posiciones aleatorias de los 4 motores de forma que se reduzca el tiempo de adquisición.

    Tenemos una matriz de tamaño $N*m$ donde N es el número de medidas y m el número de motores. Estas posiciones las hemos determinado de forma aleatoria (u otro procedimiento). Queremos ordenar la matriz de forma que el desplazamiento de los motores sea el mínimo posible

    Con los resultados generamos una función donde entra una Matriz Matriz1 y sale otra Matriz2, pero ordenada
    """
    num_medidas, num_motores = M.shape

    M2 = sp.zeros_like(M)
    pos_current = pos_ini

    for i in range(num_medidas):
        # medida de distancia como suma de distancias
        # distance=np.sum(np.abs(M-pos_current),axis=1)
        # medida de distancia como distancia maxima
        distance = np.max(np.abs(M - pos_current), axis=1)

        i_min = distance.argmin()

        M2[i, :] = M[i_min, :]
        pos_current = M[i_min, :]
        M = np.delete(M, [i_min], axis=0)

    return M2


def percentage_complete(current=None, total=None):
    """Function that easily calculates and prints the completion percentage of a current measurement."""
    if (current is None) or (total is None):
        print('\nCompletion:   0 %', end='')
    else:
        # Adapt to lists if necessary
        try:
            Ncurrent = np.ravel_multi_index(current, total) + 1
            Ncurrent = Ncurrent / np.prod(total)
        except:
            Ncurrent = (current + 1) / total
        # Calculate percentage
        Ncurrent = int(np.floor(Ncurrent * 100))
        # Print it
        print('\b\b\b\b\b\b\b{:3d} %'.format(Ncurrent), end='')
        if Ncurrent == 100:
            print('')


def limit_pes(p):
    """Function that limits p parameters of polarimeters to be between 0 and 1."""
    if np.size(p) == 1:
        if p < 0:
            p = abs(p)
        if abs(p) > 1:
            p = 1
    else:
        for ind, value in enumerate(p):
            if value < 0:
                p[ind] = abs(value)
            if abs(value) > 1:
                p[ind] = 1
    return p


def sort_pes(param, wrap=np.pi):
    """Function that rearranges pes so p1 is always >= p2. If the pes are changed, the angle is changed accordingly."""
    N = int(len(param) / 3)
    ind_angles = 2 * N
    for ind in range(N):
        # Extract pes and angle
        p1, p2 = (param[2 * ind:2 * (ind + 1)])
        p1, p2 = limit_pes([p1, p2])
        angle = param[ind_angles + ind]
        # Restrict them between 0 and 1
        p1 = min(1, max(p1, 0))
        p2 = min(1, max(p2, 0))
        # Order pes
        po1 = max(p1, p2)
        po2 = min(p1, p2)
        new_angle = angle
        # Update the array
        param[2 * ind] = po1
        param[2 * ind + 1] = po2
        if p1 != po1:
            param[2 * ind] = po1
            param[2 * ind + 1] = po2
            param[ind_angles + ind] = (angle + np.pi / 2) % wrap
        else:
            param[2 * ind] = p1
            param[2 * ind + 1] = p2
    return param


def adapt_step_to_polarimeter(ax, ay, I, Nmotors=4, indices=[2, 3]):
    """Function that adapts data from a calibration step to a polarimeter experiment."""
    # Calculate size
    Nx = ax.size
    Ny = ay.size
    Ntotal = Nx * Ny
    # Adapt
    angles = np.zeros((Ntotal, Nmotors))
    Ifinal = np.zeros(Ntotal)
    for indX, angle_x in enumerate(ax):
        for indY, angle_y in enumerate(ay):
            indC = np.ravel_multi_index([indX, indY], [Nx, Ny])
            Ifinal[indC] = I[indX, indY]
            angles[indC, indices[0]] = ax[indX]
            angles[indC, indices[1]] = ay[indY]
    return angles, Ifinal


def set_cal_dict(cal_dict):
    new_dict = {}
    for key, value in cal_dict.items():
        if key in ('equipment'):
            pass
        else:
            new_dict[key] = value
    return new_dict


def plot_experiment_1D(x,
                       y,
                       title='',
                       xlabel='Angle (deg)',
                       ylabel='Intensity (V)',
                       unit='rad'):
    if unit in ('rad', 'Rad', 'Radian', 'radian', 'radians', 'Radians'):
        x = x / degrees
    plt.figure(figsize=(16, 8))
    plt.subplot(2, 2, 1)
    plt.plot(x, y, "bo", lw=2, label='Exp. data')
    plt.title(title)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)


def plot_experiment_residuals_1D(x,
                                 y,
                                 y_fitting,
                                 title='',
                                 xlabel='Angle (deg)',
                                 ylabel='Intensity (V)',
                                 unit='rad'):
    if unit in ('rad', 'Rad', 'Radian', 'radian', 'radians', 'Radians'):
        x = x / degrees
    plt.figure(figsize=(16, 8))
    plt.subplot(2, 2, 1)
    plt.plot(x, y, "bo", lw=2, label='Exp. data')
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.title(title)

    if y_fitting is not None:
        plt.plot(x, y_fitting, "b-", lw=2, label='Fitting')
        plt.legend()

        residuals = y_fitting - y

        plt.subplot(2, 2, 2)
        plt.plot(x, residuals / y_fitting.max(), 'r', lw=2)
        plt.title('Normalized residuals')
        plt.xlabel(xlabel)
        plt.ylabel(ylabel)
        plt.show()

        error = np.linalg.norm(residuals) / (
            np.size(residuals) * np.max(y_fitting))
        print('The normalized MSE is: {:.2f} %.\n'.format(error * 100))


def plot_experiment_residuals_2D(x,
                                 y,
                                 Z,
                                 Z_fitting,
                                 title='',
                                 xlabel='Angle 1 (deg)',
                                 ylabel='Angle 2 (deg)',
                                 unit='rad',
                                 color="magma"):
    # Calculate range
    extension = sp.array([x[0], x[-1], y[0], y[-1]])
    dif_norm = (Z - Z_fitting) / Z.max()
    # First image, experimental
    plt.figure(figsize=(18, 6))
    plt.subplot(1, 3, 1)
    IDimage = plt.imshow(
        Z,
        interpolation='bilinear',
        aspect='auto',
        origin='lower',
        extent=extension)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.suptitle('Experimental ' + title)
    plt.axis(extension)
    plt.axis('scaled')
    plt.colorbar()
    IDimage.set_cmap(color)  # YlGnBu  RdBu
    # Second image, model
    plt.subplot(1, 3, 2)
    IDimage = plt.imshow(
        Z_fitting,
        interpolation='bilinear',
        aspect='auto',
        origin='lower',
        extent=extension)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.suptitle('Model')
    plt.axis(extension)
    plt.axis('scaled')
    plt.colorbar()
    IDimage.set_cmap(color)  # YlGnBu  RdBu
    # Third image, residuals
    plt.subplot(1, 3, 3)
    IDimage = plt.imshow(
        dif_norm,
        interpolation='bilinear',
        aspect='auto',
        origin='lower',
        extent=extension)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.suptitle('Normalized residuals')
    plt.axis(extension)
    plt.axis('scaled')
    plt.colorbar()
    IDimage.set_cmap(color)  # YlGnBu  RdBu
    # Print errors
    error = np.linalg.norm(dif_norm) / dif_norm.size
    print('The normalized MSE is: {:.2f} %'.format(error * 100))


def plot_error_Mueller(Mexp, Mtarget, title='Error'):
    pass


def medir_distancia_array(M):
    """en esta función medimos las distancias recorridas por los motores"""
    num_medidas, num_motores = M.shape

    total_distance = 0

    for i in range(1, num_medidas):
        # medida de distancia como suma de distancias
        # distance=np.sum(np.abs(M[i,:]-M[i-1,:]))
        # medida de distancia como distancia maxima
        distance = np.max(np.abs(M[i, :] - M[i - 1, :]))

        total_distance = total_distance + distance
    return total_distance


def dibujar_2d_fitting(x, y, Z, title='', color="magma"):

    extension = sp.array([x[0], x[-1], y[0], y[-1]]) / degrees

    plt.figure()
    IDimage = plt.imshow(
        Z,
        interpolation='bilinear',
        aspect='auto',
        origin='lower',
        extent=extension)
    plt.xlabel("$\phi_2$")
    plt.ylabel("$\phi_4$")
    plt.suptitle(title)

    plt.axis(extension)
    plt.axis('scaled')
    plt.colorbar()
    IDimage.set_cmap(color)  # YlGnBu  RdBu


def plot_2d(x, y, Z, title='', color="magma", xy_are_angles=True):

    if x.ndim == 1:
        extension = sp.array([x[0], x[-1], y[0], y[-1]])
    elif x.ndim == 2:
        extension = sp.array([x[0, 0], x[-1, -1], y[0, 0], y[-1, -1]])
    if xy_are_angles:
        extension /= degrees
    IDimage = plt.imshow(
        Z,
        interpolation='spline36',
        aspect='auto',
        origin='lower',
        extent=extension)
    plt.xlabel("$\phi_2$")
    plt.ylabel("$\phi_4$")
    plt.title(title)

    plt.axis(extension)
    plt.axis('scaled')
    plt.colorbar()
    IDimage.set_cmap(color)  # YlGnBu  RdBu


def plot_2d_scattered(x,
                      y,
                      z,
                      Nxy=25,
                      method='linear',
                      title='',
                      color="magma",
                      xy_are_angles=True):
    """Function that plots a 2D surface given by 1D arrays of X, Y and Z by interpolating."""
    # First, we have to interpolate the data. For doing that, we need to create the space where our data is in.
    xspace, yspace = np.linspace(x.min(), x.max(), Nxy), np.linspace(
        y.min(), y.max(), Nxy)
    xgrid, ygrid = ndgrid(xspace, yspace)
    # Now we can interpolate
    z2d = sp.interpolate.griddata((x, y), z, (xgrid, ygrid), method=method)
    # Plot
    plot_2d(
        xgrid,
        ygrid,
        z2d,
        title=title,
        color=color,
        xy_are_angles=xy_are_angles)


def clean_intensity(Iexp, Iref_mean, Nd):
    if Nd == 1:
        Iclean = Iexp[:, 0] * Iref_mean / (Iexp[:, 1])
    else:
        Iclean = Iexp[:, :, 0] * Iref_mean / (Iexp[:, :, 1])
    return Iclean


def generate_angles_distribution(Nmeasures,
                                 limits,
                                 method='linear',
                                 Nlimit=0,
                                 N_total_motors=4):
    """Function that creates random values but evenly distributed among the four motors."""
    # Change Nlimit to a very high value if there are 0s
    if Nlimit < 1:
        Nlimit = 1e16 * np.ones(N_total_motors)
    elif np.size(Nlimit) == 1:
        Nlimit = Nlimit * np.ones(N_total_motors)
    else:
        for ind, elem in enumerate(Nlimit):
            if elem < 1:
                Nlimit[ind] = 1e16
    # Calculate the number of motors that we are going to move effectively
    moving_motors = np.array(limits) > 0
    aux = np.array(range(N_total_motors))
    moving_motors_indices = aux[moving_motors]
    first_moving_motor = min(moving_motors_indices)
    Nmotors = float(sum(moving_motors))
    # Calculate the number of different values in each motor, and the center values
    Nvalues = np.zeros(N_total_motors)
    centers = list(np.matrix(np.zeros(N_total_motors)).T)
    #centers = list(np.zeros(N_total_motors))
    Dinterval = np.zeros(N_total_motors)
    used_motors = 0
    Nmeasures_origin = int(Nmeasures)
    for ind in range(N_total_motors):
        if limits[ind] == 0:
            Nvalues[ind] = 0
        else:
            # Calculate the number of intervals
            Nvalues[ind] = np.floor(Nmeasures**(1 / (Nmotors - used_motors)))
            Nvalues[ind] = min(Nvalues[ind], Nlimit[ind])
            Nmeasures /= Nvalues[ind]
            used_motors += 1
            # Calculate centers
            Dinterval[ind] = limits[ind] / Nvalues[ind]
            centers[ind] = (
                np.array(range(int(Nvalues[ind]))) + 0.5) * Dinterval[ind]
    # Make a big loop to start puting each value
    angles = np.zeros([N_total_motors, Nmeasures_origin])
    indiv_ind = np.zeros(N_total_motors, dtype=int)
    for ind in range(Nmeasures_origin):
        # Calculate the angles
        random_values = np.random.rand(N_total_motors) - 0.5
        aux = np.zeros(N_total_motors)
        for indA in range(N_total_motors):
            # Linear distribution inside each interval
            if method == 'linear' or 'Linear' or 'LINEAR':
                aux[indA] = centers[indA][indiv_ind[indA]] + \
                    Dinterval[indA] * random_values[indA]
            elif method == 'gaussian' or 'Gaussian' or 'GAUSSIAN':
                moved = np.sign(np.random.rand(1) -
                                0.5) * Dinterval[indA] * sqrt(
                                    np.log(random_values[indA]**(-1)))
                aux[indA] = centers[indA][indiv_ind[indA]] + moved
            elif method == 'Fix' or method == 'fix' or method == 'FIX':
                aux[indA] = centers[indA][indiv_ind[indA]]
        angles[:, ind] = aux
        # Calculate the new individual indices
        indiv_ind[first_moving_motor] += 1
        indiv_ind = check_indices(indiv_ind, Nvalues)
    return angles


def check_indices(indices, Nvalues):
    """Function that checks that indices don't reach their limits."""
    # Find moving motors
    N_total_motors = len(Nvalues)
    moving_motors = np.array(Nvalues) > 1
    N_moving_motors = sum(moving_motors)
    aux = np.array(range(N_total_motors))
    moving_indices = aux[moving_motors]
    # Check the indices. If one reaches the maximum, add one to the following
    for counter, ind in enumerate(moving_indices):
        if indices[ind] == Nvalues[ind] and Nvalues[ind] != 0:
            indices[ind] = 0
            # TCheck that there is a following motor
            if counter < N_moving_motors - 1:
                indices[moving_indices[counter + 1]] += 1
    return indices


def dist_angles(th1, th2, th0=None):
    """Function to calculate the individual angles in order to introduce an experiment of the polarimeter calibration in the function
    to determine Mueller matrix."""

    # Create initial angle distribution
    (N1, N2) = (th1.size, th2.size)
    Nmeas = N1 * N2
    angles = np.zeros([4, Nmeas])
    # fill the medium values
    for ind1 in range(N1):
        for ind2 in range(N2):
            ind3 = ind1 * (N1 - 1) + ind2
            #             print([ind1, ind2, ind3])
            if th0 is None:
                angles[1, ind3] = th1[ind1]
                angles[2, ind3] = th2[ind2]
            else:
                angles[1, ind3] = th1[ind1] + th0[1]
                angles[2, ind3] = th2[ind2] + th0[2]
                angles[0, ind3] = th0[0]
                angles[3, ind3] = th0[3]
    return angles


def iscolumn(v):
    """Checks if the array v is a column array or not.
    Args:
        v (array): Array to be tested.
    Output:
        cond (bool): True if v is a column array."""
    cond = False
    s = v.shape
    if len(s) == 2:
        if s[1] == 1:
            cond = True
    return cond


def isrow(v):
    """Checks if the array v is a row array or not.
    Args:
        v (array): Array to be tested.
    Output:
        cond (bool): True if v is a row array."""
    cond = False
    s = v.shape
    if len(s) == 1:
        cond = True
    elif len(s) == 2:
        if s[1] == 1:
            cond = True
    return cond


def generar_dict_de_param(analysis=False):
    dictionary = {}
    dictionary["theta0"] = theta0
    dictionary["Nmedidas"] = N
    dictionary["type_array_angles"] = type_angles
    dictionary["type_motor_angles"] = mode_motors
    dictionary["limits"] = limits
    dictionary["Nlimit"] = Nlimit
    dictionary["Imodel"] = Imodel
    dictionary["Measure_file"] = name_notebook
    try:
        dictionary["date_cal"] = pol_dict["Date"]
    except:
        dictionary["date_cal"] = 'Unknown'
    if analysis:
        dictionary["Mcalculated"] = Mcalculated
        dictionary["Mnormalized"] = Mnormalized
        dictionary["Mfiltered"] = Mfiltered
        dictionary["Mp"] = Mp
        dictionary["Mr"] = Mr
        dictionary["Md"] = Md
        dictionary["is_vacuum"] = is_vacuum
        dictionary["daily_transmission"] = daily_transmission
        dictionary["tol_filter"] = tolerance
        dictionary["Mbefore"] = Mbefore
        dictionary["Mafter"] = Mafter
    return dictionary


def ndgrid(*args, **kwargs):
    """n-dimensional gridding like Matlab's NDGRID

        The input *args are an arbitrary number of numerical sequences,
        e.g. lists, arrays, or tuples.
        The i-th dimension of the i-th output argument
        has copies of the i-th input argument.

        Optional keyword argument:
        same_dtype : If False (default), the result is an ndarray.
                     If True, the result is a lists of ndarrays, possibly with
                       different dtype. This can save space if some *args
                       have a smaller dtype than others.

        Typical usage:
        >>> x, y, z = [0, 1], [2, 3, 4], [5, 6, 7, 8]
        >>> X, Y, Z = ndgrid(x, y, z)
        # unpacking the returned ndarray into X, Y, Z

        Each of X, Y, Z has shape [len(v) for v in x, y, z].
        >>> X.shape == Y.shape == Z.shape == (2, 3, 4)
        True
        >>> X
        array([[[0, 0, 0, 0],
                        [0, 0, 0, 0],
                        [0, 0, 0, 0]],
                   [[1, 1, 1, 1],
                        [1, 1, 1, 1],
                        [1, 1, 1, 1]]])
        >>> Y
        array([[[2, 2, 2, 2],
                        [3, 3, 3, 3],
                        [4, 4, 4, 4]],
                   [[2, 2, 2, 2],
                        [3, 3, 3, 3],
                        [4, 4, 4, 4]]])
        >>> Z
        array([[[5, 6, 7, 8],
                        [5, 6, 7, 8],
                        [5, 6, 7, 8]],
                   [[5, 6, 7, 8],
                        [5, 6, 7, 8],
                        [5, 6, 7, 8]]])

        With an unpacked argument list:
        >>> V = [[0, 1], [2, 3, 4]]
        >>> ndgrid(*V) # an array of two arrays with shape (2, 3)
        array([[[0, 0, 0],
                        [1, 1, 1]],
                   [[2, 3, 4],
                        [2, 3, 4]]])

        For input vectors of different data kinds,
        same_dtype=False makes ndgrid()
        return a list of arrays with the respective dtype.
        >>> ndgrid([0, 1], [1.0, 1.1, 1.2], same_dtype=False)
        [array([[0, 0, 0], [1, 1, 1]]),
         array([[ 1. ,  1.1,  1.2], [ 1. ,  1.1,  1.2]])]

        Default is to return a single array.
        >>> ndgrid([0, 1], [1.0, 1.1, 1.2])
        array([[[ 0. ,  0. ,  0. ], [ 1. ,  1. ,  1. ]],
                   [[ 1. ,  1.1,  1.2], [ 1. ,  1.1,  1.2]]])
        """
    same_dtype = kwargs.get("same_dtype", True)
    V = [array(v) for v in args]  # ensure all input vectors are arrays
    shape = [len(v) for v in args]  # common shape of the outputs
    result = []
    for i, v in enumerate(V):
        # reshape v so it can broadcast to the common shape
        # http://docs.scipy.org/doc/numpy/user/basics.broadcasting.html
        zero = zeros(shape, dtype=v.dtype)
        thisshape = ones_like(shape)
        thisshape[i] = shape[i]
        result.append(zero + v.reshape(thisshape))
    if same_dtype:
        return array(result)  # converts to a common dtype
    else:
        return result  # keeps separate dtype for each output


def get_calibration(path=def_cal_path, name=def_cal_name, verbose=False):
    """Returns parameters of all the elements of the polarimeter. They are stored in a dictionary.

    Parameters:
        path (str): Path to the folder of the calibration that is going to be loaded.
        name (str): Name of the file with the calibration fit data.
        verbose (bool): If true, some comments will be printed.
    """
    # Go to folder
    old_path = os.getcwd()
    os.chdir(path)
    # Load data
    data = np.load(name, encoding='latin1')
    print(data)
    pol_dict = data["polarimeter"]
    pol_dict = pol_dict.tolist()
    # Return to old path
    os.chdir(old_path)

    # If the calibration is old, otical elements and illumination were saved as matrices, not Mueller or Stokes objects. We have to make the old ones compatible
    try:
        pol_dict["Mp1"].type == 'Mueller'
        if verbose:
            print('New calibration')
        # If Mbefore and Mafter doesn't exist, create them
        try:
            test = pol_dict["Mbefore"]
            if verbose:
                print('Using saved Mbefore and Mafter')
        except:
            M = Mueller('Mbefore')
            if verbose:
                print('Using default (vacuum) Mbefore and Mafter')
            M.from_matrix(np.identity(4))
            pol_dict["Mbefore"] = deepcopy(M)

            M.name = 'Mafter'
            M.from_matrix(np.identity(4))
            pol_dict["Mafter"] = deepcopy(M)

    except:
        if verbose:
            print('Old calibration')
        # Convert to objects
        M = Mueller('Mp1')
        M.from_matrix(pol_dict["Mp1"])
        pol_dict["Mp1"] = deepcopy(M)

        M.name = 'Mr1'
        M.from_matrix(pol_dict["Mr1"])
        pol_dict["Mr1"] = deepcopy(M)

        M.name = 'Mr2'
        M.from_matrix(pol_dict["Mr2"])
        pol_dict["Mr2"] = deepcopy(M)

        M.name = 'Mp2'
        M.from_matrix(pol_dict["Mp2"])
        pol_dict["Mp2"] = deepcopy(M)

        S = Stokes('I0')
        S.from_matrix(pol_dict["Ifuente"])
        pol_dict["Ifuente"] = S

        # If Mbefore and Mafter doesn't exist, create them
        try:
            M.name = 'Mbefore'
            M.from_matrix(pol_dict["Mbefore"])
            pol_dict["Mbefore"] = deepcopy(M)

            M.name = 'Mafter'
            M.from_matrix(pol_dict["Mafter"])
            pol_dict["Mafter"] = deepcopy(M)

            if verbose:
                print('Using saved Mbefore and Mafter')
        except:
            if verbose:
                print('Using default (vacuum) Mbefore and Mafter')
            M.name = 'Mbefore'
            M.from_matrix(np.identity(4))
            pol_dict["Mbefore"] = deepcopy(M)

            M.name = 'Mafter'
            M.from_matrix(np.identity(4))
            pol_dict["Mafter"] = deepcopy(M)

    if verbose:
        print('Calibration loaded succesfully')
    return pol_dict
