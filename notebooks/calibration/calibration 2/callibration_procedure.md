# Calibration procedure

Here we analyze the calibration procedure

* Stability of source
  -  **Procedure** We perform several measurements at a given intensity and check that it is stable.
  - No polarizers or polarizers still.
  - results: RMS of intensity distribution

.

* Stability of source
  -  **Procedure** With l/4 polarizer at light source, we move a linear polarizer P1, for example, and check that intensity distribution is constant
  - results: flucutuations of light polarization.

.
*  Parameters of linear polarizers.
   -  Linear polarizers can be studied with Jones formalism, but are not ideal .
    - **Procedure**:
      * 1  No polarizer, check intensity
      * 2  Polarizer P1 and polarizer P2. Move P2 360 degrees.
      * 3  Perform fit to Malus law. Determine minimum and maximum intensity
      * 4  Determine angle of maximum intensity (polarizers aligned).
    - **Parameters to measure**
      * I0 - maximum intensity without polarizers
      * Imin - minimum intensity of Malus law
      * Imax - maximum intensity of Malus Law
    - Results: (p0, p1, theta)
      * Data on 2018/02/14 - **p0 = 0.0653499037607, p1 = 0.758623923874, theta_4= 70.306º**

*  Parameters of $\lambda/4$ polarizers.
  -  **Procedure** To do
