'''connects PI controller, references the axes and makes some demo motions'''
from __future__ import print_function, division
import pyPICommands
# reload(pyPICommands)

import numpy as np
import time
import os

from polaripython import COMPORTS, DIR_MOV, def_cal_path, degrees
from polaripython.utils import get_calibration

# os.chdir('C:\Users\luismiguel\software\polaripython\polaripython\motors')
# os.chdir('C:\Users\luismiguel\software\polarimeter\polarimeter\motors')
sleeping_time = 0.001

# Load reference of angles
try:
    polarimeter_data = get_calibration()
    theta0 = np.array([
        polarimeter_data["th0p1"], polarimeter_data["th0r1"],
        polarimeter_data["th0r2"], polarimeter_data["th0p2"]
    ]) / degrees
except:
    theta0 = [0, 0, 0, 0]
    print('Reference of angles not found')

# Go to motors folder
os.chdir(def_cal_path + r'\motors')


class Motor(object):
    def __init__(self,
                 RS232_COMPORT,
                 RS232_BAUDRATE=115200,
                 dll_lib='PI_GCS2_DLL_x64',
                 def_vel=50,
                 CONNECTED_AXES=['1']):
        '''references the axes'''
        self.CMDS = pyPICommands.pyPICommands('PI_GCS2_DLL_x64', 'PI_')
        try:
            if not self.CMDS.ConnectRS232(RS232_COMPORT, RS232_BAUDRATE):
                print('Cannot connect to COM %d, %d Baud' % \
                    (RS232_COMPORT, RS232_BAUDRATE))
                exit()
            print("Connected to " + self.CMDS.qIDN())

        except Exception as exc:
            ERR_NUM = self.CMDS.GetError()
            print('GCS ERROR %d: %s' % (ERR_NUM,
                                        self.CMDS.TranslateError(ERR_NUM)))
            self.CMDS.CloseConnection()
            raise exc

        self.CMDS.VEL({"1": def_vel})
        ref_ok = self.CMDS.qFRF(' '.join(CONNECTED_AXES))
        for axis_to_ref in CONNECTED_AXES:
            self.CMDS.SVO({axis_to_ref: 1})  # switch on servo
            if ref_ok[axis_to_ref] != 1:
                print('referencing axis {}, PORT: {}'.format(
                    axis_to_ref, RS232_COMPORT))
                self.CMDS.FNL(axis_to_ref)

        ref_ok = self.CMDS.FRF(' '.join(CONNECTED_AXES))
        axes_are_referencing = True
        while axes_are_referencing:
            time.sleep(sleeping_time)
            ref = self.CMDS.IsMoving(' '.join(CONNECTED_AXES))
            axes_are_referencing = sum(ref.values()) > 0

        self.RS232_COMPORT = RS232_COMPORT
        self.NUM_MOTOR = COMPORTS.index(RS232_COMPORT)
        self.MIN_TRAVEL = {"1": 0}
        self.MAX_TRAVEL = {"1": 360}

    def __del__(self):
        self.CMDS.CloseConnection()

    def close(self):
        self.CMDS.CloseConnection()

    def move(self, position, velocity, kind='absolute', verbose=True):
        """movement of Motor.

        Args:
            position (float): angular position in degrees
            velocity (float): angular velocity in degrees/second
            kind (str): 'motor': with respect to encoder I0: without considering sign of movement
                        'absolute': with respect to encoder I0: considering sign of movement
                        'polarizer': with respect to polarizer axis
        """
        CONNECTED_AXES = ['1']
        num_motor = self.NUM_MOTOR

        # se da la vuelta al movimiento ya que los motores estan al reves
        if kind == 'motor':
            pos_motor = position
        elif kind == 'absolute':
            pos_motor = position * DIR_MOV[num_motor]
        elif kind == 'polarizer':
            pos_motor = (position - theta0[num_motor]) * DIR_MOV[num_motor]

        if verbose is True:
            for axis_to_move in CONNECTED_AXES:
                print('%s: %.2f\t' % (axis_to_move, pos_motor), )
                print("num_motor={}, signo={}".format(num_motor, DIR_MOV[
                    num_motor]))

        self.CMDS.VEL({"1": velocity})
        time.sleep(sleeping_time)
        self.CMDS.MOV({"1": pos_motor})

        # check stop
        axes_are_moving = True
        while axes_are_moving:
            if verbose:
                print(self.CMDS.qPOS('1'))
            time.sleep(sleeping_time)
            moving = self.CMDS.IsMoving(' '.join(CONNECTED_AXES))
            axes_are_moving = sum(moving.values()) > 0

    def move_home(self, verbose=True):
        self.move(position=0, velocity=500, verbose=verbose)

    def wait_movement(self, verbose=True):
        # check stop
        CONNECTED_AXES = ['1']
        axes_are_moving = True
        while axes_are_moving:
            if verbose is True:
                print(self.CMDS.qPOS('1'))
            time.sleep(sleeping_time)
            moving = self.CMDS.IsMoving(' '.join(CONNECTED_AXES))
            axes_are_moving = sum(moving.values()) > 0

    def get_position(self, kind='absolute', verbose=True):
        """get position of Motor.

        Args:
            kind (str): 'absolute': with respect to encoder I0 (considering sign)
                        'motor': position of motor (without considering sign)
                        'polarizer': with respect to polarizer axis
                        'all'
            verbose (bool): print location

        Returns (float): current position of motor
        """
        num_motor = self.NUM_MOTOR
        position_motor = self.CMDS.qPOS('1')['1']

        if kind == 'motor':
            if verbose:
                print("position_motor [{}] = {}, ".format(
                    num_motor, position_motor))
            return position_motor

        elif kind == 'absolute':
            if verbose:
                print("absolute position  [{}] = {}".format(
                    num_motor, position_motor * DIR_MOV[num_motor]))
            return position_motor * DIR_MOV[num_motor]

        elif kind == 'polarizer':
            pos_pol = (position_motor * DIR_MOV[num_motor] + theta0[num_motor])
            if verbose:
                print("position pol  [{}] = {}, ".format(num_motor, pos_pol))
                return pos_pol
        elif kind == 'all':
            position_absolute = position_motor * DIR_MOV[num_motor]
            pos_pol = (position_motor * DIR_MOV[num_motor] + theta0[num_motor])
            if verbose:
                print("motor    [{}] = {:6.2f}, ".format(
                    num_motor, position_motor))
                print("absolute [{}] = {:6.2f}".format(
                    num_motor, position_motor * DIR_MOV[num_motor]))
                print("polarizer[{}] = {:6.2f}, ".format(num_motor, pos_pol))


class Motors(object):
    def __init__(self,
                 RS232_COMPORT,
                 RS232_BAUDRATE=115200,
                 dll_lib='PI_GCS2_DLL_x64',
                 init_vels=[50, 50, 50, 50],
                 CONNECTED_AXES=['1'],
                 COMPORTS=[3, 4, 5, 7]):
        '''references the axes'''
        self.COMPORTS = COMPORTS
        self.VELS = init_vels
        self.motors = []
        for com, vel in zip(self.COMPORTS, self.VELS):
            self.motors.append(Motor(com, def_vel=vel))
            time.sleep(0.01)

    def __del__(self):
        for motor in self.motors:
            motor.CMDS.CloseConnection()

    def close(self):
        for motor in self.motors:
            motor.CMDS.CloseConnection()

    def move(self, positions, velocities, kind='absolute', verbose=True):
        """movement of Motor.

        Args:
            positions (float): angular positions in degrees
            velocities (float): angular velocities in degrees/second
            kind (str): 'motor': with respect to encoder I0: without considering sign of movement
                        'absolute': with respect to encoder I0: considering sign of movement
                        'polarizer': with respect to polarizer axis
        """

        for num_motor, mot in enumerate(self.motors):

            if kind == 'motor':
                pos_motor = positions[num_motor]
            elif kind == 'absolute':
                pos_motor = positions[num_motor] * DIR_MOV[num_motor]
            elif kind == 'polarizer':
                pos_motor = ((positions[num_motor] - theta0[num_motor]) *
                             DIR_MOV[num_motor])

            if verbose is True:
                print("Ports USED {},  MOTOR {}".format(
                    mot.RS232_COMPORT, num_motor))

            mot.CMDS.VEL({"1": velocities[num_motor]})
            time.sleep(sleeping_time)
            mot.CMDS.MOV({"1": pos_motor})
            time.sleep(sleeping_time)

        # check stop
        while True:
            for mot in self.motors:
                if verbose:
                    print(mot.CMDS.qPOS('1'))

            moving = [
                mot.CMDS.IsMoving('1').values()[0] for mot in self.motors
            ]
            if sum(moving) == 0:
                break
            else:
                if verbose:
                    print("Moving", sum(moving))

    def wait_movement(self, verbose=True):
        while True:
            for mot in self.motors:
                if verbose:
                    print(mot.CMDS.qPOS('1'))

            moving = [
                mot.CMDS.IsMoving('1').values()[0] for mot in self.motors
            ]
            if sum(moving) == 0:
                break
            else:
                if verbose:
                    print("Moving", sum(moving))

    def move_home(self, verbose=True):
        self.move(
            [0, 0, 0, 0], velocities=[500, 500, 500, 500], verbose=verbose)

    def get_position(self, verbose=True):
        pos_motor = np.zeros((4, 1), dtype=float)
        pos_pol = np.zeros((4, 1), dtype=float)
        pos_absolute = np.zeros((4, 1), dtype=float)
        for i, mot in enumerate(self.motors):
            pos_motor[i] = mot.get_position(verbose=False, kind='motor')
            pos_pol[i] = (pos_motor[i] * DIR_MOV[i] + theta0[i])
            pos_absolute[i] = pos_motor[i] * DIR_MOV[i]

        pos_motor = pos_motor.flatten()
        pos_absolute = pos_absolute.flatten()
        pos_pol = pos_pol.flatten()

        if verbose:
            print("motor     = ({:6.2f}, {:6.2f}, {:6.2f}, {:6.2f})".format(
                *pos_motor))
            print("absolute  = ({:6.2f}, {:6.2f}, {:6.2f}, {:6.2f})".format(
                *pos_absolute))
            print("polarizer = ({:6.2f}, {:6.2f}, {:6.2f}, {:6.2f})".format(
                *pos_pol))
        return pos_motor, pos_absolute, pos_pol


def main_simple_motor():
    COMPORT = 3

    motor = Motor(COMPORT)
    print(motor.MAX_TRAVEL)
    print(motor.MIN_TRAVEL)

    current_position = motor.get_position()
    print("current position = {}".format(current_position))
    motor.move(position=150, velocity=300, verbose=False)

    current_position = motor.get_position()
    print("current position = {}".format(current_position))
    motor.move(position=90, velocity=10, verbose=False)
    current_position = motor.get_position()
    print("current position = {}".format(current_position))
    motor.move(position=0, velocity=100, verbose=False)

    del motor


def main_simple_motors():
    # TODO USE SERIAL
    VELS_1 = [10, 30, 50, 100]
    VELS_2 = [600, 600, 600, 600]

    motors = Motors(RS232_COMPORT=COMPORTS)
    motors.get_position(verbose=True)
    motors.move(
        positions=[180, 180, 180, 180], velocities=VELS_1, verbose=False)
    motors.get_position(verbose=True)
    motors.move(positions=[0, 0, 0, 0], velocities=VELS_2, verbose=False)
    motors.get_position(verbose=True)
    motors.move(positions=[0, 0, 180, 0], velocities=VELS_2, verbose=False)
    motors.get_position(verbose=True)
    motors.move(positions=[0, 0, 0, 0], velocities=VELS_1, verbose=False)
    motors.get_position(verbose=True)
    del motors


if __name__ == "__main__":
    # main_simple_motors()
    main_simple_motors()
