# -*- coding: utf-8 -*-
"""Top-level package for Ecograb polarimeter."""

import scipy as sp
import numpy as np

__author__ = """Luis Miguel Sanchez-Brea"""
__email__ = 'optbrea@ucm.es'
__version__ = '0.1.1'

um = 1e-6
degrees = np.pi / 180

# Experimental parameters of polarizers
# Determined in calibration notebooks

# communcation ports
COMPORTS = [3, 4, 5, 7]
# direction of positive movement, some motors are rotated
DIR_MOV = np.array([-1, +1, +1, -1])
# Volocities
Vels = [50, 50, 50, 50]
N_motors = len(Vels)
# Default angle
theta_default = [0, 0, 0, 0]

# Card
AIN_signal = 1
AIN_ref = 2

# Default folders
def_cal_path = r"C:\Users\luismiguel\software\polaripython\polaripython"
def_cal_name = r'Last_cal.npz'
