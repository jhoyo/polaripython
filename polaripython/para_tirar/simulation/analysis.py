# !/usr/bin/env python
# -*- coding: utf-8 -*-

# ----------------------------------------------------------------------
# Name:      analysis
# Purpose:   Ecograb: analysis of polarimetry
#
# Author:    Luis Miguel Sanchez Brea
#
# Created:    14/12/2017
# RCS-ID:
# Copyright:
# Licence:    GPL
# ----------------------------------------------------------------------
"""

The objective of this module is use numerical Mueller Matrices,
defined in phyton_optics.polarization_stokes in order to determine
 the intensity distribution for a polarimeter (P1, R1, S, R1, P2),
 where P1 and P2 are polarimeters, R1 and R2 are retarders,
 and S is a (4x4) matrix unknon sample.

TODO

- use of image (matrices) for the object.

"""

import phyton_optics.polarization_stokes as polarization
from phyton_optics import degrees, mm, np, plt, sp, um
from phyton_optics.polarization_stokes import (
    intensity, polarized_light, polarizer_linear, quarter_waveplate, rotate_mueller)


class Compute_mueller(object):
    """Class for determining polarimetric caracteristics of Ecograb gratings.

    Args:
        P1_0 (4x4 numpy.matrix): Mueller Matrix of polarizer 1 (theta=0)
        P2_0 (4x4 numpy.matrix): Mueller Matrix of retarder 1 (theta=0)
        P3_0 (4x4 numpy.matrix): Mueller Matrix of retarder 2 (theta=0)
        P4_0 (4x4 numpy.matrix): Mueller Matrix of polarizer 2 (theta=0)
        L (4x1 numpy.matrix): Stokes parameters of incident light


    Attributes:
        self.wavelength (float): wavelength
        self.P1_0 (4x4 numpy.matrix): Mueller Matrix of polarizer 1 (theta=0)
        self.P1_0 (4x4 numpy.matrix): Mueller Matrix of retarder 1 (theta=0)
        self.P1_0 (4x4 numpy.matrix): Mueller Matrix of retarder 2 (theta=0)
        self.P1_0 (4x4 numpy.matrix): Mueller Matrix of polarizer 2 (theta=0)
        self.u0 (4x1 numpy.matrix): Stokes parameters of incident light
        self.num_experiments (int): number of experiments

    """

    def __init__(self, P1_0, P2_0, P3_0, P4_0, u0, wavelength):
        self.wavelength = wavelength
        self.P1_0 = P1_0
        self.P2_0 = P2_0
        self.P3_0 = P3_0
        self.P4_0 = P4_0
        self.u0 = u0

    def charge_experimental_data(self, Angles, Intensities):
        """
        get the angles and intensities
        """
        self.Angles = Angles
        self.Intensities = Intensities
        self.num_measurements = len(Intensities)

    def compute_mueller_sample(self):
        """
        1. Determine the generator and analyzed matrixes
            by rotating the input polarizators

        2. Determines $g_j$ (4x1 generator)  and $ a_i$ (1x4 analyzer) vector
            $I_{k}=\sum_{ij} a_{i}^{k}g_{j}^{k}m_{ij}$, for each angle pattern

        3. Performs inversion to detemine $m_{ij}$
        """

        B = sp.zeros((self.num_measurements, 16), dtype=float)

        for i in range(self.num_measurements):
            P1 = rotate_mueller(self.P1_0, self.Angles[i, 0])
            P2 = rotate_mueller(self.P2_0, self.Angles[i, 1])
            P3 = rotate_mueller(self.P3_0, self.Angles[i, 2])
            P4 = rotate_mueller(self.P4_0, self.Angles[i, 3])

            G = P2 * P1 * self.u0  # generation
            A = P4 * P3  # analizator
            a_first_line = A[0, :]
            B[i, :] = (G * a_first_line).flatten()

        I = np.matrix(self.Intensities).T
        B = np.matrix(B)
        mueller_sample = (((B.T * B).I * B.T) * I).reshape(4, 4)
        return mueller_sample
